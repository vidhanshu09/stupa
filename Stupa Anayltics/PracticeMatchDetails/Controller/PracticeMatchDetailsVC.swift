//
//  PracticeMatchDetailsVC.swift
//  Stupa
//
//  Created by Vidhanshu Bhardwaj on 04/04/21.
//

import UIKit
import DropDown

class PracticeMatchDetailsVC: BaseVC {
    
    var ballType:Utils.BallsType?
    var strokeDropdown = DropDown()
    var placementDropdown = DropDown()
    var playerName:String?
    var playerdominatingHand:String?
    var playerSide:String?
    var matchAngle:Utils.matchAngle?
    var practiceMatchViewModel:PracticeMatchDetailsViewModel!
    
    //MARK:- Outlets
    
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var placementView: UIView!
    @IBOutlet weak var strokeView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var participantarrowView: UIView!
    @IBOutlet weak var manyballsbtnObj: UIButton!
    @IBOutlet weak var singleballsbtnObj: UIButton!
    @IBOutlet weak var strokearrowView: UIView!
    @IBOutlet weak var placementarrowView: UIView!
    @IBOutlet weak var participantLabel: UILabel!
    @IBOutlet weak var strokeLabel: UILabel!
    @IBOutlet weak var placementLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }
    
    func adjustingUI(){
        matchAngle = .UmpireAngle
        setNavigationwithTitle(navView: navigationView, navTitle: "Practice Details")
        if let userName = SUserDefaults.value(forKey: SUDConstants.userName) as? String{
            playerName = userName
        }
        manyballsbtnObj.backgroundColor = Colors.AppGreenColor
        manyballsbtnObj.setTitleColor(UIColor.white, for: .normal)
        ballType = .Many
        participantLabel.text = playerName
        dateLabel.text = Utils().getCurrentDateString()
        gestures()
        setstrokeDropdown()
        setplacementDropdown()
        practiceMatchViewModel = PracticeMatchDetailsViewModel(root: self, matchAngle: self.matchAngle)
    }
    
    func gestures(){
        strokeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openstrokeDropdown)))
        placementView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openplacementDropdown)))
        participantarrowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openPopUpView)))
    }
    
    @objc func openstrokeDropdown(){
        strokeDropdown.show()
    }
    
    @objc func openplacementDropdown(){
        placementDropdown.show()
    }
    
    @objc func openPopUpView(){
        let matchdetailsPopup = StoryBoards.ExtrasStoryboard.instantiateViewController(withIdentifier: "MatchDetailsPopUpVC") as! MatchDetailsPopUpVC
        matchdetailsPopup.matchType = .Practice
        matchdetailsPopup.playerdelegate = self
        matchdetailsPopup.playerName = self.playerName
        matchdetailsPopup.playerdominatingHand = self.playerdominatingHand
        matchdetailsPopup.playerSide = self.playerSide
        matchdetailsPopup.modalPresentationStyle = .overFullScreen
        self.present(matchdetailsPopup, animated: false, completion: nil)
    }
    
    func setstrokeDropdown(){
        self.strokeDropdown.selectedTextColor = Colors.AppGreenColor
        self.strokeDropdown.backgroundColor = UIColor.black
        self.strokeDropdown.textColor = UIColor.white
        self.strokeDropdown.textFont = UIFont(name: "montserrat-regular", size: 12)!
        self.strokeDropdown.bottomOffset = CGPoint(x: 0, y: self.strokeLabel.bounds.height + 15)
        self.strokeDropdown.anchorView = self.strokeView.forLastBaselineLayout // UIView or UIBarButtonItem
        self.strokeDropdown.direction = .bottom
        self.strokeDropdown.dataSource = ["Forehand Topspin", "Backhand Topspin", "Forehand Long Push", "Backhand Short Push","Backhand Long Push", "Forehand Short Push", "Forehand Flick", "Banana Flip", "Forehand Tap", "Backhand Tap", "Forehand Counter", "Backhand Counter", "Forehand Block", "Backhand Block", "Forehand Smash", "Backend Smash", "Forehand Counter Topspin", "Backhand Counter Topspin"]
        self.strokeDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print(item)
            self.strokeLabel.text = item
        }
    }
    
    func setplacementDropdown(){
        self.placementDropdown.selectedTextColor = Colors.AppGreenColor
        self.placementDropdown.backgroundColor = UIColor.black
        self.placementDropdown.textColor = UIColor.white
        self.placementDropdown.textFont = UIFont(name: "montserrat-regular", size: 12)!
        self.placementDropdown.bottomOffset = CGPoint(x: 0, y: self.placementLabel.bounds.height + 15)
        self.placementDropdown.anchorView = self.placementView.forLastBaselineLayout // UIView or UIBarButtonItem
        self.placementDropdown.direction = .bottom
        self.placementDropdown.dataSource = ["One Place - Diagonal", "One Place - Straight", "Alternative on opponent side (One Diagonal & One Straight)", "Alternative Opponent (Forehand Backhand)","Alternative Opponent (Forehand - Centre)", "Alternative Opponent (Backhand - Centre)", "Alternative Self(Forehand - Backhand)", "Alternative Self (Forehand - Centre)", "Alternative Self (Backhand - Centre)", "Full Table Random"]
        self.placementDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print(item)
            self.placementLabel.text = item
        }
    }
    
    func checkValidation() -> Bool{
        
        var isValidate = true
        
        guard let playerdominatinghand = self.playerdominatingHand, !playerdominatinghand.isEmpty else {
            self.showAlert(with: AlertConstants.selectplayerDominating)
            isValidate = false
            return false
        }
        guard let playerSide = self.playerSide, !playerSide.isEmpty else {
            self.showAlert(with: AlertConstants.selectplayerSide)
            isValidate = false
            return false
        }
        guard let stroke = self.strokeLabel.text, stroke != "Select One" else{
            self.showAlert(with: AlertConstants.selectStroke)
            isValidate = false
            return false
        }
        guard let placement = self.placementLabel.text, placement != "Select One" else{
            self.showAlert(with: AlertConstants.selectPlacement)
            isValidate = false
            return false
        }
        
        return isValidate
    }
    
    @IBAction func manyballsbtnAct(_ sender: Any) {
        ballType = .Many
        manyballsbtnObj.backgroundColor = Colors.AppGreenColor
        manyballsbtnObj.setTitleColor(UIColor.white, for: .normal)
        singleballsbtnObj.backgroundColor = .white
        singleballsbtnObj.setTitleColor(UIColor.darkGray, for: .normal)
    }
    @IBAction func singleballsbtnAct(_ sender: Any) {
        ballType = .Single
        singleballsbtnObj.backgroundColor = Colors.AppGreenColor
        singleballsbtnObj.setTitleColor(UIColor.white, for: .normal)
        manyballsbtnObj.backgroundColor = .white
        manyballsbtnObj.setTitleColor(UIColor.darkGray, for: .normal)
    }
    @IBAction func savebtnAct(_ sender: Any) {
        if !checkValidation(){
            return
        }
    
        practiceMatchViewModel.stroke = self.strokeLabel.text ?? ""
        practiceMatchViewModel.placement = self.placementLabel.text ?? ""
        practiceMatchViewModel.ballType = self.ballType
        practiceMatchViewModel.playerdominatingHand = self.playerdominatingHand
        practiceMatchViewModel.playerSide = self.playerSide
        
        practiceMatchViewModel.savePracticeMatchDetailsApi()

    }
}

extension PracticeMatchDetailsVC:updatePlayerData{
    func update(dominatingHand: String?, side: String?, foreHand: String?, backHand: String?) {
        self.playerSide = side
        self.playerdominatingHand = dominatingHand
    }
}
