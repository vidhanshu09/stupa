//
//  PracticeMatchDetailsViewModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 21/04/21.
//

import Foundation
import UIKit

class PracticeMatchDetailsViewModel:NSObject{

    var stroke:String?
    var placement:String?
    var ballType:Utils.BallsType?
    var playerdominatingHand:String?
    var playerSide:String?
    private var matchAngle:Utils.matchAngle?
    private var rightyPlayerId:String?
    private var videoId:Int?
    private var root:UIViewController!
    
    init(root:UIViewController,matchAngle:Utils.matchAngle?){
        self.root = root
        self.matchAngle = matchAngle
    }
 
    func savePracticeMatchDetailsApi(){
        
        guard let userID = SUserDefaults.value(forKey: SUDConstants.userID) as? Int else{
            return
        }
        guard let playerName = SUserDefaults.value(forKey: SUDConstants.userName) as? String else{
            return
        }
        
        if let rightplayerID = SUserDefaults.value(forKey: SUDConstants.RightyplayerID) as? Int{
            rightyPlayerId = String(rightplayerID)
        }

        let timeStamp = Utils().getCurrentTimeStamp()
        let filename = "\(userID)" + "_" + "\(timeStamp)" + ".mp4"
        
        let saveMatchParams = SaveMatchDetailsRequestModel(opponentID: rightyPlayerId ?? "", chunkNumber: "1", finalClip: "0", videoID: "0", userID: String(userID), deviceID: StupaStrings.DeviceUUID ?? "", deviceType: StupaStrings.DeviceType, tokenValue: Utils().getToken(), matchType: "Practice", playerName: playerName, opponentName: "Opponent Righty", playerSide: playerSide ?? "", uploadType: "Live", fileName: filename, fileURL: "", fromAngle: matchAngle?.rawValue ?? "", stroke: stroke ?? "", placement: placement ?? "", ballType: ballType?.rawValue ?? "", matchDate: Utils().getCurrentDateString(), playerDominatinghand: playerdominatingHand ?? "", opponentDominatinghand: "Right", playerForehand: "", playerBackhand: "", opponentForehand: "", opponentBackhand: "", deviceDetail: "", packageID: "", userPackageID: "", isDefault: "false", minute: "0", drillType: "")
        
        print(saveMatchParams)
        
        let encoder = JSONEncoder()
        let jsonData = try! encoder.encode(saveMatchParams)
        
        Connector.sharedinstance.runDataApiwithModelandEncodedData(sender: root, data: jsonData, method: .post, parameters: [:], success: nil, url: ApiConstants.saveMatchDetails, completion: { (Data, Error) in
            if let error = Error{
                print(error.localizedDescription)
                return
            }
            guard let response = Data else{
                return
            }
            
            self.videoId = response.data?.videoID
            
            DispatchQueue.main.async {
                let navigate = StoryBoards.VideoRecordingStoryboard.instantiateViewController(withIdentifier: "ShowDemoVC") as! ShowDemoVC
                navigate.fixedTimeStamps = "\(timeStamp)"
                navigate.videoId = self.videoId
                navigate.playerSide = self.playerSide
                navigate.matchAngle = self.matchAngle
                navigate.isPractice = true
                self.root.navigationController?.pushViewController(navigate, animated: true)
            }
            
        }, type: SaveMatchDetailsResponseModel.self)
        
    }
}
