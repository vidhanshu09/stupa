//
//  MatchStatsVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 16/04/21.
//

import UIKit
import FSPagerView
import Kingfisher

protocol ChangeContainerHeightforRally:class {
    func update()
}
protocol ChangeContainerHeightforService:class {
    func updatedata()
}

class MatchStatsVC: BaseVC {

    var ralliesArray = ["5 Fastest Rallies","5 Longest Rallies"]
    var playerAstats: RealTimeMatchPlayerAStats?
    var playerBstats: RealTimeMatchPlayerBStats?
    var playerA:RealTimeMatchStatsPlayer?
    var playerB:RealTimeMatchStatsPlayer?
    var heatMap:HeatMap?
    var games:[RealTimeMatchStatsGame]?
    var zoneheatMap:ZoneHeatMap?
    var speedGraph:[SpeedGraph]?
    var serviceAnalysis, recieveAnalysis: EAnalysis?
    var rally:[Rally]?
    var fastestRally,longestRally:[ErrorPlacementVideo]?
    var matchNo:Int?
    
    
    //MARK:- Objects
    
    @IBOutlet weak var sppedanalysiscontainerheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var ballheatmapcontainerView: UIView!
    @IBOutlet weak var ballheatpageControl: UIPageControl!
    @IBOutlet weak var speedanalysispageControl: UIPageControl!
    @IBOutlet weak var speedanalysiscontainerView: UIView!
    
    fileprivate var introPageViewController: IntroPageViewController?{
        didSet{
            introPageViewController?.introPageViewControllerDataSource = self
            introPageViewController?.introPageViewControllerDelegate = self
        }
    }
    
    fileprivate var ballheatPageViewController: BallHeatMapPageViewController?{
        didSet{
            ballheatPageViewController?.ballHeatMapPageViewControllerDataSource = self
            ballheatPageViewController?.ballHeatMapPageViewControllerDelegate = self
        }
    }
    
    fileprivate var speedanalysisPageViewController: SpeedAnalysisPageViewController?{
        didSet{
            speedanalysisPageViewController?.speedanalysisPageViewControllerDataSource = self
            speedanalysisPageViewController?.speedanalysisPageViewControllerDelegate = self
        }
    }
    
    @IBOutlet weak var fastestballspagerView: FSPagerView!{
        didSet {
            self.fastestballspagerView.register(UINib(nibName:"FastestBalls", bundle: Bundle.main), forCellWithReuseIdentifier: "cell")
            self.fastestballspagerView.transformer = FSPagerViewTransformer(type: .linear)
            let screensize = UIScreen.main.bounds
            let screenwidth = screensize.width
            self.fastestballspagerView.itemSize = CGSize(width: screenwidth - 70, height: 220)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        openPopUp()
        // Do any additional setup after loading the view.
    }
    func adjustingUI(){
        setNavigationwithTitle(navView: navigationView, navTitle: "Match No. - \(matchNo ?? 0)",isbackNeeded: false){
            self.navigationController?.popToViewController(ofClass: MyAnalysisVC.self)
        }
    }
    
    func openPopUp(){
        let ourartificalPopup = StoryBoards.ExtrasStoryboard.instantiateViewController(withIdentifier: "OurArtificialPopUpVC") as! OurArtificialPopUpVC
        ourartificalPopup.modalPresentationStyle = .overFullScreen
        self.present(ourartificalPopup, animated: false, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if let introPageViewController = segue.destination as? IntroPageViewController
        {
            introPageViewController.playerAstats = self.playerAstats
            introPageViewController.playerBstats = self.playerBstats
            introPageViewController.playerA = self.playerA
            introPageViewController.playerB = self.playerB
            self.introPageViewController = introPageViewController
        }
        
        if let ballmapPageViewController = segue.destination as? BallHeatMapPageViewController{
            ballmapPageViewController.heatMap = self.heatMap
            ballmapPageViewController.playerA = self.playerA
            ballmapPageViewController.playerB = self.playerB
            ballmapPageViewController.games = self.games
            ballmapPageViewController.zoneheatMap = self.zoneheatMap
            self.ballheatPageViewController = ballmapPageViewController
        }
        
        if let speedanalysisPageViewController = segue.destination as? SpeedAnalysisPageViewController{
            speedanalysisPageViewController.games = self.games
            speedanalysisPageViewController.rally = self.rally
            speedanalysisPageViewController.speedGraph = self.speedGraph
            speedanalysisPageViewController.playerA = self.playerA
            speedanalysisPageViewController.playerB = self.playerB
            speedanalysisPageViewController.changeheightrallydelegate = self
            speedanalysisPageViewController.changeheightservicedelegate = self
            speedanalysisPageViewController.serviceAnalysis = self.serviceAnalysis
            speedanalysisPageViewController.recieveAnalysis = self.recieveAnalysis
            self.speedanalysisPageViewController = speedanalysisPageViewController
        }
        
    }
}

extension MatchStatsVC: IntroPageViewControllerDataSource{
    func introPageViewController(_ pageViewController: IntroPageViewController, numberOfPages pages: Int){
        pageControl.numberOfPages = pages
    }
}

extension MatchStatsVC: IntroPageViewControllerDelegate{
    func introPageViewController(_ pageViewController: IntroPageViewController, didChangePageIndex index: Int){
        pageControl.currentPage = index
    }
}

extension MatchStatsVC: BallHeatMapPageViewControllerDataSource{
    func ballheatmapPageViewController(_ pageViewController: BallHeatMapPageViewController, numberOfPages pages: Int){
        ballheatpageControl.numberOfPages = pages
    }
}

extension MatchStatsVC: BallHeatMapPageViewControllerDelegate{
    func ballheatmapPageViewController(_ pageViewController: BallHeatMapPageViewController, didChangePageIndex index: Int){
        ballheatpageControl.currentPage = index
    }
}

extension MatchStatsVC: SpeedAnalysisPageViewControllerDataSource{
    func speedanalysisPageViewController(_ pageViewController: SpeedAnalysisPageViewController, numberOfPages pages: Int){
        speedanalysispageControl.numberOfPages = pages
    }
}

extension MatchStatsVC:ChangeContainerHeightforRally{
    func update() {
        sppedanalysiscontainerheightConstraint.constant = 480
    }
}

extension MatchStatsVC:ChangeContainerHeightforService{
    func updatedata() {
        sppedanalysiscontainerheightConstraint.constant = 550
    }
}

extension MatchStatsVC: SpeedAnalysisPageViewControllerDelegate{
    func speedanalysisPageViewController(_ pageViewController: SpeedAnalysisPageViewController, didChangePageIndex index: Int){
        speedanalysispageControl.currentPage = index
    }
}

extension MatchStatsVC : FSPagerViewDataSource,FSPagerViewDelegate{
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return 2
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = fastestballspagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index) as! FastestBallsCell
        cell.headingLabel.text = ralliesArray[index]
        cell.videoImageView.layer.cornerRadius = 5
        if index == 0{
            if fastestRally?.count != 0{
                let url = URL(string: fastestRally?[0].thumbnail ?? "")
                cell.videoImageView.kf.indicatorType = .activity
                cell.videoImageView.kf.setImage(with: url, placeholder: UIImage(named: "empty"), options: [.memoryCacheExpiration(.days(1))])
            }
        }else{
            if longestRally?.count != 0{
                let url = URL(string: longestRally?[0].thumbnail ?? "")
                cell.videoImageView.kf.indicatorType = .activity
                cell.videoImageView.kf.setImage(with: url, placeholder: UIImage(named: "empty"), options: [.memoryCacheExpiration(.days(1))])
            }
        }
        return cell
    }
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        let navigate = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
        if index == 0{
            navigate.videoType = .FastestRally
            navigate.fastestRally = self.fastestRally
        }else{
            navigate.videoType = .LongestRally
            navigate.longestRally = self.longestRally
        }
        navigate.matchNo = self.matchNo
        self.navigationController?.pushViewController(navigate, animated: true)
    }
}
