//
//  FastestBallsCell.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 17/04/21.
//

import UIKit
import FSPagerView

class FastestBallsCell: FSPagerViewCell {
    
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var headingLabel: UILabel!
}
