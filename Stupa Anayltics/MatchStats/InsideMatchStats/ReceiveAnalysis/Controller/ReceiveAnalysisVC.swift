//
//  ReceiveAnalysisVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 16/04/21.
//

import UIKit

class ReceiveAnalysisVC: UIViewController {

    var recieveAnalysis: EAnalysis?
    
    //MARK:- Objects
    
    @IBOutlet weak var halflongplayerBlostlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var halflongplayerBwonLabel: UILabel!
    @IBOutlet weak var halflongplayerBlostLabel: UILabel!
    @IBOutlet weak var halflongplayerAlostlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var halflongplayerAWonLabel: UILabel!
    @IBOutlet weak var halflongplayerAlostLabel: UILabel!
    
    @IBOutlet weak var shortplayerBlostlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var longplayerBlostlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var shortplayerAlostlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var shortplayerBwonLabel: UILabel!
    @IBOutlet weak var shortplayerBlostLabel: UILabel!
    @IBOutlet weak var longplayerBwonLabel: UILabel!
    @IBOutlet weak var longplayerBlostLabel: UILabel!
    @IBOutlet weak var shortplayerAwonLabel: UILabel!
    @IBOutlet weak var shortplayerAlostLabel: UILabel!
    @IBOutlet weak var longplayerAlostlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var longplayerAWonLabel: UILabel!
    @IBOutlet weak var longplayerAlostLabel: UILabel!
    
    
    @IBOutlet weak var halflongplayerBView: UIView!
    @IBOutlet weak var halflongplayerAView: UIView!
    @IBOutlet weak var halflongplayerBnameLabel: UILabel!
    @IBOutlet weak var halflongplayerAnameLabel: UILabel!
    @IBOutlet weak var shortplayerBView: UIView!
    @IBOutlet weak var shortplayerAView: UIView!
    @IBOutlet weak var longplayerBView: UIView!
    @IBOutlet weak var longplayerAView: UIView!
    @IBOutlet weak var shortplayerBnameLabel: UILabel!
    @IBOutlet weak var longplayerBnameLabel: UILabel!
    @IBOutlet weak var shortplayerAnameLabel: UILabel!
    @IBOutlet weak var longplayerAnameLabel: UILabel!
    @IBOutlet weak var playerBnameLabel: UILabel!
    @IBOutlet weak var playerAnameLabel: UILabel!
    @IBOutlet weak var totalserviceplayerBLabel: UILabel!
    @IBOutlet weak var totalserviceplayerALabel: UILabel!
    
    @IBOutlet weak var totalservicesView: UIView!
    @IBOutlet weak var totalservicesplayerbView: UIView!
    @IBOutlet weak var totalservicesheadingView: UIView!
    @IBOutlet weak var totalservicesplayerbheadingView: UIView!
    @IBOutlet weak var dottedView: UIView!
    @IBOutlet weak var lengthconversionheadingView: UIView!
    @IBOutlet weak var lostlegendView: UIView!
    @IBOutlet weak var wonlegendView: UIView!
    @IBOutlet weak var seconddottedView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        totalservicesView.makeround()
        totalservicesView.layer.borderWidth = 2
        totalservicesView.layer.borderColor = Colors.AppGreenColor.cgColor
        totalservicesplayerbView.makeround()
        totalservicesplayerbView.layer.borderWidth = 2
        totalservicesplayerbView.layer.borderColor = Colors.AppGreenColor.cgColor
        totalservicesheadingView.layer.cornerRadius = 18
        totalservicesplayerbheadingView.layer.cornerRadius = 18
        lengthconversionheadingView.layer.borderWidth = 1
        lengthconversionheadingView.layer.borderColor = UIColor.white.cgColor
        lengthconversionheadingView.layer.cornerRadius = 22
        dottedView.createDottedLine(width: 1.0, color: UIColor.white.cgColor)
        seconddottedView.createDottedLine(width: 1.0, color: UIColor.white.cgColor)
        lostlegendView.makeround()
        wonlegendView.makeround()
        settingData()
    }
    
    func settingData(){
        totalserviceplayerALabel.text = recieveAnalysis?.playerA?.totalService?.toString()
        totalserviceplayerBLabel.text = recieveAnalysis?.playerB?.totalService?.toString()
        playerAnameLabel.text = recieveAnalysis?.playerA?.name
        playerBnameLabel.text = recieveAnalysis?.playerB?.name
        halflongplayerAnameLabel.text = recieveAnalysis?.pionWonOnShots?.mediumSort?[0].playerName
        halflongplayerBnameLabel.text = recieveAnalysis?.pionWonOnShots?.mediumSort?[1].playerName
        longplayerAnameLabel.text = recieveAnalysis?.pionWonOnShots?.longSort?[0].playerName
        longplayerBnameLabel.text = recieveAnalysis?.pionWonOnShots?.longSort?[1].playerName
        shortplayerAnameLabel.text = recieveAnalysis?.pionWonOnShots?.shortSort?[0].playerName
        shortplayerBnameLabel.text = recieveAnalysis?.pionWonOnShots?.shortSort?[1].playerName
        longplayerAlostLabel.text = recieveAnalysis?.pionWonOnShots?.longSort?[0].lost?.toString()
        longplayerAWonLabel.text = recieveAnalysis?.pionWonOnShots?.longSort?[0].won?.toString()
        longplayerBlostLabel.text = recieveAnalysis?.pionWonOnShots?.longSort?[1].lost?.toString()
        longplayerBwonLabel.text = recieveAnalysis?.pionWonOnShots?.longSort?[1].won?.toString()
        shortplayerAlostLabel.text = recieveAnalysis?.pionWonOnShots?.shortSort?[0].lost?.toString()
        shortplayerAwonLabel.text = recieveAnalysis?.pionWonOnShots?.shortSort?[0].won?.toString()
        shortplayerBlostLabel.text = recieveAnalysis?.pionWonOnShots?.shortSort?[1].lost?.toString()
        shortplayerBwonLabel.text = recieveAnalysis?.pionWonOnShots?.shortSort?[1].won?.toString()
        halflongplayerAlostLabel.text = recieveAnalysis?.pionWonOnShots?.mediumSort?[0].lost?.toString()
        halflongplayerAWonLabel.text = recieveAnalysis?.pionWonOnShots?.mediumSort?[0].won?.toString()
        halflongplayerBlostLabel.text = recieveAnalysis?.pionWonOnShots?.mediumSort?[1].lost?.toString()
        halflongplayerBwonLabel.text = recieveAnalysis?.pionWonOnShots?.mediumSort?[1].won?.toString()
        settingWonLostLabels()
    }
    func settingWonLostLabels(){
        Utils().setupMultiLabelView(labelwidthConstraint: longplayerAlostlabelwidthConstraint, frameView: longplayerAView, won: recieveAnalysis?.pionWonOnShots?.longSort?[0].won, lost: recieveAnalysis?.pionWonOnShots?.longSort?[0].lost)
        
        Utils().setupMultiLabelView(labelwidthConstraint: longplayerBlostlabelwidthConstraint, frameView: longplayerBView, won: recieveAnalysis?.pionWonOnShots?.longSort?[1].won, lost: recieveAnalysis?.pionWonOnShots?.longSort?[1].lost)

        Utils().setupMultiLabelView(labelwidthConstraint: shortplayerAlostlabelwidthConstraint, frameView: shortplayerAView, won: recieveAnalysis?.pionWonOnShots?.shortSort?[0].won, lost: recieveAnalysis?.pionWonOnShots?.shortSort?[0].lost)
        
        Utils().setupMultiLabelView(labelwidthConstraint: shortplayerBlostlabelwidthConstraint, frameView: shortplayerBView, won: recieveAnalysis?.pionWonOnShots?.shortSort?[1].won, lost: recieveAnalysis?.pionWonOnShots?.shortSort?[1].lost)
        
        Utils().setupMultiLabelView(labelwidthConstraint: halflongplayerAlostlabelwidthConstraint, frameView: halflongplayerAView, won: recieveAnalysis?.pionWonOnShots?.mediumSort?[0].won, lost: recieveAnalysis?.pionWonOnShots?.mediumSort?[0].lost)
        
        Utils().setupMultiLabelView(labelwidthConstraint: halflongplayerBlostlabelwidthConstraint, frameView: halflongplayerBView, won: recieveAnalysis?.pionWonOnShots?.mediumSort?[1].won, lost: recieveAnalysis?.pionWonOnShots?.mediumSort?[1].lost)
    }
    
}
