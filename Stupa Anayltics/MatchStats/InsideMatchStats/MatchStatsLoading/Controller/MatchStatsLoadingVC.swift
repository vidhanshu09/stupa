//
//  MatchStatsLoadingVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 17/04/21.
//

import UIKit

class MatchStatsLoadingVC: BaseVC {
    
    var matchstatsType:Utils.MatchStatsType?
    var matchstatsViewModel:MatchStatsLoadingViewModel!
    var matchNo:Int?
    
    //MARK:- Objects
    
    @IBOutlet weak var navigationView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    func adjustingUI(){
        self.navigationController?.navigationBar.isHidden = true
        self.matchNo = 745
        self.matchstatsType = .Expert
        setNavigationwithTitle(navView: navigationView, navTitle: "Match No. - \(matchNo ?? 0)")
        matchstatsViewModel = MatchStatsLoadingViewModel(root: self, matchNo: matchNo, matchstatsType: matchstatsType)
        if self.matchstatsType == .Expert{
            matchstatsViewModel.expertAnalysisMatchStatsApi()
        }else{
            matchstatsViewModel.realtimeMatchStatsApi()
        }
    }
    
}
