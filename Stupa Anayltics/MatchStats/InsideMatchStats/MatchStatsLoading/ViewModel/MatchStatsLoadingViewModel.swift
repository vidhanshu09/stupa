//
//  MatchStatsLoadingViewModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 17/04/21.
//

import Foundation
import UIKit

class MatchStatsLoadingViewModel:NSObject{
    
    private var matchstatsType:Utils.MatchStatsType?
    private var root:UIViewController!
    private var matchNo:Int?
    init(root:UIViewController,matchNo:Int?,matchstatsType:Utils.MatchStatsType?){
        self.root = root
        self.matchNo = matchNo
        self.matchstatsType = matchstatsType
    }
    
    func realtimeMatchStatsApi(){
        
        let realtimematchStatsParams = RealTimeMatchStatsRequestModel(deviceID: StupaStrings.DeviceUUID ?? "", deviceType: StupaStrings.DeviceType, tokenValue: Utils().getToken(), matchID: matchNo ?? 0)
        
        let encoder = JSONEncoder()
        let jsonData = try! encoder.encode(realtimematchStatsParams)
        
        Connector.sharedinstance.runDataApiwithModelandEncodedData(sender: root, data: jsonData, method: .post, parameters: [:], success: nil, url: ApiConstants.getMatchStatsforRealTime, completion: { (Data, Error) in
            if let error = Error{
                print(error.localizedDescription)
                return
            }
            guard let response = Data else{
                return
            }
            
            if self.matchstatsType == .Practice {
                DispatchQueue.main.async {
                    let navigate = StoryBoards.PracticeAnalysisStoryboard.instantiateViewController(withIdentifier: "PracticeMatchStatsVC") as! PracticeMatchStatsVC
                    
                    navigate.matchNo = self.matchNo
                    
                    if let playerA = response.data?.analysisData?.playerA{
                        navigate.playerA = playerA
                    }
                    
                    if let playerB = response.data?.analysisData?.playerB{
                        navigate.playerB = playerB
                    }
                    
                    if let heatMap = response.data?.aiData?.aiResponse?.heatMap{
                        navigate.heatMap = heatMap
                    }
                    
                    if let zoneheatMap = response.data?.aiData?.aiResponse?.zoneHeatMap{
                        navigate.zoneheatMap = zoneheatMap
                    }
                    
                    if let games = response.data?.analysisData?.games{
                        navigate.games = games
                    }
                    
                    if let fastestRally = response.data?.aiData?.aiResponse?.fastestRally{
                        navigate.fastestRally = fastestRally
                    }
                    
                    if let longestRally = response.data?.aiData?.aiResponse?.longestRally{
                        navigate.longestRally = longestRally
                    }
                    
                    if let practiceStats = response.data?.aiData?.aiResponse?.practiceStats{
                        navigate.practiceStats = practiceStats
                    }
                    
                    if let speedGraph = response.data?.aiData?.aiResponse?.speedGraph{
                        navigate.speedGraph = speedGraph
                    }
                    
                    self.root.navigationController?.pushViewController(navigate, animated: false)
                }
            }else if self.matchstatsType == .Match {
                
                DispatchQueue.main.async {
                    let navigate = StoryBoards.MyAnalysisStoryboard.instantiateViewController(withIdentifier: "MatchStatsVC") as! MatchStatsVC
                    
                    navigate.matchNo = self.matchNo
                    
                    if let playerA = response.data?.analysisData?.playerA{
                        navigate.playerA = playerA
                    }
                    if let playerAstats = response.data?.analysisData?.playerAStats{
                        navigate.playerAstats = playerAstats
                    }
                    if let playerB = response.data?.analysisData?.playerB{
                        navigate.playerB = playerB
                    }
                    if let playerBstats = response.data?.analysisData?.playerBStats{
                        navigate.playerBstats = playerBstats
                    }
                    if let heatMap = response.data?.aiData?.aiResponse?.heatMap{
                        navigate.heatMap = heatMap
                    }
                    
                    if let zoneheatMap = response.data?.aiData?.aiResponse?.zoneHeatMap{
                        navigate.zoneheatMap = zoneheatMap
                    }
                    
                    if let games = response.data?.analysisData?.games{
                        navigate.games = games
                    }
                    
                    if let speedGraph = response.data?.aiData?.aiResponse?.speedGraph{
                        navigate.speedGraph = speedGraph
                    }
                    
                    if let serviceAnalysis = response.data?.aiData?.aiResponse?.serviceAnalysis{
                        navigate.serviceAnalysis = serviceAnalysis
                    }
                    
                    if let recieveAnalysis = response.data?.aiData?.aiResponse?.recieveAnalysis{
                        navigate.recieveAnalysis = recieveAnalysis
                    }
                    
                    if let rally = response.data?.aiData?.aiResponse?.rallies{
                        navigate.rally = rally
                    }
                    
                    if let fastestRally = response.data?.aiData?.aiResponse?.fastestRally{
                        navigate.fastestRally = fastestRally
                    }
                    
                    if let longestRally = response.data?.aiData?.aiResponse?.longestRally{
                        navigate.longestRally = longestRally
                    }
                    
                    self.root.navigationController?.pushViewController(navigate, animated: false)
                }
            }
            
        }, type: RealTimeMatchStatsResponseModel.self)
        
    }
    
    func expertAnalysisMatchStatsApi(){
        
        let eamatchstatsParams = EAMatchStatsRequestModel(deviceID: StupaStrings.DeviceUUID ?? "", deviceType: StupaStrings.DeviceType, tokenValue: Utils().getToken(), matchID: matchNo ?? 0)
        
        let encoder = JSONEncoder()
        let jsonData = try! encoder.encode(eamatchstatsParams)
        
        Connector.sharedinstance.runDataApiwithModelandEncodedData(sender: root, data: jsonData, method: .post, parameters: [:], success: nil, url: ApiConstants.getMatchStatsforExpertAnalysis, completion: { (Data, Error) in
            if let error = Error{
                print(error.localizedDescription)
                return
            }
            guard let response = Data else{
                return
            }
            DispatchQueue.main.async {
                let navigate = StoryBoards.ExpertAnalysisStoryboard.instantiateViewController(withIdentifier: "ExpertAnalysisVC") as! ExpertAnalysisVC
                navigate.matchNo = self.matchNo
                navigate.errorPlacement = response.data?.errorPlacement
                navigate.placementAnalysis = response.data?.placementAnalysis
                navigate.summaryAnalysis = response.data?.summaryAnalysis
                navigate.errorDistribution = response.data?.errorDistribution
                self.root.navigationController?.pushViewController(navigate, animated: false)
            }
            
        }, type: EAMatchStatsResponseModel.self)
    }
    
}
