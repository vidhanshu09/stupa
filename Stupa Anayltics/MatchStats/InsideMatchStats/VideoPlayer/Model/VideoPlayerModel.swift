//
//  VideoPlayerModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 24/04/21.
//

import Foundation

struct EAPlacementErrorVideoRequestModel:Codable {
    let MatchId:String
    let placement:String
    let placementType:Int
    let playerId:Int
    let rType: Int
    let additionalParam:String
    let DeviceId:String
    let DeviceType:String
    let TokenValue:String
}


// MARK: - EAPlacementErrorVideosResponseModel
struct EAPlacementErrorVideosResponseModel: Codable {
    let isSuccess: Bool?
    let message: String?
    let responseData1: [ErrorPlacementVideo]?

    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case message = "Message"
        case responseData1 = "ResponseData1"
    }
}
