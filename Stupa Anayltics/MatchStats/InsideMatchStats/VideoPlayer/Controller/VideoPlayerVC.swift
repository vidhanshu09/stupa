//
//  VideoPlayerVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 18/04/21.
//

import UIKit
import AVKit

class VideoPlayerVC: BaseVC {

    var additionalParams:String?
    var placement:String?
    var placementType:Int?
    var playerId:Int?
    var rType:Int?
    var matchNo:Int?
    var videoType:Utils.VideoType?
    var fastestRally,longestRally:[ErrorPlacementVideo]?
    var videoArray:[ErrorPlacementVideo]?
    var selectedIndex = 0
    var player:AVPlayer?
    var avPlayerController = AVPlayerViewController()
    var videoplayerViewModel:VideoPlayerViewModel!
    
    //MARK:- Objects
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var videoTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        setNavigationwithTitle(navView: navigationView, navTitle: "Match No. - \(matchNo ?? 0)")
        if videoType == .LongestRally{
            self.videoArray = longestRally
            videoTableView.reloadData()
            if videoArray?.count != 0{
                playVideo(index: 0)
            }
        }else if videoType == .FastestRally{
            self.videoArray = fastestRally
            videoTableView.reloadData()
            if videoArray?.count != 0{
                playVideo(index: 0)
            }
        }else{
            videoplayerViewModel = VideoPlayerViewModel(root: self, placement: self.placement, placementType: self.placementType, playerId: self.playerId, rType: self.rType, MatchId: self.matchNo?.toString(), additionalParams: self.additionalParams)
            videoplayerViewModel.getVideosApi { (videos) in
                self.videoArray = videos
                self.videoTableView.reloadData()
                if self.videoArray?.count != 0{
                    self.playVideo(index: 0)
                }
            }
        }
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification){
        print("Video Finished")
        NotificationCenter.default.removeObserver(self)
        self.selectedIndex += 1
        if self.selectedIndex < self.videoArray?.count ?? 0{
            playVideo(index: selectedIndex)
            self.videoTableView.reloadData()
            let index = IndexPath(row: selectedIndex, section: 0)
            DispatchQueue.main.async {
                self.videoTableView.scrollToRow(at: index, at: .bottom, animated: true)
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        removeoldInstance()
    }
    
    func removeoldInstance(){
        NotificationCenter.default.removeObserver(self)
        self.player = nil
        avPlayerController.player = nil
        avPlayerController.removeFromParent()
        avPlayerController.view.removeFromSuperview()
    }
    
    func playVideo(index:Int) {
        
        removeoldInstance()
        player = AVPlayer(url: URL(string: self.videoArray?[index].videoFile ?? "")!)
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        avPlayerController.player = player;
        avPlayerController.view.frame = self.videoView.bounds;

        player?.play()
        self.addChild(avPlayerController)
        self.videoView.addSubview(avPlayerController.view);

    }
    
}

extension VideoPlayerVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.videoArray?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = videoTableView.dequeueReusableCell(withIdentifier: "video", for: indexPath) as! VideoPlayerTVC
        cell.backView.backgroundColor = UIColor.clear
        if self.selectedIndex == indexPath.row {
            cell.backView.backgroundColor = UIColor.systemBlue
        }
        cell.video = self.videoArray?[indexPath.row]
        cell.pointView.makeround()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.selectedIndex = indexPath.row
            self.videoTableView.reloadData()
            self.playVideo(index: self.selectedIndex)
        }
    }
}
