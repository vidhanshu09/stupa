//
//  VideoPlayerViewModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 24/04/21.
//

import Foundation

class VideoPlayerViewModel:NSObject{

    private var additionalParams:String?
    private var MatchId:String?
    private var placement:String?
    private var placementType:Int?
    private var playerId:Int?
    private var rType:Int?
    private var root:UIViewController!
    
    init(root:UIViewController,placement:String?,placementType:Int?,playerId:Int?,rType:Int?,MatchId:String?,additionalParams:String?){
        self.root = root
        self.MatchId = MatchId
        self.additionalParams = additionalParams
        self.placement = placement
        self.placementType = placementType
        self.playerId = playerId
        self.rType = rType
    }
    
    func getVideosApi(completion: @escaping([ErrorPlacementVideo]?) -> Void){
        
        let videoParams = EAPlacementErrorVideoRequestModel(MatchId: MatchId ?? "", placement: placement ?? "", placementType: placementType ?? 0, playerId: playerId ?? 0, rType: rType ?? 0, additionalParam: additionalParams ?? "", DeviceId: StupaStrings.DeviceUUID ?? "", DeviceType: StupaStrings.DeviceType, TokenValue: Utils().getToken())
    
        
        let encoder = JSONEncoder()
        let jsonData = try! encoder.encode(videoParams)
        
        Connector.sharedinstance.runDataApiwithModelandEncodedData(sender: root, data: jsonData, method: .post, parameters: [:], success: nil, url: ApiConstants.getGameVideos, completion: { (Data, Error) in
            if let error = Error{
                print(error.localizedDescription)
                return
            }
            guard let response = Data else{
                return
            }
            
            completion(response.responseData1)
            
        }, type: EAPlacementErrorVideosResponseModel.self)
        
    }

}
