//
//  VideoPlayerTVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 18/04/21.
//

import UIKit
import Kingfisher

class VideoPlayerTVC: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var pointView: UIView!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var gamenoLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    var video:ErrorPlacementVideo?{
        didSet{
            let url = URL(string: video?.thumbnail ?? "")
            thumbnailImageView.kf.indicatorType = .activity
            thumbnailImageView.kf.setImage(with: url, placeholder: UIImage(named: "empty"), options: [.memoryCacheExpiration(.days(1))])
            gamenoLabel.text = "Game \(video?.game?.toString() ?? "")"
            pointLabel.text = video?.point?.toString()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
