//
//  ServiceAnalysisVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 16/04/21.
//

import UIKit

class ServiceAnalysisVC: UIViewController {

    var serviceAnalysis: EAnalysis?
    
    //MARK:- Objects
    
    @IBOutlet weak var longplayerBwonlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var shortplayerBlostlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var longplayerBlostlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var shortplayerAlostlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var shortplayerBwonLabel: UILabel!
    @IBOutlet weak var shortplayerBlostLabel: UILabel!
    @IBOutlet weak var longplayerBwonLabel: UILabel!
    @IBOutlet weak var longplayerBlostLabel: UILabel!
    @IBOutlet weak var shortplayerAwonLabel: UILabel!
    @IBOutlet weak var shortplayerAlostLabel: UILabel!
    @IBOutlet weak var longplayerAlostlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var longplayerAWonLabel: UILabel!
    @IBOutlet weak var longplayerAlostLabel: UILabel!
    @IBOutlet weak var shortplayerBView: UIView!
    @IBOutlet weak var shortplayerAView: UIView!
    @IBOutlet weak var longplayerBView: UIView!
    @IBOutlet weak var longplayerAView: UIView!
    @IBOutlet weak var shortplayerBnameLabel: UILabel!
    @IBOutlet weak var longplayerBnameLabel: UILabel!
    @IBOutlet weak var shortplayerAnameLabel: UILabel!
    @IBOutlet weak var longplayerAnameLabel: UILabel!
    @IBOutlet weak var playerBnameLabel: UILabel!
    @IBOutlet weak var playerAnameLabel: UILabel!
    @IBOutlet weak var totalserviceplayerBLabel: UILabel!
    @IBOutlet weak var totalserviceplayerALabel: UILabel!
    @IBOutlet weak var totalservicesView: UIView!
    @IBOutlet weak var totalservicesplayerbView: UIView!
    @IBOutlet weak var totalservicesheadingView: UIView!
    @IBOutlet weak var totalservicesplayerbheadingView: UIView!
    @IBOutlet weak var dottedView: UIView!
    @IBOutlet weak var lengthconversionheadingView: UIView!
    @IBOutlet weak var lostlegendView: UIView!
    @IBOutlet weak var wonlegendView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        totalservicesView.makeround()
        totalservicesView.layer.borderWidth = 2
        totalservicesView.layer.borderColor = Colors.AppGreenColor.cgColor
        totalservicesplayerbView.makeround()
        totalservicesplayerbView.layer.borderWidth = 2
        totalservicesplayerbView.layer.borderColor = Colors.AppGreenColor.cgColor
        totalservicesheadingView.layer.cornerRadius = 18
        totalservicesplayerbheadingView.layer.cornerRadius = 18
        lengthconversionheadingView.layer.borderWidth = 1
        lengthconversionheadingView.layer.borderColor = UIColor.white.cgColor
        lengthconversionheadingView.layer.cornerRadius = 22
        dottedView.createDottedLine(width: 1.0, color: UIColor.white.cgColor)
        lostlegendView.makeround()
        wonlegendView.makeround()
        settingData()
    }
    
    func settingData(){
        totalserviceplayerALabel.text = serviceAnalysis?.playerA?.totalService?.toString()
        totalserviceplayerBLabel.text = serviceAnalysis?.playerB?.totalService?.toString()
        playerAnameLabel.text = serviceAnalysis?.playerA?.name
        playerBnameLabel.text = serviceAnalysis?.playerB?.name
        longplayerAnameLabel.text = serviceAnalysis?.pionWonOnShots?.longSort?[0].playerName
        longplayerBnameLabel.text = serviceAnalysis?.pionWonOnShots?.longSort?[1].playerName
        shortplayerAnameLabel.text = serviceAnalysis?.pionWonOnShots?.shortSort?[0].playerName
        shortplayerBnameLabel.text = serviceAnalysis?.pionWonOnShots?.shortSort?[1].playerName
        longplayerAlostLabel.text = serviceAnalysis?.pionWonOnShots?.longSort?[0].lost?.toString()
        longplayerAWonLabel.text = serviceAnalysis?.pionWonOnShots?.longSort?[0].won?.toString()
        longplayerBlostLabel.text = serviceAnalysis?.pionWonOnShots?.longSort?[1].lost?.toString()
        longplayerBwonLabel.text = serviceAnalysis?.pionWonOnShots?.longSort?[1].won?.toString()
        shortplayerAlostLabel.text = serviceAnalysis?.pionWonOnShots?.shortSort?[0].lost?.toString()
        shortplayerAwonLabel.text = serviceAnalysis?.pionWonOnShots?.shortSort?[0].won?.toString()
        shortplayerBlostLabel.text = serviceAnalysis?.pionWonOnShots?.shortSort?[1].lost?.toString()
        shortplayerBwonLabel.text = serviceAnalysis?.pionWonOnShots?.shortSort?[1].won?.toString()
        settingWonLostLabels()
    }
   
    func settingWonLostLabels(){
        Utils().setupMultiLabelView(labelwidthConstraint: longplayerAlostlabelwidthConstraint, frameView: longplayerAView, won: serviceAnalysis?.pionWonOnShots?.longSort?[0].won, lost: serviceAnalysis?.pionWonOnShots?.longSort?[0].lost)
        
        Utils().setupMultiLabelView(labelwidthConstraint: longplayerBlostlabelwidthConstraint, frameView: longplayerBView, won: serviceAnalysis?.pionWonOnShots?.longSort?[1].won, lost: serviceAnalysis?.pionWonOnShots?.longSort?[1].lost)

        Utils().setupMultiLabelView(labelwidthConstraint: shortplayerAlostlabelwidthConstraint, frameView: shortplayerAView, won: serviceAnalysis?.pionWonOnShots?.shortSort?[0].won, lost: serviceAnalysis?.pionWonOnShots?.shortSort?[0].lost)
        
        Utils().setupMultiLabelView(labelwidthConstraint: shortplayerBlostlabelwidthConstraint, frameView: shortplayerBView, won: serviceAnalysis?.pionWonOnShots?.shortSort?[1].won, lost: serviceAnalysis?.pionWonOnShots?.shortSort?[1].lost)
    }
    
}
