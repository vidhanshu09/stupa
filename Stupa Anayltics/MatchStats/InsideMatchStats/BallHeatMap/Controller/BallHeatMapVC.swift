//
//  BallHeatMapVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 16/04/21.
//

import UIKit

class BallHeatMapVC: UIViewController {

    var heatMap:HeatMap?
    var games:[RealTimeMatchStatsGame]?
    var playerA:RealTimeMatchStatsPlayer?
    var playerB:RealTimeMatchStatsPlayer?
    var ballHeatMapViewModel:BallHeatMapViewModel!
    
    
    //MARK:- Objects
    
    @IBOutlet weak var playerBnameLabel: UILabel!
    @IBOutlet weak var playerAnameLabel: UILabel!
    @IBOutlet weak var heatmapView: UIView!
    @IBOutlet weak var setCollectionView: UICollectionView!
    @IBOutlet weak var setcollectionviewheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var serviceView: UIView!
    @IBOutlet weak var receiveView: UIView!
    @IBOutlet weak var thirdballView: UIView!
    @IBOutlet weak var fourthballView: UIView!
    @IBOutlet weak var othersView: UIView!
    @IBOutlet weak var iserrorSwitch: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
    }
    
    func adjustingUI(){
        serviceView.layer.borderWidth = 1
        serviceView.layer.borderColor = UIColor.white.cgColor
        receiveView.layer.borderWidth = 1
        receiveView.layer.borderColor = UIColor.white.cgColor
        thirdballView.layer.borderWidth = 1
        thirdballView.layer.borderColor = UIColor.white.cgColor
        fourthballView.layer.borderWidth = 1
        fourthballView.layer.borderColor = UIColor.white.cgColor
        othersView.layer.borderWidth = 1
        othersView.layer.borderColor = UIColor.white.cgColor
        iserrorSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        ballHeatMapViewModel = BallHeatMapViewModel(heatmap: self.heatMap, heatmapView: heatmapView,playerA: playerA,playerB: playerB,setCollectionView: setCollectionView,setcollectionviewheightConstraint: setcollectionviewheightConstraint, games: self.games)
        ballHeatMapViewModel.getheatPoints()
        ballHeatMapViewModel.observers()
        setCollectionView.dataSource = ballHeatMapViewModel
        setCollectionView.delegate = ballHeatMapViewModel
        playerAnameLabel.text = playerA?.name
        playerBnameLabel.text = playerB?.name
        addGestures()
    }
    
    func addGestures(){
        serviceView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(serviceAct)))
        receiveView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(recieveAct)))
        thirdballView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(thirdballAct)))
        fourthballView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(fourthballAct)))
        othersView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(othersAct)))
    }
    
    @objc func serviceAct(){
        if ballHeatMapViewModel.selectedGame == 1{
            serviceView.backgroundColor = UIColor.black
            ballHeatMapViewModel.pointfilter = false
            ballHeatMapViewModel.selectedGame = 0
        }else{
            ballHeatMapViewModel.selectedGame = 1
            ballHeatMapViewModel.pointfilter = true
            serviceView.backgroundColor = Colors.TableBackColor
            receiveView.backgroundColor = UIColor.black
            thirdballView.backgroundColor = UIColor.black
            fourthballView.backgroundColor = UIColor.black
            othersView.backgroundColor = UIColor.black
        }
        ballHeatMapViewModel.getheatPoints()
    }
    @objc func recieveAct(){
        if ballHeatMapViewModel.selectedGame == 2{
            ballHeatMapViewModel.selectedGame = 0
            ballHeatMapViewModel.pointfilter = false
            receiveView.backgroundColor = UIColor.black
        }else{
            ballHeatMapViewModel.selectedGame = 2
            ballHeatMapViewModel.pointfilter = true
            receiveView.backgroundColor = Colors.TableBackColor
            serviceView.backgroundColor = UIColor.black
            thirdballView.backgroundColor = UIColor.black
            fourthballView.backgroundColor = UIColor.black
            othersView.backgroundColor = UIColor.black
        }
        ballHeatMapViewModel.getheatPoints()
    }
    @objc func thirdballAct(){
        if ballHeatMapViewModel.selectedGame == 3{
            ballHeatMapViewModel.selectedGame = 0
            ballHeatMapViewModel.pointfilter = false
            thirdballView.backgroundColor = UIColor.black
        }else{
            ballHeatMapViewModel.selectedGame = 3
            ballHeatMapViewModel.pointfilter = true
            thirdballView.backgroundColor = Colors.TableBackColor
            receiveView.backgroundColor = UIColor.black
            serviceView.backgroundColor = UIColor.black
            fourthballView.backgroundColor = UIColor.black
            othersView.backgroundColor = UIColor.black
        }
        ballHeatMapViewModel.getheatPoints()
    }
    @objc func fourthballAct(){
        if ballHeatMapViewModel.selectedGame == 4{
            ballHeatMapViewModel.selectedGame = 0
            ballHeatMapViewModel.pointfilter = false
            fourthballView.backgroundColor = UIColor.black
        }else{
            ballHeatMapViewModel.selectedGame = 4
            ballHeatMapViewModel.pointfilter = true
            fourthballView.backgroundColor = Colors.TableBackColor
            receiveView.backgroundColor = UIColor.black
            serviceView.backgroundColor = UIColor.black
            thirdballView.backgroundColor = UIColor.black
            othersView.backgroundColor = UIColor.black
        }
        ballHeatMapViewModel.getheatPoints()
    }
    @objc func othersAct(){
        if ballHeatMapViewModel.selectedGame == 7{
            ballHeatMapViewModel.selectedGame = 0
            ballHeatMapViewModel.pointfilter = false
            othersView.backgroundColor = UIColor.black
        }else{
            ballHeatMapViewModel.selectedGame = 7
            ballHeatMapViewModel.pointfilter = true
            othersView.backgroundColor = Colors.TableBackColor
            receiveView.backgroundColor = UIColor.black
            serviceView.backgroundColor = UIColor.black
            thirdballView.backgroundColor = UIColor.black
            fourthballView.backgroundColor = UIColor.black
        }
        ballHeatMapViewModel.getheatPoints()
    }
    
    
    @IBAction func iserrorswitchAct(_ sender: Any) {
        ballHeatMapViewModel.isErrorOrWinningSelected = iserrorSwitch.isOn
        ballHeatMapViewModel.getheatPoints()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
