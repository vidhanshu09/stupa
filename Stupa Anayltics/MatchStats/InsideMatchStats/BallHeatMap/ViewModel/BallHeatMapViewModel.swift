//
//  BallHeatMapViewModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 17/04/21.
//

import Foundation

class BallHeatMapViewModel:NSObject{
    
    private var heatMap:HeatMap?
    private var playerA:RealTimeMatchStatsPlayer?
    private var playerB:RealTimeMatchStatsPlayer?
    private var games:[RealTimeMatchStatsGame]?
    private var heatMapView:UIView!
    private var pointView = [UIView]()
    private var setCollectionView: UICollectionView!
    private var setcollectionviewheightConstraint: NSLayoutConstraint!
    var isErrorOrWinningSelected = false
    var selectedGame = 0
    var pointfilter = false
    var issetFilter = false
    var selectedSet = 0
    
    init(heatmap:HeatMap?,heatmapView:UIView,playerA:RealTimeMatchStatsPlayer?,playerB:RealTimeMatchStatsPlayer?,setCollectionView:UICollectionView,setcollectionviewheightConstraint:NSLayoutConstraint,games:[RealTimeMatchStatsGame]?){
        self.heatMap = heatmap
        self.heatMapView = heatmapView
        self.playerA = playerA
        self.playerB = playerB
        self.games = games
        self.setCollectionView = setCollectionView
        self.setcollectionviewheightConstraint = setcollectionviewheightConstraint
    }
    
    func observers(){
        self.setCollectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    func getheatPoints(){
        
        if isErrorOrWinningSelected {
            
            if issetFilter && pointfilter{
                if selectedGame == 7{
                    let totalmapssetwiseData = self.heatMap?.winningHeatMaps?.filter{ $0.shotNo ?? 0 > 4 && $0.gameNo == self.selectedSet}
                    setHeatMapFiltereddataError(totalmapssetwiseData: totalmapssetwiseData)
                }else{
                    let totalmapssetwiseData = self.heatMap?.winningHeatMaps?.filter{ $0.shotNo == self.selectedGame && $0.gameNo == self.selectedSet }
                    setHeatMapFiltereddataError(totalmapssetwiseData: totalmapssetwiseData)
                }
                return
            }
            
            
            if issetFilter{
                let totalmapssetwiseData = self.heatMap?.winningHeatMaps?.filter{ $0.gameNo == self.selectedSet }
                setHeatMapFiltereddataError(totalmapssetwiseData: totalmapssetwiseData)
                return
            }
            
            if pointfilter{
                if selectedGame == 7{
                    let totalmapssetwiseData = self.heatMap?.winningHeatMaps?.filter{ $0.shotNo ?? 0 > 4 }
                    setHeatMapFiltereddataError(totalmapssetwiseData: totalmapssetwiseData)
                }else{
                    let totalmapssetwiseData = self.heatMap?.winningHeatMaps?.filter{ $0.shotNo == self.selectedGame }
                    setHeatMapFiltereddataError(totalmapssetwiseData: totalmapssetwiseData)
                }
                return
            }
            
            let totalmapssetwiseData = self.heatMap?.winningHeatMaps
            setHeatMapFiltereddataError(totalmapssetwiseData: totalmapssetwiseData)
            
        } else {
            
            if issetFilter && pointfilter{
                if selectedGame == 7{
                    let totalmapssetwiseData = self.heatMap?.totalHeatMaps?.filter{ $0.shotNo ?? 0 > 4 && $0.gameNo == self.selectedSet}
                    setHeatMapFiltereddata(totalmapssetwiseData: totalmapssetwiseData)
                }else{
                    let totalmapssetwiseData = self.heatMap?.totalHeatMaps?.filter{ $0.shotNo == self.selectedGame && $0.gameNo == self.selectedSet }
                    setHeatMapFiltereddata(totalmapssetwiseData: totalmapssetwiseData)
                }
                return
            }


            if issetFilter{
                let totalmapssetwiseData = self.heatMap?.totalHeatMaps?.filter{ $0.gameNo == self.selectedSet }
                setHeatMapFiltereddata(totalmapssetwiseData: totalmapssetwiseData)
                return
            }
            
            if pointfilter{
                if selectedGame == 7{
                    let totalmapssetwiseData = self.heatMap?.totalHeatMaps?.filter{ $0.shotNo ?? 0 > 4 }
                    setHeatMapFiltereddata(totalmapssetwiseData: totalmapssetwiseData)
                }else{
                    let totalmapssetwiseData = self.heatMap?.totalHeatMaps?.filter{ $0.shotNo == self.selectedGame }
                    setHeatMapFiltereddata(totalmapssetwiseData: totalmapssetwiseData)
                }
                return
            }
            
            let totalmapssetwiseData = self.heatMap?.totalHeatMaps
            setHeatMapFiltereddata(totalmapssetwiseData: totalmapssetwiseData)
            
        }
        
    }
    
    func setHeatMapFiltereddataError(totalmapssetwiseData:[WinningHeatMap]?){
        
        let p1 = playerA?.name
        for v in self.pointView {
            v.removeFromSuperview()
        }
        self.pointView.removeAll()
        
        for v in self.pointView {
            v.removeFromSuperview()
        }
    
        for data in totalmapssetwiseData ?? [] {
            if data.playedBy == p1 {
                let x = (Double(data.ballX ?? "0") ?? 0)*(300/685)
                let y = (Double(data.ballY ?? "0") ?? 0)*(552/1260)
                let pointView = UIView()
                pointView.center = CGPoint(x: x, y: y)
                pointView.frame.size.width = 6
                pointView.frame.size.height = 6
                pointView.layer.cornerRadius = 3
                pointView.layer.masksToBounds = true
                pointView.backgroundColor = UIColor.orange
                self.pointView.append(pointView)
            } else {

                let x = (Double(data.ballX ?? "0") ?? 0)*(300/685)
                let y = (Double(data.ballY ?? "0") ?? 0)*(552/1260)
                let pointView = UIView()
                pointView.center = CGPoint(x: x, y: y)
                pointView.frame.size.width = 6
                pointView.frame.size.height = 6
                pointView.layer.cornerRadius = 3
                pointView.layer.masksToBounds = true
                pointView.backgroundColor = UIColor.yellow
                self.pointView.append(pointView)
            }
        }
        
        for v in self.pointView {
            self.heatMapView.addSubview(v)
        }

        self.heatMapView.reloadInputViews()
    }
    
    func setHeatMapFiltereddata(totalmapssetwiseData:[TotalHeatMap]?){
        
        for v in self.pointView {
            v.removeFromSuperview()
        }
        self.pointView.removeAll()
        
        let p1 = playerA?.name
        for v in self.pointView {
            v.removeFromSuperview()
        }
    
        for data in totalmapssetwiseData ?? [] {
            if data.playedBy == p1 {
                let x = (Double(data.ballX ?? "0") ?? 0)*(300/685)
                let y = (Double(data.ballY ?? "0") ?? 0)*(552/1260)
                let pointView = UIView()
                pointView.center = CGPoint(x: x, y: y)
                pointView.frame.size.width = 6
                pointView.frame.size.height = 6
                pointView.layer.cornerRadius = 3
                pointView.layer.masksToBounds = true
                pointView.backgroundColor = UIColor.orange
                self.pointView.append(pointView)
            } else {

                let x = (Double(data.ballX ?? "0") ?? 0)*(300/685)
                let y = (Double(data.ballY ?? "0") ?? 0)*(552/1260)
                let pointView = UIView()
                pointView.center = CGPoint(x: x, y: y)
                pointView.frame.size.width = 6
                pointView.frame.size.height = 6
                pointView.layer.cornerRadius = 3
                pointView.layer.masksToBounds = true
                pointView.backgroundColor = UIColor.yellow
                self.pointView.append(pointView)
            }
        }
        
        for v in self.pointView {
            self.heatMapView.addSubview(v)
        }

        self.heatMapView.reloadInputViews()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension BallHeatMapViewModel:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return games?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = setCollectionView.dequeueReusableCell(withReuseIdentifier: "set", for: indexPath) as! SetCVC
        if selectedSet == games?[indexPath.row].gameID{
            if games?[indexPath.row].isSelected == true{
                games?[indexPath.row].isSelected = false
                cell.backView.backgroundColor = UIColor.black
                issetFilter = false
                selectedSet = 0
                getheatPoints()
            }else{
                games?[indexPath.row].isSelected = true
                cell.backView.backgroundColor = Colors.TableBackColor
                getheatPoints()
            }
        }else{
            games?[indexPath.row].isSelected = false
            cell.backView.backgroundColor = UIColor.black
        }
        cell.setnameLabel.text = "Set - \(games?[indexPath.row].gameID ?? 0)"
        cell.backView.layer.borderWidth = 1
        cell.backView.layer.borderColor = UIColor.white.cgColor
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let size = CGSize(width: collectionView.frame.size.width/5, height: collectionView.frame.size.width/5 - 40)
        return size
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedSet = games?[indexPath.row].gameID ?? 0
        issetFilter = true
        self.setCollectionView.reloadData()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        self.setCollectionView.layer.removeAllAnimations()
        if games?.count == 0{
            setcollectionviewheightConstraint.constant = 0
        }else{
            setcollectionviewheightConstraint.constant = setCollectionView.contentSize.height
        }
    }
    
}
