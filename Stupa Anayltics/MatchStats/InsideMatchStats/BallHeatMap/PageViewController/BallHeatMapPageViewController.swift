//
//  BallHeatMapPageViewController.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 16/04/21.
//

import UIKit

protocol BallHeatMapPageViewControllerDataSource: class
{
    func ballheatmapPageViewController(_ pageViewController: BallHeatMapPageViewController, numberOfPages pages: Int)
}

protocol BallHeatMapPageViewControllerDelegate: class
{
    func ballheatmapPageViewController(_ pageViewController: BallHeatMapPageViewController, didChangePageIndex index: Int)
}

class BallHeatMapPageViewController: UIPageViewController {
    
    var zoneheatMap:ZoneHeatMap?
    var heatMap:HeatMap?
    var games:[RealTimeMatchStatsGame]?
    var playerA:RealTimeMatchStatsPlayer?
    var playerB:RealTimeMatchStatsPlayer?
    weak var ballHeatMapPageViewControllerDataSource: BallHeatMapPageViewControllerDataSource?
    weak var ballHeatMapPageViewControllerDelegate: BallHeatMapPageViewControllerDelegate?
    
    fileprivate(set) lazy var contentViewControllers: [UIViewController] =
    {
        return [
            self.contentViewController(withIdentifier: "BallHeatMapVC"),
            self.contentViewController(withIdentifier: "BallPlacementVC")
        ]
    }()
    
    fileprivate var allowRotate: Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        dataSource = self
        delegate = self
        
        if let initialViewController = contentViewControllers.first as? BallHeatMapVC
        {
            initialViewController.heatMap = self.heatMap
            initialViewController.playerA = self.playerA
            initialViewController.playerB = self.playerB
            initialViewController.games = self.games
            contentViewController(followingViewController: initialViewController)
        }
        
        if let initialViewController = contentViewControllers[1] as? BallPlacementVC
        {
            initialViewController.zoneheatMap = self.zoneheatMap
            initialViewController.playerA = self.playerA
            initialViewController.playerB = self.playerB
            initialViewController.games = self.games
        }
        
        
        ballHeatMapPageViewControllerDataSource?.ballheatmapPageViewController(self, numberOfPages: contentViewControllers.count)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}

extension BallHeatMapPageViewController
{
    fileprivate func contentViewController(withIdentifier identifier: String) -> UIViewController
    {
        return UIStoryboard(name: "MyAnalysis", bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
    
    fileprivate func contentViewController(followingViewController viewController: UIViewController, to direction: UIPageViewController.NavigationDirection = .forward)
    {
        setViewControllers([viewController], direction: direction, animated: true)
        {
            (finished) -> Void in
            self.didChangePage()
        }
    }
    
    fileprivate func didChangePage()
    {
        if let firstViewController = viewControllers?.first, let index = contentViewControllers.firstIndex(of: firstViewController)
        {
            
            print(firstViewController)
            
            ballHeatMapPageViewControllerDelegate?.ballheatmapPageViewController(self, didChangePageIndex: index)
        }
    }
}

extension BallHeatMapPageViewController
{
    public func next()
    {
        if let visibleViewController = viewControllers?.first, let viewController = pageViewController(self, viewControllerAfter: visibleViewController)
        {
            
            print(viewController)
            print(visibleViewController)
            contentViewController(followingViewController: viewController)
            
        }
        
        
    }
    
    func pageTo(at index: Int)
    {
        if let firstViewController = viewControllers?.first, let currentIndex = contentViewControllers.firstIndex(of: firstViewController)
        {
            let direction: UIPageViewController.NavigationDirection = index >= currentIndex ? .forward : .reverse
            let viewController = contentViewControllers[index]
            
            contentViewController(followingViewController: viewController, to: direction)
        }
    }
}

extension BallHeatMapPageViewController: UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        guard let index = contentViewControllers.firstIndex(of: viewController) else { return nil }
        
        let previous = index - 1
        
        guard previous >= 0 else { return nil }
        guard contentViewControllers.count > previous else { return nil }
        
        return contentViewControllers[previous]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let index = contentViewControllers.firstIndex(of: viewController) else { return nil }
        
        let next = index + 1
  
        let count = contentViewControllers.count
        
        //guard count != next else { return contentViewControllers.first } // rotatable
        guard count != next else { return nil }
        guard count > next else { return nil }
        
        return contentViewControllers[next]
    }
}

extension BallHeatMapPageViewController: UIPageViewControllerDelegate
{
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        didChangePage()
    }
    
    
}
