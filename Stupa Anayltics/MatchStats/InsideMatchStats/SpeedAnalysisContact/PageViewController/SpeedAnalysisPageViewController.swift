//
//  SpeedAnalysisPageViewController.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 16/04/21.
//

import UIKit

protocol SpeedAnalysisPageViewControllerDataSource: class
{
    func speedanalysisPageViewController(_ pageViewController: SpeedAnalysisPageViewController, numberOfPages pages: Int)
}

protocol SpeedAnalysisPageViewControllerDelegate: class
{
    func speedanalysisPageViewController(_ pageViewController: SpeedAnalysisPageViewController, didChangePageIndex index: Int)
}

class SpeedAnalysisPageViewController: UIPageViewController {
    
    weak var changeheightrallydelegate:ChangeContainerHeightforRally?
    weak var changeheightservicedelegate:ChangeContainerHeightforService?
    var playerA:RealTimeMatchStatsPlayer?
    var playerB:RealTimeMatchStatsPlayer?
    var rally:[Rally]?
    var serviceAnalysis, recieveAnalysis: EAnalysis?
    var speedGraph:[SpeedGraph]?
    var games:[RealTimeMatchStatsGame]?
    weak var speedanalysisPageViewControllerDataSource: SpeedAnalysisPageViewControllerDataSource?
    weak var speedanalysisPageViewControllerDelegate: SpeedAnalysisPageViewControllerDelegate?
    
    fileprivate(set) lazy var contentViewControllers: [UIViewController] =
    {
        return [
            self.contentViewController(withIdentifier: "SpeedAnalysisContactVC"),
            self.contentViewController(withIdentifier: "ServiceAnalysisVC"),
            self.contentViewController(withIdentifier: "ReceiveAnalysisVC"),
            self.contentViewController(withIdentifier: "RallyAnalysisVC")
        ]
    }()
    
    fileprivate var allowRotate: Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        dataSource = self
        delegate = self
        
        if let initialViewController = contentViewControllers.first as? SpeedAnalysisContactVC
        {
            initialViewController.games = self.games
            initialViewController.speedGraph = self.speedGraph
            contentViewController(followingViewController: initialViewController)
        }
        
        if let serviceanalysisVC = contentViewControllers[1] as? ServiceAnalysisVC{
            serviceanalysisVC.serviceAnalysis = self.serviceAnalysis
        }
        
        if let receiveanalysisVC = contentViewControllers[2] as? ReceiveAnalysisVC{
            receiveanalysisVC.recieveAnalysis = self.recieveAnalysis
        }
        
        if let rallyanalysisVC = contentViewControllers[3] as? RallyAnalysisVC{
            rallyanalysisVC.playerA = self.playerA
            rallyanalysisVC.playerB = self.playerB
            rallyanalysisVC.rally = self.rally
        }
        
        speedanalysisPageViewControllerDataSource?.speedanalysisPageViewController(self, numberOfPages: contentViewControllers.count)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}

extension SpeedAnalysisPageViewController
{
    fileprivate func contentViewController(withIdentifier identifier: String) -> UIViewController
    {
        return UIStoryboard(name: "MyAnalysis", bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
    
    fileprivate func contentViewController(followingViewController viewController: UIViewController, to direction: UIPageViewController.NavigationDirection = .forward)
    {
        setViewControllers([viewController], direction: direction, animated: true)
        {
            (finished) -> Void in
            self.didChangePage()
        }
    }
    
    fileprivate func didChangePage()
    {
        if let firstViewController = viewControllers?.first, let index = contentViewControllers.firstIndex(of: firstViewController)
        {
            
            print(firstViewController)
              
            if let _ = firstViewController as? RallyAnalysisVC{
                self.changeheightrallydelegate?.update()
            }
            
            if let _ = firstViewController as? ReceiveAnalysisVC{
                self.changeheightservicedelegate?.updatedata()
            }
            
            speedanalysisPageViewControllerDelegate?.speedanalysisPageViewController(self, didChangePageIndex: index)
        }
    }
}

extension SpeedAnalysisPageViewController
{
    public func next()
    {
        if let visibleViewController = viewControllers?.first, let viewController = pageViewController(self, viewControllerAfter: visibleViewController)
        {
            
            print(viewController)
            print(visibleViewController)
            contentViewController(followingViewController: viewController)
            
        }
        
        
    }
    
    func pageTo(at index: Int)
    {
        if let firstViewController = viewControllers?.first, let currentIndex = contentViewControllers.firstIndex(of: firstViewController)
        {
            let direction: UIPageViewController.NavigationDirection = index >= currentIndex ? .forward : .reverse
            let viewController = contentViewControllers[index]
            
            contentViewController(followingViewController: viewController, to: direction)
        }
    }
}

extension SpeedAnalysisPageViewController: UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        guard let index = contentViewControllers.firstIndex(of: viewController) else { return nil }
        
        let previous = index - 1
        
        guard previous >= 0 else { return nil }
        guard contentViewControllers.count > previous else { return nil }
        
        return contentViewControllers[previous]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let index = contentViewControllers.firstIndex(of: viewController) else { return nil }
        
        let next = index + 1
  
        let count = contentViewControllers.count
        
        //guard count != next else { return contentViewControllers.first } // rotatable
        guard count != next else { return nil }
        guard count > next else { return nil }
        
        return contentViewControllers[next]
    }
}

extension SpeedAnalysisPageViewController: UIPageViewControllerDelegate
{
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        didChangePage()
    }
    
    
}
