//
//  SpeedAnalysisContactViewModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 18/04/21.
//

import Foundation
import UIKit
import Charts

class SpeedAnalysisContactViewModel:NSObject{
    
    private var speedGraph:[SpeedGraph]?
    private var games:[RealTimeMatchStatsGame]?
    private var lineChartView:LineChartView
    private var setCollectionView: UICollectionView!
    private var setcollectionviewheightConstraint: NSLayoutConstraint!
    var selectedChartType:Utils.SpeedChartType?
    var isstatusFilter = false
    var issetFilter = false
    var p1 = String()
    var p2 = String()
    var selectedSet = 0
    
    init(setCollectionView:UICollectionView,setcollectionviewheightConstraint: NSLayoutConstraint,games:[RealTimeMatchStatsGame]?,lineChartView:LineChartView,speedGraph:[SpeedGraph]?){
        self.setCollectionView = setCollectionView
        self.games = games
        self.speedGraph = speedGraph
        self.lineChartView = lineChartView
        self.setcollectionviewheightConstraint = setcollectionviewheightConstraint
    }
    func observers(){
        self.setCollectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    func setupChart(){
        self.p1 = self.speedGraph?.first?.winningSpeeds?.first?.playedBy ?? ""
        self.p2 = self.speedGraph?.first?.winningSpeeds?.filter({$0.playedBy != self.p1}).first?.playedBy ?? ""
        getChartValues()
    }
    
    func getChartValues() {
        if isstatusFilter && issetFilter{
            if self.selectedSet == 1{
                if self.speedGraph?.count ?? 0 > 1{
                    if self.selectedChartType == .WON {
                        let game = self.speedGraph?[1]
                        let winningP1 = game?.winningSpeeds?.filter({$0.playedBy == p1})
                        let winningp2 = game?.winningSpeeds?.filter({$0.playedBy == p2})
                        self.setChartData(p1: winningp2 ?? [], p2: winningP1 ?? [])
                    }else{
                        let game = self.speedGraph?[1]
                        let winningP1 = game?.totalSpeeds?.filter({$0.playedBy == p1})
                        let winningp2 = game?.totalSpeeds?.filter({$0.playedBy == p2})
                        self.setChartData(p1: winningp2 ?? [], p2: winningP1 ?? [])
                    }
                }
            }else{
                if self.speedGraph?.count ?? 0 > 2{
                    if self.selectedChartType == .WON {
                        let game = self.speedGraph?[2]
                        let winningP1 = game?.winningSpeeds?.filter({$0.playedBy == p1})
                        let winningp2 = game?.winningSpeeds?.filter({$0.playedBy == p2})
                        self.setChartData(p1: winningp2 ?? [], p2: winningP1 ?? [])
                    }else{
                        let game = self.speedGraph?[2]
                        let winningP1 = game?.totalSpeeds?.filter({$0.playedBy == p1})
                        let winningp2 = game?.totalSpeeds?.filter({$0.playedBy == p2})
                        self.setChartData(p1: winningp2 ?? [], p2: winningP1 ?? [])
                    }
                }
            }
            return
        }
        
        if isstatusFilter{
            let game = self.speedGraph?.first
            if self.selectedChartType == .WON {
                let winningP1 = game?.winningSpeeds?.filter({$0.playedBy == p1})
                let winningp2 = game?.winningSpeeds?.filter({$0.playedBy == p2})
                self.setChartData(p1: winningp2 ?? [], p2: winningP1 ?? [])
                return
            }else{
                let totalP1 = game?.totalSpeeds?.filter({$0.playedBy == p1})
                let totalP2 = game?.totalSpeeds?.filter({$0.playedBy == p2})
                self.setChartData(p1: totalP2 ?? [], p2: totalP1 ?? [])
                return
            }
        }
        
        if issetFilter{
            let game = self.speedGraph?.filter{ $0.gameNo == selectedSet }.first
            let totalP1 = game?.totalSpeeds?.filter({$0.playedBy == p1})
            let totalP2 = game?.totalSpeeds?.filter({$0.playedBy == p2})
            self.setChartData(p1: totalP2 ?? [], p2: totalP1 ?? [])
            return
        }
        
        let game = self.speedGraph?.filter{ $0.gameNo == selectedSet }.first
        let totalP1 = game?.totalSpeeds?.filter({$0.playedBy == p1})
        let totalP2 = game?.totalSpeeds?.filter({$0.playedBy == p2})
        self.setChartData(p1: totalP2 ?? [], p2: totalP1 ?? [])

    }
    
    func setChartData(p1:[Speed],p2:[Speed]) {
        let yVals1 = (0..<p1.count).map { (i) -> ChartDataEntry in
            return ChartDataEntry(x: Double(i), y: Double(p1[i].averageSpeed ?? 0))
        }
        let yVals2 = (0..<p2.count).map { (i) -> ChartDataEntry in
            return ChartDataEntry(x: Double(i), y: Double(p2[i].averageSpeed ?? 0))
        }
        let set1 = LineChartDataSet(entries: yVals1, label: self.p1)
        //set1.valueFormatter = CustomIntFormatter()
        //set1.axisDependency = .left
        set1.setColor(UIColor(red: 238/255, green: 56/255, blue: 127/255, alpha: 1))
        set1.setCircleColor(UIColor(red: 238/255, green: 56/255, blue: 127/255, alpha: 1))
        set1.lineWidth = 2
        set1.circleRadius = 3
        set1.fillAlpha = 65/255
        set1.fillColor = UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1)
        set1.highlightColor = UIColor(red: 244/255, green: 117/255, blue: 117/255, alpha: 1)
        set1.drawFilledEnabled = false
        set1.drawCircleHoleEnabled = false
        
        let set2 = LineChartDataSet(entries: yVals2, label: self.p2)
        //set2.valueFormatter = CustomIntFormatter()
        //set2.axisDependency = .right
        set2.setColor(UIColor(red: 247/255, green: 196/255, blue: 121/255, alpha: 1))
        set2.setCircleColor(UIColor(red: 247/255, green: 196/255, blue: 121/255, alpha: 1))
        set2.lineWidth = 2
        set2.circleRadius = 3
        set2.fillAlpha = 65/255
        set2.fillColor = .red
        set2.drawFilledEnabled = false
        set2.highlightColor = UIColor(red: 244/255, green: 117/255, blue: 117/255, alpha: 1)
        set2.drawCircleHoleEnabled = false
        let data: LineChartData =  LineChartData(dataSets: [set1,set2])

        data.setValueTextColor(.clear)
        lineChartView.data = data
    }
    
}

extension SpeedAnalysisContactViewModel:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return games?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = setCollectionView.dequeueReusableCell(withReuseIdentifier: "set", for: indexPath) as! SpeedAnalysisContactSetCVC
        if selectedSet == games?[indexPath.row].gameID{
            if games?[indexPath.row].isSelected == true{
                games?[indexPath.row].isSelected = false
                cell.backView.backgroundColor = UIColor.black
                issetFilter = false
                selectedSet = 0
                setupChart()
            }else{
                games?[indexPath.row].isSelected = true
                cell.backView.backgroundColor = Colors.TableBackColor
                setupChart()
            }
        }else{
            games?[indexPath.row].isSelected = false
            cell.backView.backgroundColor = UIColor.black
        }
        cell.setnoLabel.text = "Set - \(games?[indexPath.row].gameID ?? 0)"
        cell.backView.layer.borderWidth = 1
        cell.backView.layer.borderColor = UIColor.white.cgColor
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let size = CGSize(width: collectionView.frame.size.width/5, height: collectionView.frame.size.width/5 - 40)
        return size
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedSet = games?[indexPath.row].gameID ?? 0
        issetFilter = true
        self.setCollectionView.reloadData()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        self.setCollectionView.layer.removeAllAnimations()
        if games?.count == 0{
            setcollectionviewheightConstraint.constant = 0
        }else{
            setcollectionviewheightConstraint.constant = setCollectionView.contentSize.height
        }
    }
    
}
