//
//  SpeedAnalysisContactVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 16/04/21.
//

import UIKit
import Charts

class SpeedAnalysisContactVC: UIViewController {

    var games:[RealTimeMatchStatsGame]?
    var speedGraph:[SpeedGraph]?
    var speedanalysisViewModel:SpeedAnalysisContactViewModel!
    
    //MARK:- Objects
    
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var setCollectionView: UICollectionView!
    @IBOutlet weak var setcollectionviewheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var wonView: UIView!
    @IBOutlet weak var totalView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
    }
    
    func adjustingUI(){
        wonView.layer.borderWidth = 1
        wonView.layer.borderColor = UIColor.white.cgColor
        totalView.layer.borderWidth = 1
        totalView.layer.borderColor = UIColor.white.cgColor
        speedanalysisViewModel = SpeedAnalysisContactViewModel(setCollectionView: setCollectionView, setcollectionviewheightConstraint: setcollectionviewheightConstraint, games: games, lineChartView: lineChartView, speedGraph: speedGraph)
        speedanalysisViewModel.observers()
        setCollectionView.dataSource = speedanalysisViewModel
        setCollectionView.delegate = speedanalysisViewModel
        setuplineChart(linechartView: lineChartView)
        speedanalysisViewModel.setupChart()
        addGestures()
    }
    
    func addGestures(){
        wonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(wonAct)))
        totalView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(totalAct)))
    }
    
    @objc func wonAct(){
        if speedanalysisViewModel.selectedChartType == .WON{
            wonView.backgroundColor = UIColor.black
            speedanalysisViewModel.isstatusFilter = false
            speedanalysisViewModel.selectedChartType = .none
        }else{
            speedanalysisViewModel.isstatusFilter = true
            speedanalysisViewModel.selectedChartType = .WON
            wonView.backgroundColor = Colors.TableBackColor
            totalView.backgroundColor = UIColor.black
        }
        speedanalysisViewModel.setupChart()
    }
    @objc func totalAct(){
        if speedanalysisViewModel.selectedChartType == .TOTAL{
            speedanalysisViewModel.isstatusFilter = false
            totalView.backgroundColor = UIColor.black
            speedanalysisViewModel.selectedChartType = .none
        }else{
            speedanalysisViewModel.selectedChartType = .TOTAL
            speedanalysisViewModel.isstatusFilter = true
            totalView.backgroundColor = Colors.TableBackColor
            wonView.backgroundColor = UIColor.black
        }
        speedanalysisViewModel.setupChart()
    }
    
}
