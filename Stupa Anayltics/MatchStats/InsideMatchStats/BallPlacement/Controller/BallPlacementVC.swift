//
//  BallPlacementVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 16/04/21.
//

import UIKit

class BallPlacementVC: UIViewController {

    var arr = [2,2,4,3]
    var zoneheatMap:ZoneHeatMap?
    var games:[RealTimeMatchStatsGame]?
    var playerA:RealTimeMatchStatsPlayer?
    var playerB:RealTimeMatchStatsPlayer?
    var ballplacementViewModel:BallPlacementViewModel!
    
    //MARK:- Objects
    
    @IBOutlet weak var playerAnameLabel: UILabel!
    @IBOutlet weak var playerBnameLabel: UILabel!
    
    @IBOutlet var r1c1BtnA : UIButton!
    @IBOutlet var r1c1Lbl : UILabel!
    @IBOutlet var r1c1BtnB : UIButton!
    @IBOutlet var r1c2Btn : UIButton!
    @IBOutlet var r1c3BtnA : UIButton!
    @IBOutlet var r1c3Lbl : UILabel!
    @IBOutlet var r1c3BtnB : UIButton!

    @IBOutlet var r2c1BtnA : UIButton!
    @IBOutlet var r2c1BtnB : UIButton!
    @IBOutlet var r2c2Btn : UIButton!
    @IBOutlet var r2c3BtnA : UIButton!
    @IBOutlet var r2c3BtnB : UIButton!
    
    @IBOutlet var r3c1BtnA : UIButton!
    @IBOutlet var r3c1BtnB : UIButton!
    @IBOutlet var r3c1Lbl : UILabel!
    @IBOutlet var r3c2Btn : UIButton!
    @IBOutlet var r3c3BtnA : UIButton!
    @IBOutlet var r3c3BtnB : UIButton!
    @IBOutlet var r3c3Lbl : UILabel!

    @IBOutlet var r4c1BtnA : UIButton!
    @IBOutlet var r4c1BtnB : UIButton!
    @IBOutlet var r4c1Lbl : UILabel!
    @IBOutlet var r4c2Btn : UIButton!
    @IBOutlet var r4c3BtnA : UIButton!
    @IBOutlet var r4c3BtnB : UIButton!
    @IBOutlet var r4c3Lbl : UILabel!

    @IBOutlet var r5c1BtnA : UIButton!
    @IBOutlet var r5c1BtnB : UIButton!
    @IBOutlet var r5c2Btn : UIButton!
    @IBOutlet var r5c3BtnA : UIButton!
    @IBOutlet var r5c3BtnB : UIButton!
    
    @IBOutlet var r6c1BtnA : UIButton!
    @IBOutlet var r6c1BtnB : UIButton!
    @IBOutlet var r6c1Lbl : UILabel!
    @IBOutlet var r6c2Btn : UIButton!
    @IBOutlet var r6c3BtnA : UIButton!
    @IBOutlet var r6c3BtnB : UIButton!
    @IBOutlet var r6c3Lbl : UILabel!
    
    @IBOutlet weak var setCollectionView: UICollectionView!
    @IBOutlet weak var setcollectionviewheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var serviceView: UIView!
    @IBOutlet weak var receiveView: UIView!
    @IBOutlet weak var thirdballView: UIView!
    @IBOutlet weak var fourthballView: UIView!
    @IBOutlet weak var othersView: UIView!
    @IBOutlet weak var iserrorSwitch: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
    }
    
    func adjustingUI(){
        serviceView.layer.borderWidth = 1
        serviceView.layer.borderColor = UIColor.white.cgColor
        receiveView.layer.borderWidth = 1
        receiveView.layer.borderColor = UIColor.white.cgColor
        thirdballView.layer.borderWidth = 1
        thirdballView.layer.borderColor = UIColor.white.cgColor
        fourthballView.layer.borderWidth = 1
        fourthballView.layer.borderColor = UIColor.white.cgColor
        othersView.layer.borderWidth = 1
        othersView.layer.borderColor = UIColor.white.cgColor
        iserrorSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        
        playerAnameLabel.text = playerA?.name
        playerBnameLabel.text = playerB?.name
        
        ballplacementViewModel = BallPlacementViewModel(zoneheatmap: self.zoneheatMap, playerA: playerA, playerB: playerB, setCollectionView: setCollectionView, setcollectionviewheightConstraint: setcollectionviewheightConstraint, games: self.games, r1c1BtnA: r1c1BtnA, r1c1Lbl: r1c1Lbl, r1c1BtnB: r1c1BtnB, r1c2Btn: r1c2Btn, r1c3BtnA: r1c3BtnA, r1c3Lbl: r1c3Lbl, r1c3BtnB: r1c3BtnB, r2c1BtnA: r2c1BtnA, r2c1BtnB: r2c1BtnB, r2c2Btn: r2c2Btn, r2c3BtnA: r2c3BtnA, r2c3BtnB: r2c3BtnB, r3c1BtnA: r3c1BtnA, r3c1BtnB: r3c1BtnB, r3c1Lbl: r3c1Lbl, r3c2Btn: r3c2Btn, r3c3BtnA: r3c3BtnA, r3c3BtnB: r3c3BtnB, r3c3Lbl: r3c3Lbl, r4c1BtnA: r4c1BtnA, r4c1BtnB: r4c1BtnB, r4c1Lbl: r4c1Lbl, r4c2Btn: r4c2Btn, r4c3BtnA: r4c3BtnA, r4c3BtnB: r4c3BtnB, r4c3Lbl: r4c3Lbl, r5c1BtnA: r5c1BtnA, r5c1BtnB: r5c1BtnB, r5c2Btn: r5c2Btn, r5c3BtnA: r5c3BtnA, r5c3BtnB: r5c3BtnB, r6c1BtnA: r6c1BtnA, r6c1BtnB: r6c1BtnB, r6c1Lbl: r6c1Lbl, r6c2Btn: r6c2Btn, r6c3BtnA: r6c3BtnA, r6c3BtnB: r6c3BtnB, r6c3Lbl: r6c3Lbl,p1Name: playerAnameLabel,p2Name: playerBnameLabel)
        
        ballplacementViewModel.playerAType = playerA?.playerType ?? ""
        ballplacementViewModel.playerBType = playerB?.playerType ?? ""
        ballplacementViewModel.setValuesAndColor()
        ballplacementViewModel.observers()
        setCollectionView.dataSource = ballplacementViewModel
        setCollectionView.delegate = ballplacementViewModel
        addGestures()
    }
    
    func addGestures(){
        serviceView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(serviceAct)))
        receiveView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(recieveAct)))
        thirdballView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(thirdballAct)))
        fourthballView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(fourthballAct)))
        othersView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(othersAct)))
    }
    
    @objc func serviceAct(){
        if ballplacementViewModel.selectedGame == 1{
            serviceView.backgroundColor = UIColor.black
            ballplacementViewModel.pointfilter = false
            ballplacementViewModel.selectedGame = 0
        }else{
            ballplacementViewModel.selectedGame = 1
            ballplacementViewModel.pointfilter = true
            serviceView.backgroundColor = Colors.TableBackColor
            receiveView.backgroundColor = UIColor.black
            thirdballView.backgroundColor = UIColor.black
            fourthballView.backgroundColor = UIColor.black
            othersView.backgroundColor = UIColor.black
        }
        ballplacementViewModel.setValuesAndColor()
    }
    @objc func recieveAct(){
        if ballplacementViewModel.selectedGame == 2{
            ballplacementViewModel.selectedGame = 0
            ballplacementViewModel.pointfilter = false
            receiveView.backgroundColor = UIColor.black
        }else{
            ballplacementViewModel.selectedGame = 2
            ballplacementViewModel.pointfilter = true
            receiveView.backgroundColor = Colors.TableBackColor
            serviceView.backgroundColor = UIColor.black
            thirdballView.backgroundColor = UIColor.black
            fourthballView.backgroundColor = UIColor.black
            othersView.backgroundColor = UIColor.black
        }
        ballplacementViewModel.setValuesAndColor()
    }
    @objc func thirdballAct(){
        if ballplacementViewModel.selectedGame == 3{
            ballplacementViewModel.selectedGame = 0
            ballplacementViewModel.pointfilter = false
            thirdballView.backgroundColor = UIColor.black
        }else{
            ballplacementViewModel.selectedGame = 3
            ballplacementViewModel.pointfilter = true
            thirdballView.backgroundColor = Colors.TableBackColor
            receiveView.backgroundColor = UIColor.black
            serviceView.backgroundColor = UIColor.black
            fourthballView.backgroundColor = UIColor.black
            othersView.backgroundColor = UIColor.black
        }
        ballplacementViewModel.setValuesAndColor()
    }
    @objc func fourthballAct(){
        if ballplacementViewModel.selectedGame == 4{
            ballplacementViewModel.selectedGame = 0
            ballplacementViewModel.pointfilter = false
            fourthballView.backgroundColor = UIColor.black
        }else{
            ballplacementViewModel.selectedGame = 4
            ballplacementViewModel.pointfilter = true
            fourthballView.backgroundColor = Colors.TableBackColor
            receiveView.backgroundColor = UIColor.black
            serviceView.backgroundColor = UIColor.black
            thirdballView.backgroundColor = UIColor.black
            othersView.backgroundColor = UIColor.black
        }
        ballplacementViewModel.setValuesAndColor()
    }
    @objc func othersAct(){
        if ballplacementViewModel.selectedGame == 7{
            ballplacementViewModel.selectedGame = 0
            ballplacementViewModel.pointfilter = false
            othersView.backgroundColor = UIColor.black
        }else{
            ballplacementViewModel.selectedGame = 7
            ballplacementViewModel.pointfilter = true
            othersView.backgroundColor = Colors.TableBackColor
            receiveView.backgroundColor = UIColor.black
            serviceView.backgroundColor = UIColor.black
            thirdballView.backgroundColor = UIColor.black
            fourthballView.backgroundColor = UIColor.black
        }
        ballplacementViewModel.setValuesAndColor()
    }
    
    
    @IBAction func iserrorswitchAct(_ sender: Any) {
        ballplacementViewModel.isErrorOrWinningSelected = iserrorSwitch.isOn
        ballplacementViewModel.setValuesAndColor()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
