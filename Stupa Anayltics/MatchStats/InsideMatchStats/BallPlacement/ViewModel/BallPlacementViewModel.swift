//
//  BallPlacementViewModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 17/04/21.
//

import Foundation

class BallPlacementViewModel:NSObject{
    
    private var zoneHeadMap:ZoneHeatMap?
    private var playerA:RealTimeMatchStatsPlayer?
    private var playerB:RealTimeMatchStatsPlayer?
    private var games:[RealTimeMatchStatsGame]?
    private var pointView = [UIView]()
    private var setCollectionView: UICollectionView!
    private var setcollectionviewheightConstraint: NSLayoutConstraint!
    
    private var r1c1BtnA : UIButton!
    private var r1c1Lbl : UILabel!
    private var r1c1BtnB : UIButton!
    private var r1c2Btn : UIButton!
    private var r1c3BtnA : UIButton!
    private var r1c3Lbl : UILabel!
    private var r1c3BtnB : UIButton!

    private var r2c1BtnA : UIButton!
    private var r2c1BtnB : UIButton!
    private var r2c2Btn : UIButton!
    private var r2c3BtnA : UIButton!
    private var r2c3BtnB : UIButton!
    
    private var r3c1BtnA : UIButton!
    private var r3c1BtnB : UIButton!
    private var r3c1Lbl : UILabel!
    private var r3c2Btn : UIButton!
    private var r3c3BtnA : UIButton!
    private var r3c3BtnB : UIButton!
    private var r3c3Lbl : UILabel!

    private var r4c1BtnA : UIButton!
    private var r4c1BtnB : UIButton!
    private var r4c1Lbl : UILabel!
    private var r4c2Btn : UIButton!
    private var r4c3BtnA : UIButton!
    private var r4c3BtnB : UIButton!
    private var r4c3Lbl : UILabel!

    private var r5c1BtnA : UIButton!
    private var r5c1BtnB : UIButton!
    private var r5c2Btn : UIButton!
    private var r5c3BtnA : UIButton!
    private var r5c3BtnB : UIButton!
    
    private var r6c1BtnA : UIButton!
    private var r6c1BtnB : UIButton!
    private var r6c1Lbl : UILabel!
    private var r6c2Btn : UIButton!
    private var r6c3BtnA : UIButton!
    private var r6c3BtnB : UIButton!
    private var r6c3Lbl : UILabel!
    
    private var p1Name : UILabel!
    private var p2Name : UILabel!
    
    var isErrorOrWinningSelected = false
    var selectedGame = 0
    var pointfilter = false
    var issetFilter = false
    var selectedSet = 0
    var maxValuePlayerA = 0
    var maxValuePlayerB = 0
    var playerAType = ""
    var playerBType = ""
    
    init(zoneheatmap:ZoneHeatMap?,playerA:RealTimeMatchStatsPlayer?,playerB:RealTimeMatchStatsPlayer?,setCollectionView:UICollectionView,setcollectionviewheightConstraint:NSLayoutConstraint,games:[RealTimeMatchStatsGame]?,r1c1BtnA : UIButton,r1c1Lbl : UILabel,r1c1BtnB : UIButton,r1c2Btn : UIButton,r1c3BtnA : UIButton,r1c3Lbl : UILabel,r1c3BtnB : UIButton,r2c1BtnA : UIButton,r2c1BtnB : UIButton,r2c2Btn : UIButton,r2c3BtnA : UIButton, r2c3BtnB : UIButton,r3c1BtnA : UIButton, r3c1BtnB : UIButton, r3c1Lbl : UILabel, r3c2Btn : UIButton,r3c3BtnA : UIButton, r3c3BtnB : UIButton, r3c3Lbl : UILabel,r4c1BtnA : UIButton,   r4c1BtnB : UIButton,r4c1Lbl : UILabel,r4c2Btn : UIButton,r4c3BtnA : UIButton,r4c3BtnB : UIButton,r4c3Lbl : UILabel,r5c1BtnA : UIButton,r5c1BtnB : UIButton,r5c2Btn : UIButton,r5c3BtnA : UIButton,r5c3BtnB : UIButton,r6c1BtnA : UIButton,r6c1BtnB : UIButton,r6c1Lbl : UILabel,r6c2Btn : UIButton,r6c3BtnA : UIButton,r6c3BtnB : UIButton,r6c3Lbl : UILabel,p1Name : UILabel,p2Name : UILabel){
        
        self.zoneHeadMap = zoneheatmap
        self.playerA = playerA
        self.playerB = playerB
        self.games = games
        self.setCollectionView = setCollectionView
        self.setcollectionviewheightConstraint = setcollectionviewheightConstraint
        
        self.r1c1BtnA = r1c1BtnA
        self.r1c1Lbl = r1c1Lbl
        self.r1c1BtnB = r1c1BtnB
        self.r1c2Btn = r1c2Btn
        self.r1c3BtnA = r1c3BtnA
        self.r1c3Lbl = r1c3Lbl
        self.r1c3BtnB = r1c3BtnB

        self.r2c1BtnA = r2c1BtnA
        self.r2c1BtnB = r2c1BtnB
        self.r2c2Btn = r2c2Btn
        self.r2c3BtnA = r2c3BtnA
        self.r2c3BtnB = r2c3BtnB
        
        self.r3c1BtnA = r3c1BtnA
        self.r3c1BtnB = r3c1BtnB
        self.r3c1Lbl = r3c1Lbl
        self.r3c2Btn = r3c2Btn
        self.r3c3BtnA = r3c3BtnA
        self.r3c3BtnB = r3c3BtnB
        self.r3c3Lbl = r3c3Lbl

        self.r4c1BtnA = r4c1BtnA
        self.r4c1BtnB = r4c1BtnB
        self.r4c1Lbl = r4c1Lbl
        self.r4c2Btn = r4c2Btn
        self.r4c3BtnA = r4c3BtnA
        self.r4c3BtnB = r4c3BtnB
        self.r4c3Lbl = r4c3Lbl

        self.r5c1BtnA = r5c1BtnA
        self.r5c1BtnB = r5c1BtnB
        self.r5c2Btn = r5c2Btn
        self.r5c3BtnA = r5c3BtnA
        self.r5c3BtnB = r5c3BtnB
        
        self.r6c1BtnA = r6c1BtnA
        self.r6c1BtnB = r6c1BtnB
        self.r6c1Lbl = r6c1Lbl
        self.r6c2Btn = r6c2Btn
        self.r6c3BtnA = r6c3BtnA
        self.r6c3BtnB = r6c3BtnB
        self.r6c3Lbl = r6c3Lbl
        
        self.p1Name = p1Name
        self.p2Name = p2Name
        
    }
    
    func setValuesAndColor() {
        self.maxValuePlayerA = 0
        self.maxValuePlayerB = 0

        //Player A
        //Set up EBHL
        if self.playerAType == "Lefty" {
            let getColor1 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EBHL", isP1: true)
            self.r1c1BtnA.setTitle("\(getColor1.0)\nEBHL", for: .normal)
            self.r1c1BtnA.backgroundColor = (getColor1.1)
            //Set up EBHH
            let getColor2 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EBHH", isP1: true)
            self.r2c1BtnA.setTitle("\(getColor2.0)\nEBHH", for: .normal)
            self.r2c1BtnA.backgroundColor = (getColor2.1)
            //Set up EBHS
            let getColor3 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EBHS", isP1: true)
            self.r3c1BtnA.setTitle("\(getColor3.0)\nEBHS", for: .normal)
            self.r3c1BtnA.backgroundColor = (getColor3.1)
            //Set up BHL
            let getColor4 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "BHL", isP1: true)
            self.r1c1Lbl.text = "\(getColor4.0)\nBHL"
            self.r1c1BtnB.setTitle("\(getColor4.0)\nBHL", for: .normal)
            self.r1c1BtnB.setTitleColor((getColor4.1), for: .normal)
            self.r1c1BtnB.backgroundColor = (getColor4.1)
            //Set up BHH
            let getColor5 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "BHH", isP1: true)
            self.r2c1BtnB.setTitle("\(getColor5.0)\nBHH", for: .normal)
            self.r2c1BtnB.backgroundColor = (getColor5.1)
            //Set up BHS
            let getColor6 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "BHS", isP1: true)
            self.r3c1Lbl.text = "\(getColor6.0)\nBHS"
            self.r3c1BtnB.setTitle("\(getColor6.0)\nBHS", for: .normal)
            self.r3c1BtnB.setTitleColor((getColor6.1), for: .normal)
            self.r3c1BtnB.backgroundColor = (getColor6.1)
            //Set up CL
            let getColor7 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "CL", isP1: true)
            self.r1c2Btn.setTitle("\(getColor7.0)\nCL", for: .normal)
            self.r1c2Btn.backgroundColor = (getColor7.1)
            //Set up CH
            let getColor8 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "CH", isP1: true)
            self.r2c2Btn.setTitle("\(getColor8.0)\nCH", for: .normal)
            self.r2c2Btn.backgroundColor = (getColor8.1)
            //Set up CS
            let getColor9 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "CS", isP1: true)
            self.r3c2Btn.setTitle("\(getColor9.0)\nCS", for: .normal)
            self.r3c2Btn.backgroundColor = (getColor9.1)
            //Set up FHL
            let getColorA = self.getColorAndPlacementDataForZoneHeadtMap(placement: "FHL", isP1: true)
            self.r1c3Lbl.text = "\(getColorA.0)\nFHL"
            self.r1c3BtnA.setTitle("\(getColorA.0)\nFHL", for: .normal)
            self.r1c3BtnA.setTitleColor((getColorA.1), for: .normal)
            self.r1c3BtnA.backgroundColor = (getColorA.1)
            //Set up FHH
            let getColorB = self.getColorAndPlacementDataForZoneHeadtMap(placement: "FHH", isP1: true)
            self.r2c3BtnA.setTitle("\(getColorB.0)\nFHH", for: .normal)
            self.r2c3BtnA.backgroundColor = (getColorB.1)
            //Set up FHS
            let getColorC = self.getColorAndPlacementDataForZoneHeadtMap(placement: "FHS", isP1: true)
            self.r3c3Lbl.text = "\(getColorC.0)\nFHS"
            self.r3c3BtnA.setTitle("\(getColorC.0)\nFHS", for: .normal)
            self.r3c3BtnA.setTitleColor((getColorC.1), for: .normal)
            self.r3c3BtnA.backgroundColor = (getColorC.1)
            //Set up EFHL
            let getColorD = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EFHL", isP1: true)
            self.r1c3BtnB.setTitle("\(getColorD.0)\nEFHL", for: .normal)
            self.r1c3BtnB.backgroundColor = (getColorD.1)
            //Set up EFHH
            let getColorE = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EFHH", isP1: true)
            self.r2c3BtnB.setTitle("\(getColorE.0)\nEFHH", for: .normal)
            self.r2c3BtnB.backgroundColor = (getColorE.1)
            //Set up EFHS
            let getColorF = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EFHS", isP1: true)
            self.r3c3BtnB.setTitle("\(getColorF.0)\nEFHS", for: .normal)
            self.r3c3BtnB.backgroundColor = (getColorF.1)
        } else {
            let getColor1 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EFHL", isP1: true)
            self.r1c1BtnA.setTitle("\(getColor1.0)\nEFHL", for: .normal)
            self.r1c1BtnA.backgroundColor = (getColor1.1)
            //Set up EFHH
            let getColor2 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EFHH", isP1: true)
            self.r2c1BtnA.setTitle("\(getColor2.0)\nEFHH", for: .normal)
            self.r2c1BtnA.backgroundColor = (getColor2.1)
            //Set up EFHS
            let getColor3 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EFHS", isP1: true)
            self.r3c1BtnA.setTitle("\(getColor3.0)\nEFHS", for: .normal)
            self.r3c1BtnA.backgroundColor = (getColor3.1)
            //Set up FHL
            let getColor4 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "FHL", isP1: true)
            self.r1c1Lbl.text = "\(getColor4.0)\nFHL"
            self.r1c1BtnB.setTitle("\(getColor4.0)\nFHL", for: .normal)
            self.r1c1BtnB.setTitleColor((getColor4.1), for: .normal)
            self.r1c1BtnB.backgroundColor = (getColor4.1)
            //Set up FHH
            let getColor5 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "FHH", isP1: true)
            self.r2c1BtnB.setTitle("\(getColor5.0)\nFHH", for: .normal)
            self.r2c1BtnB.backgroundColor = (getColor5.1)
            //Set up FHS
            let getColor6 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "FHS", isP1: true)
            self.r3c1Lbl.text = "\(getColor6.0)\nFHS"
            self.r3c1BtnB.setTitle("\(getColor6.0)\nFHS", for: .normal)
            self.r3c1BtnB.setTitleColor((getColor6.1), for: .normal)
            self.r3c1BtnB.backgroundColor = (getColor6.1)
            //Set up CL
            let getColor7 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "CL", isP1: true)
            self.r1c2Btn.setTitle("\(getColor7.0)\nCL", for: .normal)
            self.r1c2Btn.backgroundColor = (getColor7.1)
            //Set up CH
            let getColor8 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "CH", isP1: true)
            self.r2c2Btn.setTitle("\(getColor8.0)\nCH", for: .normal)
            self.r2c2Btn.backgroundColor = (getColor8.1)
            //Set up CS
            let getColor9 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "CS", isP1: true)
            self.r3c2Btn.setTitle("\(getColor9.0)\nCS", for: .normal)
            self.r3c2Btn.backgroundColor = (getColor9.1)
            //Set up BHL
            let getColorA = self.getColorAndPlacementDataForZoneHeadtMap(placement: "BHL", isP1: true)
            self.r1c3Lbl.text = "\(getColorA.0)\nBHL"
            self.r1c3BtnA.setTitle("\(getColorA.0)\nBHL", for: .normal)
            self.r1c3BtnA.setTitleColor((getColorA.1), for: .normal)
            self.r1c3BtnA.backgroundColor = (getColorA.1)
            //Set up BHH
            let getColorB = self.getColorAndPlacementDataForZoneHeadtMap(placement: "BHH", isP1: true)
            self.r2c3BtnA.setTitle("\(getColorB.0)\nBHH", for: .normal)
            self.r2c3BtnA.backgroundColor = (getColorB.1)
            //Set up BHS
            let getColorC = self.getColorAndPlacementDataForZoneHeadtMap(placement: "BHS", isP1: true)
            self.r3c3Lbl.text = "\(getColorC.0)\nBHS"
            self.r3c3BtnA.setTitle("\(getColorC.0)\nBHS", for: .normal)
            self.r3c3BtnA.setTitleColor((getColorC.1), for: .normal)
            self.r3c3BtnA.backgroundColor = (getColorC.1)
            //Set up EBHL
            let getColorD = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EBHL", isP1: true)
            self.r1c3BtnB.setTitle("\(getColorD.0)\nEBHL", for: .normal)
            self.r1c3BtnB.backgroundColor = (getColorD.1)
            //Set up EBHH
            let getColorE = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EBHH", isP1: true)
            self.r2c3BtnB.setTitle("\(getColorE.0)\nEBHH", for: .normal)
            self.r2c3BtnB.backgroundColor = (getColorE.1)
            //Set up EBHS
            let getColorF = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EBHS", isP1: true)
            self.r3c3BtnB.setTitle("\(getColorF.0)\nEBHS", for: .normal)
            self.r3c3BtnB.backgroundColor = (getColorF.1)
        }
        if self.playerBType == "Lefty" {
            //Player B
            //Set up EBHL
            let getColor1 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EBHL", isP1: false)
            self.r6c3BtnB.setTitle("\(getColor1.0)\nEBHL", for: .normal)
            self.r6c3BtnB.backgroundColor = (getColor1.1)
            //Set up EBHH
            let getColor2 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EBHH", isP1: false)
            self.r5c3BtnB.setTitle("\(getColor2.0)\nEBHH", for: .normal)
            self.r5c3BtnB.backgroundColor = (getColor2.1)
            //Set up EBHS
            let getColor3 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EBHS", isP1: false)
            self.r4c3BtnB.setTitle("\(getColor3.0)\nEBHS", for: .normal)
            self.r4c3BtnB.backgroundColor = (getColor3.1)
            //Set up BHL
            let getColor4 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "BHL", isP1: false)
            self.r6c3Lbl.text = "\(getColor4.0)\nBHL"
            self.r6c3BtnA.setTitle("\(getColor4.0)\nBHL", for: .normal)
            self.r6c3BtnA.setTitleColor((getColor4.1), for: .normal)
            self.r6c3BtnA.backgroundColor = (getColor4.1)
            //Set up BHH
            let getColor5 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "BHH", isP1: false)
            self.r5c3BtnA.setTitle("\(getColor5.0)\nBHH", for: .normal)
            self.r5c3BtnA.backgroundColor = (getColor5.1)
            //Set up BHS
            let getColor6 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "BHS", isP1: false)
            self.r4c3Lbl.text = "\(getColor6.0)\nBHS"
            self.r4c3BtnA.setTitle("\(getColor6.0)\nBHS", for: .normal)
            self.r4c3BtnA.setTitleColor((getColor6.1), for: .normal)
            self.r4c3BtnA.backgroundColor = (getColor6.1)
            //Set up CL
            let getColor7 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "CL", isP1: false)
            self.r6c2Btn.setTitle("\(getColor7.0)\nCL", for: .normal)
            self.r6c2Btn.backgroundColor = (getColor7.1)
            //Set up CH
            let getColor8 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "CH", isP1: false)
            self.r5c2Btn.setTitle("\(getColor8.0)\nCH", for: .normal)
            self.r5c2Btn.backgroundColor = (getColor8.1)
            //Set up CS
            let getColor9 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "CS", isP1: false)
            self.r4c2Btn.setTitle("\(getColor9.0)\nCS", for: .normal)
            self.r4c2Btn.backgroundColor = (getColor9.1)
            //Set up FHL
            let getColorA = self.getColorAndPlacementDataForZoneHeadtMap(placement: "FHL", isP1: false)
            self.r6c1Lbl.text = "\(getColorA.0)\nFHL"
            self.r6c1BtnB.setTitle("\(getColorA.0)\nFHL", for: .normal)
            self.r6c1BtnB.setTitleColor((getColorA.1), for: .normal)
            self.r6c1BtnB.backgroundColor = (getColorA.1)
            //Set up FHH
            let getColorB = self.getColorAndPlacementDataForZoneHeadtMap(placement: "FHH", isP1: false)
            self.r5c1BtnB.setTitle("\(getColorB.0)\nFHH", for: .normal)
            self.r5c1BtnB.backgroundColor = (getColorB.1)
            //Set up FHS
            let getColorC = self.getColorAndPlacementDataForZoneHeadtMap(placement: "FHS", isP1: false)
            self.r4c1Lbl.text = "\(getColorC.0)\nFHS"
            self.r4c1BtnB.setTitle("\(getColorC.0)\nFHS", for: .normal)
            self.r4c1BtnB.setTitleColor((getColorC.1), for: .normal)
            self.r4c1BtnB.backgroundColor = (getColorC.1)
            //Set up EFHL
            let getColorD = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EFHL", isP1: false)
            self.r6c1BtnA.setTitle("\(getColorD.0)\nEFHL", for: .normal)
            self.r6c1BtnA.backgroundColor = (getColorD.1)
            //Set up EFHH
            let getColorE = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EFHH", isP1: false)
            self.r5c1BtnA.setTitle("\(getColorE.0)\nEFHH", for: .normal)
            self.r5c1BtnA.backgroundColor = (getColorE.1)
            //Set up EFHS
            let getColorF = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EFHS", isP1: false)
            self.r4c1BtnA.setTitle("\(getColorF.0)\nEFHS", for: .normal)
            self.r4c1BtnA.backgroundColor = (getColorF.1)
        } else {
            //Player B
            //Set up EFHL
            let getColor1 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EFHL", isP1: false)
            self.r6c3BtnB.setTitle("\(getColor1.0)\nEFHL", for: .normal)
            self.r6c3BtnB.backgroundColor = (getColor1.1)
            //Set up EFHH
            let getColor2 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EFHH", isP1: false)
            self.r5c3BtnB.setTitle("\(getColor2.0)\nEFHH", for: .normal)
            self.r5c3BtnB.backgroundColor = (getColor2.1)
            //Set up EFHS
            let getColor3 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EFHS", isP1: false)
            self.r4c3BtnB.setTitle("\(getColor3.0)\nEFHS", for: .normal)
            self.r4c3BtnB.backgroundColor = (getColor3.1)
            //Set up FHL
            let getColor4 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "FHL", isP1: false)
            self.r6c3Lbl.text = "\(getColor4.0)\nFHL"
            self.r6c3BtnA.setTitle("\(getColor4.0)\nFHL", for: .normal)
            self.r6c3BtnA.setTitleColor((getColor4.1), for: .normal)
            self.r6c3BtnA.backgroundColor = (getColor4.1)
            //Set up FHH
            let getColor5 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "FHH", isP1: false)
            self.r5c3BtnA.setTitle("\(getColor5.0)\nFHH", for: .normal)
            self.r5c3BtnA.backgroundColor = (getColor5.1)
            //Set up FHS
            let getColor6 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "FHS", isP1: false)
            self.r4c3Lbl.text = "\(getColor6.0)\nFHS"
            self.r4c3BtnA.setTitle("\(getColor6.0)\nFHS", for: .normal)
            self.r4c3BtnA.setTitleColor((getColor6.1), for: .normal)
            self.r4c3BtnA.backgroundColor = (getColor6.1)
            //Set up CL
            let getColor7 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "CL", isP1: false)
            self.r6c2Btn.setTitle("\(getColor7.0)\nCL", for: .normal)
            self.r6c2Btn.backgroundColor = (getColor7.1)
            //Set up CH
            let getColor8 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "CH", isP1: false)
            self.r5c2Btn.setTitle("\(getColor8.0)\nCH", for: .normal)
            self.r5c2Btn.backgroundColor = (getColor8.1)
            //Set up CS
            let getColor9 = self.getColorAndPlacementDataForZoneHeadtMap(placement: "CS", isP1: false)
            self.r4c2Btn.setTitle("\(getColor9.0)\nCS", for: .normal)
            self.r4c2Btn.backgroundColor = (getColor9.1)
            //Set up BHL
            let getColorA = self.getColorAndPlacementDataForZoneHeadtMap(placement: "BHL", isP1: false)
            self.r6c1Lbl.text = "\(getColorA.0)\nBHL"
            self.r6c1BtnB.setTitle("\(getColorA.0)\nBHL", for: .normal)
            self.r6c1BtnB.setTitleColor((getColorA.1), for: .normal)
            self.r6c1BtnB.backgroundColor = (getColorA.1)
            //Set up BHH
            let getColorB = self.getColorAndPlacementDataForZoneHeadtMap(placement: "BHH", isP1: false)
            self.r5c1BtnB.setTitle("\(getColorB.0)\nBHH", for: .normal)
            self.r5c1BtnB.backgroundColor = (getColorB.1)
            //Set up BHS
            let getColorC = self.getColorAndPlacementDataForZoneHeadtMap(placement: "BHS", isP1: false)
            self.r4c1Lbl.text = "\(getColorC.0)\nBHS"
            self.r4c1BtnB.setTitle("\(getColorC.0)\nBHS", for: .normal)
            self.r4c1BtnB.setTitleColor((getColorC.1), for: .normal)
            self.r4c1BtnB.backgroundColor = (getColorC.1)
            //Set up EBHL
            let getColorD = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EBHL", isP1: false)
            self.r6c1BtnA.setTitle("\(getColorD.0)\nEBHL", for: .normal)
            self.r6c1BtnA.backgroundColor = (getColorD.1)
            //Set up EBHH
            let getColorE = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EBHH", isP1: false)
            self.r5c1BtnA.setTitle("\(getColorE.0)\nEBHH", for: .normal)
            self.r5c1BtnA.backgroundColor = (getColorE.1)
            //Set up EBHS
            let getColorF = self.getColorAndPlacementDataForZoneHeadtMap(placement: "EBHS", isP1: false)
            self.r4c1BtnA.setTitle("\(getColorF.0)\nEBHS", for: .normal)
            self.r4c1BtnA.backgroundColor = (getColorF.1)
        }
        self.changeColor()
    }
    
    func getColorAndPlacementDataForZoneHeadtMap(placement:String,isP1:Bool) -> (Int,UIColor) {
        if isErrorOrWinningSelected {
            
            if isP1 {
                
                if issetFilter && pointfilter{
                    if selectedGame == 7{
                        let val = self.zoneHeadMap?.winningZoneHeatMaps?.filter{ $0.playedBy == self.p1Name.text && $0.shotNo ?? 0 > 4 && $0.gameNo == self.selectedSet && $0.placement == placement}
                        let count = val?.count
                        if self.maxValuePlayerA < count ?? 0 {
                            self.maxValuePlayerA = count ?? 0
                        }
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }else{
                        let val = self.zoneHeadMap?.winningZoneHeatMaps?.filter{ $0.playedBy == self.p1Name.text && $0.shotNo == self.selectedGame && $0.gameNo == self.selectedSet && $0.placement == placement}
                        let count = val?.count
                        if self.maxValuePlayerA < count ?? 0 {
                            self.maxValuePlayerA = count ?? 0
                        }
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }
                }
                
                if pointfilter{
                    if selectedGame == 7{
                        let val = self.zoneHeadMap?.winningZoneHeatMaps?.filter{ $0.playedBy == self.p1Name.text && $0.shotNo ?? 0 > 4 && $0.placement == placement}
                        let count = val?.count
                        if self.maxValuePlayerA < count ?? 0 {
                            self.maxValuePlayerA = count ?? 0
                        }
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }else{
                        let val = self.zoneHeadMap?.winningZoneHeatMaps?.filter{ $0.playedBy == self.p1Name.text && $0.shotNo == self.selectedGame && $0.placement == placement }
                        let count = val?.count
                        if self.maxValuePlayerA < count ?? 0 {
                            self.maxValuePlayerA = count ?? 0
                        }
                        
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }
                }
                
                if issetFilter{
                    let val = self.zoneHeadMap?.winningZoneHeatMaps?.filter{ $0.playedBy == self.p1Name.text && $0.gameNo == self.selectedSet && $0.placement == placement }
                    let count = val?.count
                    if self.maxValuePlayerA < count ?? 0 {
                        self.maxValuePlayerA = count ?? 0
                    }
                    
                    
                    return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                }
                
                let val = self.zoneHeadMap?.winningZoneHeatMaps?.filter{ $0.playedBy == self.p1Name.text && $0.placement == placement }
                let count = val?.count
                if self.maxValuePlayerA < count ?? 0 {
                    self.maxValuePlayerA = count ?? 0
                }
                
                
                return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
            }else{
                
                if issetFilter && pointfilter{
                    if selectedGame == 7{
                        let val = self.zoneHeadMap?.winningZoneHeatMaps?.filter{ $0.playedBy == self.p2Name.text && $0.shotNo ?? 0 > 4 && $0.gameNo == self.selectedSet && $0.placement == placement}
                        let count = val?.count
                        if self.maxValuePlayerB < count ?? 0 {
                            self.maxValuePlayerB = count ?? 0
                        }
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }else{
                        let val = self.zoneHeadMap?.winningZoneHeatMaps?.filter{ $0.playedBy == self.p2Name.text && $0.shotNo == self.selectedGame && $0.gameNo == self.selectedSet && $0.placement == placement}
                        let count = val?.count
                        if self.maxValuePlayerB < count ?? 0 {
                            self.maxValuePlayerB = count ?? 0
                        }
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }
                }
                
                if pointfilter{
                    if selectedGame == 7{
                        let val = self.zoneHeadMap?.winningZoneHeatMaps?.filter{ $0.playedBy == self.p2Name.text && $0.shotNo ?? 0 > 4 && $0.placement == placement}
                        let count = val?.count
                        if self.maxValuePlayerB < count ?? 0 {
                            self.maxValuePlayerB = count ?? 0
                        }
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }else{
                        let val = self.zoneHeadMap?.winningZoneHeatMaps?.filter{ $0.playedBy == self.p2Name.text && $0.shotNo == self.selectedGame && $0.placement == placement }
                        let count = val?.count
                        if self.maxValuePlayerB < count ?? 0 {
                            self.maxValuePlayerB = count ?? 0
                        }
                        
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }
                }
                
                if issetFilter{
                    let val = self.zoneHeadMap?.winningZoneHeatMaps?.filter{ $0.playedBy == self.p2Name.text && $0.gameNo == self.selectedSet && $0.placement == placement }
                    let count = val?.count
                    if self.maxValuePlayerB < count ?? 0 {
                        self.maxValuePlayerB = count ?? 0
                    }
                    
                    
                    return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                }
                
                let val = self.zoneHeadMap?.winningZoneHeatMaps?.filter{ $0.playedBy == self.p2Name.text && $0.placement == placement }
                let count = val?.count
                if self.maxValuePlayerB < count ?? 0 {
                    self.maxValuePlayerB = count ?? 0
                }
                
                return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                
            }
            
        }else{
            if isP1 {
                
                if issetFilter && pointfilter{
                    if selectedGame == 7{
                        let val = self.zoneHeadMap?.totalZoneHeatMaps?.filter{ $0.playedBy == self.p1Name.text && $0.shotNo ?? 0 > 4 && $0.gameNo == self.selectedSet && $0.placement == placement}
                        let count = val?.count
                        if self.maxValuePlayerA < count ?? 0 {
                            self.maxValuePlayerA = count ?? 0
                        }
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }else{
                        let val = self.zoneHeadMap?.totalZoneHeatMaps?.filter{ $0.playedBy == self.p1Name.text && $0.shotNo == self.selectedGame && $0.gameNo == self.selectedSet && $0.placement == placement}
                        let count = val?.count
                        if self.maxValuePlayerA < count ?? 0 {
                            self.maxValuePlayerA = count ?? 0
                        }
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }
                }
                
                if pointfilter{
                    if selectedGame == 7{
                        let val = self.zoneHeadMap?.totalZoneHeatMaps?.filter{ $0.playedBy == self.p1Name.text && $0.shotNo ?? 0 > 4 && $0.placement == placement}
                        let count = val?.count
                        if self.maxValuePlayerA < count ?? 0 {
                            self.maxValuePlayerA = count ?? 0
                        }
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }else{
                        let val = self.zoneHeadMap?.totalZoneHeatMaps?.filter{ $0.playedBy == self.p1Name.text && $0.shotNo == self.selectedGame && $0.placement == placement }
                        let count = val?.count
                        if self.maxValuePlayerA < count ?? 0 {
                            self.maxValuePlayerA = count ?? 0
                        }
                        
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }
                }
                
                if issetFilter{
                    let val = self.zoneHeadMap?.totalZoneHeatMaps?.filter{ $0.playedBy == self.p1Name.text && $0.gameNo == self.selectedSet && $0.placement == placement }
                    let count = val?.count
                    if self.maxValuePlayerA < count ?? 0 {
                        self.maxValuePlayerA = count ?? 0
                    }
                    
                    return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                }
                
                let val = self.zoneHeadMap?.totalZoneHeatMaps?.filter{ $0.playedBy == self.p1Name.text && $0.placement == placement }
                let count = val?.count
                if self.maxValuePlayerA < count ?? 0 {
                    self.maxValuePlayerA = count ?? 0
                }
                
                return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                
            }else{
                
                if issetFilter && pointfilter{
                    if selectedGame == 7{
                        let val = self.zoneHeadMap?.totalZoneHeatMaps?.filter{ $0.playedBy == self.p2Name.text && $0.shotNo ?? 0 > 4 && $0.gameNo == self.selectedSet && $0.placement == placement}
                        let count = val?.count
                        if self.maxValuePlayerB < count ?? 0 {
                            self.maxValuePlayerB = count ?? 0
                        }
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }else{
                        let val = self.zoneHeadMap?.totalZoneHeatMaps?.filter{ $0.playedBy == self.p2Name.text && $0.shotNo == self.selectedGame && $0.gameNo == self.selectedSet && $0.placement == placement}
                        let count = val?.count
                        if self.maxValuePlayerB < count ?? 0 {
                            self.maxValuePlayerB = count ?? 0
                        }
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }
                }
                
                if pointfilter{
                    if selectedGame == 7{
                        let val = self.zoneHeadMap?.totalZoneHeatMaps?.filter{ $0.playedBy == self.p2Name.text && $0.shotNo ?? 0 > 4 && $0.placement == placement}
                        let count = val?.count
                        if self.maxValuePlayerB < count ?? 0 {
                            self.maxValuePlayerB = count ?? 0
                        }
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }else{
                        let val = self.zoneHeadMap?.totalZoneHeatMaps?.filter{ $0.playedBy == self.p2Name.text && $0.shotNo == self.selectedGame && $0.placement == placement }
                        let count = val?.count
                        if self.maxValuePlayerB < count ?? 0 {
                            self.maxValuePlayerB = count ?? 0
                        }
                        
                        return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                    }
                }
                
                if issetFilter{
                    let val = self.zoneHeadMap?.totalZoneHeatMaps?.filter{ $0.playedBy == self.p2Name.text && $0.gameNo == self.selectedSet && $0.placement == placement }
                    let count = val?.count
                    if self.maxValuePlayerB < count ?? 0 {
                        self.maxValuePlayerB = count ?? 0
                    }
                    
                    
                    return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
                }
                
                let val = self.zoneHeadMap?.totalZoneHeatMaps?.filter{ $0.playedBy == self.p2Name.text && $0.placement == placement }
                let count = val?.count
                if self.maxValuePlayerB < count ?? 0 {
                    self.maxValuePlayerB = count ?? 0
                }
                
                return (count ?? 0,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
            }
        }
    }
    
    func changeColor() {
        var isColorSetA = false
        var isColorSetB = false
        let val1 = self.r1c1BtnA.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val1) != self.maxValuePlayerA || isColorSetA {
            self.r1c1BtnA.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetA = true
            self.r1c1BtnA.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let val2 = self.r1c1BtnB.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val2) != self.maxValuePlayerA || isColorSetA {
            self.r1c1BtnB.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
            self.r1c1BtnB.setTitleColor(UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0), for: .normal)
        } else {
            isColorSetA = true
            self.r1c1BtnB.setTitleColor(UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0), for: .normal)
            self.r1c1BtnB.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let val3 = self.r1c2Btn.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val3) != self.maxValuePlayerA || isColorSetA {
            self.r1c2Btn.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetA = true
            self.r1c2Btn.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let val4 = self.r1c3BtnA.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val4) != self.maxValuePlayerA || isColorSetA {
            self.r1c3BtnA.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
            self.r1c3BtnA.setTitleColor(UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0), for: .normal)
        } else {
            isColorSetA = true
            self.r1c3BtnA.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
            self.r1c3BtnA.setTitleColor(UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0), for: .normal)
        }
        
        let val5 = self.r1c3BtnB.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val5) != self.maxValuePlayerA || isColorSetA {
            self.r1c3BtnB.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetA = true
            self.r1c3BtnB.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let val6 = self.r2c1BtnA.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val6) != self.maxValuePlayerA || isColorSetA {
            self.r2c1BtnA.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetA = true
            self.r2c1BtnA.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let val7 = self.r2c1BtnB.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val7) != self.maxValuePlayerA || isColorSetA {
            self.r2c1BtnB.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetA = true
            self.r2c1BtnB.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let val8 = self.r2c2Btn.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val8) != self.maxValuePlayerA || isColorSetA {
            self.r2c2Btn.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetA = true
            self.r2c2Btn.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let val9 = self.r2c3BtnA.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val9) != self.maxValuePlayerA || isColorSetA {
            self.r2c3BtnA.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetA = true
            self.r2c3BtnA.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let valA = self.r2c3BtnB.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(valA) != self.maxValuePlayerA || isColorSetA {
            self.r2c3BtnB.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetA = true
            self.r2c3BtnB.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let valB = self.r3c1BtnA.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(valB) != self.maxValuePlayerA || isColorSetA {
            self.r3c1BtnA.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetA = true
            self.r3c1BtnA.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let valC = self.r3c1BtnB.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(valC) != self.maxValuePlayerA || isColorSetA {
            self.r3c1BtnB.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
            self.r3c1BtnB.setTitleColor(UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0), for: .normal)
        } else {
            isColorSetA = true
            self.r3c1BtnB.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
            self.r3c1BtnB.setTitleColor(UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0), for: .normal)
        }
        
        let valD = self.r3c2Btn.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(valD) != self.maxValuePlayerA || isColorSetA {
            self.r3c2Btn.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetA = true
            self.r3c2Btn.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let valE = self.r3c3BtnA.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(valE) != self.maxValuePlayerA || isColorSetA {
            self.r3c3BtnA.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
            self.r3c3BtnA.setTitleColor(UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0), for: .normal)
        } else {
            isColorSetA = true
            self.r3c3BtnA.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
            self.r3c3BtnA.setTitleColor(UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0), for: .normal)
        }
        
        let valF = self.r3c3BtnB.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(valF) != self.maxValuePlayerA || isColorSetA {
            self.r3c3BtnB.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetA = true
            self.r3c3BtnB.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        
        
        let val11 = self.r4c1BtnA.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val11) != self.maxValuePlayerB || isColorSetB {
            self.r4c1BtnA.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetB = true
            self.r4c1BtnA.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let val22 = self.r4c1BtnB.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val22) != self.maxValuePlayerB || isColorSetB {
            self.r4c1BtnB.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
            self.r4c1BtnB.setTitleColor(UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0), for: .normal)
        } else {
            isColorSetB = true
            self.r4c1BtnB.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
            self.r4c1BtnB.setTitleColor(UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0), for: .normal)
        }
        
        let val33 = self.r4c2Btn.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val33) != self.maxValuePlayerB || isColorSetB {
            self.r4c2Btn.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetB = true
            self.r4c2Btn.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let val44 = self.r4c3BtnA.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val44) != self.maxValuePlayerB || isColorSetB {
            self.r4c3BtnA.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
            self.r4c3BtnA.setTitleColor(UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0), for: .normal)
        } else {
            isColorSetB = true
            self.r4c3BtnA.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
            self.r4c3BtnA.setTitleColor(UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0), for: .normal)
        }
        
        let val55 = self.r4c3BtnB.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val55) != self.maxValuePlayerB || isColorSetB {
            self.r4c3BtnB.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetB = true
            self.r4c3BtnB.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let val66 = self.r5c1BtnA.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val66) != self.maxValuePlayerB || isColorSetB {
            self.r5c1BtnA.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetB = true
            self.r5c1BtnA.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let val77 = self.r5c1BtnB.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val77) != self.maxValuePlayerB || isColorSetB {
            self.r5c1BtnB.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetB = true
            self.r5c1BtnB.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let val88 = self.r5c2Btn.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val88) != self.maxValuePlayerB || isColorSetB {
            self.r5c2Btn.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetB = true
            self.r5c2Btn.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let val99 = self.r5c3BtnA.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(val99) != self.maxValuePlayerB || isColorSetB {
            self.r5c3BtnA.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetB = true
            self.r5c3BtnA.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let valAA = self.r5c3BtnB.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(valAA) != self.maxValuePlayerB || isColorSetB {
            self.r5c3BtnB.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetB = true
            self.r5c3BtnB.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let valBB = self.r6c1BtnA.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(valBB) != self.maxValuePlayerB || isColorSetB {
            self.r6c1BtnA.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetB = true
            self.r6c1BtnA.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let valCC = self.r6c1BtnB.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(valCC) != self.maxValuePlayerB || isColorSetB {
            self.r6c1BtnB.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
            self.r6c1BtnB.setTitleColor(UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0), for: .normal)
        } else {
            isColorSetB = true
            self.r6c1BtnB.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
            self.r6c1BtnB.setTitleColor(UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0), for: .normal)
        }
        
        let valDD = self.r6c2Btn.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(valDD) != self.maxValuePlayerB || isColorSetB {
            self.r6c2Btn.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetB = true
            self.r6c2Btn.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
        
        let valEE = self.r6c3BtnA.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(valEE) != self.maxValuePlayerB || isColorSetB {
            self.r6c3BtnA.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
            self.r6c3BtnA.setTitleColor(UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0), for: .normal)
        } else {
            isColorSetB = true
            self.r6c3BtnA.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
            self.r6c3BtnA.setTitleColor(UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0), for: .normal)
        }
        
        let valFF = self.r6c3BtnB.currentTitle?.components(separatedBy: "\n").first ?? "0"
        if Int(valFF) != self.maxValuePlayerB || isColorSetB {
            self.r6c3BtnB.backgroundColor = UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        } else {
            isColorSetB = true
            self.r6c3BtnB.backgroundColor = UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        }
    }
    
    func observers(){
        self.setCollectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }

}

extension BallPlacementViewModel:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return games?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = setCollectionView.dequeueReusableCell(withReuseIdentifier: "set", for: indexPath) as! BallPlacementSetCVC
        if selectedSet == games?[indexPath.row].gameID{
            if games?[indexPath.row].isSelected == true{
                games?[indexPath.row].isSelected = false
                cell.backView.backgroundColor = UIColor.black
                issetFilter = false
                selectedSet = 0
                setValuesAndColor()
            }else{
                games?[indexPath.row].isSelected = true
                cell.backView.backgroundColor = Colors.TableBackColor
                setValuesAndColor()
            }
        }else{
            games?[indexPath.row].isSelected = false
            cell.backView.backgroundColor = UIColor.black
        }
        cell.setnoLabel.text = "Set - \(games?[indexPath.row].gameID ?? 0)"
        cell.backView.layer.borderWidth = 1
        cell.backView.layer.borderColor = UIColor.white.cgColor
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let size = CGSize(width: collectionView.frame.size.width/5, height: collectionView.frame.size.width/5 - 40)
        return size
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedSet = games?[indexPath.row].gameID ?? 0
        issetFilter = true
        self.setCollectionView.reloadData()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        self.setCollectionView.layer.removeAllAnimations()
        if games?.count == 0{
            setcollectionviewheightConstraint.constant = 0
        }else{
            setcollectionviewheightConstraint.constant = setCollectionView.contentSize.height
        }
    }
}
