//
//  RallyAnalysisVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 16/04/21.
//

import UIKit

class RallyAnalysisVC: UIViewController {
    
    var isLayoutUpdated = 0
    var rally:[Rally]?
    var playerA:RealTimeMatchStatsPlayer?
    var playerB:RealTimeMatchStatsPlayer?
    var highestElement = 0
    
    //MARK:- Objects
    
    @IBOutlet weak var longplayerBtipView: UIView!
    @IBOutlet weak var longplayerAtipView: UIView!
    @IBOutlet weak var mediumplayerBtipView: UIView!
    @IBOutlet weak var mediumplayerAtipView: UIView!
    @IBOutlet weak var shortplayerBtipView: UIView!
    @IBOutlet weak var shortplayerAtipView: UIView!
    @IBOutlet weak var longplayerBwholebarView: UIView!
    @IBOutlet weak var longplayerAwholebarView: UIView!
    @IBOutlet weak var longplayerBbarwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var longplayerAbarwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediumplayerBbarwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediumplayerAbarwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediumplayerBwholebarView: UIView!
    @IBOutlet weak var mediumplayerAwholebarView: UIView!
    @IBOutlet weak var shortplayerBwholebarView: UIView!
    @IBOutlet weak var shortplayerAwholebarView: UIView!
    @IBOutlet weak var shortplayerBbarwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var shortplayerAbarwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var longplayerBbarView: UIView!
    @IBOutlet weak var longplayerAbarView: UIView!
    @IBOutlet weak var mediumplayerBbarView: UIView!
    @IBOutlet weak var mediumplayerAbarView: UIView!
    @IBOutlet weak var shortplayerBbarView: UIView!
    @IBOutlet weak var shortplayerAbarView: UIView!
    @IBOutlet weak var longplayerBLabel: UILabel!
    @IBOutlet weak var longplayerALabel: UILabel!
    @IBOutlet weak var mediumplayerBLabel: UILabel!
    @IBOutlet weak var mediumplayerALabel: UILabel!
    @IBOutlet weak var shortplayerBLabel: UILabel!
    @IBOutlet weak var shortplayerALabel: UILabel!
    @IBOutlet weak var longheadingLabel: UILabel!
    @IBOutlet weak var mediumheadingLabel: UILabel!
    @IBOutlet weak var shortheadingLabel: UILabel!
    @IBOutlet weak var playerBnameLabel: UILabel!
    @IBOutlet weak var playerAnameLabel: UILabel!
    @IBOutlet weak var playerAlegendView: UIView!
    @IBOutlet weak var playerBlegendView: UIView!
    @IBOutlet weak var iswonSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        playerAlegendView.makeround()
        playerBlegendView.makeround()
        shortplayerAtipView.makeround()
        shortplayerBtipView.makeround()
        mediumplayerAtipView.makeround()
        mediumplayerBtipView.makeround()
        longplayerAtipView.makeround()
        longplayerBtipView.makeround()
        shortplayerAbarView.layer.cornerRadius = 5
        shortplayerBbarView.layer.cornerRadius = 5
        mediumplayerAbarView.layer.cornerRadius = 5
        mediumplayerBbarView.layer.cornerRadius = 5
        longplayerAbarView.layer.cornerRadius = 5
        longplayerBbarView.layer.cornerRadius = 5
        iswonSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        shortheadingLabel.text = "Short\n(1-4)"
        mediumheadingLabel.text = "Medium\n(5-8)"
        longheadingLabel.text = "Long\n(9 above)"
        settingData()
    }
    
    func settingData(){
        playerAnameLabel.text = playerA?.name
        playerBnameLabel.text = playerB?.name
    }
    
    func gettinghighestElement(){
        
        let rally1a = rally?[0].a ?? 0
        let rally1b = rally?[0].b ?? 0
        let rally2a = rally?[1].a ?? 0
        let rally2b = rally?[1].b ?? 0
        let rally3a = rally?[2].a ?? 0
        let rally3b = rally?[2].b ?? 0
        
        let array = [rally1a,rally1b,rally2a,rally2b,rally3a,rally3b]
        highestElement = array.max() ?? 0
        
        print(highestElement)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.settingProgress(isWon: false)
            UIView.animate(withDuration: 1){[weak self] in
                self?.view.layoutIfNeeded()
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isLayoutUpdated == 0{
            gettinghighestElement()
            isLayoutUpdated = 1
        }
    }
  
    func settingProgress(isWon:Bool){
        if !isWon{
            if let rallyshortA = rally?[0].a{
                let width = Double(rallyshortA)/Double(highestElement)
                let width2 = (shortplayerAwholebarView.frame.size.width - 35) * CGFloat(width)
                self.shortplayerAbarwidthConstraint.constant = width2
                shortplayerALabel.text = rallyshortA.toString()
            }
            if let rallyshortB = rally?[0].b{
                let width = Double(rallyshortB)/Double(highestElement)
                let width2 = (shortplayerBwholebarView.frame.size.width - 35) * CGFloat(width)
                self.shortplayerBbarwidthConstraint.constant = width2
                shortplayerBLabel.text = rallyshortB.toString()
            }
            if let rallymediumA = rally?[1].a{
                let width = Double(rallymediumA)/Double(highestElement)
                let width2 = (mediumplayerAwholebarView.frame.size.width - 35) * CGFloat(width)
                self.mediumplayerAbarwidthConstraint.constant = width2
                mediumplayerALabel.text = rallymediumA.toString()
            }
            if let rallymediumB = rally?[1].b{
                let width = Double(rallymediumB)/Double(highestElement)
                let width2 = (mediumplayerBwholebarView.frame.size.width - 35) * CGFloat(width)
                self.mediumplayerBbarwidthConstraint.constant = width2
                mediumplayerBLabel.text = rallymediumB.toString()
            }
            if let rallylongA = rally?[2].a{
                let width = Double(rallylongA)/Double(highestElement)
                let width2 = (longplayerAwholebarView.frame.size.width - 35) * CGFloat(width)
                self.longplayerAbarwidthConstraint.constant = width2
                longplayerALabel.text = rallylongA.toString()
            }
            if let rallylongB = rally?[2].b{
                let width = Double(rallylongB)/Double(highestElement)
                let width2 = (longplayerBwholebarView.frame.size.width - 35) * CGFloat(width)
                self.longplayerBbarwidthConstraint.constant = width2
                longplayerBLabel.text = rallylongB.toString()
            }
        }else{
            if let rallyshortA = rally?[0].b{
                let width = Double(rallyshortA)/Double(highestElement)
                let width2 = (shortplayerAwholebarView.frame.size.width - 35) * CGFloat(width)
                self.shortplayerAbarwidthConstraint.constant = width2
                shortplayerALabel.text = rallyshortA.toString()
            }
            if let rallyshortB = rally?[0].a{
                let width = Double(rallyshortB)/Double(highestElement)
                let width2 = (shortplayerBwholebarView.frame.size.width - 35) * CGFloat(width)
                self.shortplayerBbarwidthConstraint.constant = width2
                shortplayerBLabel.text = rallyshortB.toString()
            }
            if let rallymediumA = rally?[1].b{
                let width = Double(rallymediumA)/Double(highestElement)
                let width2 = (mediumplayerAwholebarView.frame.size.width - 35) * CGFloat(width)
                self.mediumplayerAbarwidthConstraint.constant = width2
                mediumplayerALabel.text = rallymediumA.toString()
            }
            if let rallymediumB = rally?[1].a{
                let width = Double(rallymediumB)/Double(highestElement)
                let width2 = (mediumplayerBwholebarView.frame.size.width - 35) * CGFloat(width)
                self.mediumplayerBbarwidthConstraint.constant = width2
                mediumplayerBLabel.text = rallymediumB.toString()
            }
            if let rallylongA = rally?[2].b{
                let width = Double(rallylongA)/Double(highestElement)
                let width2 = (longplayerAwholebarView.frame.size.width - 35) * CGFloat(width)
                self.longplayerAbarwidthConstraint.constant = width2
                longplayerALabel.text = rallylongA.toString()
            }
            if let rallylongB = rally?[2].a{
                let width = Double(rallylongB)/Double(highestElement)
                let width2 = (longplayerBwholebarView.frame.size.width - 35) * CGFloat(width)
                self.longplayerBbarwidthConstraint.constant = width2
                longplayerBLabel.text = rallylongB.toString()
            }
        }
    }
    
    @IBAction func iswonAct(_ sender: Any) {
        settingProgress(isWon: iswonSwitch.isOn)
        UIView.animate(withDuration: 0.5){[weak self] in
            self?.view.layoutIfNeeded()
        }
    }
}
