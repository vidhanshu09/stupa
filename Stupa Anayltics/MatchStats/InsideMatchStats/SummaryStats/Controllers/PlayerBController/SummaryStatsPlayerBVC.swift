//
//  SummaryStatsPlayerBVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 16/04/21.
//

import UIKit

class SummaryStatsPlayerBVC: UIViewController {

    var playerBstats: RealTimeMatchPlayerBStats?
    var playerB:RealTimeMatchStatsPlayer?
    
    //MARK:- Objects
    
    @IBOutlet weak var playernameLabel: UILabel!
    @IBOutlet weak var totalservewinsLabel: UILabel!
    @IBOutlet weak var totalserveerrorsLabel: UILabel!
    @IBOutlet weak var servereturnwinsLabel: UILabel!
    @IBOutlet weak var servereturnerrorsLabel: UILabel!
    @IBOutlet weak var winsonthirdballLabel: UILabel!
    @IBOutlet weak var errorsonthirsballLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingData()
        // Do any additional setup after loading the view.
    }
    
    func settingData(){
        playernameLabel.text = playerB?.name
        totalservewinsLabel.text = playerBstats?.servePoint?.toString()
        totalserveerrorsLabel.text = playerBstats?.serverError?.toString()
        servereturnwinsLabel.text = playerBstats?.serveReturnWin?.toString()
        servereturnerrorsLabel.text = playerBstats?.serveReturnError?.toString()
        winsonthirdballLabel.text = playerBstats?.pointsOn3RDBall?.toString()
        errorsonthirsballLabel.text = playerBstats?.errorsOn3RDBall?.toString()
    }

}
