//
//  SummaryStatsPlayerAVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 16/04/21.
//

import UIKit

class SummaryStatsPlayerAVC: UIViewController {
    
    var playerAstats: RealTimeMatchPlayerAStats?
    var playerA:RealTimeMatchStatsPlayer?
    
    //MARK:- Objects
    
    @IBOutlet weak var playernameLabel: UILabel!
    @IBOutlet weak var totalservewinsLabel: UILabel!
    @IBOutlet weak var totalserveerrorsLabel: UILabel!
    @IBOutlet weak var servereturnwinsLabel: UILabel!
    @IBOutlet weak var servereturnerrorsLabel: UILabel!
    @IBOutlet weak var winsonthirdballLabel: UILabel!
    @IBOutlet weak var errorsonthirsballLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingData()
    }
    
    func settingData(){
        playernameLabel.text = playerA?.name
        totalservewinsLabel.text = playerAstats?.servePoint?.toString()
        totalserveerrorsLabel.text = playerAstats?.serverError?.toString()
        servereturnwinsLabel.text = playerAstats?.serveReturnWin?.toString()
        servereturnerrorsLabel.text = playerAstats?.serveReturnError?.toString()
        winsonthirdballLabel.text = playerAstats?.pointsOn3RDBall?.toString()
        errorsonthirsballLabel.text = playerAstats?.errorsOn3RDBall?.toString()
    }
    
}
