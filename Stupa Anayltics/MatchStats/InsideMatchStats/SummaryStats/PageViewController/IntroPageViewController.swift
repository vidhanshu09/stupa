//
//  IntroPageViewController.swift
//  Stupa Analytics
//
//  Created by Vidhanshu on 16/04/21.
//  Copyright © 2021 Vidhanshu. All rights reserved.
//

import UIKit

protocol IntroPageViewControllerDataSource: class
{
	func introPageViewController(_ pageViewController: IntroPageViewController, numberOfPages pages: Int)
}

protocol IntroPageViewControllerDelegate: class
{
	func introPageViewController(_ pageViewController: IntroPageViewController, didChangePageIndex index: Int)
}

class IntroPageViewController: UIPageViewController
{

	weak var introPageViewControllerDataSource: IntroPageViewControllerDataSource?
	weak var introPageViewControllerDelegate: IntroPageViewControllerDelegate?
	
	fileprivate(set) lazy var contentViewControllers: [UIViewController] =
	{
		return [
			self.contentViewController(withIdentifier: "SummaryStatsPlayerAVC"),
			self.contentViewController(withIdentifier: "SummaryStatsPlayerBVC")
		]
	}()
	
	fileprivate var allowRotate: Bool = false
    var playerAstats: RealTimeMatchPlayerAStats?
    var playerBstats: RealTimeMatchPlayerBStats?
    var playerA:RealTimeMatchStatsPlayer?
    var playerB:RealTimeMatchStatsPlayer?
	
	override func viewDidLoad()
	{
		super.viewDidLoad()

        
		dataSource = self
		delegate = self
		
		if let initialViewController = contentViewControllers.first as? SummaryStatsPlayerAVC
		{
            initialViewController.playerAstats = self.playerAstats
            initialViewController.playerA = self.playerA
			contentViewController(followingViewController: initialViewController)
        }
        
        if let secondViewController = contentViewControllers[1] as? SummaryStatsPlayerBVC
        {
            secondViewController.playerBstats = self.playerBstats
            secondViewController.playerB = self.playerB
        }
		
		introPageViewControllerDataSource?.introPageViewController(self, numberOfPages: contentViewControllers.count)
        
//        Timer.scheduledTimer(timeInterval: 3,
//                             target: self,
//                             selector: #selector(next(_:)),
//                             userInfo: nil,
//                             repeats: true)
        
	}
    
    @objc func next(_ timer: Timer) {
        
        next()
//        currentIndex = currentIndex! + 1
//        if currentIndex == pageControl.numberOfPages{
//            currentIndex = 0
//        }
//        pageControl.currentPage = currentIndex!
    }
	
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
}

extension IntroPageViewController
{
	fileprivate func contentViewController(withIdentifier identifier: String) -> UIViewController
	{
		return UIStoryboard(name: "MyAnalysis", bundle: nil).instantiateViewController(withIdentifier: identifier)
	}
	
	fileprivate func contentViewController(followingViewController viewController: UIViewController, to direction: UIPageViewController.NavigationDirection = .forward)
	{
		setViewControllers([viewController], direction: direction, animated: true)
		{
			(finished) -> Void in
			self.didChangePage()
		}
	}
	
	fileprivate func didChangePage()
	{
		if let firstViewController = viewControllers?.first, let index = contentViewControllers.firstIndex(of: firstViewController)
		{
//            if let summaryplayeBC = firstViewController as? SummaryStatsPlayerBVC{
//                summaryplayeBC.playerBstats = self.playerBstats
//                summaryplayeBC.settingData()
//            }
        
			introPageViewControllerDelegate?.introPageViewController(self, didChangePageIndex: index)
		}
	}
}

extension IntroPageViewController
{
	public func next()
	{
		if let visibleViewController = viewControllers?.first, let viewController = pageViewController(self, viewControllerAfter: visibleViewController)
		{
            
            print(viewController)
            print(visibleViewController)
			contentViewController(followingViewController: viewController)
            
		}
        
        
	}
	
	func pageTo(at index: Int)
	{
		if let firstViewController = viewControllers?.first, let currentIndex = contentViewControllers.firstIndex(of: firstViewController)
		{
			let direction: UIPageViewController.NavigationDirection = index >= currentIndex ? .forward : .reverse
			let viewController = contentViewControllers[index]
			
			contentViewController(followingViewController: viewController, to: direction)
		}
	}
}

extension IntroPageViewController: UIPageViewControllerDataSource
{
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
	{
		guard let index = contentViewControllers.firstIndex(of: viewController) else { return nil }
		
		let previous = index - 1
		
		guard previous >= 0 else { return nil }
		guard contentViewControllers.count > previous else { return nil }
		
		return contentViewControllers[previous]
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
	{
		guard let index = contentViewControllers.firstIndex(of: viewController) else { return nil }
        
        let next = index + 1
  
		let count = contentViewControllers.count
		
        //guard count != next else { return contentViewControllers.first } // rotatable
		guard count != next else { return nil }
		guard count > next else { return nil }
		
		return contentViewControllers[next]
	}
}

extension IntroPageViewController: UIPageViewControllerDelegate
{
	func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
	{
		didChangePage()
	}
    
    
}
