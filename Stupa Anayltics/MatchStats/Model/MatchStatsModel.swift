//
//  MatchStatsModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 17/04/21.
//

import Foundation

// MARK: - RealTimeMatchStatsRequestModel
struct RealTimeMatchStatsRequestModel: Codable {
    let deviceID, deviceType, tokenValue: String
    let matchID: Int

    enum CodingKeys: String, CodingKey {
        case deviceID = "DeviceId"
        case deviceType = "DeviceType"
        case tokenValue = "TokenValue"
        case matchID = "MatchId"
    }
}

// MARK: - RealTimeMatchStatsResponseModel
struct RealTimeMatchStatsResponseModel: Codable {
    let isSuccess: Bool?
    let message: String?
    let data: RealTimeMatchStatsData?

    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case message = "Message"
        case data = "Data"
    }
}

// MARK: - DataClass
struct RealTimeMatchStatsData: Codable {
    let analysisData: RealTimeMatchStatsAnalysisData?
    let aiData: RealTimeMatchStatsAIData?

    enum CodingKeys: String, CodingKey {
        case analysisData = "AnalysisData"
        case aiData = "AIData"
    }
}

// MARK: - AnalysisData
struct RealTimeMatchStatsAnalysisData: Codable {
    let matchID, matchNumber: Int?
    let games: [RealTimeMatchStatsGame]?
    let playerA, playerB: RealTimeMatchStatsPlayer?
    let playerAStats: RealTimeMatchPlayerAStats?
    let playerBStats: RealTimeMatchPlayerBStats?

    enum CodingKeys: String, CodingKey {
        case matchID = "MatchId"
        case matchNumber = "MatchNumber"
        case playerA = "PlayerA"
        case playerB = "PlayerB"
        case games = "Games"
        case playerAStats = "PlayerAStats"
        case playerBStats = "PlayerBStats"
    }
}

// MARK: - Game
struct RealTimeMatchStatsGame: Codable {
    let gameID, gameNumber, playerAScore, playerBScore: Int?
    let gameWinnerID: Int?
    let gameWinnerName: String?
    var isSelected:Bool? = false

    enum CodingKeys: String, CodingKey {
        case gameID = "GameId"
        case gameNumber = "GameNumber"
        case playerAScore = "PlayerAScore"
        case playerBScore = "PlayerBScore"
        case gameWinnerID = "GameWinnerId"
        case gameWinnerName = "GameWinnerName"
        case isSelected = "isSelected"
    }
}

// MARK: - PlayerAStats
struct RealTimeMatchPlayerAStats: Codable {
    let matchAnalysisStatsID, matchID, playerID: Int?
    let forehandWinnerRate, backhandWinnerRate, forehandErrorRate, backhandErrorRate: Double?
    let servePoint, serverError, serveReturnWin, serveReturnError: Int?
    let pointsOn3RDBall, errorsOn3RDBall, biggestLead, gamePointsSaved: Int?
    let mostUserdServe, mostEffectiveServe, leastEffectiveServe, mostUsedServeReturn: String?
    let mostEffectiveReturn, leastEffectiveReturn, top2ErrorZonePlacement, top2HighestErrorRatePlacement: String?
    let top2LowestErrorRatePlacement, top2WinningOtherSidePlacements, top2HighestWinningOtherSideRatePlacement, top2LowestWinningOtherSideRatePlacement: String?
    let top1WinningSameSidePlacement, mostEffectiveWinningPlacement, leastEffectiveWinningPlacement, mostEffectiveStroke: String?
    let mostUsedStroke, leastEffectiveStroke, doc: String?

    enum CodingKeys: String, CodingKey {
        case matchAnalysisStatsID = "MatchAnalysisStatsId"
        case matchID = "MatchId"
        case playerID = "PlayerId"
        case forehandWinnerRate = "ForehandWinnerRate"
        case backhandWinnerRate = "BackhandWinnerRate"
        case forehandErrorRate = "ForehandErrorRate"
        case backhandErrorRate = "BackhandErrorRate"
        case servePoint = "ServePoint"
        case serverError = "ServerError"
        case serveReturnWin = "ServeReturnWin"
        case serveReturnError = "ServeReturnError"
        case pointsOn3RDBall = "PointsOn3rdBall"
        case errorsOn3RDBall = "ErrorsOn3rdBall"
        case biggestLead = "BiggestLead"
        case gamePointsSaved = "GamePointsSaved"
        case mostUserdServe = "MostUserdServe"
        case mostEffectiveServe = "MostEffectiveServe"
        case leastEffectiveServe = "LeastEffectiveServe"
        case mostUsedServeReturn = "MostUsedServeReturn"
        case mostEffectiveReturn = "MostEffectiveReturn"
        case leastEffectiveReturn = "LeastEffectiveReturn"
        case top2ErrorZonePlacement = "Top2ErrorZonePlacement"
        case top2HighestErrorRatePlacement = "Top2HighestErrorRatePlacement"
        case top2LowestErrorRatePlacement = "Top2LowestErrorRatePlacement"
        case top2WinningOtherSidePlacements = "Top2WinningOtherSidePlacements"
        case top2HighestWinningOtherSideRatePlacement = "Top2HighestWinningOtherSideRatePlacement"
        case top2LowestWinningOtherSideRatePlacement = "Top2LowestWinningOtherSideRatePlacement"
        case top1WinningSameSidePlacement = "Top1WinningSameSidePlacement"
        case mostEffectiveWinningPlacement = "MostEffectiveWinningPlacement"
        case leastEffectiveWinningPlacement = "LeastEffectiveWinningPlacement"
        case mostEffectiveStroke = "MostEffectiveStroke"
        case mostUsedStroke = "MostUsedStroke"
        case leastEffectiveStroke = "LeastEffectiveStroke"
        case doc = "DOC"
    }
}

// MARK: - PlayerBStats
struct RealTimeMatchPlayerBStats: Codable {
    let matchAnalysisStatsID: Int?
    let matchID: String?
    let playerID: Int?
    let forehandWinnerRate, backhandWinnerRate, forehandErrorRate, backhandErrorRate: Double?
    let servePoint, serverError, serveReturnWin, serveReturnError: Int?
    let pointsOn3RDBall, errorsOn3RDBall, biggestLead, gamePointsSaved: Int?
    let mostUserdServe, mostEffectiveServe, leastEffectiveServe, mostUsedServeReturn: String?
    let mostEffectiveReturn, leastEffectiveReturn, top2ErrorZonePlacement, top2HighestErrorRatePlacement: String?
    let top2LowestErrorRatePlacement, top2WinningOtherSidePlacements, top2HighestWinningOtherSideRatePlacement, top2LowestWinningOtherSideRatePlacement: String?
    let top1WinningSameSidePlacement, mostEffectiveWinningPlacement, leastEffectiveWinningPlacement, mostEffectiveStroke: String?
    let mostUsedStroke, leastEffectiveStroke, doc: String?

    enum CodingKeys: String, CodingKey {
        case matchAnalysisStatsID = "MatchAnalysisStatsId"
        case matchID = "MatchId"
        case playerID = "PlayerId"
        case forehandWinnerRate = "ForehandWinnerRate"
        case backhandWinnerRate = "BackhandWinnerRate"
        case forehandErrorRate = "ForehandErrorRate"
        case backhandErrorRate = "BackhandErrorRate"
        case servePoint = "ServePoint"
        case serverError = "ServerError"
        case serveReturnWin = "ServeReturnWin"
        case serveReturnError = "ServeReturnError"
        case pointsOn3RDBall = "PointsOn3rdBall"
        case errorsOn3RDBall = "ErrorsOn3rdBall"
        case biggestLead = "BiggestLead"
        case gamePointsSaved = "GamePointsSaved"
        case mostUserdServe = "MostUserdServe"
        case mostEffectiveServe = "MostEffectiveServe"
        case leastEffectiveServe = "LeastEffectiveServe"
        case mostUsedServeReturn = "MostUsedServeReturn"
        case mostEffectiveReturn = "MostEffectiveReturn"
        case leastEffectiveReturn = "LeastEffectiveReturn"
        case top2ErrorZonePlacement = "Top2ErrorZonePlacement"
        case top2HighestErrorRatePlacement = "Top2HighestErrorRatePlacement"
        case top2LowestErrorRatePlacement = "Top2LowestErrorRatePlacement"
        case top2WinningOtherSidePlacements = "Top2WinningOtherSidePlacements"
        case top2HighestWinningOtherSideRatePlacement = "Top2HighestWinningOtherSideRatePlacement"
        case top2LowestWinningOtherSideRatePlacement = "Top2LowestWinningOtherSideRatePlacement"
        case top1WinningSameSidePlacement = "Top1WinningSameSidePlacement"
        case mostEffectiveWinningPlacement = "MostEffectiveWinningPlacement"
        case leastEffectiveWinningPlacement = "LeastEffectiveWinningPlacement"
        case mostEffectiveStroke = "MostEffectiveStroke"
        case mostUsedStroke = "MostUsedStroke"
        case leastEffectiveStroke = "LeastEffectiveStroke"
        case doc = "DOC"
    }
}

// MARK: - AIData
struct RealTimeMatchStatsAIData: Codable {
    let aiResponse: RealTimeMatchStatsAIResponse?

    enum CodingKeys: String, CodingKey {
        case aiResponse = "AIResponse"
    }
}

// MARK: - AIResponse
struct RealTimeMatchStatsAIResponse: Codable {
    let rallies: [Rally]?
    let fastestRally, longestRally: [ErrorPlacementVideo]?
    let heatMap: HeatMap?
    let zoneHeatMap: ZoneHeatMap?
//    let winningFastestRally: [ErrorPlacementVideo]?
    let speedGraph: [SpeedGraph]?
    let serviceAnalysis, recieveAnalysis: EAnalysis?
    let practiceStats: PracticeStats?
//    let winningPlacementVideos, errorPlacementVideos: [ErrorPlacementVideo]?
//    let shotAnalysis: ShotAnalysis?

    enum CodingKeys: String, CodingKey {
        case rallies = "Rallies"
        case fastestRally = "FastestRally"
        case longestRally = "LongestRally"
        case heatMap = "HeatMap"
        case zoneHeatMap = "ZoneHeatMap"
//        case winningFastestRally = "WinningFastestRally"
        case speedGraph = "SpeedGraph"
        case serviceAnalysis = "ServiceAnalysis"
        case recieveAnalysis = "RecieveAnalysis"
        case practiceStats = "PracticeStats"
//        case winningPlacementVideos = "WinningPlacementVideos"
//        case errorPlacementVideos = "ErrorPlacementVideos"
//        case shotAnalysis = "ShotAnalysis"
    }
}

// MARK: - PracticeStats
struct PracticeStats: Codable {
    let totalShotsPlayed, totalShotsLanded: Int?
    let averageSpeed: Double?
    let playedTime, drillType, stroke, strokeAbbreviations: String?
    let ballType, placement: String?

    enum CodingKeys: String, CodingKey {
        case totalShotsPlayed = "TotalShotsPlayed"
        case totalShotsLanded = "TotalShotsLanded"
        case averageSpeed = "AverageSpeed"
        case playedTime = "PlayedTime"
        case drillType = "DrillType"
        case stroke = "Stroke"
        case strokeAbbreviations = "Stroke_abbreviations"
        case ballType = "BallType"
        case placement = "Placement"
    }
}

// MARK: - ErrorPlacementVideo
struct ErrorPlacementVideo: Codable {
    let point, game: Int?
    let videoFile: String?
    let downloadURL: String?
    let thumbnail: String?
    let videoFormat: String?
    let speed: Int?
    let placement: String?
    let playedBy: String?
    let rally: Int?

    enum CodingKeys: String, CodingKey {
        case point = "Point"
        case game = "Game"
        case videoFile = "VideoFile"
        case downloadURL = "downloadUrl"
        case thumbnail = "Thumbnail"
        case videoFormat = "VideoFormat"
        case speed = "Speed"
        case placement = "Placement"
        case playedBy = "PlayedBy"
        case rally = "Rally"
    }
}

// MARK: - Rally
struct Rally: Codable {
    let rally: String?
    let a, b: Int?

    enum CodingKeys: String, CodingKey {
        case rally = "Rally"
        case a = "A"
        case b = "B"
    }
}

// MARK: - EAnalysis
struct EAnalysis: Codable {
    let matchID, matchNumber: Int?
    let playerA, playerB: RealTimeMatchStatsPlayer?
    let poinWon: PoinWon?
    let pionWonOnShots: PionWonOnShots?

    enum CodingKeys: String, CodingKey {
        case matchID = "MatchId"
        case matchNumber = "MatchNumber"
        case playerA = "PlayerA"
        case playerB = "PlayerB"
        case poinWon = "PoinWon"
        case pionWonOnShots = "PionWonOnShots"
    }
}

// MARK: - PoinWon
struct PoinWon: Codable {
    let playerAWon, playerAWonPer, playerALost, playerALostPer: Int?
    let playerBWon, playerBWonPer, playerBLost, playerBLostPer: Int?

    enum CodingKeys: String, CodingKey {
        case playerAWon = "PlayerAWon"
        case playerAWonPer = "PlayerAWonPer"
        case playerALost = "PlayerALost"
        case playerALostPer = "PlayerALostPer"
        case playerBWon = "PlayerBWon"
        case playerBWonPer = "PlayerBWonPer"
        case playerBLost = "PlayerBLost"
        case playerBLostPer = "PlayerBLostPer"
    }
}

// MARK: - PionWonOnShots
struct PionWonOnShots: Codable {
    let longSort, shortSort, mediumSort: [Sort]?

    enum CodingKeys: String, CodingKey {
        case longSort = "LongSort"
        case shortSort = "ShortSort"
        case mediumSort = "MediumSort"
    }
}

// MARK: - Sort
struct Sort: Codable {
    let playerName: String?
    let won, lost: Int?
    let wonPer, lostPer: Double?

    enum CodingKeys: String, CodingKey {
        case playerName = "PlayerName"
        case won = "Won"
        case lost = "Lost"
        case wonPer = "WonPer"
        case lostPer = "LostPer"
    }
}


// MARK: - SpeedGraph
struct SpeedGraph: Codable {
    let gameNo: Int?
    let winningSpeeds, totalSpeeds: [Speed]?

    enum CodingKeys: String, CodingKey {
        case gameNo = "GameNo"
        case winningSpeeds = "WinningSpeeds"
        case totalSpeeds = "TotalSpeeds"
    }
}

// MARK: - Speed
struct Speed: Codable {
    let pointNo, gameNo, shotNo: Int?
    let playedBy: String?
    let averageSpeed, totalShots: Int?

    enum CodingKeys: String, CodingKey {
        case pointNo = "PointNo"
        case gameNo = "GameNo"
        case shotNo = "ShotNo"
        case playedBy = "PlayedBy"
        case averageSpeed = "AverageSpeed"
        case totalShots = "TotalShots"
    }
}

// MARK: - HeatMap
struct HeatMap: Codable {
    let totalHeatMaps: [TotalHeatMap]?
    let winningHeatMaps: [WinningHeatMap]?

    enum CodingKeys: String, CodingKey {
        case totalHeatMaps = "TotalHeatMaps"
        case winningHeatMaps = "WinningHeatMaps"
    }
}

// MARK: - TotalHeatMap
struct TotalHeatMap: Codable {
    let playedBy: String?
    let ballX, ballY: String?
    let speedZone, shotNo, gameNo, pointNo: Int?
    let drillPlace: Bool?
    let drillPlacePosition: Int?

    enum CodingKeys: String, CodingKey {
        case playedBy = "PlayedBy"
        case ballX = "BallX"
        case ballY = "BallY"
        case speedZone = "SpeedZone"
        case shotNo = "ShotNo"
        case gameNo = "GameNo"
        case pointNo = "PointNo"
        case drillPlace = "drill_place"
        case drillPlacePosition = "drill_place_position"
    }
}

// MARK: - WinningHeatMap
struct WinningHeatMap: Codable {
    let playedBy: String?
    let ballX, ballY: String?
    let shotNo, speedZone, gameNo, pointNo: Int?
    let drillPlace: Bool?
    let drillPlacePosition: Int?
    let videoFile: String?
    let downloadURL: String?
    let thumbnail: String?
    let videoFormat: String?
    let placement: String?
    let zoneCount: Int?

    enum CodingKeys: String, CodingKey {
        case playedBy = "PlayedBy"
        case ballX = "BallX"
        case ballY = "BallY"
        case shotNo = "ShotNo"
        case speedZone = "SpeedZone"
        case gameNo = "GameNo"
        case pointNo = "PointNo"
        case drillPlace = "drill_place"
        case drillPlacePosition = "drill_place_position"
        case videoFile = "VideoFile"
        case downloadURL = "downloadUrl"
        case thumbnail = "Thumbnail"
        case videoFormat = "VideoFormat"
        case placement = "Placement"
        case zoneCount = "ZoneCount"
    }
}

// MARK: - Player
struct RealTimeMatchStatsPlayer: Codable {
    let playerID: Int?
    let name: String?
    let playingStyle: String?
    let playerType: String?
    let totalService, score: Int?

    enum CodingKeys: String, CodingKey {
        case playerID = "PlayerId"
        case name = "Name"
        case playingStyle = "PlayingStyle"
        case playerType = "PlayerType"
        case totalService = "TotalService"
        case score = "Score"
    }
}


// MARK: - ZoneHeatMap
struct ZoneHeatMap: Codable {
    let totalZoneHeatMaps: [TotalZoneHeatMap]?
    let winningZoneHeatMaps: [WinningHeatMap]?

    enum CodingKeys: String, CodingKey {
        case totalZoneHeatMaps = "TotalZoneHeatMaps"
        case winningZoneHeatMaps = "WinningZoneHeatMaps"
    }
}

// MARK: - TotalZoneHeatMap
struct TotalZoneHeatMap: Codable {
    let gameNo, shotNo: Int?
    let playedBy: String?
    let placement: String?
    let pointNo, zoneCount: Int?
    let drillPlace: Bool?
    let drillPlacePosition: Int?

    enum CodingKeys: String, CodingKey {
        case gameNo = "GameNo"
        case shotNo = "ShotNo"
        case playedBy = "PlayedBy"
        case placement = "Placement"
        case pointNo = "PointNo"
        case zoneCount = "ZoneCount"
        case drillPlace = "drill_place"
        case drillPlacePosition = "drill_place_position"
    }
}
