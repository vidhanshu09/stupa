//
//  PracticeMatchStatsVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 19/04/21.
//

import UIKit
import FSPagerView
import Charts

class PracticeMatchStatsVC: BaseVC {

    var speedGraph:[SpeedGraph]?
    var matchNo:Int?
    var fastestRally,longestRally:[ErrorPlacementVideo]?
    var ralliesArray = ["5 Fastest Rallies","5 Longest Rallies"]
    var playerA:RealTimeMatchStatsPlayer?
    var playerB:RealTimeMatchStatsPlayer?
    var heatMap:HeatMap?
    var games:[RealTimeMatchStatsGame]?
    var zoneheatMap:ZoneHeatMap?
    var practiceStats: PracticeStats?
    
    //MARK:- Objects
    
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var landedLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var strokeLabel: UILabel!
    @IBOutlet weak var drilltypeLabel: UILabel!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var ballheatpageControl: UIPageControl!
    @IBOutlet weak var fastestballspagerView: FSPagerView!{
        didSet {
            self.fastestballspagerView.register(UINib(nibName:"FastestBalls", bundle: Bundle.main), forCellWithReuseIdentifier: "cell")
            self.fastestballspagerView.transformer = FSPagerViewTransformer(type: .linear)
            let screensize = UIScreen.main.bounds
            let screenwidth = screensize.width
            self.fastestballspagerView.itemSize = CGSize(width: screenwidth - 70, height: 220)
        }
    }
    
    fileprivate var ballheatPageViewController: BallHeatMapPageViewController?{
        didSet{
            ballheatPageViewController?.ballHeatMapPageViewControllerDataSource = self
            ballheatPageViewController?.ballHeatMapPageViewControllerDelegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        openPopUp()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        setNavigationwithTitle(navView: navigationView, navTitle: "Match No. - \(matchNo ?? 0)",isbackNeeded: false){
            self.navigationController?.popToViewController(ofClass: MyAnalysisVC.self)
        }
        drilltypeLabel.text = practiceStats?.drillType
        strokeLabel.text = practiceStats?.stroke
        totalLabel.text = practiceStats?.totalShotsPlayed?.toString()
        landedLabel.text = practiceStats?.totalShotsLanded?.toString()
        durationLabel.text = practiceStats?.playedTime
        speedLabel.text = "\(Int(practiceStats?.averageSpeed?.rounded() ?? 0.0)) KMPH"
        setuplineChart(linechartView: lineChartView)
        setChartData()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if let ballmapPageViewController = segue.destination as? BallHeatMapPageViewController{
            ballmapPageViewController.heatMap = self.heatMap
            ballmapPageViewController.playerA = self.playerA
            ballmapPageViewController.playerB = self.playerB
            ballmapPageViewController.games = self.games
            ballmapPageViewController.zoneheatMap = self.zoneheatMap
            self.ballheatPageViewController = ballmapPageViewController
        }
    }
    
    func openPopUp(){
        let ourartificalPopup = StoryBoards.ExtrasStoryboard.instantiateViewController(withIdentifier: "OurArtificialPopUpVC") as! OurArtificialPopUpVC
        ourartificalPopup.modalPresentationStyle = .overFullScreen
        self.present(ourartificalPopup, animated: false, completion: nil)
    }
    
    func setChartData() {
        let game = self.speedGraph?.filter({$0.gameNo == 0}).first?.totalSpeeds ?? []
        let yVals1 = (0..<game.count).map { (i) -> ChartDataEntry in
            return ChartDataEntry(x: Double(i), y: Double(game[i].averageSpeed ?? 0))
        }
        let set1 = LineChartDataSet(entries: yVals1, label: game.first?.playedBy ?? "")
        //set1.valueFormatter = CustomIntFormatter()
        //set1.axisDependency = .left
        set1.setColor(UIColor(red: 238/255, green: 56/255, blue: 127/255, alpha: 1))
        set1.setCircleColor(UIColor(red: 238/255, green: 56/255, blue: 127/255, alpha: 1))
        set1.drawCirclesEnabled = false
        set1.lineWidth = 2.5
        set1.circleRadius = 3
        set1.fillAlpha = 65/255
        set1.fillColor = UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1)
        set1.highlightColor = UIColor(red: 244/255, green: 117/255, blue: 117/255, alpha: 1)
        set1.drawFilledEnabled = false
        set1.drawCircleHoleEnabled = false
        
        
        lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: game.enumerated().map{ (index,element) in
            return index.toString()
        })
        let data: LineChartData =  LineChartData(dataSets: [set1])

        data.setValueTextColor(.clear)
        lineChartView.data = data
    }

}

extension PracticeMatchStatsVC: BallHeatMapPageViewControllerDataSource{
    func ballheatmapPageViewController(_ pageViewController: BallHeatMapPageViewController, numberOfPages pages: Int){
        ballheatpageControl.numberOfPages = pages
    }
}

extension PracticeMatchStatsVC: BallHeatMapPageViewControllerDelegate{
    func ballheatmapPageViewController(_ pageViewController: BallHeatMapPageViewController, didChangePageIndex index: Int){
        ballheatpageControl.currentPage = index
    }
}

extension PracticeMatchStatsVC : FSPagerViewDataSource,FSPagerViewDelegate{
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return 2
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = fastestballspagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index) as! FastestBallsCell
        cell.headingLabel.text = ralliesArray[index]
        cell.videoImageView.layer.cornerRadius = 5
        if index == 0{
            if fastestRally?.count != 0{
                let url = URL(string: fastestRally?[0].thumbnail ?? "")
                cell.videoImageView.kf.indicatorType = .activity
                cell.videoImageView.kf.setImage(with: url, placeholder: UIImage(named: "empty"), options: [.memoryCacheExpiration(.days(1))])
            }
        }else{
            if longestRally?.count != 0{
                let url = URL(string: longestRally?[0].thumbnail ?? "")
                cell.videoImageView.kf.indicatorType = .activity
                cell.videoImageView.kf.setImage(with: url, placeholder: UIImage(named: "empty"), options: [.memoryCacheExpiration(.days(1))])
            }
        }
        return cell
    }
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        let navigate = StoryBoards.MyAnalysisStoryboard.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
        if index == 0{
            navigate.videoType = .FastestRally
        }else{
            navigate.videoType = .LongestRally
            navigate.longestRally = self.longestRally
        }
        navigate.matchNo = self.matchNo
        self.navigationController?.pushViewController(navigate, animated: true)
    }
}
