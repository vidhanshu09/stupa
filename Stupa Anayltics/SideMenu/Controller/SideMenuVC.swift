//
//  SideMenuVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 19/04/21.
//

import UIKit

class SideMenuVC: UIViewController {

    private var selectedIndex = 0
    weak var delegate:sendData?
    var sideMenuArray = ["Home","My Subscriptions","Packages","Glossary","Failed Uploads","Preferred Contacts","Help & Guide","Subscription PLans","Logout"]
    
    //MARK:- Objects
    
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var backView: UIImageView!
    @IBOutlet weak var playercountryLabel: UILabel!
    @IBOutlet weak var playernameLabel: UILabel!
    @IBOutlet weak var sidemenuTableView:UITableView!
    @IBOutlet weak var sidemenutableheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        observers()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        self.tabBarController?.tabBar.isHidden = true
        gestures()
        profileImageView.makeroundimage()
        if let userName = SUserDefaults.value(forKey: SUDConstants.userName) as? String{
            playernameLabel.text = userName
        }
        if let country = SUserDefaults.value(forKey: SUDConstants.userCountry) as? String{
            playercountryLabel.text = country
        }
        if let userPic = SUserDefaults.value(forKey: SUDConstants.userPic) as? String{
            let url = URL(string: userPic)
            profileImageView.kf.indicatorType = .activity
            profileImageView.kf.setImage(with: url, placeholder: UIImage(named: "empty"), options: [.memoryCacheExpiration(.days(1))])
        }
    }
    
    func observers(){
        self.sidemenuTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    func gestures(){
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissVC)))
    }
    
    @objc func dismissVC(){
        let nav = self.navigationController
        DispatchQueue.main.async { //make sure all UI updates are on the main thread.
            nav?.view.layer.add(CATransition().popFromRight(), forKey: nil)
            nav?.popViewController(animated: false)
        }
    }

}
extension SideMenuVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = sidemenuTableView.dequeueReusableCell(withIdentifier: "side", for: indexPath) as! SideMenuTVC
        if indexPath.row != 0{
            cell.titleLabel.textColor = UIColor.darkGray
        }
        cell.titleLabel.text = sideMenuArray[indexPath.row]
        return cell
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        self.sidemenuTableView.layer.removeAllAnimations()
        if sideMenuArray.count == 0{
            sidemenutableheightConstraint.constant = 40
        }else{
            sidemenutableheightConstraint.constant = sidemenuTableView.contentSize.height
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        switch sideMenuArray[indexPath.row] {
        case "Home":
            let nav = self.navigationController
            DispatchQueue.main.async { //make sure all UI updates are on the main thread.
                nav?.view.layer.add(CATransition().popFromRight(), forKey: nil)
                nav?.popViewController(animated: false)
            }
            delegate?.getPageId(id: 0)
        case "Logout":
            self.navigationController?.popViewController(animated: false)
            delegate?.getPageId(id: 8)
        default:
            print("NO")
            //root.navigationController?.popViewController(animated: false)
            //delegate?.getPageId(id: 9)
        }
    }
}
