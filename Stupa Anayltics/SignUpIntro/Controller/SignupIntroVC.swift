//
//  SignupIntroVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import UIKit

class SignupIntroVC:BaseVC {

    //MARK:- Objects

    @IBOutlet weak var continueView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var termsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        self.setupBackIcon(view: backView)
        addingGestures()
        setattributedLabel()
    }
    
    func addingGestures(){
        continueView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(navigatetoSignup)))
    }
    
    func setattributedLabel(){
        let continueString = NSMutableAttributedString(string:"By continuing, you agree to our \n")
        let attrs = [NSAttributedString.Key.font :UIFont(name: "Montserrat-Medium", size: 16)!,NSAttributedString.Key.foregroundColor: Colors.AppGreenColor,NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue] as [NSAttributedString.Key : Any]
        let termsofUseString = NSMutableAttributedString(string:"Terms of Use ", attributes:attrs as [NSAttributedString.Key : Any])
        let andString = NSMutableAttributedString(string:" &  ")
        let privacypolString = NSMutableAttributedString(string:"Privacy Policy", attributes:attrs as [NSAttributedString.Key : Any])
        andString.append(privacypolString)
        termsofUseString.append(andString)
        continueString.append(termsofUseString)
        termsLabel.attributedText = continueString
    }
    
    @objc func navigatetoSignup(){
        let navigate = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC")
        self.navigationController?.pushViewController(navigate!, animated: true)
    }

}
