//
//  MatchAnalysisVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 15/04/21.
//

import UIKit

class MatchAnalysisVC: UIViewController {

    var matchAnalysisViewModel:MatchAnalysisViewModel!
    
    //MARK:- Objects
    
    @IBOutlet weak var matchanalysisTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
            adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        matchAnalysisViewModel = MatchAnalysisViewModel(root: self, matchanalysisTableView: matchanalysisTableView)
        matchanalysisTableView.dataSource = matchAnalysisViewModel
        matchanalysisTableView.delegate = matchAnalysisViewModel
        matchAnalysisViewModel.getMatchesApi()
    }

}
