//
//  MatchAnalysisTVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 15/04/21.
//

import UIKit

class MatchAnalysisTVC: UITableViewCell {

    @IBOutlet weak var speedView: UIView!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var matchtypeLabel: UILabel!
    @IBOutlet weak var opponentnameLabel: UILabel!
    @IBOutlet weak var playernameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var matchnoLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    
    var match:Match?{
        didSet{
            matchnoLabel.text = "Match No. \(match?.matchID ?? 0)"
            dateLabel.text = match?.matchDate
            playernameLabel.text = match?.playerAName
            opponentnameLabel.text = match?.playerBName
            speedLabel.text = "\(Int(match?.averageSpeed?.rounded() ?? 0.0))\nKmph"
            if match?.matchType == nil {
                self.matchtypeLabel.text =  "(Expert Analysis)"
            } else {
                self.matchtypeLabel.text =  "(Real Time Analysis)"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
