//
//  MatchAnalysisViewModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 15/04/21.
//

import Foundation
import UIKit

class MatchAnalysisViewModel:NSObject{

    private var root:UIViewController!
    private var matchanalysisTableView: UITableView!
    var matchListArray = [Match]()
    private var isDataLoading:Bool = false
    private var Offset:Int = 10
    private var pageNo:Int = 1
    private var didEndReached:Bool = false
    
    init(root:UIViewController,matchanalysisTableView:UITableView){
        self.root = root
        self.matchanalysisTableView = matchanalysisTableView
    }
    
    func getMatchesApi(){
        
        self.isDataLoading = false
        
        let matchParams = MatchAnalysisRequestModel(deviceID: StupaStrings.DeviceUUID ?? "", deviceType: StupaStrings.DeviceType, tokenValue: Utils().getToken(), pageSize: 0, pageNo: 0)
        
        let encoder = JSONEncoder()
        let jsonData = try! encoder.encode(matchParams)
        
        Connector.sharedinstance.runDataApiwithModelandEncodedData(sender: root, data: jsonData, method: .post, parameters: [:], success: nil, url: ApiConstants.getMatchesList, completion: { (Data, Error) in
            if let error = Error{
                print(error.localizedDescription)
                return
            }
            guard let response = Data else{
                self.didEndReached = true
                return
            }
            
            self.matchListArray = response.data?.matches ?? []
            
            if self.matchListArray.count == 0{
                self.matchanalysisTableView.setEmptyMessage(StupaStrings.NoMatches)
            }else{
                self.matchanalysisTableView.restore(withseperator: false)
            }
            
//            if self.Offset == 10{
//                if self.matchListArray.count == 0{
//                    self.matchanalysisTableView.setEmptyMessage(StupaStrings.NoMatches)
//                }else{
//                    self.matchanalysisTableView.restore(withseperator: false)
//                }
//            }
//
            self.matchanalysisTableView.reloadData()
        
            
        }, type: MatchAnalysisResponseModel.self)
        
    }
}

extension MatchAnalysisViewModel:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchListArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = matchanalysisTableView.dequeueReusableCell(withIdentifier: "match", for: indexPath) as! MatchAnalysisTVC
        cell.speedView.makeround()
        cell.speedView.layer.borderWidth = 6
        cell.speedView.layer.borderColor = Colors.AppGreenColor.cgColor
        cell.match = matchListArray[indexPath.row]
        if indexPath.row % 2 == 0{
            cell.backView.backgroundColor = UIColor(red: 66/255, green: 66/255, blue: 66/255, alpha: 1.0)
        }else{
            cell.backView.backgroundColor = UIColor.clear
        }
        return cell
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if indexPath.row + 1 == self.matchListArray.count {
//            print("do something")
//            if !isDataLoading && didEndReached == false{
//                isDataLoading = true
//                self.Offset = self.Offset + 10
//                self.pageNo = self.pageNo + 1
//                getMatchesApi()
//            }
//        }
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if matchListArray[indexPath.row].matchType == nil {
            let navigate = StoryBoards.MyAnalysisStoryboard.instantiateViewController(withIdentifier: "MatchStatsLoadingVC") as! MatchStatsLoadingVC
            navigate.matchNo = matchListArray[indexPath.row].matchID ?? 0
            navigate.matchstatsType = .Expert
            root.navigationController?.pushViewController(navigate, animated: true)
        } else {
            let navigate = StoryBoards.MyAnalysisStoryboard.instantiateViewController(withIdentifier: "MatchStatsLoadingVC") as! MatchStatsLoadingVC
            navigate.matchNo = matchListArray[indexPath.row].matchID ?? 0
            navigate.matchstatsType = .Match
            root.navigationController?.pushViewController(navigate, animated: true)
        }
    }
    
}
