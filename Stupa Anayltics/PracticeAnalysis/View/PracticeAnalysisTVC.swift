//
//  PracticeAnalysisTVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 15/04/21.
//

import UIKit

class PracticeAnalysisTVC: UITableViewCell {
    
    @IBOutlet weak var speedView: UIView!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var matchtypeLabel: UILabel!
    @IBOutlet weak var drilltypeLabel: UILabel!
    @IBOutlet weak var placementLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var matchnoLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    
    var match:Practice?{
        didSet{
            matchnoLabel.text = "Match No. \(match?.matchID ?? 0)"
            dateLabel.text = match?.matchDate
            drilltypeLabel.text = match?.drillType
            placementLabel.text = match?.placement
            speedLabel.text = "\(Int(match?.averageSpeed?.rounded() ?? 0.0))\nKmph"
            matchtypeLabel.text = match?.playerName
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
