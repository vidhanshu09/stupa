//
//  PracticeAnalysisViewModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 15/04/21.
//

import Foundation
import UIKit

class PracticeAnalysisViewModel:NSObject{

    private var root:UIViewController!
    private var practiceanalysisTableView: UITableView!
    var matchListArray = [Practice]()
    
    init(root:UIViewController,practiceanalysisTableView:UITableView){
        self.root = root
        self.practiceanalysisTableView = practiceanalysisTableView
    }
    
    func getMatchesApi(){
        
        let matchParams = MatchAnalysisRequestModel(deviceID: StupaStrings.DeviceUUID ?? "", deviceType: StupaStrings.DeviceType, tokenValue: Utils().getToken(), pageSize: 0, pageNo: 0)
        
        let encoder = JSONEncoder()
        let jsonData = try! encoder.encode(matchParams)
        
        Connector.sharedinstance.runDataApiwithModelandEncodedData(sender: root, data: jsonData, method: .post, parameters: [:], success: nil, url: ApiConstants.getMatchesList, completion: { (Data, Error) in
            if let error = Error{
                print(error.localizedDescription)
                return
            }
            guard let response = Data else{
                return
            }
            
            self.matchListArray = response.data?.practices ?? []
            
            if self.matchListArray.count == 0{
                self.practiceanalysisTableView.setEmptyMessage(StupaStrings.NoMatches)
            }else{
                self.practiceanalysisTableView.restore(withseperator: false)
            }
            
            self.practiceanalysisTableView.reloadData()
        
            
        }, type: MatchAnalysisResponseModel.self)
        
    }
}

extension PracticeAnalysisViewModel:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchListArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = practiceanalysisTableView.dequeueReusableCell(withIdentifier: "practice", for: indexPath) as! PracticeAnalysisTVC
        cell.speedView.makeround()
        cell.speedView.layer.borderWidth = 6
        cell.match = matchListArray[indexPath.row]
        cell.speedView.layer.borderColor = Colors.AppGreenColor.cgColor
        if indexPath.row % 2 == 0{
            cell.backView.backgroundColor = UIColor(red: 66/255, green: 66/255, blue: 66/255, alpha: 1.0)
        }else{
            cell.backView.backgroundColor = UIColor.clear
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navigate = StoryBoards.MyAnalysisStoryboard.instantiateViewController(withIdentifier: "MatchStatsLoadingVC") as! MatchStatsLoadingVC
        navigate.matchNo = matchListArray[indexPath.row].matchID ?? 0
        navigate.matchstatsType = .Practice
        root.navigationController?.pushViewController(navigate, animated: true)
    }
}
