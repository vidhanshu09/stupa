//
//  PracticeAnalysisVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 15/04/21.
//

import UIKit

class PracticeAnalysisVC: UIViewController {

    var practiceAnalysisViewModel:PracticeAnalysisViewModel!
    
    //MARK:- Objects
    
    @IBOutlet weak var practiceanalysisTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
            adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        practiceAnalysisViewModel = PracticeAnalysisViewModel(root: self, practiceanalysisTableView: practiceanalysisTableView)
        practiceanalysisTableView.dataSource = practiceAnalysisViewModel
        practiceanalysisTableView.delegate = practiceAnalysisViewModel
        practiceAnalysisViewModel.getMatchesApi()
    }

}
