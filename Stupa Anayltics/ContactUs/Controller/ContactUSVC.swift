//
//  ContactUSVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 19/04/21.
//

import UIKit

class ContactUSVC: UIViewController {
    
    //MARK:- Objects
    
    @IBOutlet weak var sidemenuView: UIView!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var phoneTextfield: UITextField!
    @IBOutlet weak var subjectTextfield: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        self.navigationController?.navigationBar.isHidden = true
        addingGestures()
    }
    
    func addingGestures(){
        sidemenuView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openSideMenu)))
    }
    
    @objc func openSideMenu(){
        let navigate = StoryBoards.SideMenuStoryboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        navigate.delegate = self
        let nav = self.navigationController
        DispatchQueue.main.async { //make sure all UI updates are on the main thread.
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(navigate, animated: false)
        }
    }
    
    func logout(){
        MyLoader.showLoadingView()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            Utils().deleteUserDefaults {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "IntroVC")
                let navigationController = UINavigationController(rootViewController: viewController)
                UIApplication.shared.keyWindow?.rootViewController = navigationController
                MyLoader.hideLoadingView()
            }
        }
    }
    
    @IBAction func submitAct(_ sender: Any) {
    }
}

extension ContactUSVC : sendData {
    func getPageId(id: Int) {
        switch id {
        case 0:
            print("Nothing")
        case 1:
            print("NO")
        case 2:
            print("NO")
        case 3:
            print("NO")
        case 4:
            print("NO")
        case 5:
            print("NO")
        case 6:
            print("NO")
        case 7:
            print("NO")
        case 8:
            let alert = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .alert)
            let yesaction = UIAlertAction(title: "Yes", style: .default){action -> Void in
                self.logout()
            }
            let noaction = UIAlertAction(title: "No", style: .default)
            alert.addAction(yesaction)
            alert.addAction(noaction)
            self.present(alert, animated: true, completion: nil)
        default:
            print("NO")
        }
    }
}
