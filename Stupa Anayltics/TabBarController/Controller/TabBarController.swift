//
//  TabBarController.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 15/04/21.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        tabBar.tintColor = Colors.AppGreenColor
        tabBar.barTintColor = nil
        UITabBar.appearance().tintColor = nil
        tabBar.barTintColor = UIColor.black
        UITabBar.appearance().tintColor = Colors.AppGreenColor
    }

}
