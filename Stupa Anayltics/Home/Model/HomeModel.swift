//
//  HomeModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import Foundation

// MARK: - HomeRequestModel
struct HomeRequestModel: Codable {
    let deviceType, deviceID, tokenValue: String

    enum CodingKeys: String, CodingKey {
        case deviceType
        case deviceID = "deviceId"
        case tokenValue
    }
}


// MARK: - HomeResponseModel
struct HomeResponseModel: Codable {
    let isSuccess: Bool?
    let message : String?
    let data: HomeDataClass?

    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case message = "Message"
        case data = "Data"
    }
}

// MARK: - DataClass
struct HomeDataClass: Codable {
    let user: HomeUser?
    let opponents: [Player]?
    let player: Player?
    let banners: [Banner]?
    let defaulPackageID: Int?
    let userPackage: HomeUserPackage?
    let packageData: [PackageDatum]?
    let insideStupa: [DataInsideStupa]?
    let matchAddons: MatchAddons?

    enum CodingKeys: String, CodingKey {
        case user = "User"
        case opponents = "Opponents"
        case player = "Player"
        case banners = "Banners"
        case defaulPackageID = "DefaulPackageID"
        case userPackage = "UserPackage"
        case packageData = "PackageData"
        case insideStupa = "InsideStupa"
        case matchAddons = "MatchAddons"
    }
}

// MARK: - Banner
struct Banner: Codable {
    let bannerID: Int?
    let bannerType: String?
    let awsURL: String?
    let active, deleted, broadCast, forAll: Bool?
    let countryID: Int?
    let countryName: String?
    let doc, dou: String?
    

    enum CodingKeys: String, CodingKey {
        case bannerID = "BannerId"
        case bannerType = "BannerType"
        case awsURL = "AWSUrl"
        case active = "Active"
        case deleted = "Deleted"
        case broadCast = "BroadCast"
        case forAll = "ForAll"
        case countryID = "CountryId"
        case countryName = "CountryName"
        case doc = "DOC"
        case dou = "DOU"
    }
}

// MARK: - DataInsideStupa
struct DataInsideStupa: Codable {
    let type: String?
    let insideStupa: [InsideStupaInsideStupa]?

    enum CodingKeys: String, CodingKey {
        case type = "Type"
        case insideStupa = "InsideStupa"
    }
}

// MARK: - InsideStupaInsideStupa
struct InsideStupaInsideStupa: Codable {
    let title: String?
    let insideStupaDescription: String?
    let fileURL: String?
    let thumbnailURL: String?
    let order: Int?

    enum CodingKeys: String, CodingKey {
        case title = "Title"
        case insideStupaDescription = "Description"
        case fileURL = "FileUrl"
        case thumbnailURL = "ThumbnailUrl"
        case order = "Order"
    }
}

// MARK: - MatchAddons
struct MatchAddons: Codable {
    let userID, quantity: Int?

    enum CodingKeys: String, CodingKey {
        case userID = "UserID"
        case quantity = "Quantity"
    }
}

// MARK: - Player
struct Player: Codable {
    let playerID: Int?
    let playerNo: String?
    let playerName: String?
    let playingStyle: String?
    let playerType: String?
    let ghostUser: Bool?
    let doc: String?
    let userID, totalCount: Int?

    enum CodingKeys: String, CodingKey {
        case playerID = "PlayerId"
        case playerNo = "PlayerNo"
        case playerName = "PlayerName"
        case playingStyle = "PlayingStyle"
        case playerType = "PlayerType"
        case ghostUser = "GhostUser"
        case doc = "DOC"
        case userID = "UserId"
        case totalCount = "TotalCount"
    }
}

// MARK: - PackageDatum
struct PackageDatum: Codable {
    let packageID, totalMinutes, totalMatch, totalCoachMatch: Int?
    let totalUmpireMatch: Int?
    let isDefault, completed: Bool?

    enum CodingKeys: String, CodingKey {
        case packageID = "PackageId"
        case totalMinutes = "TotalMinutes"
        case totalMatch = "TotalMatch"
        case totalCoachMatch = "TotalCoachMatch"
        case totalUmpireMatch = "TotalUmpireMatch"
        case isDefault = "IsDefault"
        case completed = "Completed"
    }
}

// MARK: - User
struct HomeUser: Codable {
    let userID: Int?
    let userType, fullName: String?
    let email: String?
    let isEmailVerified, isMobielVerified: Bool?
    let userName, password: String?
    let isAdmin, claimed: Bool?
    let deviceID, lastLoginOn: String?
    let loginCount: Int?
    let countryID: Int?
    let countryName,profilePic: String?
    let profileType: Int?
    let allowVideoUpload: Bool?
    let notificationToken, tokenValue, deviceType: String?
    let isTermsRead, isSubscribe: Bool?
    let stripeCustomerID: String?
    let playerID: Int?
    let razorpayCustomerID: String?
    let isActive, isDeleted: Bool?
    let doc, dou: String?
    let createdBy, modifiedBy: Int?

    enum CodingKeys: String, CodingKey {
        case userID = "UserId"
        case userType = "UserType"
        case fullName = "FullName"
        case email = "Email"
        case isEmailVerified = "IsEmailVerified"
        case isMobielVerified = "IsMobielVerified"
        case userName = "UserName"
        case password = "Password"
        case isAdmin = "IsAdmin"
        case profilePic = "ProfilePic"
        case claimed = "Claimed"
        case deviceID = "DeviceId"
        case lastLoginOn = "LastLoginOn"
        case loginCount = "LoginCount"
        case countryID = "CountryId"
        case countryName = "CountryName"
        case profileType = "ProfileType"
        case allowVideoUpload = "AllowVideoUpload"
        case notificationToken = "NotificationToken"
        case tokenValue = "TokenValue"
        case deviceType = "DeviceType"
        case isTermsRead = "IsTermsRead"
        case isSubscribe = "IsSubscribe"
        case stripeCustomerID = "Stripe_Customer_Id"
        case playerID = "PlayerId"
        case razorpayCustomerID = "Razorpay_Customer_Id"
        case isActive = "IsActive"
        case isDeleted = "IsDeleted"
        case doc = "DOC"
        case dou = "DOU"
        case createdBy = "CreatedBy"
        case modifiedBy = "ModifiedBy"
    }
}

// MARK: - UserPackage
struct HomeUserPackage: Codable {
    let stripePriceID: String?
    let userPackageID: Int?
    let stripeProdID, packageName: String?
    let price: Int?
    let discount: Int?
    let isFree: Bool?
    let packageCountryPricingID, duration: Int?
    let currency: String?
    let packageID: Int?
    let matchFeature, practiceFeature, athleteProfiling: [String: Int?]?

    enum CodingKeys: String, CodingKey {
        case stripePriceID = "Stripe_PriceId"
        case userPackageID = "UserPackageID"
        case stripeProdID = "Stripe_ProdId"
        case packageName = "PackageName"
        case price = "Price"
        case discount = "Discount"
        case isFree = "IsFree"
        case packageCountryPricingID = "PackageCountryPricingID"
        case duration = "Duration"
        case currency = "Currency"
        case packageID = "PackageID"
        case matchFeature = "MatchFeature"
        case practiceFeature = "PracticeFeature"
        case athleteProfiling = "AthleteProfiling"
    }
}

struct StaticItems{
    var name:String?
    var data:[StaticItemsData]?
}

struct StaticItemsData {
    var name:String?
    var image:UIImage?
}
