//
//  StaticItemsTVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import UIKit

class StaticItemsTVC: UITableViewCell {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var staticitemCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension StaticItemsTVC {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        staticitemCollectionView.delegate = dataSourceDelegate
        staticitemCollectionView.dataSource = dataSourceDelegate
        staticitemCollectionView.tag = row
        staticitemCollectionView.setContentOffset(staticitemCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        staticitemCollectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { staticitemCollectionView.contentOffset.x = newValue }
        get { return staticitemCollectionView.contentOffset.x }
    }
}
