//
//  AllDataTVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 15/04/21.
//

import UIKit

class AllDataTVC: UITableViewCell {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var allDataCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension AllDataTVC {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        allDataCollectionView.delegate = dataSourceDelegate
        allDataCollectionView.dataSource = dataSourceDelegate
        allDataCollectionView.tag = row
        allDataCollectionView.setContentOffset(allDataCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        allDataCollectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { allDataCollectionView.contentOffset.x = newValue }
        get { return allDataCollectionView.contentOffset.x }
    }
}
