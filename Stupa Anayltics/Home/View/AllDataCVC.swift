//
//  AllDataCVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 15/04/21.
//

import UIKit
import Kingfisher

class AllDataCVC: UICollectionViewCell {
    
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    var item:InsideStupaInsideStupa?{
        didSet{
            if let thumbnail = item?.thumbnailURL{
                let youtubeThumbnail = "https://img.youtube.com/vi/" + thumbnail + "/0.jpg"
                let url = URL(string: youtubeThumbnail)
                backImageView.kf.indicatorType = .activity
                backImageView.kf.setImage(with: url, placeholder: UIImage(named: "empty"), options: [.memoryCacheExpiration(.days(1))])
            }
            titleLabel.text = item?.title
            descLabel.text = item?.insideStupaDescription
        }
    }
}
