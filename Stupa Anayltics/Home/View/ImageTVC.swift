//
//  ImageTVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 15/04/21.
//

import UIKit

class ImageTVC: UITableViewCell {

    let introScreenImagesList = ["login_adriana", "login_darko", "login_kanak", "login_manav"]
    var counter = 0
    
    @IBOutlet weak var backImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func startScheduler() {
        DispatchQueue.global().asyncAfter(deadline: .now() + 3.0) {
            self.updateImage()
            self.startScheduler()
        }
    }
    
    func updateImage() {
        DispatchQueue.main.async {
            self.backImageView.image = UIImage(named: self.introScreenImagesList[self.counter])
            if self.counter != 3{
                self.counter += 1
            } else {
                self.counter = 0
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
