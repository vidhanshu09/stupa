//
//  StaticItemsCVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import UIKit

class StaticItemsCVC: UICollectionViewCell {
    
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var item:StaticItemsData?{
        didSet{
            backImageView.image = item?.image
            titleLabel.text = item?.name
        }
    }
    
}
