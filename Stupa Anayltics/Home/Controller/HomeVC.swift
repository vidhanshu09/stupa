//
//  HomeVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import UIKit
import FSPagerView
import Kingfisher

protocol sendData:class{
    func getPageId(id:Int)
}

class HomeVC: UIViewController {

    var homeViewModel:HomeViewModel!
    var bannersArray:[Banner]?
    
    //MARK:- Objects
    @IBOutlet weak var sidemenuView: UIView!
    @IBOutlet weak var statictableviewheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var alldatatableheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var staticItemsTableView: UITableView!
    @IBOutlet weak var alldataTableView: UITableView!
    @IBOutlet weak var homebannerView: FSPagerView!{
        didSet {
            self.homebannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        }
    }
    @IBOutlet weak var homebannerPageControl: FSPageControl!{
        didSet {
            self.homebannerPageControl.contentHorizontalAlignment = .center
            self.homebannerPageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
    }
    
    func adjustingUI(){
        homeViewModel = HomeViewModel(root: self, staticItemsTableView: staticItemsTableView, alldataTableView: alldataTableView, alldatatableheightConstraint: alldatatableheightConstraint, statictableviewheightConstraint: statictableviewheightConstraint)
        staticItemsTableView.delegate = homeViewModel
        staticItemsTableView.dataSource = homeViewModel
        alldataTableView.delegate = homeViewModel
        alldataTableView.dataSource = homeViewModel
        homeViewModel.observers()
        getBanners()
        self.navigationController?.navigationBar.isHidden = true
        homeViewModel.settingupStaticItems()
        addingGestures()
        addObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }
    
    func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(showvideoUploadedPopUp), name: Notification.Name("ShowUploadVideosPopUp"), object: nil)
    }
    
    @objc func showvideoUploadedPopUp(notification:Notification){
        self.tabBarController?.tabBar.isHidden = false
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        let videouploadedPopup = StoryBoards.ExtrasStoryboard.instantiateViewController(withIdentifier: "VideosUploadedVC") as! VideosUploadedVC
        if let iscoach  = notification.object as? Bool{
            if iscoach == true{
                videouploadedPopup.isCoachAngle = true
            }
        }
        videouploadedPopup.modalPresentationStyle = .overFullScreen
        self.present(videouploadedPopup, animated: false, completion: nil)
    }
    
    func addingGestures(){
        sidemenuView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openSideMenu)))
    }
    
    @objc func openSideMenu(){
        let navigate = StoryBoards.SideMenuStoryboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        navigate.delegate = homeViewModel
        let nav = self.navigationController
        DispatchQueue.main.async { //make sure all UI updates are on the main thread.
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(navigate, animated: false)
        }
    }
    
    func getBanners(){
        homeViewModel.homeApi { (banner) in
            self.bannersArray = banner
            if self.bannersArray?.count == 1{
                self.homebannerPageControl.numberOfPages = 0
            }else if self.bannersArray?.count ?? 0 > 1{
                self.homebannerView.isInfinite = true
                self.homebannerView.automaticSlidingInterval = 2.0
                self.homebannerPageControl.numberOfPages = self.bannersArray?.count ?? 0
            }
            self.homebannerView.reloadData()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}

extension HomeVC : FSPagerViewDataSource,FSPagerViewDelegate{
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.bannersArray?.count ?? 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = homebannerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.image = #imageLiteral(resourceName: "dummyimage")
        
     //   let animatedImage = FLAnimatedImage(animatedGIFData: UIImage(named: "empty"))
      //  let placeholder = SDFLAnimatedImage(animatedImage: animatedImage)
      //  let gif = self.bannersArray?[index].awsURL?.replacingOccurrences(of: " ", with: "%20") ?? ""
      //  cell.imageView?.sd_setImage(with: URL(string: gif))
        
        
//        DispatchQueue.global(qos: .background).async {
//            let gif = self.bannersArray?[index].awsURL?.replacingOccurrences(of: " ", with: "%20") ?? ""
//            let imagegif = UIImage.gifImageWithURL(gifUrl: gif)
//            DispatchQueue.main.async{
//                cell.imageView?.image = imagegif
//            }
//        }
        return cell
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.homebannerPageControl.currentPage = pagerView.currentIndex
    }
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.homebannerPageControl.currentPage = targetIndex
    }
}
