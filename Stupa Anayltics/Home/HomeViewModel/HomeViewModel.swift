//
//  HomeViewModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import Foundation
import UIKit
import FSPagerView

class HomeViewModel:NSObject{

    private var statictableviewheightConstraint: NSLayoutConstraint!
    private var root:UIViewController!
    private var staticItemsTableView: UITableView!
    private var alldataTableView: UITableView!
    private var alldatatableheightConstraint: NSLayoutConstraint!
    var storedOffsets = [Int: CGFloat]()
    var staticItems = [StaticItems]()
    var insideStupa = [DataInsideStupa]()
    
    init(root:UIViewController,staticItemsTableView:UITableView,alldataTableView:UITableView,alldatatableheightConstraint: NSLayoutConstraint,statictableviewheightConstraint: NSLayoutConstraint){
        self.root = root
        self.statictableviewheightConstraint = statictableviewheightConstraint
        self.staticItemsTableView = staticItemsTableView
        self.alldataTableView = alldataTableView
        self.alldatatableheightConstraint = alldatatableheightConstraint
    }
    
    func observers(){
        self.alldataTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        self.staticItemsTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }

    func homeApi(completion: @escaping([Banner]?) -> Void){
        let homeParams = HomeRequestModel(deviceType: StupaStrings.DeviceType, deviceID: StupaStrings.DeviceUUID ?? "", tokenValue: Utils().getToken())
        
        let encoder = JSONEncoder()
        let jsonData = try! encoder.encode(homeParams)
        
        Connector.sharedinstance.runDataApiwithModelandEncodedData(sender: root, data: jsonData, method: .post, parameters: [:], success: nil, url: ApiConstants.getHomeData, completion: { (Data, Error) in
            if let error = Error{
                print(error.localizedDescription)
                return
            }
            guard let response = Data else{
                return
            }
            
            self.insideStupa = response.data?.insideStupa ?? []
            
            if let userName = response.data?.user?.fullName {
                SUserDefaults.set(userName, forKey: SUDConstants.userName)
                SUserDefaults.synchronize()
            }
            if let userCountry = response.data?.user?.countryName {
                SUserDefaults.set(userCountry, forKey: SUDConstants.userCountry)
                SUserDefaults.synchronize()
            }
            if let userPic = response.data?.user?.profilePic {
                SUserDefaults.set(userPic, forKey: SUDConstants.userPic)
                SUserDefaults.synchronize()
            }
            
            if let opponents = response.data?.opponents, opponents.count == 2{
                if let rightplayerName = opponents[0].playerName{
                    SUserDefaults.set(rightplayerName, forKey: SUDConstants.RightplayerName)
                    SUserDefaults.synchronize()
                }
                if let rightplayerId = opponents[0].playerID{
                    SUserDefaults.set(rightplayerId, forKey: SUDConstants.RightyplayerID)
                    SUserDefaults.synchronize()
                }
                if let leftplayerName = opponents[1].playerName{
                    SUserDefaults.set(leftplayerName, forKey: SUDConstants.LeftplayerName)
                    SUserDefaults.synchronize()
                }
                if let leftplayerId = opponents[1].playerID{
                    SUserDefaults.set(leftplayerId, forKey: SUDConstants.LeftyplayerID)
                    SUserDefaults.synchronize()
                }
            }
            
            self.alldataTableView.reloadData()
            
            completion(response.data?.banners)
            
        }, type: HomeResponseModel.self)
        
    }
    
    func settingupStaticItems(){
        let staticitem11Data = StaticItemsData(name: "Match", image: #imageLiteral(resourceName: "matchHomeStatic"))
        let staticitem12Data = StaticItemsData(name: "Practice", image: #imageLiteral(resourceName: "practiceHomeStatic"))
        let staticitemData = [staticitem11Data,staticitem12Data]
        
        let staticitem21Data = StaticItemsData(name: "My Analysis", image: #imageLiteral(resourceName: "myanalysisHomeStatic"))
        let staticitem22Data = StaticItemsData(name: "Shared Matches", image: #imageLiteral(resourceName: "backGroundImage"))
        let staticitem23Data = StaticItemsData(name: "My Dashboard", image: #imageLiteral(resourceName: "mydashboardHomeStatic"))
        let staticitem24Data = StaticItemsData(name: "Sample Analysis", image: #imageLiteral(resourceName: "sampleanalysisHomeStatic"))
        let staticitem2Data = [staticitem21Data,staticitem22Data,staticitem23Data,staticitem24Data]
        
        let staticitem1 = StaticItems(name: "Start Analyzing", data: staticitemData)
        let staticitem2 = StaticItems(name: "My Analysis", data: staticitem2Data)
        staticItems = [staticitem1,staticitem2]
        self.staticItemsTableView.reloadData()
    }
    
    func logout(){
        MyLoader.showLoadingView()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            Utils().deleteUserDefaults {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "IntroVC")
                let navigationController = UINavigationController(rootViewController: viewController)
                UIApplication.shared.keyWindow?.rootViewController = navigationController
                MyLoader.hideLoadingView()
            }
        }
    }
    
    func openPopUp(){
        let selectMatchPopUpVC = StoryBoards.ExtrasStoryboard.instantiateViewController(withIdentifier: "SelectMatchPopUpVC") as! SelectMatchPopUpVC
        selectMatchPopUpVC.naviagatedelegate = self
        selectMatchPopUpVC.modalPresentationStyle = .overFullScreen
        root.present(selectMatchPopUpVC, animated: false, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension HomeViewModel: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == staticItemsTableView{
            return staticItems.count
        }
        return insideStupa.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == staticItemsTableView{
            let cell = staticItemsTableView.dequeueReusableCell(withIdentifier: "staticitems", for: indexPath) as! StaticItemsTVC
            cell.headingLabel.text = self.staticItems[indexPath.row].name
            return cell
        }
        
        if insideStupa[indexPath.row].type == "World Rank Users"{
            let cell = alldataTableView.dequeueReusableCell(withIdentifier: "image", for: indexPath) as! ImageTVC
            cell.updateImage()
            cell.startScheduler()
            return cell
        }
        
        let cell = alldataTableView.dequeueReusableCell(withIdentifier: "allitems", for: indexPath) as! AllDataTVC
        cell.headingLabel.text = self.insideStupa[indexPath.row].type
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == staticItemsTableView{
            guard let tableViewCell = cell as? StaticItemsTVC else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }else if insideStupa[indexPath.row].type != "World Rank Users"{
            guard let tableViewCell = cell as? AllDataTVC else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == staticItemsTableView{
            guard let tableViewCell = cell as? StaticItemsTVC else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }else if insideStupa[indexPath.row].type != "World Rank Users"{
            guard let tableViewCell = cell as? StaticItemsTVC else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == staticItemsTableView{
            let screensize = UIScreen.main.bounds
            let screenwidth = screensize.width
            return screenwidth/2.5
        }else if insideStupa[indexPath.row].type != "World Rank Users"{
            let screensize = UIScreen.main.bounds
            let screenwidth = screensize.width
            return screenwidth/1.55
        }else{
            let screensize = UIScreen.main.bounds
            let screenwidth = screensize.width
            return screenwidth + 60
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        self.alldataTableView.layer.removeAllAnimations()
        if insideStupa.count == 0{
            alldatatableheightConstraint.constant = 40
        }else{
            alldatatableheightConstraint.constant = alldataTableView.contentSize.height
        }
        
        self.staticItemsTableView.layer.removeAllAnimations()
        if staticItems.count == 0{
            statictableviewheightConstraint.constant = 40
        }else{
            statictableviewheightConstraint.constant = staticItemsTableView.contentSize.height
        }
        
    }
}

extension HomeViewModel: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.restorationIdentifier == "Static"{
            return staticItems[collectionView.tag].data?.count ?? 0
        }else{
            return insideStupa[collectionView.tag].insideStupa?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.restorationIdentifier == "Static"{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! StaticItemsCVC
            cell.contentView.layer.borderWidth = 0.65
            cell.contentView.layer.borderColor = Colors.AppGreenColor.cgColor
            cell.contentView.layer.masksToBounds = true
            cell.contentView.layer.cornerRadius = 10.0
            cell.layer.shadowColor = UIColor.darkGray.cgColor
            cell.layer.shadowOffset = .zero
            cell.layer.shadowRadius = 3.0
            cell.layer.shadowOpacity = 0.75
            cell.layer.masksToBounds = false
            
            if !staticItems.isEmpty{
                if let item = staticItems[collectionView.tag].data?[indexPath.row]{
                    cell.item = item
                }
            }
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "datacell", for: indexPath) as! AllDataCVC
            cell.contentView.layer.borderWidth = 1.0
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = true
            cell.contentView.layer.cornerRadius = 8.0
            cell.layer.shadowColor = UIColor.darkGray.cgColor
            cell.layer.shadowOffset = .zero
            cell.layer.shadowRadius = 2.0
            cell.layer.shadowOpacity = 0.75
            cell.layer.masksToBounds = false
            cell.item = insideStupa[collectionView.tag].insideStupa?[indexPath.row]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.restorationIdentifier == "Static"{
            if collectionView.tag == 1{
                if indexPath.row == 0{
                    let navigate = StoryBoards.MyAnalysisStoryboard.instantiateViewController(withIdentifier: "MyAnalysisVC")
                    root.navigationController?.pushViewController(navigate, animated: true)
                }
            }else if collectionView.tag == 0{
                if indexPath.row == 0{
                    openPopUp()
                }else if indexPath.row == 1{
                    let navigate = StoryBoards.MatchDetailsStoryboard.instantiateViewController(withIdentifier: "PracticeMatchDetailsVC") as! PracticeMatchDetailsVC
                    root.navigationController?.pushViewController(navigate, animated: true)
                }
            }
        }
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        if collectionView.restorationIdentifier == "Static"{
            let screensize = UIScreen.main.bounds
            let screenwidth = screensize.width
            let size = CGSize(width: screenwidth/2.7, height: screenwidth/4)
            return size
        }else{
            let screensize = UIScreen.main.bounds
            let screenwidth = screensize.width
            let size = CGSize(width: screenwidth/1.5, height: screenwidth/2)
            return size
        }
    }
    
}

extension HomeViewModel : sendData {
    func getPageId(id: Int) {
        switch id {
        case 0:
            print("Nothing")
        case 1:
            print("NO")
        case 2:
            print("NO")
        case 3:
            print("NO")
        case 4:
            print("NO")
        case 5:
            print("NO")
        case 6:
            print("NO")
        case 7:
            print("NO")
        case 8:
            let alert = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .alert)
            let yesaction = UIAlertAction(title: "Yes", style: .default){action -> Void in
                self.logout()
            }
            let noaction = UIAlertAction(title: "No", style: .default)
            alert.addAction(yesaction)
            alert.addAction(noaction)
            root.present(alert, animated: true, completion: nil)
        default:
            print("NO")
        }
    }
}

extension HomeViewModel:MatchSelected{
    func navigate(isCoach: Bool) {
        if isCoach{
            let selectmatchTypePopup = StoryBoards.ExtrasStoryboard.instantiateViewController(withIdentifier: "SelectMatchTypePopUp") as! SelectMatchTypePopUp
            selectmatchTypePopup.naviagatedelegate = self
            selectmatchTypePopup.modalPresentationStyle = .overFullScreen
            self.root.present(selectmatchTypePopup, animated: false, completion: nil)
        }else{
            let navigate = StoryBoards.MatchDetailsStoryboard.instantiateViewController(withIdentifier: "MatchDetailsVC") as! MatchDetailsVC
            navigate.isCoachAngle = false
            root.navigationController?.pushViewController(navigate, animated: true)
        }
    }
}

extension HomeViewModel:MatchTypeSelected{
    func navigate(isRecord: Bool) {
        if isRecord{
            let navigate = StoryBoards.MatchDetailsStoryboard.instantiateViewController(withIdentifier: "MatchDetailsVC") as! MatchDetailsVC
            navigate.isCoachAngle = true
            root.navigationController?.pushViewController(navigate, animated: true)
        }else{
            let navigate = StoryBoards.ExtrasStoryboard.instantiateViewController(withIdentifier: "CustomUploadPopUpVC")
            root.navigationController?.pushViewController(navigate, animated: true)
        }
    }
}
