//
//  LoginViewModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 09/04/21.
//

import Foundation
import UIKit

class LoginViewModel:NSObject{

    private var root:UIViewController!
    private var emailTextfield:UITextField
    private var passwordTextfield:UITextField
    private var isEmail:Bool? = true

    init(root:UIViewController,emailTextfield:UITextField,passwordTextfield:UITextField,isEmail:Bool){
        self.root = root
        self.emailTextfield = emailTextfield
        self.passwordTextfield = passwordTextfield
        self.isEmail = isEmail
    }

    func loginApi(){
        
        let loginParams = LoginRequestModel(userName: emailTextfield.text ?? "", password: passwordTextfield.text ?? "", deviceID: StupaStrings.DeviceUUID ?? "", isEmail: true, deviceType: StupaStrings.DeviceType, notificationToken: StupaStrings.NotificationToken)
        
        let encoder = JSONEncoder()
        let jsonData = try! encoder.encode(loginParams)
        
        Connector.sharedinstance.runDataApiwithModelandEncodedData(sender: root, data: jsonData, method: .post, parameters: [:], success: nil, url: ApiConstants.login, completion: { (Data, Error) in
            if let error = Error{
                print(error.localizedDescription)
                return
            }
            guard let response = Data else{
                return
            }
            
            Utils().savedataonLogin(data: response.data)
            
            DispatchQueue.main.async {
                let viewController = StoryBoards.MainStoryboard.instantiateViewController(withIdentifier: "TabBar")
                UIApplication.shared.keyWindow?.rootViewController = viewController
            }
            
        }, type: LoginResponseModel.self)
        
    }

}
