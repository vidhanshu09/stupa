//
//  LoginVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 09/04/21.
//

import UIKit

class LoginVC: BaseVC {
    
    var isEmail:Bool? = true
    var loginViewModel:LoginViewModel!
    
    //MARK:- Objects
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var emailphonesegmentControl: UISegmentedControl!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var newtostupaLabel: UILabel!
    @IBOutlet weak var emailtitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        
       emailTextfield.text = "ritesh.k.ritz0212@gmail.com"
       passwordTextfield.text = "9808903955"
        
        self.setupBackIcon(view: backView)
        newtostupaLabel.text = "New to Stupa?\nSignup Now"
        let font = UIFont(name: "Montserrat-Medium", size: 16)!
        emailphonesegmentControl.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        addingGestures()
        loginViewModel = LoginViewModel(root: self, emailTextfield: emailTextfield, passwordTextfield: passwordTextfield, isEmail: isEmail ?? true)
    }
    
    func addingGestures(){
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popView)))
        newtostupaLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(navigatetosignupIntro)))
    }
    
    @objc func navigatetosignupIntro(){
        let navigate = self.storyboard?.instantiateViewController(withIdentifier: "SignupIntroVC")
        self.navigationController?.pushViewController(navigate!, animated: true)
    }
    
    @IBAction func forgotpassbtnAct(_ sender: Any) {
    }
    
    @IBAction func loginbtnAct(_ sender: Any) {
        if !checkValidation(){
            return
        }
        loginViewModel.loginApi()
    }
    
    func checkValidation() -> Bool{
        
        var isValidate = true
        
        if isEmail ?? true{
            guard let email = self.emailTextfield, email.text?.trimmingCharacters(in: .whitespaces) != "" else {
                self.showAlert(with: AlertConstants.enterEmail)
                isValidate = false
                return false
            }
            if !Utils().isValidEmail(email.text ?? ""){
                self.showAlert(with: AlertConstants.entervalidEmail)
                isValidate = false
                return false
            }
        }else{
            guard let phone = self.emailTextfield, phone.text?.trimmingCharacters(in: .whitespaces) != "" else {
                self.showAlert(with: AlertConstants.enterPhone)
                isValidate = false
                return false
            }
            if phone.text?.count != 10{
                self.showAlert(with: AlertConstants.entervalidPhone)
                isValidate = false
                return false
            }
        }
        
        guard let password = self.passwordTextfield, password.text?.trimmingCharacters(in: .whitespaces) != "" else {
            self.showAlert(with: AlertConstants.enterPassword)
            isValidate = false
            return false
        }
        
        return isValidate
    }
    @IBAction func emailphsegmentAct(_ sender: Any) {
        if emailphonesegmentControl.selectedSegmentIndex == 0{
            emailtitleLabel.text = "Email"
            emailTextfield.placeholder = "Email"
            emailTextfield.keyboardType = .emailAddress
            emailTextfield.text = ""
            isEmail = true
            emailTextfield.endEditing(true)
        }else{
            emailtitleLabel.text = "Phone"
            emailTextfield.placeholder = "Phone"
            emailTextfield.keyboardType = .numberPad
            emailTextfield.text = ""
            isEmail = false
            emailTextfield.endEditing(true)
        }
    }
}
