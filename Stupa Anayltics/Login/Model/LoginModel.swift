//
//  LoginModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import Foundation

// MARK: - LoginRequestModel
struct LoginRequestModel: Codable {
    let userName, password, deviceID: String
    let isEmail: Bool
    let deviceType, notificationToken: String

    enum CodingKeys: String, CodingKey {
        case userName = "UserName"
        case password = "Password"
        case deviceID = "DeviceId"
        case isEmail = "IsEmail"
        case deviceType = "DeviceType"
        case notificationToken = "NotificationToken"
    }
}

// MARK: - LoginResponseModel
struct LoginResponseModel: Codable {
    let isSuccess: Bool?
    let message: String?
    let data: LoginResponseData?

    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case message = "Message"
        case data = "Data"
    }
}

// MARK: - DataClass
struct LoginResponseData: Codable {
    let token: String?
    let user: LoginUser?
    let player, opponent: Opponent?

    enum CodingKeys: String, CodingKey {
        case token = "Token"
        case user = "User"
        case player = "Player"
        case opponent = "Opponent"
    }
}

// MARK: - Opponent
struct Opponent: Codable {
    let playerID: Int?
    let playerNo: String?
    let playerName: String?
    let playingStyle: String?
    let playerType: String?
    let ghostUser: Bool?
    let doc: String?
    let userID, totalCount: Int?

    enum CodingKeys: String, CodingKey {
        case playerID = "PlayerId"
        case playerNo = "PlayerNo"
        case playerName = "PlayerName"
        case playingStyle = "PlayingStyle"
        case playerType = "PlayerType"
        case ghostUser = "GhostUser"
        case doc = "DOC"
        case userID = "UserId"
        case totalCount = "TotalCount"
    }
}

// MARK: - User
struct LoginUser: Codable {
    let userID: Int?
    let userType, fullName: String?
    let email: String?
    let isEmailVerified, isMobielVerified: Bool?
    let userName, password: String?
    let isAdmin, claimed: Bool?
    let deviceID, lastLoginOn: String?
    let loginCount: Int?
    let countryID: Int?
    let countryName: String?
    let profileType: Int?
    let allowVideoUpload: Bool?
    let notificationToken, tokenValue, deviceType: String?
    let isTermsRead, isSubscribe: Bool?
    let stripeCustomerID: String?
    let playerID: Int?
    let razorpayCustomerID: String?
    let isActive, isDeleted: Bool?
    let doc, dou: String?
    let createdBy, modifiedBy: Int?

    enum CodingKeys: String, CodingKey {
        case userID = "UserId"
        case userType = "UserType"
        case fullName = "FullName"
        case email = "Email"
        case isEmailVerified = "IsEmailVerified"
        case isMobielVerified = "IsMobielVerified"
        case userName = "UserName"
        case password = "Password"
        case isAdmin = "IsAdmin"
        case claimed = "Claimed"
        case deviceID = "DeviceId"
        case lastLoginOn = "LastLoginOn"
        case loginCount = "LoginCount"
        case countryID = "CountryId"
        case countryName = "CountryName"
        case profileType = "ProfileType"
        case allowVideoUpload = "AllowVideoUpload"
        case notificationToken = "NotificationToken"
        case tokenValue = "TokenValue"
        case deviceType = "DeviceType"
        case isTermsRead = "IsTermsRead"
        case isSubscribe = "IsSubscribe"
        case stripeCustomerID = "Stripe_Customer_Id"
        case playerID = "PlayerId"
        case razorpayCustomerID = "Razorpay_Customer_Id"
        case isActive = "IsActive"
        case isDeleted = "IsDeleted"
        case doc = "DOC"
        case dou = "DOU"
        case createdBy = "CreatedBy"
        case modifiedBy = "ModifiedBy"
    }
}
