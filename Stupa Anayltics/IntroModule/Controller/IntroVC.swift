//
//  IntroVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 09/04/21.
//

import UIKit
//import GoogleSignIn
//import FBSDKLoginKit

class IntroVC: UIViewController {
    
    var introViewModel: IntroViewModel!
    
    @IBOutlet weak var introImageView: UIImageView!
    
    @IBOutlet weak var fbBtn: UIView!
    @IBOutlet weak var googleBtn: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var contactView: UIView!
    
    let introScreenImagesList = ["login_adriana", "login_darko", "login_kanak", "login_manav"]
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateImage()
        startScheduler()
        addGestures()
        adjustinUI()
    }
    
    func adjustinUI(){
        introViewModel = IntroViewModel(root: self)
       // GIDSignIn.sharedInstance().delegate = introViewModel
       // GIDSignIn.sharedInstance()?.presentingViewController = self
        loginButton.layer.cornerRadius = 5
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func addGestures(){
        contactView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(navigatetoSignUp)))
        googleBtn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(googleSignIn)))
        fbBtn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(fbSignIn)))
    }
    
    @objc func googleSignIn(){
        //GIDSignIn.sharedInstance().signIn()
    }
    @objc func fbSignIn(){
       // introViewModel.fbLogin()
    }
    
    @objc func navigatetoSignUp(){
        let navigate = self.storyboard?.instantiateViewController(withIdentifier: "EulaViewController")
        self.navigationController?.pushViewController(navigate!, animated: true)
    }
    
    @IBAction func loginAct(_ sender: Any) {
        let navigate = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC")
        self.navigationController?.pushViewController(navigate!, animated: true)
    }
    
    
    func startScheduler() {
        DispatchQueue.global().asyncAfter(deadline: .now() + 3.0) {
            self.updateImage()
            self.startScheduler()
        }
    }
    
    func updateImage() {
        DispatchQueue.main.async {
            self.introImageView.image = UIImage(named: self.introScreenImagesList[self.counter])
            if self.counter != 3{
                self.counter += 1
            } else {
                self.counter = 0
            }
        }
    }
}
