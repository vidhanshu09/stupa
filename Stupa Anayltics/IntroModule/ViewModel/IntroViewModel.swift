//
//  IntroViewModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 09/04/21.
//

import Foundation
import UIKit
//import GoogleSignIn
//import FBSDKLoginKit

class IntroViewModel:NSObject {
    
    var root:UIViewController
    
    init(root:UIViewController){
        self.root = root
    }
    
//    func socialLoginApi(request:SocialLoginRequestModel?){
//
//        let encoder = JSONEncoder()
//        let jsonData = try! encoder.encode(request)
//        print(jsonData)
//
//        Connector.sharedinstance.runDataApiwithModelandEncodedData(sender: root, data: jsonData, method: .post, parameters: [:], success: nil,url: Constants.API.socialLogin, completion: { (Data, Error) in
//            guard let responseString = String(data: Data!, encoding: .utf8) else {
//                return
//            }
//            let respDictinary = Util().convertStringToDictionary(text: responseString)
//            if let data = respDictinary["Data"]{
//                CacheManager.shared.updateUserData(data: (respDictinary["Data"] as? [String : Any] ?? [:]))
//            }
//            self.checkIfLogInSucces(response: respDictinary)
//        })
//    }
//
//    private func checkIfLogInSucces(response:[String:Any]) {
//        if response["IsSuccess"] != nil && response["IsSuccess"] as? Int == 1 {
//            Logger.writeLog(log: "Logged In Succesfully")
//            DispatchQueue.main.async {
//
//
//
//
//                let viewController = self.root.storyboard?.instantiateViewController(withIdentifier: "HomeViewController")
//                let navigationController = UINavigationController(rootViewController: viewController!)
//                UIApplication.shared.keyWindow?.rootViewController = navigationController
//
////                let navigate = self.root.storyboard?.instantiateViewController(withIdentifier: "HomeViewController")
////                self.root.navigationController?.pushViewController(navigate!, animated: true)
//            }
//        } else {
//            Logger.writeLog(log: "LogIn Failure!!!")
//            DispatchQueue.main.async {
//                self.root.showAlertwithTitle(with: "Login Failed!!!", title: "Error!")
//            }
//        }
//    }
//
//
//    func fbLogin(){
//
//        let fbLoginManager : LoginManager = LoginManager()
//        fbLoginManager.logIn(permissions: ["email"], from: root, handler:{
//            (result, error) in
//            if ((error) != nil)
//            {
//                // Process error
//            }
//            else if (result?.isCancelled)!
//            {
//                // Handle cancellations
//            }
//            else
//            {
//                let fbloginresult : LoginManagerLoginResult = result!
//                if(fbloginresult.grantedPermissions.contains("email"))
//                {
//                    self.getFBUserData()
//                }
//            }
//        })
//    }
//
//    func getFBUserData()
//    {
//        if((AccessToken.current) != nil)
//        {
//            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name,gender, picture.type(large), email"]).start{ (connection, result, error)  in
//                if (error == nil)
//                {
//                    let info  = result as! NSDictionary
//                    let name = info["name"] as! String
//                    let email = info["email"] as! String
//                    //  let gender = info["gender"] as! String
//                    //    let dict = ["username":name,"emailid":email]
//
//                    print(name)
//                    print(email)
//                }
//            }
//        }else{
//            root.showAlertwithTitle(with: "We didn't get your email id.Check your privacy or try again.", title: "Sorry!")
//        }
//    }
//
//
//}
//
//extension IntroViewModel:GIDSignInDelegate{
//    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
//        if let error = error {
//            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
//                print("The user has not signed in before or they have since signed out.")
//            } else {
//                print("\(error.localizedDescription)")
//            }
//            return
//        }
//
//        var FullName = String()
//        var GoogleId = String()
//        var Email = String()
//
//        if let fullName = user.profile.name{
//            FullName = fullName
//        }
//        if let googleId = user.authentication.idToken{
//            GoogleId = googleId
//        }
//        if let email = user.profile.email{
//            Email = email
//        }
//
//        let request = SocialLoginRequestModel(firstname: FullName, lastname: "", phone: "", FacebookId: "", googleId: GoogleId, DeviceId:  Util().getDeviceId(), DeviceType: AppConstants.DeviceType, NotificationToken: AppConstants.NotificationToken,email: Email)
//
//
//       // self.socialLoginApi(request: request)
//
//
//
//        //        DispatchQueue.main.async {
//        //            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        //            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "CameraController") as! CameraController
//        //            UIApplication.shared.keyWindow?.rootViewController = viewController
//        //        }
//
//    }
}
