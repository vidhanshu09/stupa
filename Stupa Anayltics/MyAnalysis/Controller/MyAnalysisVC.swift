//
//  MyAnalysisVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 15/04/21.
//

import UIKit
import CarbonKit

class MyAnalysisVC: BaseVC,CarbonTabSwipeNavigationDelegate {

    //MARK:- Objects
    
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var carbonView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    func adjustingUI(){
        settingCarbonKit()
        setNavigationwithTitle(navView: navigationView, navTitle: "My Analysis")
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func settingCarbonKit(){
        let items = ["Match", "Practice"]
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items ,delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: carbonView)
        carbonTabSwipeNavigation.setIndicatorColor(Colors.AppGreenColor)
        carbonTabSwipeNavigation.setNormalColor(UIColor.white, font: UIFont(name: "Montserrat-Medium", size:14)!)
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.backgroundColor = UIColor.black
        carbonTabSwipeNavigation.setSelectedColor(Colors.AppGreenColor, font: UIFont(name: "Montserrat-Medium", size:15)!)
        let ScreenWidth = UIScreen.main.bounds.width
        carbonTabSwipeNavigation.setTabExtraWidth(ScreenWidth/2 - 68)
        carbonTabSwipeNavigation.setTabBarHeight(50)
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {

        guard let storyboard = storyboard else { return UIViewController() }
        if index == 0{
            return storyboard.instantiateViewController(withIdentifier: "MatchAnalysisVC")
        }
        else{
            return storyboard.instantiateViewController(withIdentifier: "PracticeAnalysisVC")
        }

    }
    
}
