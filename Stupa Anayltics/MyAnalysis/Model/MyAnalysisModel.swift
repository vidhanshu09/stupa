//
//  MyAnalysisModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 18/04/21.
//

import Foundation

// MARK: - MatchAnalysisRequestModel
struct MatchAnalysisRequestModel: Codable {
    let deviceID, deviceType, tokenValue: String
    let pageSize, pageNo: Int

    enum CodingKeys: String, CodingKey {
        case deviceID = "DeviceId"
        case deviceType = "DeviceType"
        case tokenValue = "TokenValue"
        case pageSize = "PageSize"
        case pageNo = "PageNo"
    }
}

// MARK: - MatchAnalysisResponseModel
struct MatchAnalysisResponseModel: Codable {
    let isSuccess: Bool?
    let message: String?
    let data: MatchDataClass?

    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case message = "Message"
        case data = "Data"
    }
}

// MARK: - DataClass
struct MatchDataClass: Codable {
    let matches: [Match]?
    let practices: [Practice]?

    enum CodingKeys: String, CodingKey {
        case matches = "Matches"
        case practices = "Practices"
    }
}

// MARK: - Match
struct Match: Codable {
    let matchNumber: String?
    let matchID: Int?
    let matchAngle: String?
    let playerAID: Int?
    let playerAName: String?
    let playerBID: Int?
    let playerBName, matchDate, matchScore, matchScorePlayerA: String?
    let matchScorePlayerB: String?
    let matchType: String?
    let hasStats: Bool?
    let averageSpeed: Double?
    let packageID, userPackageID: Int?
    let isDefault: Bool?
    let sharedWithUserIDS: String?

    enum CodingKeys: String, CodingKey {
        case matchNumber = "MatchNumber"
        case matchID = "MatchId"
        case matchAngle = "MatchAngle"
        case playerAID = "PlayerAId"
        case playerAName = "PlayerAName"
        case playerBID = "PlayerBId"
        case playerBName = "PlayerBName"
        case matchDate = "MatchDate"
        case matchScore = "MatchScore"
        case matchScorePlayerA = "MatchScore_PlayerA"
        case matchScorePlayerB = "MatchScore_PlayerB"
        case matchType = "MatchType"
        case hasStats = "HasStats"
        case averageSpeed = "AverageSpeed"
        case packageID = "PackageId"
        case userPackageID = "UserPackageId"
        case isDefault = "IsDefault"
        case sharedWithUserIDS = "SharedWithUserIds"
    }
}

// MARK: - Practice
struct Practice: Codable {
    let matchID: Int?
    let matchAngle: String?
    let matchNumber: Int?
    let playerName: String?
    let totalShots: Int?
    let averageSpeed: Double?
    let matchDate: String?
    let matchType: String?
    let doc: String?
    let hasStats: Bool?
    let ballType: String?
    let placement, stroke, drillType: String?
    let packageID, userPackageID: Int?
    let isDefault: Bool?

    enum CodingKeys: String, CodingKey {
        case matchID = "MatchId"
        case matchAngle = "MatchAngle"
        case matchNumber = "MatchNumber"
        case playerName = "PlayerName"
        case totalShots = "TotalShots"
        case averageSpeed = "AverageSpeed"
        case matchDate = "MatchDate"
        case matchType = "MatchType"
        case doc = "DOC"
        case hasStats = "HasStats"
        case ballType = "BallType"
        case placement = "Placement"
        case stroke = "Stroke"
        case drillType = "DrillType"
        case packageID = "PackageId"
        case userPackageID = "UserPackageId"
        case isDefault = "IsDefault"
    }
}
