//
//  Connector.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias SuccessHandler = (_ response: JSON?) -> Void
typealias ErrorHandler = (_ error: Error?) -> Void
typealias FailureHandler = (_ error: String?) -> Void
typealias FinishedHandler = () -> Void

class Connector{
    
    static let sharedinstance = Connector()
    
    
    func runDataApiwithModelandEncodedData<T>(loaderStart:Bool? = true,loaderStop:Bool? = true,sender:UIViewController,data:Data,method:HTTPMethod,parameters: [String: Any],success: SuccessHandler!,url:String,completion: @escaping (T?,Error?) -> (),type:T.Type)where T:Codable{
        
        let Url = URL(string: url)
        var request = URLRequest(url: Url!)
        request.httpMethod = method.rawValue
        request.setValue("S1ur93EfKS92tu02pa", forHTTPHeaderField: "x-auth-token")
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = data
        
        if loaderStart ?? true{
            MyLoader.showLoadingView()
        }
        AF.request(request).responseJSON { response in
            
            //print(response)
            
            switch response.result{
            case .success( _):
                let jsonObj = JSON(response.data!)
                if (jsonObj["IsSuccess"].int == StupaStrings.successString) {
                        do {
                            let decodedData = try JSONDecoder().decode(T.self, from: response.data!)
                            if loaderStop ?? true{
                                MyLoader.hideLoadingView()
                            }
                            completion(decodedData,nil)
                        }catch let error{
                            print(error.localizedDescription)
                            completion(nil,error)
                            if loaderStop ?? true{
                                MyLoader.hideLoadingView()
                            }
                        }
                    } else {
                        if let errorMessage = jsonObj["Message"].string {
                            if errorMessage == StupaStrings.loginAgain{
                                Utils().deleteUserDefaults {
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "IntroVC")
                                    let navigationController = UINavigationController(rootViewController: viewController)
                                    navigationController.showAlertwithTitle(with: errorMessage, title: AlertConstants.Errortitle)
                                    UIApplication.shared.keyWindow?.rootViewController = navigationController
                                    return
                                }
                            }
                            sender.showAlertwithTitle(with: errorMessage, title: AlertConstants.Errortitle)
                            let error = NSError(domain:"", code:401, userInfo:[ NSLocalizedDescriptionKey: errorMessage]) as Error
                            if loaderStop ?? true{
                                MyLoader.hideLoadingView()
                            }
                            completion(nil,error)
                        }
                    }
            case .failure(let error):
                print(error.localizedDescription)
                CheckInternet.checkInternetandServer(error: error, sender: sender)
                MyLoader.hideLoadingView()
            }
            
        }
        
    }
    
    
}
