//
//  EAReceiveAnalysisVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 23/04/21.
//

import UIKit

class EAReceiveAnalysisVC: UIViewController {
    
    //MARK:- Objects
    
    @IBOutlet weak var playerBnameLabel: UILabel!
    @IBOutlet weak var playerAnameLabel: UILabel!
    @IBOutlet weak var totalserviceplayerBLabel: UILabel!
    @IBOutlet weak var totalserviceplayerALabel: UILabel!
    @IBOutlet weak var totalservicesView: UIView!
    @IBOutlet weak var totalservicesplayerbView: UIView!
    @IBOutlet weak var totalservicesheadingView: UIView!
    @IBOutlet weak var totalservicesplayerbheadingView: UIView!
    @IBOutlet weak var lengthconversionheadingView: UIView!
    @IBOutlet weak var lostlegendView: UIView!
    @IBOutlet weak var wonlegendView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
