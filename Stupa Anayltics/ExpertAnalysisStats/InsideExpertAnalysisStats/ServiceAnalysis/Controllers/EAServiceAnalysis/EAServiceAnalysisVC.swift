//
//  EAServiceAnalysisVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 23/04/21.
//

import UIKit

class EAServiceAnalysisVC: UIViewController {
    
    
    //MARK:- Objects
    
    @IBOutlet weak var longplayerBwonlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var shortplayerBlostlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var longplayerBlostlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var shortplayerAlostlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var shortplayerBwonLabel: UILabel!
    @IBOutlet weak var shortplayerBlostLabel: UILabel!
    @IBOutlet weak var longplayerBwonLabel: UILabel!
    @IBOutlet weak var longplayerBlostLabel: UILabel!
    @IBOutlet weak var shortplayerAwonLabel: UILabel!
    @IBOutlet weak var shortplayerAlostLabel: UILabel!
    @IBOutlet weak var longplayerAlostlabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var longplayerAWonLabel: UILabel!
    @IBOutlet weak var longplayerAlostLabel: UILabel!
    @IBOutlet weak var shortplayerBView: UIView!
    @IBOutlet weak var shortplayerAView: UIView!
    @IBOutlet weak var longplayerBView: UIView!
    @IBOutlet weak var longplayerAView: UIView!
    @IBOutlet weak var shortplayerBnameLabel: UILabel!
    @IBOutlet weak var longplayerBnameLabel: UILabel!
    @IBOutlet weak var shortplayerAnameLabel: UILabel!
    @IBOutlet weak var longplayerAnameLabel: UILabel!
    @IBOutlet weak var playerBnameLabel: UILabel!
    @IBOutlet weak var playerAnameLabel: UILabel!
    @IBOutlet weak var totalserviceplayerBLabel: UILabel!
    @IBOutlet weak var totalserviceplayerALabel: UILabel!
    @IBOutlet weak var totalservicesView: UIView!
    @IBOutlet weak var totalservicesplayerbView: UIView!
    @IBOutlet weak var totalservicesheadingView: UIView!
    @IBOutlet weak var totalservicesplayerbheadingView: UIView!
    @IBOutlet weak var dottedView: UIView!
    @IBOutlet weak var lengthconversionheadingView: UIView!
    @IBOutlet weak var lostlegendView: UIView!
    @IBOutlet weak var wonlegendView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
