//
//  EASummaryStatsVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 23/04/21.
//

import UIKit

class EASummaryStatsVC: UIViewController {
    
    var playerAStats,playerBStats:EAPlayerStats?
    var playerA, playerB: EAMatchPlayer?
    
    //MARK:- Objects
    
    @IBOutlet weak var playerAnameLabel: UILabel!
    @IBOutlet weak var playerswitchObj: UISwitch!
    @IBOutlet weak var playerBLabel: UILabel!
    @IBOutlet weak var playernameLabel: UILabel!
    @IBOutlet weak var totalservewinsLabel: UILabel!
    @IBOutlet weak var totalserveerrorsLabel: UILabel!
    @IBOutlet weak var servereturnwinsLabel: UILabel!
    @IBOutlet weak var servereturnerrorsLabel: UILabel!
    @IBOutlet weak var winsonthirdballLabel: UILabel!
    @IBOutlet weak var errorsonthirsballLabel: UILabel!
    @IBOutlet weak var gamepointssavedLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
    }
    
    func adjustingUI(){
        playerswitchObj.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        settingData(player: playerAStats)
    }
    
    func settingData(player:EAPlayerStats?){
        playerAnameLabel.text = playerA?.name
        playerBLabel.text = playerB?.name
        playernameLabel.text = playerA?.name
        totalservewinsLabel.text = player?.servePoint?.toString()
        totalserveerrorsLabel.text = player?.serverError?.toString()
        servereturnwinsLabel.text = player?.serveReturnWin?.toString()
        servereturnerrorsLabel.text = player?.serveReturnError?.toString() ?? "0"
        winsonthirdballLabel.text = player?.pointsOn3RDBall?.toString()
        errorsonthirsballLabel.text = player?.errorsOn3RDBall?.toString()
    }
    
    @IBAction func changeplayerswitchAct(_ sender: Any) {
        if playerswitchObj.isOn{
            settingData(player: playerBStats)
            playernameLabel.text = playerB?.name
        }else{
            settingData(player: playerAStats)
            playernameLabel.text = playerA?.name
        }
    }
    
}
