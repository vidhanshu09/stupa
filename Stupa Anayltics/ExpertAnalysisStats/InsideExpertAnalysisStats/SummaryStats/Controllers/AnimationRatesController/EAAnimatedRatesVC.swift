//
//  EAAnimatedRatesVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 23/04/21.
//

import UIKit
import WaveAnimationView

class EAAnimatedRatesVC: UIViewController {

    var playerA, playerB: EAMatchPlayer?
    var playerAStats,playerBStats:EAPlayerStats?
    var forehandwinnerWave: WaveAnimationView?
    var backhandwinnerWave: WaveAnimationView?
    var forehanderrorWave: WaveAnimationView?
    var backhanderrorWave: WaveAnimationView?
    
    //MARK:- Objects

    @IBOutlet weak var playerAnameLabel: UILabel!
    @IBOutlet weak var playerswitchObj: UISwitch!
    @IBOutlet weak var playerBLabel: UILabel!
    @IBOutlet weak var forehandwinnerView: UIView!
    @IBOutlet weak var backhandwinnerView: UIView!
    @IBOutlet weak var forehanderrorView: UIView!
    @IBOutlet weak var backhanderrorView: UIView!
    @IBOutlet weak var forehandwinnerLabel: UILabel!
    @IBOutlet weak var backhandwinnerLabel: UILabel!
    @IBOutlet weak var forehanderrorLabel: UILabel!
    @IBOutlet weak var backhanderrorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        settingWave(player: playerAStats)
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        playerswitchObj.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        forehandwinnerView.makeround()
        forehandwinnerView.layer.borderWidth = 2
        forehandwinnerView.layer.borderColor = Colors.AppWinGreenColor.cgColor
        
        forehanderrorView.makeround()
        forehanderrorView.layer.borderWidth = 2
        forehanderrorView.layer.borderColor = Colors.AppLoseRedColor.cgColor
        
        backhandwinnerView.makeround()
        backhandwinnerView.layer.borderWidth = 2
        backhandwinnerView.layer.borderColor = Colors.AppWinGreenColor.cgColor
        
        backhanderrorView.makeround()
        backhanderrorView.layer.borderWidth = 2
        backhanderrorView.layer.borderColor = Colors.AppLoseRedColor.cgColor
        
        playerAnameLabel.text = playerA?.name
        playerBLabel.text = playerB?.name
        
    }
    
    func settingWave(player:EAPlayerStats?){
        
        forehandwinnerWave?.removeFromSuperview()
        forehanderrorWave?.removeFromSuperview()
        backhandwinnerWave?.removeFromSuperview()
        backhanderrorWave?.removeFromSuperview()

        forehandwinnerWave = WaveAnimationView(frame: CGRect(x: 0, y: 0, width: forehandwinnerView.frame.size.width, height: forehandwinnerView.frame.size.height), color: UIColor.green.withAlphaComponent(0.5))
        forehandwinnerWave?.progress = (player?.forehandWinnerRate?.toFloat() ?? 0)/100
        forehandwinnerView.addSubview(forehandwinnerWave!)
        forehandwinnerWave?.startAnimation()
        self.forehandwinnerLabel.text = String(format: "%.0f", player?.forehandWinnerRate?.toFloat() ?? 0) + "%"
        self.forehandwinnerView.bringSubviewToFront(self.forehandwinnerLabel)

        forehanderrorWave = WaveAnimationView(frame: CGRect(origin: .zero, size: forehanderrorView.bounds.size), color: UIColor.red.withAlphaComponent(0.5))
        forehanderrorWave?.progress = (player?.forehandErrorRate?.toFloat() ?? 0)/100
        forehanderrorView.addSubview(forehanderrorWave!)
        forehanderrorWave?.startAnimation()
        self.forehanderrorLabel.text = String(format: "%.0f", player?.forehandErrorRate?.toFloat() ?? 0) + "%"
        self.forehanderrorView.bringSubviewToFront(self.forehanderrorLabel)

        backhandwinnerWave = WaveAnimationView(frame: CGRect(origin: .zero, size: backhandwinnerView.bounds.size), color: UIColor.green.withAlphaComponent(0.5))
        backhandwinnerWave?.progress = (player?.backhandWinnerRate?.toFloat() ?? 0)/100
        backhandwinnerView.addSubview(backhandwinnerWave!)
        backhandwinnerWave?.startAnimation()
        self.backhandwinnerLabel.text = String(format: "%.0f", player?.backhandWinnerRate?.toFloat() ?? 0) + "%"
        self.backhandwinnerView.bringSubviewToFront(self.backhandwinnerLabel)

        backhanderrorWave = WaveAnimationView(frame: CGRect(origin: .zero, size: backhanderrorView.bounds.size), color: UIColor.red.withAlphaComponent(0.5))
        backhanderrorWave?.progress = (player?.backhandErrorRate?.toFloat() ?? 0)/100
        backhanderrorView.addSubview(backhanderrorWave!)
        backhanderrorWave?.startAnimation()
        self.backhanderrorLabel.text = String(format: "%.0f", player?.backhandErrorRate?.toFloat() ?? 0) + "%"
        self.backhanderrorView.bringSubviewToFront(self.backhanderrorLabel)
        
    }
    
    @IBAction func changeplayerswitchAct(_ sender: Any) {
        if playerswitchObj.isOn{
            settingWave(player: playerBStats)
        }else{
            settingWave(player: playerAStats)
        }
    }
}
