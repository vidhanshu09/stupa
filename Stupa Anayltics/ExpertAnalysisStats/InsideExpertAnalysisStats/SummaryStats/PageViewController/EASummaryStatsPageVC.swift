//
//  EASummaryStatsPageVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 23/04/21.
//

import UIKit

protocol EASummaryStatsPageViewControllerDataSource: class
{
    func easummarystatsPageViewController(_ pageViewController: EASummaryStatsPageVC, numberOfPages pages: Int)
}

protocol EASummaryStatsPageViewControllerDelegate: class
{
    func easummarystatsPageViewController(_ pageViewController: EASummaryStatsPageVC, didChangePageIndex index: Int)
}

class EASummaryStatsPageVC: UIPageViewController {
    
    var playerAStats, playerBStats: EAPlayerStats?
    var playerA, playerB: EAMatchPlayer?
    weak var easummarystatsPageViewControllerDataSource: EASummaryStatsPageViewControllerDataSource?
    weak var easummarystatsPageViewControllerDelegate: EASummaryStatsPageViewControllerDelegate?
    
    fileprivate(set) lazy var contentViewControllers: [UIViewController] =
    {
        return [
            self.contentViewController(withIdentifier: "EAAnimatedRatesVC"),
            self.contentViewController(withIdentifier: "EASummaryStatsVC")
        ]
    }()
    
    fileprivate var allowRotate: Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        dataSource = self
        delegate = self
        
        if let initialViewController = contentViewControllers.first as? EAAnimatedRatesVC
        {
            initialViewController.playerA = self.playerA
            initialViewController.playerB = self.playerB
            initialViewController.playerAStats = self.playerAStats
            initialViewController.playerBStats = self.playerBStats
            contentViewController(followingViewController: initialViewController)
        }
        
        if let easummarystatsVC = contentViewControllers[1] as? EASummaryStatsVC{
            easummarystatsVC.playerA = self.playerA
            easummarystatsVC.playerB = self.playerB
            easummarystatsVC.playerAStats = self.playerAStats
            easummarystatsVC.playerBStats = self.playerBStats
        }
        
        easummarystatsPageViewControllerDataSource?.easummarystatsPageViewController(self, numberOfPages: contentViewControllers.count)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}

extension EASummaryStatsPageVC
{
    fileprivate func contentViewController(withIdentifier identifier: String) -> UIViewController
    {
        return UIStoryboard(name: "ExpertAnalysis", bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
    
    fileprivate func contentViewController(followingViewController viewController: UIViewController, to direction: UIPageViewController.NavigationDirection = .forward)
    {
        setViewControllers([viewController], direction: direction, animated: true)
        {
            (finished) -> Void in
            self.didChangePage()
        }
    }
    
    fileprivate func didChangePage()
    {
        if let firstViewController = viewControllers?.first, let index = contentViewControllers.firstIndex(of: firstViewController)
        {
            
            print(firstViewController)
            
            easummarystatsPageViewControllerDelegate?.easummarystatsPageViewController(self, didChangePageIndex: index)
        }
    }
}

extension EASummaryStatsPageVC
{
    public func next()
    {
        if let visibleViewController = viewControllers?.first, let viewController = pageViewController(self, viewControllerAfter: visibleViewController)
        {
            
            print(viewController)
            print(visibleViewController)
            contentViewController(followingViewController: viewController)
            
        }
        
        
    }
    
    func pageTo(at index: Int)
    {
        if let firstViewController = viewControllers?.first, let currentIndex = contentViewControllers.firstIndex(of: firstViewController)
        {
            let direction: UIPageViewController.NavigationDirection = index >= currentIndex ? .forward : .reverse
            let viewController = contentViewControllers[index]
            
            contentViewController(followingViewController: viewController, to: direction)
        }
    }
}

extension EASummaryStatsPageVC: UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        guard let index = contentViewControllers.firstIndex(of: viewController) else { return nil }
        
        let previous = index - 1
        
        guard previous >= 0 else { return nil }
        guard contentViewControllers.count > previous else { return nil }
        
        return contentViewControllers[previous]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let index = contentViewControllers.firstIndex(of: viewController) else { return nil }
        
        let next = index + 1
  
        let count = contentViewControllers.count
        
        //guard count != next else { return contentViewControllers.first } // rotatable
        guard count != next else { return nil }
        guard count > next else { return nil }
        
        return contentViewControllers[next]
    }
}

extension EASummaryStatsPageVC: UIPageViewControllerDelegate
{
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        didChangePage()
    }
}
