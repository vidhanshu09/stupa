//
//  StrokeAnalysisTVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 23/04/21.
//

import UIKit

class StrokeAnalysisTVC: UITableViewCell {

    var superViewWidth = 0.0
    
    @IBOutlet weak var valuelabelssuperView: UIView!
    @IBOutlet weak var shotnameLabel: UILabel!
    @IBOutlet weak var value1Label: UILabel!
    @IBOutlet weak var value2Label: UILabel!
    @IBOutlet weak var value3Label: UILabel!
    @IBOutlet weak var value1labelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var value2labelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var value3labelwidthConstraint: NSLayoutConstraint!
    
    var stroke:EAMatchChartData?{
        didSet{
            self.valuelabelssuperView.backgroundColor = .clear
            shotnameLabel.text = stroke?.shotType
            value1Label.text = stroke?.errorShotNo?.toString()
            value2Label.text = stroke?.successshotNo?.toString()
            value3Label.text = stroke?.winnershortNo?.toString()
            self.superViewWidth = Double(self.valuelabelssuperView.frame.size.width)
            value1labelwidthConstraint.constant = CGFloat(getPercentageValueForError(percent: (stroke?.errorShotPer ?? 0)))
            value2labelwidthConstraint.constant = CGFloat(getPercentageValue(percent: (stroke?.successshotPer ?? 0)))
            value3labelwidthConstraint.constant = CGFloat(getPercentageValue(percent: (stroke?.winnershortPer ?? 0)))
        }
    }
    
    func getPercentageValue(percent:Double)  -> Double {
        return (percent*(self.superViewWidth*0.6))/100
    }
    func getPercentageValueForError(percent:Double)  -> Double {
        if percent < 20 {
            return ((percent*2)*(self.superViewWidth*0.4))/100
        }
        return (percent*(self.superViewWidth*0.4))/100
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
