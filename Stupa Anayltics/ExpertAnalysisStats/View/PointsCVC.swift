//
//  PointsCVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 23/04/21.
//

import UIKit

class PointsCVC: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var value2Label: UILabel!
    @IBOutlet weak var value1Label: UILabel!
    @IBOutlet weak var indexLabel: UILabel!
    @IBOutlet weak var backImageView: UIImageView!
    
    var game:EAMatchStatsGame?{
        didSet{
            nameLabel.text = game?.gameWinnerName
            indexLabel.text = game?.gameNumber
            value1Label.text = game?.playerAScore?.toString()
            value2Label.text = game?.playerBScore?.toString()
        }
    }
}
