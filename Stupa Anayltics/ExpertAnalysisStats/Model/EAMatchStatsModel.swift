//
//  EAMatchStatsModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 23/04/21.
//

import Foundation

// MARK: - EAMatchStatsRequestModel
struct EAMatchStatsRequestModel: Codable {
    let deviceID, deviceType, tokenValue: String
    let matchID: Int
    
    enum CodingKeys: String, CodingKey {
        case deviceID = "DeviceId"
        case deviceType = "DeviceType"
        case tokenValue = "TokenValue"
        case matchID = "MatchId"
    }
}

// MARK: - EAMatchStatsResponseModel
struct EAMatchStatsResponseModel: Codable {
    let isSuccess: Bool?
    let message: String?
    let data: EAMatchStatsResponseData?
    
    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case message = "Message"
        case data = "Data"
    }
}

// MARK: - EAMatchStatsResponseData
struct EAMatchStatsResponseData: Codable {
    //var winningAnalysis: WinningAnalysis?
    // var errorAnalysis: ErrorAnalysis?
    var errorPlacement: EAErrorPlacement?
    // var recievedAnalysis, servicedAnalysis: EdAnalysis?
    var summaryAnalysis: EASummaryAnalysis?
    var placementAnalysis: EAErrorPlacement?
    // var observationdata: String?
    // var errorDetailOnStroke: [ErrorDetailOnStroke]?
     var errorDistribution: [EAErrorDistribution]?
    
    enum CodingKeys: String, CodingKey {
        //  case winningAnalysis = "WinningAnalysis"
        //  case errorAnalysis = "ErrorAnalysis"
        case errorPlacement = "ErrorPlacement"
        // case recievedAnalysis = "RecievedAnalysis"
        // case servicedAnalysis = "ServicedAnalysis"
        case summaryAnalysis = "SummaryAnalysis"
        case placementAnalysis = "PlacementAnalysis"
        //  case observationdata = "Observationdata"
        //  case errorDetailOnStroke = "ErrorDetailOnStroke"
        case errorDistribution = "ErrorDistribution"
    }
}

// MARK: - EAErrorDistribution
struct EAErrorDistribution: Codable {
    let playerID: Int?
    let playerName: String?
    let errorType: ErrorType?
    let shotNumber, errorCount: Int?
    let remarksMessage: String?
    
    enum CodingKeys: String, CodingKey {
        case playerID = "PlayerId"
        case playerName = "PlayerName"
        case errorType = "ErrorType"
        case shotNumber = "ShotNumber"
        case errorCount = "ErrorCount"
        case remarksMessage = "RemarksMessage"
    }
}

enum ErrorType: String, Codable {
    case forced = "Forced"
    case others = "Others"
    case unforced = "Unforced"
}

// MARK: - EAErrorPlacement
struct EAErrorPlacement: Codable {
    let matchID, matchNumber: Int?
    let playerA, playerB: ErrorAnalysisPlayerA?
    let errorPlacementTable: EAErrorPlacementTable?
    let errorPlacementTotalRecords: ErrorPlacementTotalRecords?
    let ttBoards, ttBoard1, ttBoard2, ttBoard3: TtBoard?
    let ttBoard4, ttBoardOthers: TtBoard?
    let totalRecord: ErrorPlacementTotalRecords?
    let stots: [Stot]?
    
    enum CodingKeys: String, CodingKey {
        case matchID = "MatchId"
        case matchNumber = "MatchNumber"
        case playerA = "PlayerA"
        case playerB = "PlayerB"
        case errorPlacementTable = "ErrorPlacementTable"
        case errorPlacementTotalRecords = "ErrorPlacementTotalRecords"
        case ttBoards = "TTBoards"
        case ttBoard1 = "TTBoard_1"
        case ttBoard2 = "TTBoard_2"
        case ttBoard3 = "TTBoard_3"
        case ttBoard4 = "TTBoard_4"
        case ttBoardOthers = "TTBoard_others"
        case totalRecord = "TotalRecord"
        case stots = "Stots"
    }
}

// MARK: - ErrorAnalysisPlayerA
struct ErrorAnalysisPlayerA: Codable {
    let playerID: Int?
    let name: String?
    let totalError: Int?
    let playingStyle: String?
    let playerType: String?
    let rubberCombination: String?
    let worldRank: String?
    let gender: String?
    let totalService: Int?
    let score: String?
    let totalWinner: Int?
    
    enum CodingKeys: String, CodingKey {
        case playerID = "PlayerId"
        case name = "Name"
        case totalError = "TotalError"
        case playingStyle = "PlayingStyle"
        case playerType = "PlayerType"
        case rubberCombination = "RubberCombination"
        case worldRank = "WorldRank"
        case gender = "Gender"
        case totalService = "TotalService"
        case score = "Score"
        case totalWinner = "TotalWinner"
    }
}

// MARK: - EAErrorPlacementTable
struct EAErrorPlacementTable: Codable {
    let playerA, playerB, gTotal: EAGTotal?
    
    enum CodingKeys: String, CodingKey {
        case playerA = "PlayerA"
        case playerB = "PlayerB"
        case gTotal = "GTotal"
    }
}

// MARK: - GTotal
struct EAGTotal: Codable {
    let forcedOther, forcedOtherTotal, unforcedOtherBackhand, unforcedOtherForehand: Int?
    let unforcedOther, unforcedTotal, grandTotal: Int?
    
    enum CodingKeys: String, CodingKey {
        case forcedOther = "ForcedOther"
        case forcedOtherTotal = "ForcedOtherTotal"
        case unforcedOtherBackhand = "UnforcedOtherBackhand"
        case unforcedOtherForehand = "UnforcedOtherForehand"
        case unforcedOther = "UnforcedOther"
        case unforcedTotal = "UnforcedTotal"
        case grandTotal = "GrandTotal"
    }
}

// MARK: - ErrorPlacementTotalRecords
struct ErrorPlacementTotalRecords: Codable {
    let service, recieve, thirdContact, fourthContact: Int?
    let others: Int?
    let servicePer, recievePer, thirdContactPer, fourthContactPer: Double?
    let othersPer: Double?
    
    enum CodingKeys: String, CodingKey {
        case service = "Service"
        case recieve = "Recieve"
        case thirdContact = "ThirdContact"
        case fourthContact = "FourthContact"
        case others = "Others"
        case servicePer = "ServicePer"
        case recievePer = "RecievePer"
        case thirdContactPer = "ThirdContactPer"
        case fourthContactPer = "FourthContactPer"
        case othersPer = "OthersPer"
    }
}

// MARK: - Stot
struct Stot: Codable {
    let value: Int?
    let valuePer: Double?
    let label: String?
    
    enum CodingKeys: String, CodingKey {
        case value = "Value"
        case valuePer = "ValuePer"
        case label = "Label"
    }
}

// MARK: - TtBoard
struct TtBoard: Codable {
    let playerA, playerB: [String: PlayerAValue]?
    
    enum CodingKeys: String, CodingKey {
        case playerA = "PlayerA"
        case playerB = "PlayerB"
    }
}

// MARK: - PlayerAValue
struct PlayerAValue: Codable {
    let value: Int?
    let chartData: PlayerA?
    
    enum CodingKeys: String, CodingKey {
        case value = "Value"
        case chartData
    }
}

// MARK: - PlayerA
struct PlayerA: Codable {
    let labels: [String]?
    let dataSets: [DataSet]?
    
    enum CodingKeys: String, CodingKey {
        case labels
        case dataSets = "DataSets"
    }
}

// MARK: - DataSet
public struct DataSet: Codable {
    public let label: String?
    public let data: [Int]
}


// MARK: - EASummaryAnalysis
struct EASummaryAnalysis: Codable {
    let isSuccess: Bool?
    let message: String?
    let data: EASummaryResponseData?
    
    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case message = "Message"
        case data = "Data"
    }
}

// MARK: - EASummaryResponseData
struct EASummaryResponseData: Codable {
    // let matchID, matchNumber: Int?
    let playerA, playerB: EAMatchPlayer?
    let games: [EAMatchStatsGame]?
    let playerAStats, playerBStats: EAPlayerStats?
    let chartDataA, chartDataB: [EAMatchChartData]?
    
    enum CodingKeys: String, CodingKey {
        // case matchID = "MatchId"
        // case matchNumber = "MatchNumber"
        case playerA = "PlayerA"
        case playerB = "PlayerB"
        case games = "Games"
        case playerAStats = "PlayerAStats"
        case playerBStats = "PlayerBStats"
        case chartDataA = "ChartDataA"
        case chartDataB = "ChartDataB"
    }
}

// MARK: - EAMatchChartData
struct EAMatchChartData: Codable {
    let shotType: String?
    let errorShotNo: Int?
    let errorShotPer: Double?
    let successshotNo: Int?
    let successshotPer: Double?
    let winnershortNo: Int?
    let winnershortPer: Double?
    
    enum CodingKeys: String, CodingKey {
        case shotType = "ShotType"
        case errorShotNo = "ErrorShotNo"
        case errorShotPer = "ErrorShotPer"
        case successshotNo = "SuccessshotNo"
        case successshotPer = "SuccessshotPer"
        case winnershortNo = "WinnershortNo"
        case winnershortPer = "WinnershortPer"
    }
}

// MARK: - EAMatchStatsGame
struct EAMatchStatsGame: Codable {
    let gameID: Int?
    let gameNumber: String?
    let playerAScore, playerBScore, gameWinnerID: Int?
    let gameWinnerName: String?
    
    enum CodingKeys: String, CodingKey {
        case gameID = "GameId"
        case gameNumber = "GameNumber"
        case playerAScore = "PlayerAScore"
        case playerBScore = "PlayerBScore"
        case gameWinnerID = "GameWinnerId"
        case gameWinnerName = "GameWinnerName"
    }
}

// MARK: - Player
struct EAMatchPlayer: Codable {
    let playerID: Int?
    let name, score, playingStyle, playerType: String?
    let rubberCombination, worldRank, gender: String?
    
    enum CodingKeys: String, CodingKey {
        case playerID = "PlayerId"
        case name = "Name"
        case score = "Score"
        case playingStyle = "PlayingStyle"
        case playerType = "PlayerType"
        case rubberCombination = "RubberCombination"
        case worldRank = "WorldRank"
        case gender = "Gender"
    }
}

// MARK: - PlayerStats
struct EAPlayerStats: Codable {
    let matchAnalysisStatsID, matchID, playerID: Int?
    let forehandWinnerRate, backhandWinnerRate, forehandErrorRate, backhandErrorRate: Double?
    let servePoint: Int?
    let serverError, serveReturnWin, serveReturnError, pointsOn3RDBall, errorsOn3RDBall: Int?
    let biggestLead, gamePointsSaved: Int?
    let mostUserdServe, mostEffectiveServe, leastEffectiveServe, mostUsedServeReturn: String?
    let mostEffectiveReturn, leastEffectiveReturn, top2ErrorZonePlacement, top2HighestErrorRatePlacement: String?
    let top2LowestErrorRatePlacement, top2WinningOtherSidePlacements, top2HighestWinningOtherSideRatePlacement, top2LowestWinningOtherSideRatePlacement: String?
    let top1WinningSameSidePlacement, mostEffectiveWinningPlacement, leastEffectiveWinningPlacement, mostEffectiveStroke: String?
    let mostUsedStroke, leastEffectiveStroke, doc: String?
    
    enum CodingKeys: String, CodingKey {
        case matchAnalysisStatsID = "MatchAnalysisStatsId"
        case matchID = "MatchId"
        case playerID = "PlayerId"
        case forehandWinnerRate = "ForehandWinnerRate"
        case backhandWinnerRate = "BackhandWinnerRate"
        case forehandErrorRate = "ForehandErrorRate"
        case backhandErrorRate = "BackhandErrorRate"
        case servePoint = "ServePoint"
        case serveReturnError = "ServeReturnError"
        case serverError = "ServerError"
        case serveReturnWin = "ServeReturnWin"
        case pointsOn3RDBall = "PointsOn3rdBall"
        case errorsOn3RDBall = "ErrorsOn3rdBall"
        case biggestLead = "BiggestLead"
        case gamePointsSaved = "GamePointsSaved"
        case mostUserdServe = "MostUserdServe"
        case mostEffectiveServe = "MostEffectiveServe"
        case leastEffectiveServe = "LeastEffectiveServe"
        case mostUsedServeReturn = "MostUsedServeReturn"
        case mostEffectiveReturn = "MostEffectiveReturn"
        case leastEffectiveReturn = "LeastEffectiveReturn"
        case top2ErrorZonePlacement = "Top2ErrorZonePlacement"
        case top2HighestErrorRatePlacement = "Top2HighestErrorRatePlacement"
        case top2LowestErrorRatePlacement = "Top2LowestErrorRatePlacement"
        case top2WinningOtherSidePlacements = "Top2WinningOtherSidePlacements"
        case top2HighestWinningOtherSideRatePlacement = "Top2HighestWinningOtherSideRatePlacement"
        case top2LowestWinningOtherSideRatePlacement = "Top2LowestWinningOtherSideRatePlacement"
        case top1WinningSameSidePlacement = "Top1WinningSameSidePlacement"
        case mostEffectiveWinningPlacement = "MostEffectiveWinningPlacement"
        case leastEffectiveWinningPlacement = "LeastEffectiveWinningPlacement"
        case mostEffectiveStroke = "MostEffectiveStroke"
        case mostUsedStroke = "MostUsedStroke"
        case leastEffectiveStroke = "LeastEffectiveStroke"
        case doc = "DOC"
    }
}
