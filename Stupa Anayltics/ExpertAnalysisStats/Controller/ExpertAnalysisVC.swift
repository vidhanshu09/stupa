//
//  ExpertAnalysisVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 23/04/21.
//

import UIKit
import Charts

class ExpertAnalysisVC: BaseVC {
    
    var errorDistribution: [EAErrorDistribution]?
    var topWindow: CustomWindow?
    var matchNo:Int?
    var expertanalysisViewModel:ExpertAnalysisViewModel!
    var summaryAnalysis: EASummaryAnalysis?
    var errorPlacement: EAErrorPlacement?
    var placementAnalysis: EAErrorPlacement?
    
    //MARK:- Score/Game Objects
    
    @IBOutlet weak var playerAscoreView: UIView!
    @IBOutlet weak var playerAscoreLabel: UILabel!
    @IBOutlet weak var playerAnameLabel: UILabel!
    @IBOutlet weak var playerAhandLabel: UILabel!
    @IBOutlet weak var playerAstrongpartLabel: UILabel!
    @IBOutlet weak var playerAdescLabel: UILabel!
    @IBOutlet weak var playerAscoreheadingView: UIView!
    
    
    @IBOutlet weak var playerBscoreView: UIView!
    @IBOutlet weak var playerBscoreLabel: UILabel!
    @IBOutlet weak var playerBnameLabel: UILabel!
    @IBOutlet weak var playerBhandLabel: UILabel!
    @IBOutlet weak var playerBstrongpartLabel: UILabel!
    @IBOutlet weak var playerBdescLabel: UILabel!
    @IBOutlet weak var playerBscoreheadingView: UIView!
    
    //MARK:- Stroke Analysis Objects
    
    @IBOutlet weak var strokeplayerAnameLabel: UILabel!
    @IBOutlet weak var strokeplayerBnameLabel: UILabel!
    @IBOutlet weak var strokeplayerswitchObj: UISwitch!
    
    //MARK:- Ball Placement
    
    @IBOutlet weak var videocameraView: UIView!
    @IBOutlet weak var bpplayerAnameLabel: UILabel!
    @IBOutlet weak var bpplayerBnameLabel: UILabel!
    
    @IBOutlet var r1c1BtnA : UIButton!
    @IBOutlet var r1c1Lbl : UILabel!
    @IBOutlet var r1c1BtnB : UIButton!
    @IBOutlet var r1c2Btn : UIButton!
    @IBOutlet var r1c3BtnA : UIButton!
    @IBOutlet var r1c3Lbl : UILabel!
    @IBOutlet var r1c3BtnB : UIButton!
    
    @IBOutlet var r2c1BtnA : UIButton!
    @IBOutlet var r2c1BtnB : UIButton!
    @IBOutlet var r2c2Btn : UIButton!
    @IBOutlet var r2c3BtnA : UIButton!
    @IBOutlet var r2c3BtnB : UIButton!
    
    @IBOutlet var r3c1BtnA : UIButton!
    @IBOutlet var r3c1BtnB : UIButton!
    @IBOutlet var r3c1Lbl : UILabel!
    @IBOutlet var r3c2Btn : UIButton!
    @IBOutlet var r3c3BtnA : UIButton!
    @IBOutlet var r3c3BtnB : UIButton!
    @IBOutlet var r3c3Lbl : UILabel!
    
    @IBOutlet var r4c1BtnA : UIButton!
    @IBOutlet var r4c1BtnB : UIButton!
    @IBOutlet var r4c1Lbl : UILabel!
    @IBOutlet var r4c2Btn : UIButton!
    @IBOutlet var r4c3BtnA : UIButton!
    @IBOutlet var r4c3BtnB : UIButton!
    @IBOutlet var r4c3Lbl : UILabel!
    
    @IBOutlet var r5c1BtnA : UIButton!
    @IBOutlet var r5c1BtnB : UIButton!
    @IBOutlet var r5c2Btn : UIButton!
    @IBOutlet var r5c3BtnA : UIButton!
    @IBOutlet var r5c3BtnB : UIButton!
    
    @IBOutlet var r6c1BtnA : UIButton!
    @IBOutlet var r6c1BtnB : UIButton!
    @IBOutlet var r6c1Lbl : UILabel!
    @IBOutlet var r6c2Btn : UIButton!
    @IBOutlet var r6c3BtnA : UIButton!
    @IBOutlet var r6c3BtnB : UIButton!
    @IBOutlet var r6c3Lbl : UILabel!
    
    @IBOutlet weak var serviceView: UIView!
    @IBOutlet weak var receiveView: UIView!
    @IBOutlet weak var thirdballView: UIView!
    @IBOutlet weak var fourthballView: UIView!
    @IBOutlet weak var othersView: UIView!
    @IBOutlet weak var iserrorSwitch: UISwitch!
    
    //MARK:- Error made on Ball Contacts
    
    @IBOutlet weak var embcplayerAnameLabel: UILabel!
    @IBOutlet weak var embcplayerBnameLabel: UILabel!
    @IBOutlet weak var embcplayerswitchObj: UISwitch!
    @IBOutlet weak var barchartView: BarChartView!
    @IBOutlet weak var nooferrorsgraphLabel: UILabel!
    
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var strokeanalysisTableView: UITableView!
    @IBOutlet weak var strokeanalysistableheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var matchpointsCollectionView:UICollectionView!
    
    @IBOutlet weak var easummarystatsPageControl: UIPageControl!
    @IBOutlet weak var easerviceanalysisPageControl: UIPageControl!
    
    fileprivate var easummaryStatsPageViewController: EASummaryStatsPageVC?{
        didSet{
            easummaryStatsPageViewController?.easummarystatsPageViewControllerDataSource = self
            easummaryStatsPageViewController?.easummarystatsPageViewControllerDelegate = self
        }
    }
    
    fileprivate var easerviceanalysisPageViewController: EAServiceAnalysisPageVC?{
        didSet{
            easerviceanalysisPageViewController?.easerviceanalysisPageViewControllerDataSource = self
            easerviceanalysisPageViewController?.easerviceanalysisPageViewControllerDelegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        
        //This window is to dismiss Easytip View on any touch, observer called in ExpertAnalysisViewModel.
        topWindow = CustomWindow(frame: UIScreen.main.bounds)
        topWindow?.rootViewController = self
        topWindow?.windowLevel = UIWindow.Level.normal + 1
        topWindow?.isHidden = false
        
        setNavigationwithTitle(navView: navigationView, navTitle: "Match No. - \(matchNo ?? 0)",isbackNeeded: false){
            self.navigationController?.popToViewController(ofClass: MyAnalysisVC.self)
        }
        expertanalysisViewModel = ExpertAnalysisViewModel(root: self, matchpointsCollectionView: matchpointsCollectionView,strokeanalysisTableView: strokeanalysisTableView,strokeanalysistableheightConstraint: strokeanalysistableheightConstraint, summaryAnalysis: summaryAnalysis, strokes: self.summaryAnalysis?.data?.chartDataA,r1c1BtnA: r1c1BtnA, r1c1Lbl: r1c1Lbl, r1c1BtnB: r1c1BtnB, r1c2Btn: r1c2Btn, r1c3BtnA: r1c3BtnA, r1c3Lbl: r1c3Lbl, r1c3BtnB: r1c3BtnB, r2c1BtnA: r2c1BtnA, r2c1BtnB: r2c1BtnB, r2c2Btn: r2c2Btn, r2c3BtnA: r2c3BtnA, r2c3BtnB: r2c3BtnB, r3c1BtnA: r3c1BtnA, r3c1BtnB: r3c1BtnB, r3c1Lbl: r3c1Lbl, r3c2Btn: r3c2Btn, r3c3BtnA: r3c3BtnA, r3c3BtnB: r3c3BtnB, r3c3Lbl: r3c3Lbl, r4c1BtnA: r4c1BtnA, r4c1BtnB: r4c1BtnB, r4c1Lbl: r4c1Lbl, r4c2Btn: r4c2Btn, r4c3BtnA: r4c3BtnA, r4c3BtnB: r4c3BtnB, r4c3Lbl: r4c3Lbl, r5c1BtnA: r5c1BtnA, r5c1BtnB: r5c1BtnB, r5c2Btn: r5c2Btn, r5c3BtnA: r5c3BtnA, r5c3BtnB: r5c3BtnB, r6c1BtnA: r6c1BtnA, r6c1BtnB: r6c1BtnB, r6c1Lbl: r6c1Lbl, r6c2Btn: r6c2Btn, r6c3BtnA: r6c3BtnA, r6c3BtnB: r6c3BtnB, r6c3Lbl: r6c3Lbl,placementError: self.errorPlacement, placementAnalysis: self.placementAnalysis, matchNo: self.matchNo,chartView: self.barchartView,errorDistribution: self.errorDistribution)
        
        expertanalysisViewModel.settingData()
        expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoards)
        
        matchpointsCollectionView.dataSource = expertanalysisViewModel
        matchpointsCollectionView.delegate = expertanalysisViewModel
        strokeanalysisTableView.dataSource = expertanalysisViewModel
        expertanalysisViewModel.observers()
        settingScoregameData()
        strokeplayerswitchObj.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        embcplayerswitchObj.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        settingStrokeAnalysisData()
        serviceView.layer.borderWidth = 1
        serviceView.layer.borderColor = UIColor.white.cgColor
        receiveView.layer.borderWidth = 1
        receiveView.layer.borderColor = UIColor.white.cgColor
        thirdballView.layer.borderWidth = 1
        thirdballView.layer.borderColor = UIColor.white.cgColor
        fourthballView.layer.borderWidth = 1
        fourthballView.layer.borderColor = UIColor.white.cgColor
        othersView.layer.borderWidth = 1
        othersView.layer.borderColor = UIColor.white.cgColor
        iserrorSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        settingBallPlacement()
        addGestures()
        settingErrorsMadeBallContactsData()
        nooferrorsgraphLabel.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        topWindow = nil
    }
    
    func addGestures(){
        serviceView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(serviceAct)))
        receiveView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(recieveAct)))
        thirdballView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(thirdballAct)))
        fourthballView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(fourthballAct)))
        othersView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(othersAct)))
    }
    
    @objc func serviceAct(){
        if expertanalysisViewModel.selectedGame == 1{
            serviceView.backgroundColor = UIColor.black
            if expertanalysisViewModel.isErrorAndWinningSelected{
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoards)
            }else{
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoards)
            }
            expertanalysisViewModel.selectedGame = 0
        }else{
            expertanalysisViewModel.selectedGame = 1
            serviceView.backgroundColor = Colors.TableBackColor
            if expertanalysisViewModel.isErrorAndWinningSelected{
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoard1)
            }else{
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoard1)
            }
            receiveView.backgroundColor = UIColor.black
            thirdballView.backgroundColor = UIColor.black
            fourthballView.backgroundColor = UIColor.black
            othersView.backgroundColor = UIColor.black
        }
    }
    @objc func recieveAct(){
        if expertanalysisViewModel.selectedGame == 2{
            expertanalysisViewModel.selectedGame = 0
            if expertanalysisViewModel.isErrorAndWinningSelected{
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoards)
            }else{
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoards)
            }
            receiveView.backgroundColor = UIColor.black
        }else{
            expertanalysisViewModel.selectedGame = 2
            if expertanalysisViewModel.isErrorAndWinningSelected{
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoard2)
            }else{
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoard2)
            }
            receiveView.backgroundColor = Colors.TableBackColor
            serviceView.backgroundColor = UIColor.black
            thirdballView.backgroundColor = UIColor.black
            fourthballView.backgroundColor = UIColor.black
            othersView.backgroundColor = UIColor.black
        }
    }
    @objc func thirdballAct(){
        if expertanalysisViewModel.selectedGame == 3{
            expertanalysisViewModel.selectedGame = 0
            if expertanalysisViewModel.isErrorAndWinningSelected{
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoards)
            }else{
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoards)
            }
            thirdballView.backgroundColor = UIColor.black
        }else{
            expertanalysisViewModel.selectedGame = 3
            if expertanalysisViewModel.isErrorAndWinningSelected{
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoard3)
            }else{
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoard3)
            }
            thirdballView.backgroundColor = Colors.TableBackColor
            receiveView.backgroundColor = UIColor.black
            serviceView.backgroundColor = UIColor.black
            fourthballView.backgroundColor = UIColor.black
            othersView.backgroundColor = UIColor.black
        }
    }
    @objc func fourthballAct(){
        if expertanalysisViewModel.selectedGame == 4{
            expertanalysisViewModel.selectedGame = 0
            if expertanalysisViewModel.isErrorAndWinningSelected{
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoards)
            }else{
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoards)
            }
            fourthballView.backgroundColor = UIColor.black
        }else{
            expertanalysisViewModel.selectedGame = 4
            if expertanalysisViewModel.isErrorAndWinningSelected{
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoard4)
            }else{
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoard4)
            }
            fourthballView.backgroundColor = Colors.TableBackColor
            receiveView.backgroundColor = UIColor.black
            serviceView.backgroundColor = UIColor.black
            thirdballView.backgroundColor = UIColor.black
            othersView.backgroundColor = UIColor.black
        }
    }
    @objc func othersAct(){
        if expertanalysisViewModel.selectedGame == 7{
            expertanalysisViewModel.selectedGame = 0
            if expertanalysisViewModel.isErrorAndWinningSelected{
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoards)
            }else{
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoards)
            }
            othersView.backgroundColor = UIColor.black
        }else{
            expertanalysisViewModel.selectedGame = 7
            if expertanalysisViewModel.isErrorAndWinningSelected{
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoardOthers)
            }else{
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoardOthers)
            }
            othersView.backgroundColor = Colors.TableBackColor
            receiveView.backgroundColor = UIColor.black
            serviceView.backgroundColor = UIColor.black
            thirdballView.backgroundColor = UIColor.black
            fourthballView.backgroundColor = UIColor.black
        }
    }
    
    @IBAction func iserrorswitchAct(_ sender: Any) {
        
        expertanalysisViewModel.isErrorAndWinningSelected = iserrorSwitch.isOn
        if expertanalysisViewModel.isErrorAndWinningSelected {
            
            self.videocameraView.isHidden = false
            
            switch expertanalysisViewModel.selectedGame {
            case 0:
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoards)
            case 1:
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoard1)
            case 2:
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoard2)
            case 3:
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoard3)
            case 4:
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoard4)
            case 7:
                expertanalysisViewModel.setValuesAndColor(self.errorPlacement?.ttBoardOthers)
            default:
                print("Nothing")
            }
            
        } else {
            
            self.videocameraView.isHidden = true
            
            switch expertanalysisViewModel.selectedGame {
            case 0:
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoards)
            case 1:
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoard1)
            case 2:
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoard2)
            case 3:
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoard3)
            case 4:
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoard4)
            case 7:
                expertanalysisViewModel.setValuesAndColor(self.placementAnalysis?.ttBoardOthers)
            default:
                print("Nothing")
            }
            
        }
    }
    
    func settingScoregameData(){
        
        playerAscoreView.makeround()
        playerAscoreView.layer.borderWidth = 2
        playerAscoreView.layer.borderColor = Colors.AppLoseRedColor.cgColor
        
        playerBscoreView.makeround()
        playerBscoreView.layer.borderWidth = 2
        playerBscoreView.layer.borderColor = Colors.AppWinGreenColor.cgColor
        
        playerAscoreheadingView.layer.cornerRadius = 18
        playerBscoreheadingView.layer.cornerRadius = 18
        
        playerAscoreLabel.text = summaryAnalysis?.data?.playerA?.score
        playerAnameLabel.text = summaryAnalysis?.data?.playerA?.name?.uppercased()
        playerAhandLabel.text = summaryAnalysis?.data?.playerA?.playerType
        playerAstrongpartLabel.text = summaryAnalysis?.data?.playerA?.playingStyle
        playerAdescLabel.text = summaryAnalysis?.data?.playerA?.rubberCombination
        
        playerBscoreLabel.text = summaryAnalysis?.data?.playerB?.score
        playerBnameLabel.text = summaryAnalysis?.data?.playerB?.name?.uppercased()
        playerBhandLabel.text = summaryAnalysis?.data?.playerB?.playerType
        playerBstrongpartLabel.text = summaryAnalysis?.data?.playerB?.playingStyle
        playerBdescLabel.text = summaryAnalysis?.data?.playerB?.rubberCombination
    }
    
    func settingStrokeAnalysisData(){
        strokeplayerAnameLabel.text = self.summaryAnalysis?.data?.playerA?.name
        strokeplayerBnameLabel.text = self.summaryAnalysis?.data?.playerB?.name
    }
    
    func settingBallPlacement(){
        bpplayerAnameLabel.text = self.errorPlacement?.playerA?.name
        bpplayerBnameLabel.text = self.errorPlacement?.playerB?.name
    }
    
    func settingErrorsMadeBallContactsData(){
        
        
        
        
        let p1 = errorDistribution?.filter({$0.playerName == errorDistribution?.first?.playerName})
        let p2 = errorDistribution?.filter({$0.playerName != errorDistribution?.first?.playerName})
        self.embcplayerAnameLabel.text = p1?.first?.playerName
        self.embcplayerBnameLabel.text = p2?.first?.playerName
        expertanalysisViewModel.fileteredErrorDistribution = p1
        expertanalysisViewModel.setChart()
        expertanalysisViewModel.setChartData()
    }
    
    @IBAction func embcplayerswitchAct(_ sender: Any) {
        let p1 = errorDistribution?.filter({$0.playerName == errorDistribution?.first?.playerName})
        let p2 = errorDistribution?.filter({$0.playerName != errorDistribution?.first?.playerName})
        if embcplayerswitchObj.isOn {
            expertanalysisViewModel.fileteredErrorDistribution = p2
        } else {
            expertanalysisViewModel.fileteredErrorDistribution = p1
        }
        expertanalysisViewModel.setChartData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if let easummaryPageViewController = segue.destination as? EASummaryStatsPageVC
        {
            easummaryPageViewController.playerA = self.summaryAnalysis?.data?.playerA
            easummaryPageViewController.playerB = self.summaryAnalysis?.data?.playerB
            easummaryPageViewController.playerAStats = self.summaryAnalysis?.data?.playerAStats
            easummaryPageViewController.playerBStats = self.summaryAnalysis?.data?.playerBStats
            self.easummaryStatsPageViewController = easummaryPageViewController
        }
        if let easervicePageViewController = segue.destination as? EAServiceAnalysisPageVC
        {
            self.easerviceanalysisPageViewController = easervicePageViewController
        }
    }
    
    @IBAction func strokeplayerswitchAct(_ sender: Any) {
        if strokeplayerswitchObj.isOn{
            expertanalysisViewModel.strokes = self.summaryAnalysis?.data?.chartDataB
        }else{
            expertanalysisViewModel.strokes = self.summaryAnalysis?.data?.chartDataA
        }
        expertanalysisViewModel.reloadStrokes()
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension ExpertAnalysisVC: EASummaryStatsPageViewControllerDataSource{
    func easummarystatsPageViewController(_ pageViewController: EASummaryStatsPageVC, numberOfPages pages: Int) {
        easummarystatsPageControl.numberOfPages = pages
    }
}

extension ExpertAnalysisVC: EASummaryStatsPageViewControllerDelegate{
    func easummarystatsPageViewController(_ pageViewController: EASummaryStatsPageVC, didChangePageIndex index: Int) {
        easummarystatsPageControl.currentPage = index
    }
}

extension ExpertAnalysisVC: EAServiceAnalysisPageViewControllerDataSource{
    func easerviceanalysisPageViewController(_ pageViewController: EAServiceAnalysisPageVC, numberOfPages pages: Int) {
        easerviceanalysisPageControl.numberOfPages = pages
    }
}

extension ExpertAnalysisVC: EAServiceAnalysisPageViewControllerDelegate{
    func easerviceanalysisPageViewController(_ pageViewController: EAServiceAnalysisPageVC, didChangePageIndex index: Int) {
        easerviceanalysisPageControl.currentPage = index
    }
}
