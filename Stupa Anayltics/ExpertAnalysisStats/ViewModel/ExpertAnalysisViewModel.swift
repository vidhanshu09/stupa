//
//  ExpertAnalysisViewModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 23/04/21.
//

import Foundation
import EasyTipView
import Charts

class ExpertAnalysisViewModel:NSObject{
    
    var chartView: BarChartView!
    var errorDistribution: [EAErrorDistribution]?
    var fileteredErrorDistribution: [EAErrorDistribution]?
    var filteredVal1 : [EAErrorDistribution]?
    var filteredVal2 : [EAErrorDistribution]?
    var tipView:EasyTipView?
    var matchNo:Int?
    var isErrorAndWinningSelected = false
    var selectedGame = 0
    var strokes:[EAMatchChartData]?
    private var root:UIViewController!
    private var matchpointsCollectionView:UICollectionView!
    private var strokeanalysisTableView: UITableView!
    private var strokeanalysistableheightConstraint: NSLayoutConstraint!
    private var summaryAnalysis: EASummaryAnalysis?
    var placementError : EAErrorPlacement?
    var placementAnalysis : EAErrorPlacement?
    var isHighlightedColorA = false
    var isHighlightedColorB = false
    var maxValuePlayerA = 0
    var maxValuePlayerB = 0
    
    private var r1c1BtnA : UIButton!
    private var r1c1Lbl : UILabel!
    private var r1c1BtnB : UIButton!
    private var r1c2Btn : UIButton!
    private var r1c3BtnA : UIButton!
    private var r1c3Lbl : UILabel!
    private var r1c3BtnB : UIButton!

    private var r2c1BtnA : UIButton!
    private var r2c1BtnB : UIButton!
    private var r2c2Btn : UIButton!
    private var r2c3BtnA : UIButton!
    private var r2c3BtnB : UIButton!
    
    private var r3c1BtnA : UIButton!
    private var r3c1BtnB : UIButton!
    private var r3c1Lbl : UILabel!
    private var r3c2Btn : UIButton!
    private var r3c3BtnA : UIButton!
    private var r3c3BtnB : UIButton!
    private var r3c3Lbl : UILabel!

    private var r4c1BtnA : UIButton!
    private var r4c1BtnB : UIButton!
    private var r4c1Lbl : UILabel!
    private var r4c2Btn : UIButton!
    private var r4c3BtnA : UIButton!
    private var r4c3BtnB : UIButton!
    private var r4c3Lbl : UILabel!

    private var r5c1BtnA : UIButton!
    private var r5c1BtnB : UIButton!
    private var r5c2Btn : UIButton!
    private var r5c3BtnA : UIButton!
    private var r5c3BtnB : UIButton!
    
    private var r6c1BtnA : UIButton!
    private var r6c1BtnB : UIButton!
    private var r6c1Lbl : UILabel!
    private var r6c2Btn : UIButton!
    private var r6c3BtnA : UIButton!
    private var r6c3BtnB : UIButton!
    private var r6c3Lbl : UILabel!
    
    init(root:UIViewController,matchpointsCollectionView:UICollectionView,strokeanalysisTableView: UITableView,strokeanalysistableheightConstraint: NSLayoutConstraint,summaryAnalysis: EASummaryAnalysis?,strokes:[EAMatchChartData]?,r1c1BtnA : UIButton,r1c1Lbl : UILabel,r1c1BtnB : UIButton,r1c2Btn : UIButton,r1c3BtnA : UIButton,r1c3Lbl : UILabel,r1c3BtnB : UIButton,r2c1BtnA : UIButton,r2c1BtnB : UIButton,r2c2Btn : UIButton,r2c3BtnA : UIButton, r2c3BtnB : UIButton,r3c1BtnA : UIButton, r3c1BtnB : UIButton, r3c1Lbl : UILabel, r3c2Btn : UIButton,r3c3BtnA : UIButton, r3c3BtnB : UIButton, r3c3Lbl : UILabel,r4c1BtnA : UIButton,   r4c1BtnB : UIButton,r4c1Lbl : UILabel,r4c2Btn : UIButton,r4c3BtnA : UIButton,r4c3BtnB : UIButton,r4c3Lbl : UILabel,r5c1BtnA : UIButton,r5c1BtnB : UIButton,r5c2Btn : UIButton,r5c3BtnA : UIButton,r5c3BtnB : UIButton,r6c1BtnA : UIButton,r6c1BtnB : UIButton,r6c1Lbl : UILabel,r6c2Btn : UIButton,r6c3BtnA : UIButton,r6c3BtnB : UIButton,r6c3Lbl : UILabel,placementError : EAErrorPlacement?,placementAnalysis : EAErrorPlacement?,matchNo:Int?,chartView: BarChartView,errorDistribution: [EAErrorDistribution]?){
        
        self.root = root
        self.strokes = strokes
        self.summaryAnalysis = summaryAnalysis
        self.matchpointsCollectionView = matchpointsCollectionView
        self.strokeanalysisTableView = strokeanalysisTableView
        self.strokeanalysistableheightConstraint = strokeanalysistableheightConstraint
        
        self.r1c1BtnA = r1c1BtnA
        self.r1c1Lbl = r1c1Lbl
        self.r1c1BtnB = r1c1BtnB
        self.r1c2Btn = r1c2Btn
        self.r1c3BtnA = r1c3BtnA
        self.r1c3Lbl = r1c3Lbl
        self.r1c3BtnB = r1c3BtnB

        self.r2c1BtnA = r2c1BtnA
        self.r2c1BtnB = r2c1BtnB
        self.r2c2Btn = r2c2Btn
        self.r2c3BtnA = r2c3BtnA
        self.r2c3BtnB = r2c3BtnB
        
        self.r3c1BtnA = r3c1BtnA
        self.r3c1BtnB = r3c1BtnB
        self.r3c1Lbl = r3c1Lbl
        self.r3c2Btn = r3c2Btn
        self.r3c3BtnA = r3c3BtnA
        self.r3c3BtnB = r3c3BtnB
        self.r3c3Lbl = r3c3Lbl

        self.r4c1BtnA = r4c1BtnA
        self.r4c1BtnB = r4c1BtnB
        self.r4c1Lbl = r4c1Lbl
        self.r4c2Btn = r4c2Btn
        self.r4c3BtnA = r4c3BtnA
        self.r4c3BtnB = r4c3BtnB
        self.r4c3Lbl = r4c3Lbl

        self.r5c1BtnA = r5c1BtnA
        self.r5c1BtnB = r5c1BtnB
        self.r5c2Btn = r5c2Btn
        self.r5c3BtnA = r5c3BtnA
        self.r5c3BtnB = r5c3BtnB
        
        self.r6c1BtnA = r6c1BtnA
        self.r6c1BtnB = r6c1BtnB
        self.r6c1Lbl = r6c1Lbl
        self.r6c2Btn = r6c2Btn
        self.r6c3BtnA = r6c3BtnA
        self.r6c3BtnB = r6c3BtnB
        self.r6c3Lbl = r6c3Lbl
        
        self.placementError = placementError
        self.placementAnalysis = placementAnalysis
        self.matchNo = matchNo
        
        self.chartView = chartView
        self.errorDistribution = errorDistribution
        
    }
    
    func settingData(){
        //Player 1
        r1c1BtnA.tag = 1
        r1c1BtnA.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r1c1BtnA.titleLabel?.textAlignment = .center
        r1c1BtnB.tag = 2
        r1c1BtnB.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r1c1BtnB.titleLabel?.textAlignment = .center
        r1c2Btn.tag = 3
        r1c2Btn.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r1c2Btn.titleLabel?.textAlignment = .center
        r1c3BtnA.tag = 4
        r1c3BtnA.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r1c3BtnA.titleLabel?.textAlignment = .center
        r1c3BtnB.tag = 5
        r1c3BtnB.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r1c3BtnB.titleLabel?.textAlignment = .center
        r2c1BtnA.tag = 6
        r2c1BtnA.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r2c1BtnA.titleLabel?.textAlignment = .center
        r2c1BtnB.tag = 7
        r2c1BtnB.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r2c1BtnB.titleLabel?.textAlignment = .center
        r2c2Btn.tag = 8
        r2c2Btn.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r2c2Btn.titleLabel?.textAlignment = .center
        r2c3BtnA.tag = 9
        r2c3BtnA.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r2c3BtnA.titleLabel?.textAlignment = .center
        r2c3BtnB.tag = 10
        r2c3BtnB.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r2c3BtnB.titleLabel?.textAlignment = .center
        r3c1BtnA.tag = 11
        r3c1BtnA.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r3c1BtnA.titleLabel?.textAlignment = .center
        r3c1BtnB.tag = 12
        r3c1BtnB.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r3c1BtnB.titleLabel?.textAlignment = .center
        r3c2Btn.tag = 13
        r3c2Btn.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r3c2Btn.titleLabel?.textAlignment = .center
        r3c3BtnA.tag = 14
        r3c3BtnA.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r3c3BtnA.titleLabel?.textAlignment = .center
        r3c3BtnB.tag = 15
        r3c3BtnB.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r3c3BtnB.titleLabel?.textAlignment = .center
        
        //Player 2
        r4c1BtnA.tag = 16
        r4c1BtnA.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r4c1BtnA.titleLabel?.textAlignment = .center
        r4c1BtnB.tag = 17
        r4c1BtnB.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r4c1BtnB.titleLabel?.textAlignment = .center
        r4c2Btn.tag = 18
        r4c2Btn.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r4c2Btn.titleLabel?.textAlignment = .center
        r4c3BtnA.tag = 19
        r4c3BtnA.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r4c3BtnA.titleLabel?.textAlignment = .center
        r4c3BtnB.tag = 20
        r4c3BtnB.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r4c3BtnB.titleLabel?.textAlignment = .center
        r5c1BtnA.tag = 21
        r5c1BtnA.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r5c1BtnA.titleLabel?.textAlignment = .center
        r5c1BtnB.tag = 22
        r5c1BtnB.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r5c1BtnB.titleLabel?.textAlignment = .center
        r5c2Btn.tag = 23
        r5c2Btn.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r5c2Btn.titleLabel?.textAlignment = .center
        r5c3BtnA.tag = 24
        r5c3BtnA.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r5c3BtnA.titleLabel?.textAlignment = .center
        r5c3BtnB.tag = 25
        r5c3BtnB.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r5c3BtnB.titleLabel?.textAlignment = .center
        r6c1BtnA.tag = 26
        r6c1BtnA.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r6c1BtnA.titleLabel?.textAlignment = .center
        r6c1BtnB.tag = 27
        r6c1BtnB.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r6c1BtnB.titleLabel?.textAlignment = .center
        r6c2Btn.tag = 28
        r6c2Btn.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r6c2Btn.titleLabel?.textAlignment = .center
        r6c3BtnA.tag = 29
        r6c3BtnA.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r6c3BtnA.titleLabel?.textAlignment = .center
        r6c3BtnB.tag = 30
        r6c3BtnB.addTarget(self, action: #selector(self.buttonsAction(_:)), for: .touchUpInside)
        r6c3BtnB.titleLabel?.textAlignment = .center
    }
    
    @objc func buttonsAction(_ sender:UIButton) {
        if !isErrorAndWinningSelected {
            return
        }
        let textVal = sender.titleLabel?.text?.components(separatedBy: "\n")
        var playerId = self.placementAnalysis?.playerA?.playerID ?? 0
        if sender.tag > 15 {
            playerId = self.placementAnalysis?.playerB?.playerID ?? 0
        }
        if (textVal?.first ?? "0") != "0" && sender.tag < 31 {
            let navigate = StoryBoards.MyAnalysisStoryboard.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
            navigate.matchNo = self.matchNo
            navigate.placement = textVal?.last ?? ""
            navigate.placementType = self.getPlacementType()
            navigate.playerId = playerId
            navigate.rType = 7
            navigate.videoType = .ErrorPlacement
            root.navigationController?.pushViewController(navigate, animated: true)
            return
        }
    }
    
    func observers(){
        self.strokeanalysisTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.dismisstooltip(notification:)), name: Notification.Name(rawValue: "eventTouch"), object: nil)
    }
    
    @objc func dismisstooltip(notification: Notification) {
        tipView?.dismiss()
    }
    
    func reloadStrokes(){
        strokeanalysisTableView.reloadData()
    }
    
    func setValuesAndColor(_ board:TtBoard?) {
        guard let ttBoard = board else {
            return
        }
        self.maxValuePlayerA = ttBoard.playerA?.sorted(by: { (obj1, obj2) -> Bool in
            (obj1.value.value ?? 0) > (obj2.value.value ?? 0)
        }).first?.value.value ?? 0
        self.maxValuePlayerB = ttBoard.playerB?.sorted(by: { (obj1, obj2) -> Bool in
            (obj1.value.value ?? 0) > (obj2.value.value ?? 0)
        }).first?.value.value ?? 0
        var playerAType = ""
        var playerBType = ""
        
        if isErrorAndWinningSelected {
            playerAType = self.placementError?.playerA?.playerType ?? ""
            playerBType = self.placementError?.playerB?.playerType ?? ""
        } else {
            playerAType = self.placementAnalysis?.playerA?.playerType ?? ""
            playerBType = self.placementAnalysis?.playerB?.playerType ?? ""
        }
        //Player A
        //Set up EBHL
        if playerAType == "Lefty" {
            isHighlightedColorA = false
            let setColor1 = self.getColorAndValue(type: ttBoard.playerA, key: "EBHL")
            self.r1c1BtnA.setTitle("\(setColor1.0)\nEBHL", for: .normal)
            self.r1c1BtnA.backgroundColor = (setColor1.1)
            //Set up EBHH
            let setColor2 = self.getColorAndValue(type: ttBoard.playerA, key: "EBHH")
            self.r2c1BtnA.setTitle("\(setColor2.0)\nEBHH", for: .normal)
            self.r2c1BtnA.backgroundColor = (setColor2.1)
            //Set up EBHS
            let setColor3 = self.getColorAndValue(type: ttBoard.playerA, key: "EBHS")
            self.r3c1BtnA.setTitle("\(setColor3.0)\nEBHS", for: .normal)
            self.r3c1BtnA.backgroundColor = (setColor3.1)
            //Set up BHL
            let setColor4 = self.getColorAndValue(type: ttBoard.playerA, key: "BHL")
            self.r1c1Lbl.text = "\(setColor4.0)\nBHL"
            self.r1c1BtnB.setTitle("\(setColor4.0)\nBHL", for: .normal)
            self.r1c1BtnB.setTitleColor((setColor4.1), for: .normal)
            self.r1c1BtnB.backgroundColor = (setColor4.1)
            //Set up BHH
            let setColor5 = self.getColorAndValue(type: ttBoard.playerA, key: "BHH")
            self.r2c1BtnB.setTitle("\(setColor5.0)\nBHH", for: .normal)
            self.r2c1BtnB.backgroundColor = (setColor5.1)
            //Set up BHS
            let setColor6 = self.getColorAndValue(type: ttBoard.playerA, key: "BHS")
            self.r3c1Lbl.text = "\(setColor6.0)\nBHS"
            self.r3c1BtnB.setTitle("\(setColor6.0)\nBHS", for: .normal)
            self.r3c1BtnB.setTitleColor((setColor6.1), for: .normal)
            self.r3c1BtnB.backgroundColor = (setColor6.1)
            //Set up CL
            let setColor7 = self.getColorAndValue(type: ttBoard.playerA, key: "CL")
            self.r1c2Btn.setTitle("\(setColor7.0)\nCL", for: .normal)
            self.r1c2Btn.backgroundColor = (setColor7.1)
            //Set up CH
            let setColor8 = self.getColorAndValue(type: ttBoard.playerA, key: "CH")
            self.r2c2Btn.setTitle("\(setColor8.0)\nCH", for: .normal)
            self.r2c2Btn.backgroundColor = (setColor8.1)
            //Set up CS
            let setColor9 = self.getColorAndValue(type: ttBoard.playerA, key: "CS")
            self.r3c2Btn.setTitle("\(setColor9.0)\nCS", for: .normal)
            self.r3c2Btn.backgroundColor = (setColor9.1)
            //Set up FHL
            let setColorA = self.getColorAndValue(type: ttBoard.playerA, key: "FHL")
            self.r1c3Lbl.text = "\(setColorA.0)\nFHL"
            self.r1c3BtnA.setTitle("\(setColorA.0)\nFHL", for: .normal)
            self.r1c3BtnA.setTitleColor((setColorA.1), for: .normal)
            self.r1c3BtnA.backgroundColor = ((setColorA).1)
            //Set up FHH
            let setColorB = self.getColorAndValue(type: ttBoard.playerA, key: "FHH")
            self.r2c3BtnA.setTitle("\(setColorB.0)\nFHH", for: .normal)
            self.r2c3BtnA.backgroundColor = (setColorB.1)
            //Set up FHS
            let setColorC = self.getColorAndValue(type: ttBoard.playerA, key: "FHS")
            self.r3c3Lbl.text = "\(setColorC.0)\nFHS"
            self.r3c3BtnA.setTitle("\(setColorC.0)\nFHS", for: .normal)
            self.r3c3BtnA.setTitleColor((setColorC.1), for: .normal)
            self.r3c3BtnA.backgroundColor = (setColorC.1)
            //Set up EFHL
            let setColorD = self.getColorAndValue(type: ttBoard.playerA, key: "EFHL")
            self.r1c3BtnB.setTitle("\(setColorD.0)\nEFHL", for: .normal)
            self.r1c3BtnB.backgroundColor = (setColorD.1)
            //Set up EFHH
            let setColorE = self.getColorAndValue(type: ttBoard.playerA, key: "EFHH")
            self.r2c3BtnB.setTitle("\(setColorE.0)\nEFHH", for: .normal)
            self.r2c3BtnB.backgroundColor = (setColorE.1)
            //Set up EFHS
            let setColorF = self.getColorAndValue(type: ttBoard.playerA, key: "EFHS")
            self.r3c3BtnB.setTitle("\(setColorF.0)\nEFHS", for: .normal)
            self.r3c3BtnB.backgroundColor = (setColorF.1)
        } else {
            isHighlightedColorA = false
            let setColor1 = self.getColorAndValue(type: ttBoard.playerA, key: "EFHL")
            self.r1c1BtnA.setTitle("\(setColor1.0)\nEFHL", for: .normal)
            self.r1c1BtnA.backgroundColor = (setColor1.1)
            //Set up EFHH
            let setColor2 = self.getColorAndValue(type: ttBoard.playerA, key: "EFHH")
            self.r2c1BtnA.setTitle("\(setColor2.0)\nEFHH", for: .normal)
            self.r2c1BtnA.backgroundColor = (setColor2.1)
            //Set up EFHS
            let setColor3 = self.getColorAndValue(type: ttBoard.playerA, key: "EFHS")
            self.r3c1BtnA.setTitle("\(setColor3.0)\nEFHS", for: .normal)
            self.r3c1BtnA.backgroundColor = (setColor3.1)
            //Set up FHL
            let setColor4 = self.getColorAndValue(type: ttBoard.playerA, key: "FHL")
            self.r1c1Lbl.text = "\(setColor4.0)\nFHL"
            self.r1c1BtnB.setTitle("\(setColor4.0)\nFHL", for: .normal)
            self.r1c1BtnB.setTitleColor((setColor4.1), for: .normal)
            self.r1c1BtnB.backgroundColor = (setColor4.1)
            //Set up FHH
            let setColor5 = self.getColorAndValue(type: ttBoard.playerA, key: "FHH")
            self.r2c1BtnB.setTitle("\(setColor5.0)\nFHH", for: .normal)
            self.r2c1BtnB.backgroundColor = (setColor5.1)
            //Set up FHS
            let setColor6 = self.getColorAndValue(type: ttBoard.playerA, key: "FHS")
            self.r3c1Lbl.text = "\(setColor6.0)\nFHS"
            self.r3c1BtnB.setTitle("\(setColor6.0)\nFHS", for: .normal)
            self.r3c1BtnB.setTitleColor((setColor6.1), for: .normal)
            self.r3c1BtnB.backgroundColor = (setColor6.1)
            //Set up CL
            let setColor7 = self.getColorAndValue(type: ttBoard.playerA, key: "CL")
            self.r1c2Btn.setTitle("\(setColor7.0)\nCL", for: .normal)
            self.r1c2Btn.backgroundColor = (setColor7.1)
            //Set up CH
            let setColor8 = self.getColorAndValue(type: ttBoard.playerA, key: "CH")
            self.r2c2Btn.setTitle("\(setColor8.0)\nCH", for: .normal)
            self.r2c2Btn.backgroundColor = (setColor8.1)
            //Set up CS
            let setColor9 = self.getColorAndValue(type: ttBoard.playerA, key: "CS")
            self.r3c2Btn.setTitle("\(setColor9.0)\nCS", for: .normal)
            self.r3c2Btn.backgroundColor = (setColor9.1)
            //Set up BHL
            let setColorA = self.getColorAndValue(type: ttBoard.playerA, key: "BHL")
            self.r1c3Lbl.text = "\(setColorA.0)\nBHL"
            self.r1c3BtnA.setTitle("\(setColorA.0)\nBHL", for: .normal)
            self.r1c3BtnA.setTitleColor((setColorA.1), for: .normal)
            self.r1c3BtnA.backgroundColor = (setColorA.1)
            //Set up BHH
            let setColorB = self.getColorAndValue(type: ttBoard.playerA, key: "BHH")
            self.r2c3BtnA.setTitle("\(setColorB.0)\nBHH", for: .normal)
            self.r2c3BtnA.backgroundColor = (setColorB.1)
            //Set up BHS
            let setColorC = self.getColorAndValue(type: ttBoard.playerA, key: "BHS")
            self.r3c3Lbl.text = "\(setColorC.0)\nBHS"
            self.r3c3BtnA.setTitle("\(setColorC.0)\nBHS", for: .normal)
            self.r3c3BtnA.setTitleColor((setColorC.1), for: .normal)
            self.r3c3BtnA.backgroundColor = (setColorC.1)
            //Set up EBHL
            let setColorD = self.getColorAndValue(type: ttBoard.playerA, key: "EBHL")
            self.r1c3BtnB.setTitle("\(setColorD.0)\nEBHL", for: .normal)
            self.r1c3BtnB.backgroundColor = (setColorD.1)
            //Set up EBHH
            let setColorE = self.getColorAndValue(type: ttBoard.playerA, key: "EBHH")
            self.r2c3BtnB.setTitle("\(setColorE.0)\nEBHH", for: .normal)
            self.r2c3BtnB.backgroundColor = (setColorE.1)
            //Set up EBHS
            let setColorF = self.getColorAndValue(type: ttBoard.playerA, key: "EBHS")
            self.r3c3BtnB.setTitle("\(setColorF.0)\nEBHS", for: .normal)
            self.r3c3BtnB.backgroundColor = (setColorF.1)
        }
        if playerBType == "Lefty" {
            //Player B
            //Set up EBHL
            isHighlightedColorB = false
            let setColor1 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "EBHL")
            self.r6c3BtnB.setTitle("\(setColor1.0)\nEBHL", for: .normal)
            self.r6c3BtnB.backgroundColor = (setColor1.1)
            //Set up EBHH
            let setColor2 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "EBHH")
            self.r5c3BtnB.setTitle("\(setColor2.0)\nEBHH", for: .normal)
            self.r5c3BtnB.backgroundColor = (setColor2.1)
            //Set up EBHS
            let setColor3 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "EBHS")
            self.r4c3BtnB.setTitle("\(setColor3.0)\nEBHS", for: .normal)
            self.r4c3BtnB.backgroundColor = (setColor3.1)
            //Set up BHL
            let setColor4 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "BHL")
            self.r6c3Lbl.text = "\(setColor4.0)\nBHL"
            self.r6c3BtnA.setTitle("\(setColor4.0)\nBHL", for: .normal)
            self.r6c3BtnA.setTitleColor((setColor4.1), for: .normal)
            self.r6c3BtnA.backgroundColor = (setColor4.1)
            //Set up BHH
            let setColor5 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "BHH")
            self.r5c3BtnA.setTitle("\(setColor5.0)\nBHH", for: .normal)
            self.r5c3BtnA.backgroundColor = (setColor5.1)
            //Set up BHS
            let setColor6 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "BHS")
            self.r4c3Lbl.text = "\(setColor6.0)\nBHS"
            self.r4c3BtnA.setTitle("\(setColor6.0)\nBHS", for: .normal)
            self.r4c3BtnA.setTitleColor((setColor6.1), for: .normal)
            self.r4c3BtnA.backgroundColor = (setColor6.1)
            //Set up CL
            let setColor7 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "CL")
            self.r6c2Btn.setTitle("\(setColor7.0)\nCL", for: .normal)
            self.r6c2Btn.backgroundColor = (setColor7.1)
            //Set up CH
            let setColor8 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "CH")
            self.r5c2Btn.setTitle("\(setColor8.0)\nCH", for: .normal)
            self.r5c2Btn.backgroundColor = (setColor8.1)
            //Set up CS
            let setColor9 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "CS")
            self.r4c2Btn.setTitle("\(setColor9.0)\nCS", for: .normal)
            self.r4c2Btn.backgroundColor = (setColor9.1)
            //Set up FHL
            let setColorA = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "FHL")
            self.r6c1Lbl.text = "\(setColorA.0)\nFHL"
            self.r6c1BtnB.setTitle("\(setColorA.0)\nFHL", for: .normal)
            self.r6c1BtnB.setTitleColor((setColorA.1), for: .normal)
            self.r6c1BtnB.backgroundColor = (setColorA.1)
            //Set up FHH
            let setColorB = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "FHH")
            self.r5c1BtnB.setTitle("\(setColorB.0)\nFHH", for: .normal)
            self.r5c1BtnB.backgroundColor = (setColorB.1)
            //Set up FHS
            let setColorC = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "FHS")
            self.r4c1Lbl.text = "\(setColorC.0)\nFHS"
            self.r4c1BtnB.setTitle("\(setColorC.0)\nFHS", for: .normal)
            self.r4c1BtnB.setTitleColor((setColorC.1), for: .normal)
            self.r4c1BtnB.backgroundColor = (setColorC.1)
            //Set up EFHL
            let setColorD = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "EFHL")
            self.r6c1BtnA.setTitle("\(setColorD.0)\nEFHL", for: .normal)
            self.r6c1BtnA.backgroundColor = (setColorD.1)
            //Set up EFHH
            let setColorE = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "EFHH")
            self.r5c1BtnA.setTitle("\(setColorE.0)\nEFHH", for: .normal)
            self.r5c1BtnA.backgroundColor = (setColorE.1)
            //Set up EFHS
            let setColorF = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "EFHS")
            self.r4c1BtnA.setTitle("\(setColorF.0)\nEFHS", for: .normal)
            self.r4c1BtnA.backgroundColor = (setColorF.1)
        } else {
            //Player B
            //Set up EFHL
            isHighlightedColorB = false
            let setColor1 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "EFHL")
            self.r6c3BtnB.setTitle("\(setColor1.0)\nEFHL", for: .normal)
            self.r6c3BtnB.backgroundColor = (setColor1.1)
            //Set up EFHH
            let setColor2 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "EFHH")
            self.r5c3BtnB.setTitle("\(setColor2.0)\nEFHH", for: .normal)
            self.r5c3BtnB.backgroundColor = (setColor2.1)
            //Set up EFHS
            let setColor3 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "EFHS")
            self.r4c3BtnB.setTitle("\(setColor3.0)\nEFHS", for: .normal)
            self.r4c3BtnB.backgroundColor = (setColor3.1)
            //Set up FHL
            let setColor4 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "FHL")
            self.r6c3Lbl.text = "\(setColor4.0)\nFHL"
            self.r6c3BtnA.setTitle("\(setColor4.0)\nFHL", for: .normal)
            self.r6c3BtnA.setTitleColor((setColor4.1), for: .normal)
            self.r6c3BtnA.backgroundColor = (setColor4.1)
            //Set up FHH
            let setColor5 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "FHH")
            self.r5c3BtnA.setTitle("\(setColor5.0)\nFHH", for: .normal)
            self.r5c3BtnA.backgroundColor = (setColor5.1)
            //Set up FHS
            let setColor6 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "FHS")
            self.r4c3Lbl.text = "\(setColor6.0)\nFHS"
            self.r4c3BtnA.setTitle("\(setColor6.0)\nFHS", for: .normal)
            self.r4c3BtnA.setTitleColor((setColor6.1), for: .normal)
            self.r4c3BtnA.backgroundColor = (setColor6.1)
            //Set up CL
            let setColor7 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "CL")
            self.r6c2Btn.setTitle("\(setColor7.0)\nCL", for: .normal)
            self.r6c2Btn.backgroundColor = (setColor7.1)
            //Set up CH
            let setColor8 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "CH")
            self.r5c2Btn.setTitle("\(setColor8.0)\nCH", for: .normal)
            self.r5c2Btn.backgroundColor = (setColor8.1)
            //Set up CS
            let setColor9 = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "CS")
            self.r4c2Btn.setTitle("\(setColor9.0)\nCS", for: .normal)
            self.r4c2Btn.backgroundColor = (setColor9.1)
            //Set up BHL
            let setColorA = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "BHL")
            self.r6c1Lbl.text = "\(setColorA.0)\nBHL"
            self.r6c1BtnB.setTitle("\(setColorA.0)\nBHL", for: .normal)
            self.r6c1BtnB.setTitleColor((setColorA.1), for: .normal)
            self.r6c1BtnB.backgroundColor = (setColorA.1)
            //Set up BHH
            let setColorB = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "BHH")
            self.r5c1BtnB.setTitle("\(setColorB.0)\nBHH", for: .normal)
            self.r5c1BtnB.backgroundColor = (setColorB.1)
            //Set up BHS
            let setColorC = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "BHS")
            self.r4c1Lbl.text = "\(setColorC.0)\nBHS"
            self.r4c1BtnB.setTitle("\(setColorC.0)\nBHS", for: .normal)
            self.r4c1BtnB.setTitleColor((setColorC.1), for: .normal)
            self.r4c1BtnB.backgroundColor = (setColorC.1)
            //Set up EBHL
            let setColorD = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "EBHL")
            self.r6c1BtnA.setTitle("\(setColorD.0)\nEBHL", for: .normal)
            self.r6c1BtnA.backgroundColor = (setColorD.1)
            //Set up EBHH
            let setColorE = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "EBHH")
            self.r5c1BtnA.setTitle("\(setColorE.0)\nEBHH", for: .normal)
            self.r5c1BtnA.backgroundColor = (setColorE.1)
            //Set up EBHS
            let setColorF = self.getColorAndValuePlayerB(type: ttBoard.playerB, key: "EBHS")
            self.r4c1BtnA.setTitle("\(setColorF.0)\nEBHS", for: .normal)
            self.r4c1BtnA.backgroundColor = (setColorF.1)
        }
    }
    
    func getColorAndValue(type:[String: PlayerAValue]?,key:String) -> (Int,UIColor) {
        let val = (type?[key]?.value ?? 0)
        if val == self.maxValuePlayerA && !isHighlightedColorA {
            //heiglighted
            isHighlightedColorA = true
            return (val,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
        } else {
            //blue
            return (val,UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0))
        }
    }
    
    func getColorAndValuePlayerB(type:[String: PlayerAValue]?,key:String) -> (Int,UIColor) {
        let val = (type?[key]?.value ?? 0)
        if val == self.maxValuePlayerB && !isHighlightedColorB {
            //heiglighted
            isHighlightedColorB = true
            return (val,UIColor(red: 2.0/255.0, green: 186.0/255.0, blue: 210.0/255.0, alpha: 1.0))
        } else {
            //blue
            return (val,UIColor(red: 0.0/255.0, green: 113.0/255.0, blue: 192.0/255.0, alpha: 1.0))
        }
    }
    
    @objc func showtooltip(tapGesture:UITapGestureRecognizer){
        let i : Int = tapGesture.view!.tag
        print(i)
        var preferences = EasyTipView.Preferences()
        preferences.drawing.font = UIFont.systemFont(ofSize: 11)
        preferences.drawing.foregroundColor = UIColor.black
        preferences.drawing.backgroundColor = UIColor.yellow
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.bottom
        tipView?.dismiss()
        let abbreviation = self.strokes?[i].shotType ?? ""
        let fullform = abbreviation.getMarkerValue()
        tipView = EasyTipView(text: fullform, preferences: preferences)
        tipView?.show(forView: tapGesture.view!)
    }
    
    func setChart()
    {
        chartView.delegate = self
        chartView.backgroundColor = .clear
        
        chartView.chartDescription!.enabled = false
        
       // chartView.maxVisibleCount = 40
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = false
        chartView.highlightFullBarEnabled = false
        chartView.xAxis.labelTextColor = .white
        chartView.leftAxis.labelTextColor = .white
        chartView.xAxis.axisLineColor = .white
        chartView.leftAxis.axisLineColor = .white
//        let leftAxis = chartView.leftAxis
//        leftAxis.axisMinimum = 0
        chartView.leftAxis.drawLabelsEnabled = false
        chartView.scaleYEnabled = false
        chartView.scaleXEnabled = true
        chartView.setVisibleXRangeMaximum(4)
        chartView.rightAxis.enabled = false
        chartView.xAxis.drawGridLinesEnabled = false
        chartView.leftAxis.drawGridLinesEnabled = false
        chartView.highlightPerDragEnabled = false
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.granularityEnabled = true
        xAxis.granularity = 1.0
        chartView.legend.enabled = false
//        chartView.legend = l
        
    }
    
    func setChartData() {
        self.filteredVal1 = fileteredErrorDistribution?.filter({$0.errorType == ErrorType.forced})
        self.filteredVal2 = fileteredErrorDistribution?.filter({$0.errorType == ErrorType.unforced})
        let val1 = self.filteredVal1?.map({Double($0.errorCount ?? 0)}) ?? []
        let val2 = self.filteredVal2?.map({Double($0.errorCount ?? 0)}) ?? []
        var yVals = [BarChartDataEntry]()
        
        if val1.count > val2.count{
            for i in 0..<val1.count {
                var value1 = 0.0
                var value2 = 0.0
                var value3 = 0.0
                if val1.count > i {
                    value1 = val1[i]
                }
                if val2.count > i {
                    value2 = val2[i]
                }
                if val2.count > i {
                    value3 = 0.1
                }
                let a = BarChartDataEntry(x: Double(i), yValues: [value1, value2,value3], icon: nil)
                yVals.append(a)
            }
        }else if val2.count >  val1.count{
            for i in 0..<val2.count {
                var value1 = 0.0
                var value2 = 0.0
                var value3 = 0.0
                if val1.count > i {
                    value1 = val1[i]
                }
                if val2.count > i {
                    value2 = val2[i]
                }
                if val2.count > i {
                    value3 = 0.1
                }
                let a = BarChartDataEntry(x: Double(i), yValues: [value1, value2,value3], icon: nil)
                yVals.append(a)
            }
        }else{
            for i in 0..<val1.count {
                var value1 = 0.0
                var value2 = 0.0
                var value3 = 0.0
                if val1.count > i {
                    value1 = val1[i]
                }
                if val2.count > i {
                    value2 = val2[i]
                }
                if val2.count > i {
                    value3 = 0.1
                }
                let a = BarChartDataEntry(x: Double(i), yValues: [value1, value2,value3], icon: nil)
                yVals.append(a)
            }
        }
    
        
        let set = BarChartDataSet(entries: yVals, label: "")
        set.valueFormatter = CustomIntFormatter()
        set.drawIconsEnabled = false
        set.colors = [Colors.AppLoseRedColor,Colors.AppGreenColor,.clear]
        set.stackLabels = ["Forced", "Unforced",""]
        set.highlightColor = .clear
        
        let data = BarChartData(dataSet: set)
        data.setValueFont(.systemFont(ofSize: 7, weight: .light))
        data.setValueTextColor(.white)
        chartView.fitBars = false
        chartView.data = data
        
        
    }
    
}

extension ExpertAnalysisViewModel:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return summaryAnalysis?.data?.games?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = matchpointsCollectionView.dequeueReusableCell(withReuseIdentifier: "points", for: indexPath) as! PointsCVC
        cell.game = summaryAnalysis?.data?.games?[indexPath.row]
        cell.indexLabel.makeround()
        cell.value1Label.makeround()
        cell.value2Label.makeround()
        cell.indexLabel.layer.borderWidth = 1
        cell.indexLabel.layer.borderColor = UIColor.white.cgColor
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true
        cell.contentView.layer.cornerRadius = 5
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 3.0
        cell.layer.shadowOpacity = 0.75
        cell.layer.masksToBounds = false
        return cell
    }
    
    func getPlacementType() -> Int {
        if selectedGame == 1 {
            return 2
        } else if selectedGame == 2 {
            return 3
        } else if  selectedGame == 3 {
            return 4
        } else if selectedGame == 4 {
            return 5
        } else if selectedGame == 7  {
            return 6
        } else {
            return 1
        }
    }
    
}

extension ExpertAnalysisViewModel:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return strokes?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = strokeanalysisTableView.dequeueReusableCell(withIdentifier: "stroke", for: indexPath) as! StrokeAnalysisTVC
        cell.stroke = strokes?[indexPath.row]
        cell.value1Label.isUserInteractionEnabled = true
        cell.value2Label.isUserInteractionEnabled = true
        cell.value3Label.isUserInteractionEnabled = true
        let tapGesture1 : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(showtooltip(tapGesture:)))
        cell.value1Label.tag = indexPath.row
        cell.value1Label.addGestureRecognizer(tapGesture1)
        let tapGesture2 : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(showtooltip(tapGesture:)))
        cell.value2Label.tag = indexPath.row
        cell.value2Label.addGestureRecognizer(tapGesture2)
        let tapGesture3 : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(showtooltip(tapGesture:)))
        cell.value3Label.tag = indexPath.row
        cell.value3Label.addGestureRecognizer(tapGesture3)
        return cell
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        self.strokeanalysisTableView.layer.removeAllAnimations()
        if strokes?.count == 0{
            strokeanalysistableheightConstraint.constant = 40
        }else{
            strokeanalysistableheightConstraint.constant = strokeanalysisTableView.contentSize.height
        }
    }
}

extension ExpertAnalysisViewModel:ChartViewDelegate{
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        if highlight.stackIndex == 0 {
            if let val1 = self.filteredVal1?[Int(entry.x)] {
                let navigate = StoryBoards.MyAnalysisStoryboard.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                navigate.matchNo = self.matchNo
                navigate.placement = "Forced"
                navigate.placementType = 0
                navigate.playerId = val1.playerID ?? 0
                navigate.rType = 8
                navigate.additionalParams = val1.shotNumber?.description ?? ""
                navigate.videoType = .ErrorPlacement
                root.navigationController?.pushViewController(navigate, animated: true)
            }
        } else if highlight.stackIndex == 1 {
            
            if (self.filteredVal2?.count ?? 0) >= (Int(entry.x)), let val2 = self.filteredVal2?[Int(entry.x)] {
                let navigate = StoryBoards.MyAnalysisStoryboard.instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                navigate.matchNo = self.matchNo
                navigate.placement = "Unforced"
                navigate.placementType = 0
                navigate.playerId = val2.playerID ?? 0
                navigate.rType = 8
                navigate.additionalParams = val2.shotNumber?.description ?? ""
                navigate.videoType = .ErrorPlacement
                root.navigationController?.pushViewController(navigate, animated: true)
            }
        }else{
            print("Done.")
        }
    }
}
