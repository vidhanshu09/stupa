//
//  ShowDemoVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 20/04/21.
//

import UIKit

class ShowDemoVC: UIViewController {

    var fixedTimeStamps:String?
    var videoId:Int?
    var playerSide:String?
    var matchAngle:Utils.matchAngle?
    var isPractice:Bool? = false
    
    //MARK:- Objects
    
    @IBOutlet weak var placeyourtripodLabel: UILabel!
    @IBOutlet weak var greentripodView: UIView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var backView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        startView.dropShadow(radius: startView.frame.height / 2)
        greentripodView.dropShadow(radius: greentripodView.frame.height / 2)
        addingGestures()
        if matchAngle == .CoachAngle{
            backgroundImageView.image = #imageLiteral(resourceName: "ic_tripod_coach")
            startView.isHidden = true
        }else{
            placeyourtripodLabel.isHidden = true
            greentripodView.isHidden = true
        }
    }
    
    func addingGestures(){
        startView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openthingstoRemember)))
        greentripodView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openthingstoRemember)))
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popView)))
    }
    
    @objc func openthingstoRemember(){
        let thingstorememberPopup = StoryBoards.ExtrasStoryboard.instantiateViewController(withIdentifier: "ThingstoRememberVC") as! ThingstoRememberVC
        thingstorememberPopup.delegate = self
        thingstorememberPopup.modalPresentationStyle = .overFullScreen
        self.present(thingstorememberPopup, animated: false, completion: nil)
    }
    
    @objc func popView(){
        self.navigationController?.popViewController(animated: true)
        self.tabBarController?.tabBar.isHidden = false
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    func openCamera(){
        let navigate = StoryBoards.VideoRecordingStoryboard.instantiateViewController(withIdentifier: "CameraVC") as! CameraVC
        if isPractice ?? false{
            navigate.isPractice = true
        }
        navigate.fixedTimeStamps = self.fixedTimeStamps ?? ""
        navigate.videoId = self.videoId
        navigate.playerSide = self.playerSide ?? ""
        navigate.matchAngle = self.matchAngle
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
    }
    
}

extension ShowDemoVC:ThingstoRememberDone{
    func update() {
        self.openCamera()
    }
}
