//
//  CustomUploadPopUpVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 22/04/21.
//

import UIKit

class CustomUploadPopUpVC: UIViewController {
    
    var selectedfileName = String()
    var selectedfileUrl = String()
    var selectedfileType = String()
    
    //MARK:- Objects
    
    @IBOutlet weak var fileselectionView: UIView!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var selectfileView: UIView!
    @IBOutlet weak var selectedfileTextfield: UITextField!
    @IBOutlet weak var videolinkTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    private func adjustingUI(){
        fileselectionView.layer.masksToBounds = true
        fileselectionView.layer.cornerRadius = 5
        addingGestures()
    }
    
    private func addingGestures(){
        closeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popView)))
        selectfileView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OpenGalleryforVideoSelection)))
    }
    
    @objc func popView(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextbtnAct(_ sender: Any) {
        if selectedfileTextfield.text?.isEmpty ?? false && videolinkTextfield.text?.isEmpty ?? false{
            self.showAlert(with: "Please select file or enter video url")
            return
        }else{
            let navigate = StoryBoards.MatchDetailsStoryboard.instantiateViewController(withIdentifier: "MatchDetailsVC") as! MatchDetailsVC
            navigate.isCoachAngle = true
            self.navigationController?.pushViewController(navigate, animated: true)
        }
    }
    
    @objc func OpenGalleryforVideoSelection(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            let picker = UIImagePickerController()
            picker.mediaTypes = ["public.movie"]
            picker.delegate = self
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
    }
    
}

extension CustomUploadPopUpVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @available(iOS 2.0, *)
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        if let imageUrl = info[.mediaURL] as? URL {
            selectedfileName = Utils().convertdoctoBase64andgetFileName(doc: imageUrl).1.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "trim.", with: "")
            print(selectedfileName)
            selectedfileUrl = Utils().convertdoctoBase64andgetFileName(doc: imageUrl).0
            selectedfileType = Utils().convertdoctoBase64andgetFileName(doc: imageUrl).2
            do {
                let docData = try Data(contentsOf: imageUrl as URL)
                print(docData)
            } catch {
                print("Unable to load data: \(error)")
            }
            selectedfileTextfield.text = "\(selectedfileName).\(selectedfileType)"
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    @available(iOS 2.0, *)
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        print("Canceled Picker")
        dismiss(animated: true, completion: nil)
    }
}
