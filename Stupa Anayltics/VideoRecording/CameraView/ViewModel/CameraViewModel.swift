//
//  CameraViewModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 21/04/21.
//

import Foundation
import UIKit

class CameraViewModel:NSObject{

    var coordinatesPercentDictionary = [Int:[String:Double]]()
    var chunkNumber:Int?
    var filename:String?
    var finalClip:Int?
    var videoId:Int?
    var playerSide:String?
    var matchAngle:Utils.matchAngle?
    private var root:UIViewController!
    
    init(root:UIViewController,matchAngle:Utils.matchAngle?){
        self.root = root
        self.matchAngle = matchAngle
    }
 
    func uploadVideoApi(url:URL){
        
        guard let userID = SUserDefaults.value(forKey: SUDConstants.userID) as? Int else{
            return
        }
        guard let playerName = SUserDefaults.value(forKey: SUDConstants.userName) as? String else{
            return
        }
        guard let playerId = SUserDefaults.value(forKey: SUDConstants.playerID) as? Int else{
            return
        }
        
        uploadVideotoAws(url: url)
        
        let uploadvideoParams = UploadVideoRequestModel(matchType: "", matchDate: "", deviceID: StupaStrings.DeviceUUID ?? "", userID: userID, playerSide: self.playerSide ?? "", playerID: playerId, playerName: playerName, ballType: "", deviceType: StupaStrings.DeviceType, tokenValue: Utils().getToken(), chunkNumber: self.chunkNumber ?? 0, finalClip: self.finalClip ?? 0, fileName: self.filename ?? "", videoID: self.videoId ?? 0, uploadType: "Upload", opponentName: "")
        
        print(uploadvideoParams)
        
        let encoder = JSONEncoder()
        let jsonData = try! encoder.encode(uploadvideoParams)
        
        Connector.sharedinstance.runDataApiwithModelandEncodedData(loaderStart:false,sender: root, data: jsonData, method: .post, parameters: [:], success: nil, url: ApiConstants.uploadVideo, completion: { (Data, Error) in
            if let error = Error{
                print(error.localizedDescription)
                return
            }
            guard let response = Data else{
                return
            }
            
            print(response)
            
        }, type: UploadVideoResponseModel.self)
        
    }
    
    func uploadVideotoAws(url:URL){
        let fileName = Utils().getFileNameFromURL(urlString: url.absoluteString)
        var video = AWSUploadVideo(id: String(format: "%d", 1), name: "name", path: fileName!, videoPath: url.path)
        let resolution = "1280*720"
        var metaData = [String : Any]()
        metaData["video_resolution"] = resolution
        metaData["table_detection"] = "manual"
        if coordinatesPercentDictionary.count == 4 {
            let st1 = coordinatesPercentDictionary[1]
            let st2 = coordinatesPercentDictionary[2]
            let st3 = coordinatesPercentDictionary[3]
            let st4 = coordinatesPercentDictionary[4]
            metaData["coordinates"]  = "[[\(st1!["x"]!),\(st1!["y"]!)],[\(st2!["x"]!),\(st2!["y"]!)],[\(st3!["x"]!),\(st3!["y"]!)],[\(st4!["x"]!),\(st4!["y"]!)]]"
        }
        
        if !fileName!.contains("setup.mp4") {
            video.metadata = metaData
        }
        
        AWSS3Manager.shared.multipartRequestUpload(file: video)
    }
    
}
