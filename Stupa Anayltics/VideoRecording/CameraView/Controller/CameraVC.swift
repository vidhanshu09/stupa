//
//  CameraVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 21/04/21.
//

import UIKit
import AVFoundation

class CameraVC: UIViewController {
    
    var isPractice:Bool? = false
    var isRecording = false
    var matchAngle:Utils.matchAngle?
    var cameraViewModel:CameraViewModel!
    var videoId:Int!
    var fixedTimeStamps = ""
    let cameraButton = UIView()
    let captureSession = AVCaptureSession()
    let movieOutput = AVCaptureMovieFileOutput()
    var previewLayer: AVCaptureVideoPreviewLayer!
    var activeInput: AVCaptureDeviceInput!
    var videoUploadFileArray = Array<String>()
    var outputURL: URL!
    var chunkNumber = 1
    var finalClip = 0
    var manualTapGestureView = UIView()
    var star1 = UIImageView()
    var star2 = UIImageView()
    var star3 = UIImageView()
    var star4 = UIImageView()
    var topLine = UIView()
    var bottomLine = UIView()
    let centerLine = UIView()
    var recTimer = Timer()
    var seconds = 0
    var minutes = 0
    var playerSide = String()
    var willswitchSides = Bool()
    var star1DefaultFrame = CGRect()
    var star2DefaultFrame = CGRect()
    var star3DefaultFrame = CGRect()
    var star4DefaultFrame = CGRect()
    let centerTextLabel = UILabel()
    var tableheight = CGFloat()
    var tap = UITapGestureRecognizer()
    var startsClicked = 0
    var coordinatesPercentDictionary = [Int:[String:Double]]()
    var coordinatesDictionary = [Int:[String:CGFloat]]()
    let shape = CAShapeLayer()
    var uploadAlert:UIAlertController?
    var ifStillUploading:Bool? = false
    
    //MARK:- Objects
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var nextarrowImageView: UIImageView!
    @IBOutlet weak var nextgameView: UIView!
    @IBOutlet weak var videodetailsbackView: UIView!
    @IBOutlet weak var videodetailsView: UIView!
    @IBOutlet weak var recordLabel: UILabel!
    @IBOutlet weak var recordbtnView: UIView!
    @IBOutlet weak var undobtnView: UIView!
    @IBOutlet weak var undoView: UIView!
    @IBOutlet weak var cameraView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.tabBarController?.tabBar.isHidden = true
        if isPractice ?? false{
            self.willswitchSides = false
        }else{
            willswitchSidesAlert()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            if self.setupSession() {
                self.setupPreview()
                self.startSession()
                self.addTableCoordinates(onlyTopBottomLine: false)
            }
        }
        addingGestures()
        adjustinUI()
    }
    
    func adjustinUI(){
        nextgameView.isHidden = true
        Utils().createStupaFolderPath()
        nextarrowImageView.makeroundimage()
        recordbtnView.dropShadow(radius: recordbtnView.frame.height / 2)
        cameraViewModel = CameraViewModel(root: self, matchAngle: matchAngle)
        AWSS3Manager.shared.delegate = self
    }
    func addingGestures(){
        undobtnView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(undoClicked)))
        videodetailsbackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popView)))
        recordbtnView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CameraVC.startCapture)))
        nextgameView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(nextGameClicked)))
    }
    
    @objc func nextGameClicked() {
        stopTimer()
        stopRecording()
        endGameAlert()
    }
    
    @objc func startNowClicked() {
        if willswitchSides{
            if self.playerSide == "Left"{
                self.playerSide = "Right"
            }else{
                self.playerSide = "Left"
            }
        }
        self.startCaptureNextGame()
    }
    
    @objc func popView(){
        if isRecording{
            stopMatchAlert()
        }
        self.navigationController?.popViewController(animated: true)
    }

    
    func setupPreview() {
        // Configure previewLayer
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = cameraView.bounds
        previewLayer.connection?.videoOrientation = .landscapeRight
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraView.layer.addSublayer(previewLayer)
    }
    
    //MARK:- Setup Camera
    
    func setupSession() -> Bool {
        captureSession.sessionPreset = AVCaptureSession.Preset.high
        // Setup Camera
        let camera = AVCaptureDevice.default(for: AVMediaType.video)!
        do {
            let input = try AVCaptureDeviceInput(device: camera)
            if captureSession.canAddInput(input) {
                captureSession.addInput(input)
                activeInput = input
            }
        } catch {
            print("Error setting device video input: \(error)")
            return false
        }
        // Setup Microphone
        let microphone = AVCaptureDevice.default(for: AVMediaType.audio)!
        do {
            let micInput = try AVCaptureDeviceInput(device: microphone)
            if captureSession.canAddInput(micInput) {
                captureSession.addInput(micInput)
            }
        } catch {
            print("Error setting device audio input: \(error)")
            return false
        }
        // Movie output
        if captureSession.canAddOutput(movieOutput) {
            captureSession.addOutput(movieOutput)
        }
        return true
    }
    
    func setupCaptureMode(_ mode: Int) {
        // Video Mode
    }
    
    //MARK:- Camera Session
    func startSession() {
        if !captureSession.isRunning {
            videoQueue().async {
                self.captureSession.startRunning()
            }
        }
    }
    
    func stopSession() {
        if captureSession.isRunning {
            videoQueue().async {
                self.captureSession.stopRunning()
            }
        }
    }
    
    func videoQueue() -> DispatchQueue {
        return DispatchQueue.main
    }
    
    func currentVideoOrientation() -> AVCaptureVideoOrientation {
        var orientation: AVCaptureVideoOrientation
        
        switch UIDevice.current.orientation {
        case .portrait:
            orientation = AVCaptureVideoOrientation.portrait
        case .landscapeRight:
            orientation = AVCaptureVideoOrientation.landscapeLeft
        case .portraitUpsideDown:
            orientation = AVCaptureVideoOrientation.portraitUpsideDown
        default:
            orientation = AVCaptureVideoOrientation.landscapeRight
        }
        
        return orientation
    }
    
    @objc func startCapture() {
        if isRecording{
            stopMatchAlert()
        }else{
            recordLabel.text = "Stop"
            startRecording()
        }
    }
    
    @objc func startCaptureNextGame() {
        stopTimer()
        startCapture()
    }

    func startRecording() {
        let connection = movieOutput.connection(with: AVMediaType.video)
        if (connection?.isVideoOrientationSupported)! {
            connection?.videoOrientation = currentVideoOrientation()
        }
        if (connection?.isVideoStabilizationSupported)! {
            connection?.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.auto
        }
        let device = activeInput.device
        if (device.isSmoothAutoFocusSupported) {
            do {
                try device.lockForConfiguration()
                device.isSmoothAutoFocusEnabled = false
                device.unlockForConfiguration()
            } catch {
                print("Error setting configuration: \(error)")
            }
        }
        isRecording = true
        //EDIT2: And I forgot this
        let fileUrl = Utils().getStupaVideosFolderPath(forFileName:getAWSVideoUploadName(forTimeStamp: fixedTimeStamps))
        if !(isPractice ?? false){
            nextgameView.isHidden = false
        }
        print("Created File at path:\(fileUrl.absoluteString)")
        movieOutput.startRecording(to: fileUrl, recordingDelegate: self)
        videoUploadFileArray.append(Utils().getFileNameFromURL(urlString: fileUrl.absoluteString)!)
        runTimer()
    }
    
    func stopMatchAlert(){
        let alert = UIAlertController(title: "Are you sure you want to stop the match?", message: "", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            self.finalClip = 1
            self.stopTimer()
            self.stopRecording()
            self.stopSession()
            self.startUploader()
        }
        let noAction = UIAlertAction(title: "No", style: .cancel)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func willswitchSidesAlert(){
        let alert = UIAlertController(title: "Will you switch sides in the match?", message: "", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            self.willswitchSides = true
        }
        let noAction = UIAlertAction(title: "No", style: .cancel){ (action) in
            self.willswitchSides = false
        }
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func endGameAlert(){
        let alert = UIAlertController(title: "End this and start new game?", message: "", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            self.startNowClicked()
        }
        let noAction = UIAlertAction(title: "No", style: .cancel)
        alert.addAction(yesAction)
        //alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }

    
    func runTimer() {
        recTimer = Timer.scheduledTimer(timeInterval:1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        var timerString = ""
        if minutes < 10 && seconds < 10 {
            timerString = "0" + "\(minutes)" + ":" + "0" + "\(seconds)"
        }else if minutes < 10 && seconds > 9 {
            timerString = "0" + "\(minutes)" + ":" + "\(seconds)"
        } else if minutes > 9 && seconds < 10 {
            timerString =  "\(minutes)" + ":" + "0" + "\(seconds)"
        } else {
            timerString = "\(minutes)" + ":" + "\(seconds)"
        }
        
        seconds += 1
        if seconds == 60 {
            minutes += 1
            seconds = 0
        }
        print(timerString)
        timerLabel.text = timerString
    }
    
    func stopTimer() {
        recTimer.invalidate()
        seconds = 0
        minutes = 0
    }
    
    func getAWSVideoUploadName(forTimeStamp:String) -> String {
        guard let userID = SUserDefaults.value(forKey: SUDConstants.userID) as? Int else{
            return "0"
        }
        let fileName = "\(userID)_" + forTimeStamp + "_" + "\(videoId ?? 0)_" + "\(chunkNumber)"
        chunkNumber += 1
        return fileName
    }
    
    func stopRecording() {
        movieOutput.stopRecording()
        isRecording = false
    }
    
    func addLabelToStars(imageView:UIImageView, labelString:String) -> UIImageView{
        let label = UILabel(frame: CGRect(x: imageView.frame.width/2-4, y: imageView.frame.height/2-4, width: 8, height: 8))
        label.text = labelString
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 5)
        imageView.addSubview(label)
        return imageView
    }
    
    func addTableCoordinates(onlyTopBottomLine:Bool) {
        
        let thirtyPercentWidth = (30*self.view.frame.width)/100
        let twentyPercentWidth = (20*self.view.frame.width)/100
        let fortyFivePercentHeight = (45*self.view.frame.height)/100
        let sixtyFivePercentHeight = (65*self.view.frame.height)/100
        
        tableheight = sixtyFivePercentHeight - fortyFivePercentHeight
        //print(tableheight)
        
        manualTapGestureView.frame = CGRect(x: 0, y: fortyFivePercentHeight, width: self.view.frame.width, height: sixtyFivePercentHeight - fortyFivePercentHeight)
        topLine.frame = CGRect(x: thirtyPercentWidth, y: 0, width: manualTapGestureView.frame.width-(2*thirtyPercentWidth), height: 2)
        topLine.backgroundColor = .yellow
        topLine.layer.cornerRadius = 6.0
        
        
        if onlyTopBottomLine {
            manualTapGestureView.addSubview(topLine)
            manualTapGestureView.addSubview(bottomLine)
        } else {
            star1.frame =  CGRect(x: thirtyPercentWidth, y: 3, width: 15, height: 15)
            star1DefaultFrame = star1.frame
            star1.image = UIImage(named: "star")
            star1.backgroundColor = .none
            star1.isUserInteractionEnabled = true
            star1 = addLabelToStars(imageView: star1, labelString: "1")
            
            star2.frame = CGRect(x: manualTapGestureView.frame.width-thirtyPercentWidth-15, y: 3, width: 15, height: 15)
            star2DefaultFrame = star2.frame
            star2.image = UIImage(named: "star")
            star2.isUserInteractionEnabled = true
            star2.backgroundColor = .none
            star2 = addLabelToStars(imageView: star2, labelString: "2")
            
            
            bottomLine.frame = CGRect(x: twentyPercentWidth, y: manualTapGestureView.frame.height-2, width: manualTapGestureView.frame.width-(2*twentyPercentWidth), height: 2)
            bottomLine.backgroundColor = .yellow
            bottomLine.layer.cornerRadius = 6.0
            
            
            star3.frame = CGRect(x: manualTapGestureView.frame.width-twentyPercentWidth-15, y: manualTapGestureView.frame.height-18, width: 15, height: 15)
            star3DefaultFrame = star3.frame
            star3.image = UIImage(named: "star")
            star3.isUserInteractionEnabled = true
            star3.backgroundColor = .none
            star3 = addLabelToStars(imageView: star3, labelString: "3")
            
            
            star4.frame = CGRect(x: twentyPercentWidth, y: manualTapGestureView.frame.height-18, width: 15, height: 15)
            star4DefaultFrame = star4.frame
            star4.image = UIImage(named: "star")
            star4.isUserInteractionEnabled = true
            star4.backgroundColor = .none
            star4 = addLabelToStars(imageView: star4, labelString: "4")
            
            centerLine.frame = CGRect(x: manualTapGestureView.frame.width/2, y: 0, width: 2, height: manualTapGestureView.frame.height)
            let shapeLayer = CAShapeLayer()
            shapeLayer.strokeColor = UIColor.yellow.cgColor
            shapeLayer.lineWidth = 2
            shapeLayer.lineDashPattern = [2,3]
            let path = CGMutablePath()
            path.addLines(between: [CGPoint(x: 0, y: 0),
                                    CGPoint(x: 0, y: manualTapGestureView.frame.height)])
            shapeLayer.path = path
            centerLine.layer.addSublayer(shapeLayer)
            
            centerTextLabel.frame = CGRect(x: manualTapGestureView.frame.width/2-200, y: manualTapGestureView.frame.height/2-15, width: 400, height: 30)
            centerTextLabel.text = "Setup your TT Table between these two lines"
            centerTextLabel.font = UIFont.systemFont(ofSize: 15)
            centerTextLabel.textColor = .white
            centerTextLabel.textAlignment = .center
            
            manualTapGestureView.addSubview(star1)
            manualTapGestureView.addSubview(star2)
            manualTapGestureView.addSubview(star3)
            manualTapGestureView.addSubview(star4)
            manualTapGestureView.addSubview(topLine)
            manualTapGestureView.addSubview(bottomLine)
            manualTapGestureView.addSubview(centerLine)
            manualTapGestureView.addSubview(centerTextLabel)
        }
        
        
        tap.addTarget(self, action: #selector(self.handleFrontTap(gestureRecognizer:)))
        self.view.addGestureRecognizer(tap)
        manualTapGestureView.isUserInteractionEnabled = false
        self.view.addSubview(manualTapGestureView)
        
    }
    
    @objc func handleFrontTap(gestureRecognizer: UITapGestureRecognizer) {
        
        if gestureRecognizer.state == UIGestureRecognizer.State.recognized
        {
            let myPoint = gestureRecognizer.location(in: self.view)
            print(gestureRecognizer.location(in: gestureRecognizer.view))
            
            startsClicked += 1
            
            switch startsClicked {
            case 1:
                star1.image = .none
                star1.backgroundColor = Colors.AppGreenColor
                star1.frame.size.width = 10
                star1.frame.size.height = 10
                star1.layer.cornerRadius = star1.frame.width*0.5
                star1.frame.origin.x = myPoint.x
                star1.frame.origin.y = myPoint.y - 80 - tableheight - 10
                updateCoordinatesClicked(starNum: 1, imageView: star1,x: myPoint.x,y: myPoint.y)
                removeAllSubview(imagView: star1)
            case 2:
                star2.image = .none
                star2.backgroundColor = Colors.AppGreenColor
                star2.frame.size.width = 10
                star2.frame.size.height = 10
                star2.layer.cornerRadius = star2.frame.width*0.5
                star2.frame.origin.x = myPoint.x
                star2.frame.origin.y = myPoint.y - 80 - tableheight - 10
                updateCoordinatesClicked(starNum: 2, imageView: star2,x: myPoint.x,y: myPoint.y)
                removeAllSubview(imagView: star2)
            case 3:
                star3.image = .none
                star3.backgroundColor = Colors.AppGreenColor
                star3.frame.size.width = 10
                star3.frame.size.height = 10
                star3.layer.cornerRadius = star3.frame.width*0.5
                star3.frame.origin.x = myPoint.x
                star3.frame.origin.y = myPoint.y - 80 - tableheight - 10
                updateCoordinatesClicked(starNum: 3, imageView: star3,x: myPoint.x,y: myPoint.y)
                removeAllSubview(imagView: star3)
            case 4:
                star4.image = .none
                star4.backgroundColor = Colors.AppGreenColor
                star4.frame.size.width = 10
                star4.frame.size.height = 10
                star4.layer.cornerRadius = star4.frame.width*0.5
                star4.frame.origin.x = myPoint.x
                star4.frame.origin.y = myPoint.y - 80 - tableheight - 10
                updateCoordinatesClicked(starNum: 4, imageView: star4,x: myPoint.x,y: myPoint.y)
                removeAllSubview(imagView: star4)
            default:
                print("Invalid Entries!!!")
            }
            
            if startsClicked == 1 {
                undoView.isHidden = false
                centerTextLabel.removeFromSuperview()
            }
            if startsClicked == 4 {
                undoView.isHidden = true
            }
        }
        
    }
    
    @objc func undoClicked() {
        updateStarsFrameToDefault()
    }
    
    func updateStarsFrameToDefault() {
        
        if startsClicked == 4 {
            shape.removeFromSuperlayer()
            star4.frame = star4DefaultFrame
            star4.image = UIImage(named: "star")
            star4.backgroundColor = .none
            star4 = addLabelToStars(imageView: star4, labelString: "4")
            startsClicked -= 1
            manualTapGestureView.addSubview(star1)
            manualTapGestureView.addSubview(star2)
            manualTapGestureView.addSubview(star3)
            manualTapGestureView.addSubview(star4)
            removeCoordinatesDictItems(num: 4)
            return
        }
        
        switch startsClicked {
        case 1:
            star1.frame = star1DefaultFrame
            star1.image = UIImage(named: "star")
            star1.backgroundColor = .none
            star1 = addLabelToStars(imageView: star1, labelString: "1")
            startsClicked -= 1
            
            removeCoordinatesDictItems(num: 1)
        case 2:
            star2.frame = star2DefaultFrame
            star2.image = UIImage(named: "star")
            star2.backgroundColor = .none
            star2 = addLabelToStars(imageView: star2, labelString: "2")
            startsClicked -= 1
            removeCoordinatesDictItems(num: 2)
        case 3:
            star3.frame = star3DefaultFrame
            star3.image = UIImage(named: "star")
            star3.backgroundColor = .none
            star3 = addLabelToStars(imageView: star3, labelString: "3")
            startsClicked -= 1
            removeCoordinatesDictItems(num: 3)
        case 4:
            star4.frame = star4DefaultFrame
            star4.image = UIImage(named: "star")
            star4.backgroundColor = .none
            star4 = addLabelToStars(imageView: star4, labelString: "4")
            startsClicked -= 1
            removeCoordinatesDictItems(num: 4)
        default:
            print("Invalid Entries!!!")
        }
        
        if startsClicked == 0 {
            undoView.isHidden = true
            coordinatesDictionary.removeAll()
            coordinatesPercentDictionary.removeAll()
        }
    }
    
    func removeCoordinatesDictItems(num:Int) {
        coordinatesDictionary.removeValue(forKey: num)
        coordinatesPercentDictionary.removeValue(forKey: num)
    }
    
    func removeAllSubview(imagView:UIImageView) {
        imagView.subviews.forEach({ $0.removeFromSuperview() })
    }
    
    func updateCoordinatesClicked(starNum:Int, imageView:UIImageView,x:CGFloat,y:CGFloat){
        //   let xPos = imageView.frame.midX
        //  let yPos = imageView.frame.midY
        //let yPosPlusMan = imageView.frame.midY + self.view.frame.origin.y
        let xPosPercentage : Double = Double((x/Utils().screenWidth) * 100)
        let yPosPercentage : Double = Double((y/Utils().screenHeight) * 100)
        coordinatesPercentDictionary[starNum] = ["x": xPosPercentage, "y": yPosPercentage]
        coordinatesDictionary[starNum] = ["x": x, "y": y - 80 - tableheight - 10]
        if coordinatesDictionary.count == 4 {
            removeTableCordinatesView()
            showSelectedCoordinatesView()
        }
    }
    
    func showSelectedCoordinatesView() {
        let st1 = coordinatesDictionary[1]
        let st2 = coordinatesDictionary[2]
        let st3 = coordinatesDictionary[3]
        let st4 = coordinatesDictionary[4]
        
        manualTapGestureView.layer.addSublayer(shape)
        shape.opacity = 0.3
        shape.lineWidth = 2
        shape.lineJoin = CAShapeLayerLineJoin.miter
        shape.strokeColor = UIColor.yellow.cgColor
        // shape.fillColor = UIColor(hue: 0.786, saturation: 0.15, brightness: 0.89, alpha: 1.0).cgColor
        
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: st1!["x"]!, y: st1!["y"]!))
        path.addLine(to: CGPoint(x: st2!["x"]!, y: st2!["y"]!))
        path.addLine(to: CGPoint(x: st3!["x"]!, y: st3!["y"]!))
        path.addLine(to: CGPoint(x: st4!["x"]!, y: st4!["y"]!))
        path.addLine(to: CGPoint(x: st1!["x"]!, y: st1!["y"]!))
        path.close()
        shape.path = path.cgPath
        
        showConfirmationforStart()
    }
    
    func showConfirmationforStart(){
        let selectMatchPopUpVC = StoryBoards.ExtrasStoryboard.instantiateViewController(withIdentifier: "ConfirmStartMatchPopupVC") as! ConfirmStartMatchPopupVC
        selectMatchPopUpVC.delegate = self
        selectMatchPopUpVC.modalPresentationStyle = .overFullScreen
        self.present(selectMatchPopUpVC, animated: false, completion: nil)
    }
    
    func removeTableCordinatesView() {
        topLine.removeFromSuperview()
        bottomLine.removeFromSuperview()
        centerLine.removeFromSuperview()
        centerTextLabel.removeFromSuperview()
        star1.removeFromSuperview()
        star2.removeFromSuperview()
        star3.removeFromSuperview()
        star4.removeFromSuperview()
    }
    
    func startUploader() {
        
        //self.showAlertwithTitle(with: "Please do not close or click Home, your videos are still uploading...", title: "Warning!")
        
        uploadAlert = UIAlertController(title: "Warning!", message: "Please do not close or click Home, your videos are still uploading...", preferredStyle: .alert)
        self.present(uploadAlert!, animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 6.0) { [self] in
            if self.videoUploadFileArray.count > 0 {
                
                for item in self.videoUploadFileArray {
                    let path = Utils().getStupaVideosActualFolderPath().absoluteString + "/\(self.videoUploadFileArray[0])"
                    let urlPath = URL(string: path)
                    if FileManager.default.fileExists(atPath: path) {
                        self.uploadRecordedVideo(url: urlPath!)
                    }
                    
                }
                
            }
        }
    }
    
    func uploadRecordedVideo(url:URL){
        
        let fileName = Utils().getFileNameFromURL(urlString: url.absoluteString)
        var newName = String()
        let updatedFilename = fileName?.split(separator: "_")
        let fileextension = String(updatedFilename?[3] ?? "").split(separator: ".")
        if let filenameid = updatedFilename?[0]{
            newName = String(filenameid)
        }
        if let filenametimestamp = updatedFilename?[1]{
            newName += "_\(String(filenametimestamp)).\(fileextension[1])"
        }
        
        cameraViewModel.chunkNumber = self.chunkNumber - 1
        cameraViewModel.filename = newName
        cameraViewModel.finalClip = self.finalClip
        cameraViewModel.videoId = self.videoId
        cameraViewModel.playerSide = self.playerSide
        cameraViewModel.matchAngle = self.matchAngle
        cameraViewModel.coordinatesPercentDictionary = self.coordinatesPercentDictionary
        cameraViewModel.uploadVideoApi(url: url)
        
    }
    
}

extension CameraVC:ConfirmStartMatch{
    func start(start: Bool) {
        if start{
            recordbtnView.isHidden = false
            videodetailsView.isHidden = false
        }else{
            undoView.isHidden = false
            addTableCoordinates(onlyTopBottomLine: true)
        }
    }
}

extension CameraVC:AVCaptureFileOutputRecordingDelegate{
    func capture(_ captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL: URL!, fromConnections connections: [Any]!, error: Error!){
        // save video to camera roll
        if error == nil {
            UISaveVideoAtPathToSavedPhotosAlbum(outputFileURL.path, nil, nil, nil)
        }
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        if (error != nil) {
            print("Error recording movie: \(error!.localizedDescription)")
        } else {
            print("File Saved at:\(outputFileURL.absoluteString)")
            self.uploadRecordedVideo(url: outputFileURL)
        }
    }
}

extension CameraVC:AWSS3ManagerDelegate{
    
    func uploadStarted(id: String, name: String, path: String?) {
        
    }
    
    func uploadProgress(id: String, progress: Progress) {
        
    }
    
    func uploadProgressCompleted(id: String, name: String, path: String?, failure: Error?, isOverAllCompleted: Bool) {
        print("Uploading Complete.",id)
        ifStillUploading = false
        let fileURL = Utils().getStupaVideosActualFolderPath().appendingPathComponent(path ?? "")
        print("Deleted file at path:\(String(describing: fileURL.absoluteString))")
        try? FileManager.default.removeItem(at: fileURL)
        if let index = videoUploadFileArray.firstIndex(of: path!) {
            videoUploadFileArray.remove(at: index)
            
            if videoUploadFileArray.isEmpty{
                DispatchQueue.main.async {
                    self.uploadAlert?.dismiss(animated: true, completion: nil)
                    if self.matchAngle == .CoachAngle{
                        NotificationCenter.default.post(name: Notification.Name("ShowUploadVideosPopUp"), object: true)
                    }else{
                        NotificationCenter.default.post(name: Notification.Name("ShowUploadVideosPopUp"), object: false)
                    }
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
            
        }
        
        print("videos left",videoUploadFileArray)
        
    }
    
    
}
