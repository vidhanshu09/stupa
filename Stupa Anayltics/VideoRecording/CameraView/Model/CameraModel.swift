//
//  CameraModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 21/04/21.
//

import Foundation

// MARK: - UploadVideoRequestModel
struct UploadVideoRequestModel: Codable {
    let matchType, matchDate, deviceID: String
    let userID: Int
    let playerSide: String
    let playerID: Int
    let playerName, ballType, deviceType, tokenValue: String
    let chunkNumber, finalClip: Int
    let fileName: String
    let videoID: Int
    let uploadType, opponentName: String

    enum CodingKeys: String, CodingKey {
        case matchType = "MatchType"
        case matchDate = "MatchDate"
        case deviceID = "DeviceId"
        case userID = "UserId"
        case playerSide
        case playerID = "PlayerId"
        case playerName = "PlayerName"
        case ballType = "BallType"
        case deviceType = "DeviceType"
        case tokenValue = "TokenValue"
        case chunkNumber, finalClip
        case fileName = "FileName"
        case videoID = "VideoId"
        case uploadType = "UploadType"
        case opponentName = "OpponentName"
    }
}

// MARK: - UploadVideoResponseModel
struct UploadVideoResponseModel: Codable {
    let isSuccess: Bool?
    let message: String?
    let data: UploadVideoData?

    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case message = "Message"
        case data = "Data"
    }
}

// MARK: - DataClass
struct UploadVideoData: Codable {
    let videoID: Int?
    let fileName: String?

    enum CodingKeys: String, CodingKey {
        case videoID = "VideoId"
        case fileName = "FileName"
    }
}
