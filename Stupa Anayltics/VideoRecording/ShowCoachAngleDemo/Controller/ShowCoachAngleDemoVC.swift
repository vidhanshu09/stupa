//
//  ShowCoachAngleDemoVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 21/04/21.
//

import UIKit

class ShowCoachAngleDemoVC: UIViewController {

    var fixedTimeStamps:String?
    var videoId:Int?
    var playerSide:String?
    var matchAngle:Utils.matchAngle?
    var isPractice:Bool? = false
    
    //MARK:- Objects
    
    @IBOutlet weak var backView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        addingGestures()
        openmatchTpePopup()
    }
    
    func addingGestures(){
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popView)))
    }
    
    @objc func openmatchTpePopup(){
        let selectmatchTypePopup = StoryBoards.ExtrasStoryboard.instantiateViewController(withIdentifier: "SelectMatchTypePopUp") as! SelectMatchTypePopUp
        selectmatchTypePopup.naviagatedelegate = self
        selectmatchTypePopup.modalPresentationStyle = .overFullScreen
        self.present(selectmatchTypePopup, animated: false, completion: nil)
    }
    
    @objc func popView(){
        self.navigationController?.popViewController(animated: true)
        self.tabBarController?.tabBar.isHidden = false
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    func openCamera(){
        let navigate = StoryBoards.VideoRecordingStoryboard.instantiateViewController(withIdentifier: "CameraVC") as! CameraVC
        if isPractice ?? false{
            navigate.isPractice = true
        }
        navigate.fixedTimeStamps = self.fixedTimeStamps ?? ""
        navigate.videoId = self.videoId
        navigate.playerSide = self.playerSide ?? ""
        navigate.matchAngle = self.matchAngle
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
    }
    
}

extension ShowCoachAngleDemoVC:ThingstoRememberDone{
    func update() {
        self.openCamera()
    }
}

extension ShowCoachAngleDemoVC:MatchTypeSelected{
    func navigate(isRecord: Bool) {
        if isRecord{
            let navigate = StoryBoards.MatchDetailsStoryboard.instantiateViewController(withIdentifier: "MatchDetailsVC") as! MatchDetailsVC
            navigate.isCoachAngle = true
            self.navigationController?.pushViewController(navigate, animated: true)
        }
    }
}
