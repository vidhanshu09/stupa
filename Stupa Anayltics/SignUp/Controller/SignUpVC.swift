//
//  SignUpVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import UIKit
import SKCountryPicker

class SignUpVC: BaseVC {
    
    //MARK:- Objects
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var countryextView: UIView!
    @IBOutlet weak var countryextImageVIew: UIImageView!
    @IBOutlet weak var countryextLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        self.setupBackIcon(view: backView)
        addingGestures()
        getcountryIndia()
    }
    
    func addingGestures(){
        countryextView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(opencountryextensionPicker)))
    }
    
    @objc func opencountryextensionPicker(){
        self.presentCountryPickerScene(withSelectionControlEnabled: false) { (country) in
            self.countryextImageVIew.image = country.flag
            self.countryextLabel.text = "\(country.countryCode) \(String(describing: country.dialingCode ?? ""))"
        }
    }
    
    func getcountryIndia(){
        let india = CountryManager.shared.country(withCode: "IN")!
        self.countryextImageVIew.image = india.flag
        self.countryextLabel.text = "\(india.countryCode) \(String(describing: india.dialingCode!))"
    }
    
}


