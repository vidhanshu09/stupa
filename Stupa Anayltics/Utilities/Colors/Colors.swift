//
//  Colors.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import Foundation

struct Colors{
    public static let AppGreenColor = UIColor(named: "AppGreenColor")!
    public static let AppYellowColor = UIColor(named: "AppYellowColor")!
    public static let AppDarkYellowColor = UIColor(named: "AppDarkYellowColor")!
    public static let TableBackColor = UIColor(named: "TableBackColor")!
    public static let AppLoseRedColor = UIColor(named: "StatusExpired")!
    public static let AppWinGreenColor = UIColor(named: "StatusCompleted")!
}
