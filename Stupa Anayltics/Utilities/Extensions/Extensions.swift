//
//  Extensions.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 13/04/21.
//

import Foundation
import SKCountryPicker
import ImageIO
import Charts

extension UIViewController:ChartViewDelegate{
    func showAlert(with message: String) {
        let alertController = UIAlertController(title: "Warning!", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: AlertConstants.Okbutton, style: .cancel) { _ in
            alertController.dismiss(animated: true, completion: nil)
        })
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    func showAlertwithTitle(with message: String,title:String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: AlertConstants.Okbutton, style: .cancel) { _ in
            alertController.dismiss(animated: true, completion: nil)
        })
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    func presentCountryPickerScene(withSelectionControlEnabled selectionControlEnabled: Bool = true,completion: @escaping (Country) -> Void) {
        switch selectionControlEnabled {
        case true:
            // Present country picker with `Section Control` enabled
            let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in
                completion(country)
            }
            countryController.flagStyle = .circular
            countryController.isCountryFlagHidden = true
            countryController.isCountryDialHidden = true
            countryController.favoriteCountriesLocaleIdentifiers = ["IN", "US"]
        case false:
            // Present country picker without `Section Control` enabled
            let countryController = CountryPickerWithSectionViewController.presentController(on: self) {(country: Country) in
                completion(country)
            }
            countryController.flagStyle = .corner
            countryController.isCountryFlagHidden = false
            countryController.isCountryDialHidden = false
            countryController.favoriteCountriesLocaleIdentifiers = ["IN", "US"]
        }
    }
    
    func setuplineChart(linechartView:LineChartView){
        linechartView.delegate = self
        linechartView.legend.enabled = true
        linechartView.chartDescription?.enabled = false
        linechartView.dragEnabled = true
        linechartView.highlightPerDragEnabled = false
        linechartView.doubleTapToZoomEnabled = false
        linechartView.setScaleEnabled(true)
        linechartView.pinchZoomEnabled = false
        //linechartView.backgroundColor = Colors().AppListBackgroundColor
        
        let leftAxis = linechartView.leftAxis
        leftAxis.removeAllLimitLines()
        
        //leftAxis.axisMaximum = 100
        //leftAxis.axisMinimum = 0
        
        // leftAxis.gridLineDashLengths = [0, 0]
        leftAxis.drawLimitLinesBehindDataEnabled = false
        leftAxis.drawAxisLineEnabled = false
        leftAxis.labelTextColor = UIColor.white
        leftAxis.labelFont = UIFont(name: "Montserrat-Regular", size: 10)!
        linechartView.rightAxis.enabled = false
        //linechartView.xAxis.drawGridLinesEnabled = false
        
        let xAxis = linechartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = UIFont(name: "Montserrat-Regular", size: 10)!
        xAxis.granularity = 1
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
       // xAxis.wordWrapEnabled = true
        xAxis.labelTextColor = UIColor.darkGray
        
        let l = linechartView.legend
        l.form = .circle
        l.font = UIFont(name: "Montserrat-Regular", size: 11)!
        l.textColor = .white
        l.horizontalAlignment = .left
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.drawInside = false
        
        linechartView.setExtraOffsets(left: 0, top: 0, right: 0, bottom: 20)
        
        // let xAxis = linechartView.xAxis
        // xAxis.labelFont = .systemFont(ofSize: 10, weight: .light)
        // xAxis.granularity = 0
        // xAxis.labelPosition = .bottom
        // xAxis.centerAxisLabelsEnabled = true
        //xAxis.granularityEnabled = true
        // xAxis.granularity = 0
        //xAxis.drawAxisLineEnabled = false
        // xAxis.drawGridLinesEnabled = false
        // xAxis.valueFormatter = IndexAxisValueFormatter(values: self.subjectArray)
        
        //[_chartView.viewPortHandler setMaximumScaleY: 2.f];
        //[_chartView.viewPortHandler setMaximumScaleX: 2.f];
        
        let marker = BalloonMarker(color: UIColor(white: 180/255, alpha: 1),
                                   font: .systemFont(ofSize: 12),
                                   textColor: .white,
                                   insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
        marker.chartView = linechartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        linechartView.marker = marker
        
        //linechartView.legend.form = .line
        // linechartView.legend.enabled  = false
        linechartView.xAxis.enabled = true
        linechartView.leftAxis.labelTextColor = UIColor.darkGray
        // sliderX.value = 45
        // sliderY.value = 100
        // slidersValueChanged(nil)
        
        linechartView.animate(xAxisDuration: 3)
    }
    
}

extension String{
    var isValidURL: Bool {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count)) {
            // it is a link, if the match covers the whole string
            return match.range.length == self.utf16.count
        } else {
            return false
        }
    }
    
    func getMarkerValue() -> String {

        var fullForm = ""

        switch self {
        case "BH-SP":
            fullForm = "Backhand Short Push"
        case "FH-BSS":
            fullForm = "Forehand Backspin Short"
        case "FH-TSL":
            fullForm = "Forehand Topspin Long"
        case "FH-SBL":
            fullForm = "Forehand Sidespin + Backspin Long"
        case "FH-SSSO":
            fullForm = "Forehand Sidespin Slight Out"
        case "FH-SSL":
            fullForm = "Forehand Sidespin Long"
        case "FH-NSL":
            fullForm = "Forehand No Spin Long"
        case "FH-BSL":
            fullForm = "Forehand Backspin Long"
        case "FH-TCTS":
            fullForm = "Forehand Turnovr Counter Topspin"
        case "FH-TTS":
            fullForm = "Forehand Turnover Topspin"
        case "FH-TSM":
            fullForm = "Forehand Turnover Smash"
        case "FH-TLOOP":
            fullForm = "Forehand Turnover Loop"
        case "BH-CBK":
            fullForm = "Backhand Chop Block"
        case "FH-CBK":
            fullForm = "Forehand Chop Block"
        case "NE":
            fullForm = "Net & Error"
        case "NR":
            fullForm = "Not Reached"
        case "BH-SM":
            fullForm = "Backhand Smash"
        case "FH-SM":
            fullForm = "Forehand Smash"
        case "FH-BK":
            fullForm = "Forehand Block"
        case "FH-TP":
            fullForm = "Forehand Tap"
        case "BH-TP":
            fullForm = "Backhand tap"
        case "FH-LB":
            fullForm = "Forehand Lob"
        case "BH-BK":
            fullForm = "Backhand Block"
        case "BH-LB":
            fullForm = "Backhand Lob"
        case "BH-DF":
            fullForm = "Backhand Defence"
        case "FH-DF":
            fullForm = "Forehand Defence"
        case "FH-F":
            fullForm = "Forehand Flick"
        case "FH-SP":
            fullForm = "Forehand Short Push"
        case "BH-BSL":
            fullForm = "Backhand Backspin Long"
        case "FH-TSS":
            fullForm = "Forehand Topspin Short"
        case "FH-SBSO":
            fullForm = "Forehand Sidespin + Backspin Slight Out"
        case "FH-SBS":
            fullForm = "Forehand Sidespin + Backspin Short"
        case "FH-SSS":
            fullForm = "Forehand Sidespin Short"
        case "FH-NSS":
            fullForm = "Forehand No Spin Short"
        case "BH-NSL":
            fullForm = "Backhand No Spin Long"
        case "BH-SSL":
            fullForm = "Backhand Sidespin Long"
        case "BH-SBS":
            fullForm = "Backhand Sidespin + Backspin Short"
        case "BH-SBL":
            fullForm = "Backhand Sidespin + Backspin Long"
        case "BH-TSL":
            fullForm = "Backhand Topspin Long"
        case "BH-BSS":
            fullForm = "Backhand Backspin Short"
        case "BH-NSS":
            fullForm = "Backhand No Spin Short"
        case "BH-SSS":
            fullForm = "Backhand Sidespin Short"
        case "BH-TSS":
            fullForm = "Backhand Topspin Short"
        case "FH-BSSO":
            fullForm = "Forehand Backspin Slight out"
        case  "BH-SSSO":
            fullForm = "Backhand Sidespin Slight out"
        case "FH-RBSL":
            fullForm = "Forehand Reverse Backspin Long"
        case "FH-RNSL":
            fullForm = "Forehand Reverse No Spin Long"
        case "FH-RSSL":
            fullForm = "Forehand Reverse Sidespin Long"
        case "FH-RSSO":
            fullForm = "Forehand Reverse Sidespin Slight Out"
        case "FH-RSSSO":
            fullForm = "Forehand Reverse Sidespin Slight Out"
        case "FH-RSBL":
            fullForm = "Forehand Reverse Sidespin + Backspin Long"
        case "FH-RTSL":
            fullForm = "Forehand Reverse Topspin Long"
        case "FH-RBSS":
            fullForm = "Forehand Reverse Backspin Short"
        case "FH-RNSS":
            fullForm = "Forehand Reverse No Spin Short"
        case "FH-RSSS":
            fullForm = "Forehand Reverse Sidespin Short"
        case "FH-RSBS":
            fullForm = "Forehand Reverse Sidespin + Backspin Short"
        case "FH-RTSS":
            fullForm = "Forehand Reverse Topspin Short"
        case "SH-BSL":
            fullForm = "Shovel Backspin Long"
        case "SH-NSL":
            fullForm = "Shovel No Spin Long"
        case "SH-TSL":
            fullForm = "Shovel Topspin Long"
        case "SH-BSS":
            fullForm = "Shovel Backspin Short"
        case "SH-NSS":
            fullForm = "Shovel No Spin Short"
        case "SH-NSSO":
            fullForm = "Shovel No Spin Slight Out"
        case "SH-TSS":
            fullForm = "Shovel Topspin Short"
        case "SH-SSL":
            fullForm = "Shovel Sidespin Long"
        case "SH-SSS":
            fullForm = "Shovel Sidespin Short"
        case "SH-SBL":
            fullForm = "Shovel Sidespin + Backspin Long"
        case "SH-SBS":
            fullForm = "Shovel Sidespin + Backspin Short"
        case "FH-TMHL":
            fullForm = "Forehand Tomahawk Long"
        case "RV-TMHL":
            fullForm = "Reverse Tomahawk Long"
        case "FH-TMHS":
            fullForm = "Forehand Tomahawk Short"
        case "RV-TMHS":
            fullForm = "Reverse Tomahawk Short"
        case "FH-RTSSO":
            fullForm = "Forehand Reverse Topspin Slight out"
        case "FH-LP":
            fullForm = "Forehand Long Push"
        case "FH-TS":
            fullForm = "Forehand Topspin"
        case  "BH-TS":
            fullForm = "Backhand Topspin"
        case  "BH-LP":
            fullForm = "Backhand Long Push"
        case  "BN-FP":
            fullForm = "Banana Flip"
        case  "ST-FP":
            fullForm = "Strawberry Flip"
            
        case "BHS":
            fullForm = "Backhand Short"
        case "BHH":
            fullForm = "Backhand Half"
        case "BHL":
            fullForm = "Backhand Long"
        case "EBHS":
            fullForm = "Extreme Backhand Short"
        case "EBHH":
            fullForm = "Extreme Backhand Half"
        case "EBHL":
            fullForm = "Extreme Backhand Long"
        case "CS":
            fullForm = "Centre Short"
        case "CH":
            fullForm = "Centre Half"
        case "CL":
            fullForm = "Centre Long"
        case "FHS":
            fullForm = "Forehand Short"
        case "FHH":
            fullForm = "Forehand Half"
        case "FHL":
            fullForm = "Forehand Long"
        case "EFHS":
            fullForm = "Extreme Forehand Short"
        case "EFHH":
            fullForm = "Extreme Forehand Half"
        case "EFHL":
            fullForm = "Extreme Forehand Long"
        case "NG":
            fullForm = "Net & Good"
        case "TE":
            fullForm = "Table Edge"
        case "SE":
            fullForm = "Service Error"
        case "FH-CTS":
            fullForm = "Forehand Counter Topspin"
        case "BH-CTS":
            fullForm = "Backhand Counter Topspin"
        case "BH-C":
            fullForm = "Backhand Counter"
        case "FH-C":
            fullForm = "Forehand Counter"
        case "FH-LOOP":
            fullForm = "Forehand Loop"
        default:
            fullForm = ""
        }
        return fullForm
    }
    
}

extension UIImage {
    
    public class func gifImageWithData(data: NSData) -> UIImage? {
        guard let source = CGImageSourceCreateWithData(data, nil) else {
            print("image doesn't exist")
            return nil
        }
        
        return UIImage.animatedImageWithSource(source: source)
    }
    
    public class func gifImageWithURL(gifUrl:String) -> UIImage? {
        guard let bundleURL = NSURL(string: gifUrl)
        else {
            print("image named \"\(gifUrl)\" doesn't exist")
            return nil
        }
        guard let imageData = NSData(contentsOf: bundleURL as URL) else {
            print("image named \"\(gifUrl)\" into NSData")
            return nil
        }
        
        return gifImageWithData(data: imageData)
    }
    
    public class func gifImageWithName(name: String) -> UIImage? {
        guard let bundleURL = Bundle.main
                .url(forResource: name, withExtension: "gif") else {
            print("SwiftGif: This image named \"\(name)\" does not exist")
            return nil
        }
        
        guard let imageData = NSData(contentsOf: bundleURL) else {
            print("SwiftGif: Cannot turn image named \"\(name)\" into NSData")
            return nil
        }
        
        return gifImageWithData(data: imageData)
    }
    
    class func delayForImageAtIndex(index: Int, source: CGImageSource!) -> Double {
        var delay = 0.1
        
        let cfProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil)
        let gifProperties: CFDictionary = unsafeBitCast(CFDictionaryGetValue(cfProperties, Unmanaged.passUnretained(kCGImagePropertyGIFDictionary).toOpaque()), to: CFDictionary.self)
        
        var delayObject: AnyObject = unsafeBitCast(CFDictionaryGetValue(gifProperties, Unmanaged.passUnretained(kCGImagePropertyGIFUnclampedDelayTime).toOpaque()), to: AnyObject.self)
        
        if delayObject.doubleValue == 0 {
            delayObject = unsafeBitCast(CFDictionaryGetValue(gifProperties, Unmanaged.passUnretained(kCGImagePropertyGIFDelayTime).toOpaque()), to: AnyObject.self)
        }
        
        delay = delayObject as! Double
        
        if delay < 0.1 {
            delay = 0.1
        }
        
        return delay
    }
    
    class func gcdForPair(a: Int?, _ b: Int?) -> Int {
        var a = a
        var b = b
        if b == nil || a == nil {
            if b != nil {
                return b!
            } else if a != nil {
                return a!
            } else {
                return 0
            }
        }
        
        if a! < b! {
            let c = a!
            a = b!
            b = c
        }
        
        var rest: Int
        while true {
            rest = a! % b!
            
            if rest == 0 {
                return b!
            } else {
                a = b!
                b = rest
            }
        }
    }
    
    class func gcdForArray(array: Array<Int>) -> Int {
        if array.isEmpty {
            return 1
        }
        
        var gcd = array[0]
        
        for val in array {
            gcd = UIImage.gcdForPair(a: val, gcd)
        }
        
        return gcd
    }
    
    class func animatedImageWithSource(source: CGImageSource) -> UIImage? {
        let count = CGImageSourceGetCount(source)
        var images = [CGImage]()
        var delays = [Int]()
        
        for i in 0..<count {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(image)
            }
            
            let delaySeconds = UIImage.delayForImageAtIndex(index: Int(i), source: source)
            delays.append(Int(delaySeconds * 1000.0)) // Seconds to ms
        }
        
        let duration: Int = {
            var sum = 0
            
            for val: Int in delays {
                sum += val
            }
            
            return sum
        }()
        
        let gcd = gcdForArray(array: delays)
        var frames = [UIImage]()
        
        var frame: UIImage
        var frameCount: Int
        for i in 0..<count {
            frame = UIImage(cgImage: images[Int(i)])
            frameCount = Int(delays[Int(i)] / gcd)
            
            for _ in 0..<frameCount {
                frames.append(frame)
            }
        }
        
        let animation = UIImage.animatedImage(with: frames, duration: Double(duration) / 1000.0)
        
        return animation
    }
}

extension UIView{
    
    func dropShadow(radius:CGFloat? = 10,color:CGColor? = UIColor.black.cgColor) {
        layer.cornerRadius = radius ?? 10
        layer.masksToBounds = true
        layer.shadowColor = color ?? UIColor.black.cgColor
        layer.shadowOffset = .zero
        layer.shadowRadius = 3.0
        layer.shadowOpacity = 0.5
        layer.masksToBounds = false
    }
    
    func setCustomNavigationwithTitle(navTitle:String,root:UIViewController,withbackbtn:Bool?,completionHandler: @escaping() -> Void,isbackNeeded:Bool? = true){
        let view = UINib(nibName: "CustomNavigation", bundle: .main).instantiate(withOwner: nil, options: nil).first as! CustomNavigation
        view.root = root
        view.navTitle = navTitle
        view.backbtnImageShow = withbackbtn
        view.completionHandler = completionHandler
        view.isbackNeeded = isbackNeeded
        self.addSubview(view)
    }
    func makeround(){
        layer.cornerRadius = frame.height / 2
        clipsToBounds = true
    }
    func makeroundimage(){
        layer.cornerRadius = layer.frame.width / 2
        clipsToBounds = true
    }
    func createDottedLine(width: CGFloat, color: CGColor) {
          let caShapeLayer = CAShapeLayer()
          caShapeLayer.strokeColor = color
          caShapeLayer.lineWidth = width
          caShapeLayer.lineDashPattern = [4,4]
          let cgPath = CGMutablePath()
        let cgPoint = [CGPoint(x: 0, y: 0), CGPoint(x: 0, y: self.frame.height)]
          cgPath.addLines(between: cgPoint)
          caShapeLayer.path = cgPath
          layer.addSublayer(caShapeLayer)
       }
}

extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}

extension AppDelegate{
    func CheckUsertoRedirect(){
        if SUserDefaults.value(forKey: SUDConstants.tokenValue) != nil{
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "TabBar")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }else{
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyBoard : UIStoryboard = UIStoryboard(name: "Login", bundle:nil)
            let loginViewController = storyBoard.instantiateViewController(withIdentifier: "IntroVC") as! IntroVC
            let navigationController = UINavigationController(rootViewController: loginViewController)
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window!.rootViewController = navigationController
        }
    }
}

extension UITableView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = UIColor.white
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Montserrat-Regular", size: 17)!
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    func restore(withseperator:Bool? = true) {
        self.backgroundView = nil
        if withseperator == true{
            self.separatorStyle = .singleLine
        }
    }
}

extension UISwitch {

    func set(width: CGFloat, height: CGFloat) {

        let standardHeight: CGFloat = 31
        let standardWidth: CGFloat = 51

        let heightRatio = height / standardHeight
        let widthRatio = width / standardWidth

        transform = CGAffineTransform(scaleX: widthRatio, y: heightRatio)
    }
}

extension Int{
    func toString() -> String{
        return String(self)
    }
}

extension Double{
    func toString() -> String{
        return String(self)
    }
}

extension Double {
    func toFloat() -> Float {
        return Float(self)
    }
}


extension UINavigationController {
  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
      popToViewController(vc, animated: animated)
    }
  }
}

extension CATransition {
    
    //New viewController will appear from bottom of screen.
    func segueFromBottom() -> CATransition {
        self.duration = 0.375 //set the duration to whatever you'd like.
        self.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        self.type = CATransitionType.moveIn
        self.subtype = CATransitionSubtype.fromTop
        return self
    }
    //New viewController will appear from top of screen.
    func segueFromTop() -> CATransition {
        self.duration = 0.375 //set the duration to whatever you'd like.
        self.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        self.type = CATransitionType.moveIn
        self.subtype = CATransitionSubtype.fromBottom
        return self
    }
    //New viewController will appear from left side of screen.
    func segueFromLeft() -> CATransition {
        self.duration = 0.3 //set the duration to whatever you'd like.
        self.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        self.type = CATransitionType.moveIn
        self.subtype = CATransitionSubtype.fromLeft
        return self
    }
    //New viewController will pop from right side of screen.
    func popFromRight() -> CATransition {
        self.duration = 0.3 //set the duration to whatever you'd like.
        self.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        self.type = CATransitionType.reveal
        self.subtype = CATransitionSubtype.fromRight
        return self
    }
    //New viewController will appear from left side of screen.
    func popFromLeft() -> CATransition {
        self.duration = 0.1 //set the duration to whatever you'd like.
        self.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        self.type = CATransitionType.reveal
        self.subtype = CATransitionSubtype.fromLeft
        return self
    }
}
