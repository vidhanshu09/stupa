//
//  ServerConfiguration.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import Foundation

// For the SeverConfigType
public enum ServerConfigType : String {
    case Production = "PROD"
    case Development = "DEV"
}

public let serverConfig = ServerConfigType.Development
