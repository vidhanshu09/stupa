//
//  AWSConfig.swift
//  Model
//
//  Created by Kartik on 11/11/20.
//

import Foundation
import AWSCore
import AWSCognito
import AWSS3

public class AWSConfig: NSObject {
    
    public static let shared = AWSConfig()
    
    override private init() {
        super.init()
    }
    
    public func configure() {
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType: .USEast1,
                                                                identityPoolId: StupaStrings.aws3poolId)
        let configurations = AWSServiceConfiguration(region: .USEast1,
                                                     credentialsProvider: credentialsProvider)
        AWSServiceManager.default()?.defaultServiceConfiguration = configurations
    }
}
