//
//  AWSUploadFile.swift
//  Model
//
//  Created by Kartik on 11/11/20.
//

import Foundation
import UIKit

public protocol AWSUploadFile {
    var id: String { get set }
    var name: String { get set }
    var path: String { get set }
    var mimeType: String { get }
    var metadata: [String: Any]? { get set }
}

public struct AWSUploadImage: AWSUploadFile {
    
    public var metadata: [String : Any]?
    public var name: String
    public var id: String
    public var image: UIImage
    public var path: String
    public var mimeType: String {
        return "image/jpeg"
    }
    
    public init(id: String, image: UIImage, path: String) {
        self.id = id
        self.image = image
        self.path = path
        self.name = ""
    }
    
}

public struct AWSUploadAudio: AWSUploadFile {
    
    public var metadata: [String : Any]?
    public var id: String
    public var name: String
    public var path: String
    public var audioPath: String
    public var mimeType: String {
        return "audio/mp4a-latm"
    }
    
    public init(id: String, name: String,
                path: String, audioPath: String) {
        self.id = id
        self.path = path
        self.audioPath = audioPath
        self.name = name
    }
    
}

public struct AWSUploadVideo: AWSUploadFile {
    public var metadata: [String : Any]?    
    public var id: String
    public var name: String
    public var path: String
    public var videoPath: String
    public var mimeType: String {
        return "video/mp4"
    }
    
    public init(id: String, name: String,
                path: String, videoPath: String) {
        self.id = id
        self.path = path
        self.videoPath = videoPath
        self.name = name
    }
    
}
