//
//  AWSS3Manager.swift
//  Model
//
//  Created by Kartik on 11/11/20.
//

import Foundation
import AWSS3
import AWSCore
import AWSCognito

public protocol AWSS3ManagerDelegate: class {
    func uploadStarted(id: String,
                       name: String,
                       path: String?)
    func uploadProgress(id: String, progress: Progress)
    func uploadProgressCompleted(id: String,
                                 name: String,
                                 path: String?,
                                 failure: Error?,
                                 isOverAllCompleted: Bool)
}

public class AWSS3Manager: NSObject {
    
    public static let shared = AWSS3Manager()
    public weak var delegate: AWSS3ManagerDelegate?
    
    var uploadCount = 0
    
    let serialQueue = DispatchQueue(label: "serialQueue")
    let concurrentQueue = DispatchQueue(label: "concurrentQueue", attributes: [ .concurrent])
    
    override public init() {
        super.init()
//        concurrentQueue.setTarget(queue: serialQueue)
//        concurrentQueue.activate()
    }
    
    public func multipartRequestUpload(files: [AWSUploadFile]) {
//        let group = DispatchGroup()
//        self.uploadCount += files.count
//        for file in files {
//            group.enter()
//            self.delegate?.uploadStarted(id: file.id,
//                                         name: file.name,
//                                         path: file.path)
//            self.uploadFile(file: file, group: group)
//        }
//
//        group.notify(queue: DispatchQueue.global(qos: .background)) {
//            //print("All \(files.count) network requests completed")
//        }
        
        self.uploadCount += files.count
        
        self.concurrentQueue.async {
            
            for file in files {
                self.delegate?.uploadStarted(id: file.id,
                                             name: file.name,
                                             path: file.path)
                self.uploadFile(file: file)
            }

        }
    }
    public func multipartRequestUpload(file: AWSUploadFile) {
        self.uploadCount += 1
        self.concurrentQueue.async(group: nil, qos: .userInitiated, flags: .enforceQoS, execute: {
            self.delegate?.uploadStarted(id: file.id,
                                         name: file.name,
                                         path: file.path)
            self.uploadFile(file: file)
        })
    }
    
    private func uploadFile(file: AWSUploadFile) {
        
        var fileData: Data?
        
        switch file {
        case let imageFile as AWSUploadImage:
            fileData = imageFile.image.jpegData(compressionQuality: 0.3)
        case let audioFile as AWSUploadAudio:
            do {
                let url = URL(fileURLWithPath: audioFile.audioPath)
                fileData = try Data(contentsOf: url)
            } catch {
                //print(error)
                return
            }
        case let audioFile as AWSUploadVideo:
            do {
                let url = URL(fileURLWithPath: audioFile.videoPath)
                fileData = try Data(contentsOf: url)
            } catch {
                print(error)
                return
            }
        default:
            break
        }
        
        guard let data = fileData else {
            return
        }
        
        let bucket = StupaStrings.awsbucketName
        let key = file.path
        let contentType = file.mimeType
        print("PathName:", key)
        
        let expression = AWSS3TransferUtilityMultiPartUploadExpression()
//        expression.setValue("public-read", forRequestHeader: "x-amz-acl")
        if let params = file.metadata {
            for key in params.keys {
                if key == "video_resolution" {
                    expression.setValue((params[key] as! String), forRequestHeader: "x-amz-meta-video_resolution")
                } else if key == "coordinates" {
                    expression.setValue(params[key] as? String, forRequestHeader: "x-amz-meta-coordinates")
                }
                else if key == "table_detection" {
                    expression.setValue(params[key] as? String, forRequestHeader: "x-amz-meta-table_detection")
                }
            }
        }
        expression.progressBlock = { (task, progress) in
            DispatchQueue.main.async(execute: {
                print(progress)
                self.delegate?.uploadProgress(id: file.id, progress: progress)
            })
        }
        
        var completionHandler: AWSS3TransferUtilityMultiPartUploadCompletionHandlerBlock?
        completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                // Do something e.g. Alert a user for transfer completion.
                // On failed uploads, `error` contains the error object.
                //print("Task: ", task.description, "Error:", error.debugDescription)
                self.uploadCount -= 1
                self.delegate?.uploadProgressCompleted(id: file.id,
                                                       name: file.name,
                                                       path: file.path,
                                                       failure: error,
                                                       isOverAllCompleted: self.uploadCount == 0)
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        transferUtility.uploadUsingMultiPart(
            data: data,
            bucket: bucket,
            key: key,
            contentType: contentType,
            expression: expression,
            completionHandler: completionHandler
        ).continueWith { (task) -> Any? in
            
            
            // success
            if let _ = task.result {
                // return path name
                print("Uploaded Path name:", key)
            }
            
            return nil
        }
    }
    
    
    private func uploadFile1(file: AWSUploadFile, group: DispatchGroup) {
        
        var fileData: Data?
        
        switch file {
        case let imageFile as AWSUploadImage:
            fileData = imageFile.image.jpegData(compressionQuality: 0.3)
        case let audioFile as AWSUploadAudio:
            do {
                let url = URL(fileURLWithPath: audioFile.audioPath)
                fileData = try Data(contentsOf: url)
            } catch {
                //print(error)
                return
            }
        case let audioFile as AWSUploadVideo:
            do {
                let url = URL(fileURLWithPath: audioFile.videoPath)
                fileData = try Data(contentsOf: url)
            } catch {
                print(error)
                return
            }
        default:
            break
        }
        
        guard let data = fileData else {
            group.leave()
            return
        }
        
        let bucket = StupaStrings.awsbucketName
        let key = file.path
        let contentType = file.mimeType
        print("PathName:", key)
        
        let expression = AWSS3TransferUtilityMultiPartUploadExpression()
//        expression.setValue("public-read", forRequestHeader: "x-amz-acl")
        if let params = file.metadata {
            for key in params.keys {
                if key == "video_resolution" {
                    expression.setValue((params[key] as! String), forRequestHeader: "x-amz-meta-video_resolution")
                } else if key == "coordinates" {
                    expression.setValue(params[key] as? String, forRequestHeader: "x-amz-meta-coordinate")
                }
                else if key == "table_detection" {
                    expression.setValue(params[key] as? String, forRequestHeader: "x-amz-meta-table_detection")
                }
            }
        }
        expression.progressBlock = { (task, progress) in
            DispatchQueue.main.async(execute: {
                print(progress)
                self.delegate?.uploadProgress(id: file.id, progress: progress)
            })
        }
        
        var completionHandler: AWSS3TransferUtilityMultiPartUploadCompletionHandlerBlock?
        completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                // Do something e.g. Alert a user for transfer completion.
                // On failed uploads, `error` contains the error object.
                //print("Task: ", task.description, "Error:", error.debugDescription)
                self.uploadCount -= 1
                self.delegate?.uploadProgressCompleted(id: file.id,
                                                       name: file.name,
                                                       path: file.path,
                                                       failure: error,
                                                       isOverAllCompleted: self.uploadCount == 0)
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        transferUtility.uploadUsingMultiPart(
            data: data,
            bucket: bucket,
            key: key,
            contentType: contentType,
            expression: expression,
            completionHandler: completionHandler
        ).continueWith { (task) -> Any? in
            
            // failure
//            if let _ = task.error {
//                //print("Error : \(error.localizedDescription)")
//            }
            
            // success
            if let _ = task.result {
                // return path name
                print("Uploaded Path name:", key)
            }
            group.leave()
            
            return nil
        }
    }
    
    public func deleteRequestFromAWSS3(key: String) {
        let awss3 = AWSS3.default()
        guard let deleteObjectRequest = AWSS3DeleteObjectRequest() else { return }
        deleteObjectRequest.bucket = StupaStrings.awsbucketName
        deleteObjectRequest.key = key
        awss3.deleteObject(deleteObjectRequest).continueWith { (task: AWSTask) -> AnyObject? in
            if let _ = task.error {
                //print("Error occurred: \(error)")
                return nil
            }
            //print("Deleted successfully.")
            return nil
        }
    }
    
}
