//
//  CustomNavigation.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 15/04/21.
//

import UIKit

class CustomNavigation: UIView {
    
    var completionHandler: () -> Void = {}
    var root: UIViewController?
    var navTitle:String?
    var backbtnImageShow:Bool? = true
    var isbackNeeded:Bool? = true
    
    //MARK:- Objects
    
    @IBOutlet weak var backbtncenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var titlecenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var backbtnImage: UIView!
    @IBOutlet weak var navigationtitleLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        adjustingUIforModes()
        
        if backbtnImageShow == true{
            backbtnImage.isHidden = false
        }else{
            backbtnImage.isHidden = true
        }
        self.frame = self.bounds
        navigationtitleLabel.text = navTitle
        backbtnImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popController)))
        
    }
    
    @objc func adjustingUIforModes(){

        if UIDevice.current.hasNotch {
            titlecenterConstraint.constant  = 14
            backbtncenterConstraint.constant = 10
        } else {
            titlecenterConstraint.constant  = 8
            backbtncenterConstraint.constant = 5
        }
    }
    
    @objc func popController(viewController:UIViewController){
        if isbackNeeded ?? false{
            root?.navigationController?.popViewController(animated: true)
        }else{
            self.completionHandler()
        }
    }
    
    func instanceFromNib() -> UIView {
        return UINib(nibName: "CustomNavigation", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}
