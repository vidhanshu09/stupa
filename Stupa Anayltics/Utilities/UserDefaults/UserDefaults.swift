//
//  UserDefaults.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import Foundation

let SUserDefaults = UserDefaults.standard

public struct SUDConstants{
    public static let tokenValue:String = "tokenValue"
    public static let userName:String = "username"
    public static let userCountry:String = "usercountry"
    public static let userPic:String = "userpic"
    public static let RightyplayerID:String = "rightplayerId"
    public static let RightplayerName:String = "rightplayerName"
    public static let LeftyplayerID:String = "leftyplayerId"
    public static let LeftplayerName:String = "leftplayerName"
    public static let userID:String = "userid"
    public static let playerID:String = "playerid"
}
