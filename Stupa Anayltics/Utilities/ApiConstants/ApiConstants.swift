//
//  ApiConstants.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import Foundation

public struct ApiConstants{
    public static let BASEURL  = { () -> String in
        switch serverConfig.rawValue {
        case ServerConfigType.Production.rawValue:      // Prod Environment
            return "https://tkwonyg9yb.execute-api.us-east-1.amazonaws.com/AiProd/api/services/"
        case ServerConfigType.Development.rawValue:     // Dev Environment
            return "https://yfll3o5s3c.execute-api.us-east-1.amazonaws.com/AiTest/api/services/"
        default:
            return ""
        }
    }

    public static let login = BASEURL() + "UserAuth/AuthenticateLogin_V1"
    public static let getHomeData = BASEURL() + "userauth/HomeData"
    public static let getMatchesList = BASEURL() + "game/GetMatchList_V1"
    public static let getMatchStatsforRealTime = BASEURL() + "Game/AllMatchData"
    public static let saveMatchDetails = BASEURL() + "VideoUpload/SaveMatchAndUpload"
    public static let uploadVideo = BASEURL() + "VideoUpload/SaveMatchFiles"
    public static let getMatchStatsforExpertAnalysis = BASEURL() + "Game/AllMatchData"
    public static let getGameVideos = BASEURL() + "Game/GetGameVideos"
}
