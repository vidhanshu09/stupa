//
//  Classes.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import Foundation
import Charts

class CheckInternet{
    
    internal static func checkInternetandServer(error:Error,sender:UIViewController){
        
        if error.localizedDescription == "URLSessionTask failed with error: The Internet connection appears to be offline."
        {
            sender.showAlertwithTitle(with: AlertConstants.NoInternetAlert, title: AlertConstants.Errortitle)
        }else{
            sender.showAlertwithTitle(with: AlertConstants.ServerErrorAlert, title: AlertConstants.Errortitle)
        }
        return
        
    }
    
}

class GraphYAxisLabel:UILabel{
    override func layoutSubviews() {
        super.layoutSubviews()
        self.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
    }
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        // you can change 'addedHeight' into any value you want.
        let addedHeight = font.pointSize * 0.2
        return CGSize(width: size.width, height: addedHeight)
    }
}

class CustomWindow: UIWindow{
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        // Do any action you would like to perform to indicate the application is active
        NotificationCenter.default.post(name: Notification.Name("eventTouch"), object: nil)
        return false
    }
}

class CustomIntFormatter: NSObject, IValueFormatter{
    public func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        let correctValue = Int(value)
        //print("correctValue: \(correctValue)")
        return String(correctValue)
    }
}
