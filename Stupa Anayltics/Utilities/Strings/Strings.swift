//
//  Strings.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import Foundation

struct StoryBoards {
    public static let MainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    public static let LoginStoryboard = UIStoryboard(name: "Login", bundle: nil)
    public static let MyAnalysisStoryboard = UIStoryboard(name: "MyAnalysis", bundle: nil)
    public static let PracticeAnalysisStoryboard = UIStoryboard(name: "PracticeAnalysis", bundle: nil)
    public static let ExtrasStoryboard = UIStoryboard(name: "Extras", bundle: nil)
    public static let SideMenuStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
    public static let MatchDetailsStoryboard = UIStoryboard(name: "MatchDetails", bundle: nil)
    public static let VideoRecordingStoryboard = UIStoryboard(name: "VideoRecording", bundle: nil)
    public static let ExpertAnalysisStoryboard = UIStoryboard(name: "ExpertAnalysis", bundle: nil)
}

struct StupaStrings {
    public static let DeviceType = "iOS"
    public static var DeviceUUID  : String?
    public static let NotificationToken = "fxcgVTFxRIKa9oUPo0wai0:APA91bHNEF0OHwS5Kw8hn8z_fYBJ5J7-9Cqbx_MOeB8JmgtdjLXt48uJVD3pSUmqF3gKocuAlKE0dWZeK-2UZO60iUlLeFJzu3Xf9aPgXe0DlWjzlDEtKIx5y2oFpiDmpsmFc0beJoC4"
    public static let successString = 1
    public static let loginAgain = "Your are Not Login.Login Please!"
    public static let NoMatches = "No Matches."
    static var aws3poolId = "us-east-1:543fdea8-031a-4bbf-b472-727dedd2282d"
    static var awsbucketName = "stupa-ai-test-user-upload-pub"
    
}
