//
//  Utils.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 13/04/21.
//

import Foundation
import AVFoundation

class Utils {
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func getFileNameFromURL(urlString:String) -> String? {
        let array = urlString.components(separatedBy: "StupaVideos/")
        return array[1]
    }
    
    func getStupaVideosActualFolderPath() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let fileUrl = paths[0].appendingPathComponent("StupaVideos/")
        return fileUrl
    }
    
    enum BallsType: String {
        case Single = "Single Ball"
        case Many = "Many Balls"
    }
    
    enum VideoType{
        case LongestRally
        case FastestRally
        case ErrorPlacement
    }
    
    enum MatchStatsType{
        case Practice
        case Match
        case Expert
    }
    
    func savedataonLogin(data:LoginResponseData?){
        SUserDefaults.set(data?.token, forKey: SUDConstants.tokenValue)
        SUserDefaults.synchronize()
        SUserDefaults.set(data?.user?.userID, forKey: SUDConstants.userID)
        SUserDefaults.synchronize()
        SUserDefaults.set(data?.user?.playerID, forKey: SUDConstants.playerID)
        SUserDefaults.synchronize()
    }
    
    func getStupaVideosFolderPath(forFileName:String) -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let fileUrl = paths[0].appendingPathComponent("StupaVideos/").appendingPathComponent(forFileName).appendingPathExtension("mp4")
        return fileUrl
    }
    
    // Screen width.
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }

    // Screen height.
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    
    func getToken() -> String{
        guard let tokenValue = SUserDefaults.value(forKey: SUDConstants.tokenValue) as? String else{
            return ""
        }
        return tokenValue
    }
    
    func getCurrentTimeStamp() -> Int64 {
        return Int64(NSDate().timeIntervalSince1970 * 1000)
    }
    
    enum matchAngle:String {
        case CoachAngle = "Coach"
        case UmpireAngle = "Umpire"
    }
    
    enum matchType {
        case CoachAnglePlayer
        case CoachAngleOpponent
        case UmpireAnglePlayer
        case UmpireAngleOpponent
        case Practice
    }
    
    func createStupaFolderPath() {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let docURL = URL(string: documentsDirectory)!
        let dataPath = docURL.appendingPathComponent("StupaVideos/")
        if !FileManager.default.fileExists(atPath: dataPath.absoluteString) {
            do {
                try FileManager.default.createDirectory(atPath:dataPath.absoluteString, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
            }
        }
        
    }
    
    func convertdoctoBase64andgetFileName(doc:URL) -> (String,String,String){
        let filePath = doc //URL of the PDF
        var base64String = String()
        do {
            let fileData = try Data.init(contentsOf: filePath)
            let fileStream:String = fileData.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
            //print(fileStream)
            base64String = fileStream
            //            let decodeData = Data(base64Encoded: fileStream, options: .ignoreUnknownCharacters)
            //            var body = Data()
            //            body = decodeData!
            //            print(body)
        }
        catch let error{
            print(error.localizedDescription)
        }
        
        let pickedFile = doc.lastPathComponent
        let sendFile = stripFileExtension(pickedFile)
        let fileExtension = doc.pathExtension
        return (base64String,sendFile,String(fileExtension))
    }
    
    func stripFileExtension ( _ filename: String ) -> String {
        var components = filename.components(separatedBy: ".")
        guard components.count > 1 else { return filename }
        components.removeLast()
        return components.joined(separator: ".")
    }
    
    func deleteUserDefaults(completion:() -> Void){
        SUserDefaults.removeObject(forKey: SUDConstants.tokenValue)
        SUserDefaults.synchronize()
        SUserDefaults.removeObject(forKey: SUDConstants.userName)
        SUserDefaults.synchronize()
        SUserDefaults.removeObject(forKey: SUDConstants.userCountry)
        SUserDefaults.synchronize()
        SUserDefaults.removeObject(forKey: SUDConstants.userPic)
        SUserDefaults.synchronize()
        completion()
    }
    enum SpeedChartType {
        case WON
        case TOTAL
    }
    
    func setupMultiLabelView(labelwidthConstraint:NSLayoutConstraint,frameView:UIView,won:Int?,lost:Int?){
        if let shortSortplayerAWon = won{
            if let shortSortplayerAlost = lost{
                let totalmatchper = shortSortplayerAWon + shortSortplayerAlost
                let lossss = Double(shortSortplayerAlost)/Double(totalmatchper)
                if shortSortplayerAWon == 0 && shortSortplayerAlost == 0{
                    let width2 = frameView.frame.width/2
                    labelwidthConstraint.constant = width2
                }
                else if shortSortplayerAWon == 0{
                    let width2 = frameView.frame.width - 10 * CGFloat(lossss)
                    if width2.isNaN{
                        labelwidthConstraint.constant = 10
                    }else{
                        labelwidthConstraint.constant = width2
                    }
                }else if shortSortplayerAlost == 0{
                    labelwidthConstraint.constant = 10
                }
                else {
                    let width2 = frameView.frame.width * CGFloat(lossss)
                    if width2.isNaN{
                        labelwidthConstraint.constant = 10
                    }else{
                        labelwidthConstraint.constant = width2
                    }
                }
            }
        }
    }
    
    func getCurrentDateString() -> String{
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        
        let year = components.year
        let month = components.month
        let day = components.day
        //        let hour = components.hour
        //        let minute = components.minute
        //        let second = components.second
        
        let today_string = String(month!) + "/" + String(day!) + "/" + String(year!)
        return today_string
        
    }
    
    func saveImage(imageName: String, image: UIImage) {

     guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }

        let fileName = imageName
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return }

        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }

        }

        do {
            try data.write(to: fileURL)
        } catch let error {
            print("error saving file with error", error)
        }

    }
    
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) -> UIImage{
        var imag = UIImage()
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                imag = image
            }
        }.resume()
        
        return imag
    }
    
    func downloadandSave(url:String?,name:String){
        saveImage(imageName: name, image: downloaded(from: URL(string: url ?? "")!))
    }
    
}
