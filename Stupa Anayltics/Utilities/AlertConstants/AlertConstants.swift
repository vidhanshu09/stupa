//
//  AlertConstants.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 13/04/21.
//

import Foundation

public struct AlertConstants{
    public static let Errortitle = "Error!"
    public static let ServerErrorAlert = "Some Error Occurred! Please try again."
    public static let NoInternetAlert = "No Internet Connection."
    public static let Okbutton = "Ok"
    public static let enterEmail = "Please enter Email."
    public static let enterPhone = "Please enter your Mobile No."
    public static let enterPassword = "Please enter your Password."
    public static let entervalidEmail = "Please enter valid Email."
    public static let entervalidPhone = "Please enter valid Mobile No."
    public static let selectplayerDominating = "Please Select Player Dominating Hand."
    public static let selectplayerSide = "Please Select Player Side."
    public static let selectStroke = "Please Select Stroke."
    public static let selectPlacement = "Please Select Placement."
    public static let selectplayerforeHand = "Please Select Player Forehand."
    public static let selectplayerbackHand = "Please Select Player Backhand."
    public static let selectopponentforeHand = "Please Select Opponent Forehand."
    public static let selectopponentbackHand = "Please Select Opponent Backhand."
    public static let enteropponentName = "Please Enter Opponent Name."
    public static let selectOpponentDominating = "Please Select Opponent Dominating Hand."
}
