//
//  BaseVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 14/04/21.
//

import UIKit

class BaseVC: UIViewController {

    var backpopView:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func setupBackIcon(view:UIView){
        backpopView = view
        addingGesture()
    }
    func addingGesture(){
        backpopView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popView)))
    }
    
    @objc func popView(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationwithTitle(navView:UIView?,navTitle:String?,withbackbtn:Bool? = true,isbackNeeded:Bool? = true,completionHandler: @escaping() -> Void = {}){
        navView?.setCustomNavigationwithTitle(navTitle: navTitle ?? "", root: self, withbackbtn: withbackbtn, completionHandler: completionHandler,isbackNeeded: isbackNeeded)
    }
    
    
}
