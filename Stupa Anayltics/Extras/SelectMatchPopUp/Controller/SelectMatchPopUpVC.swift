//
//  SelectMatchPopUpVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 20/04/21.
//

import UIKit

protocol MatchSelected:class {
    func navigate(isCoach:Bool)
}

class SelectMatchPopUpVC: UIViewController {
    
    weak var naviagatedelegate:MatchSelected?
    
    //MARK:- Outlets
    
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var realtimeView: UIView!
    @IBOutlet weak var expertanalysisView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        popupView.dropShadow()
        addingGestures()
    }
    
    func addingGestures(){
        closeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissView)))
        realtimeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(realTimeSelected)))
        expertanalysisView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(expertAnalsysisSelected)))
    }
    
    @objc func realTimeSelected(){
        self.dismiss(animated: false, completion: nil)
        self.naviagatedelegate?.navigate(isCoach: false)
    }
    
    @objc func expertAnalsysisSelected(){
        self.dismiss(animated: false, completion: nil)
        self.naviagatedelegate?.navigate(isCoach: true)
    }
    
    @objc func dismissView(){
        self.dismiss(animated: false, completion: nil)
    }

}
