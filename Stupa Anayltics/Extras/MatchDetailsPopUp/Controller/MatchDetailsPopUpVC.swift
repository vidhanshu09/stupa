//
//  MatchDetailsPopUpVC.swift
//  Stupa
//
//  Created by Vidhanshu Bhardwaj on 03/04/21.
//

import UIKit
import DropDown

protocol updatePlayerData:class{
    func update(dominatingHand:String?,side:String?,foreHand:String?,backHand:String?)
}
protocol updateOpponentData:class{
    func update(name:String?,dominatingHand:String?,foreHand:String?,backHand:String?)
}

class MatchDetailsPopUpVC: UIViewController {
    
    var playerName:String?
    var playerforehand:String?
    var playerbackhand:String?
    var opponentforehand:String?
    var opponentbackhand:String?
    var playerdominatingHand:String?
    var playerSide:String?
    var opponentName:String?
    var opponentdominatingHand:String?
    var matchType:Utils.matchType!
    var dominatingDropdown = DropDown()
    var sideDropdown = DropDown()
    var forehandDropdown = DropDown()
    var backhandDropdown = DropDown()
    weak var playerdelegate: updatePlayerData?
    weak var opponentdelegate: updateOpponentData?
    
    //MARK:- Outlets
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var dominatingView: UIView!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var sideTextfield: UITextField!
    @IBOutlet weak var dominatinghandTextfield: UITextField!
    @IBOutlet weak var popuptitleLabel: UILabel!
    @IBOutlet weak var sideviewtopConstraint: NSLayoutConstraint!
    @IBOutlet weak var sidetitleLabel: UILabel!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var sidetextfieldheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sidedownarrowheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var forehandtitleLabel: UILabel!
    @IBOutlet weak var backhandtitleLabel: UILabel!
    @IBOutlet weak var forehandtextfieldheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backhandhandtextfieldheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backhandarrowheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var forehandarrowheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var forebackviewtopConstraint: NSLayoutConstraint!
    @IBOutlet weak var forehanddownarrowImageView: UIImageView!
    @IBOutlet weak var backhanddownarrowImageView: UIImageView!
    @IBOutlet weak var backdownarrowView: UIView!
    @IBOutlet weak var forehanddownarrowView: UIView!
    @IBOutlet weak var forehandTextfield: UITextField!
    @IBOutlet weak var backhandTextfield: UITextField!
    @IBOutlet weak var forehandView: UIView!
    @IBOutlet weak var backhandView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gestures()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func gestures(){
        closeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closePopupView)))
        dominatingView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openDropdown)))
        sideView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openSideDropdown)))
        forehandView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openforehandDropdown)))
        backhandView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openbackhandDropdown)))
    }
    
    @objc func closePopupView(){
        self.dismiss(animated: false, completion: nil)
        if matchType == .UmpireAnglePlayer || matchType == .Practice{
            self.playerdelegate?.update(dominatingHand: dominatinghandTextfield.text ?? "", side: sideTextfield.text ?? "",foreHand: "",backHand: "")
        }else if matchType == .UmpireAngleOpponent{
            self.opponentdelegate?.update(name: nameTextfield.text ?? "Opponent", dominatingHand: dominatinghandTextfield.text ?? "",foreHand: "",backHand: "")
        }else if matchType == .CoachAnglePlayer{
            self.playerdelegate?.update(dominatingHand: dominatinghandTextfield.text ?? "", side: sideTextfield.text ?? "",foreHand: forehandTextfield.text ?? "",backHand: backhandTextfield.text ?? "")
        }else{
            self.opponentdelegate?.update(name: nameTextfield.text ?? "Opponent", dominatingHand: dominatinghandTextfield.text ?? "",foreHand: forehandTextfield.text ?? "",backHand: backhandTextfield.text ?? "")
        }
    }
    
    func adjustingUI(){
        if matchType == .UmpireAngleOpponent{
            popuptitleLabel.text = "Opponent Details"
            sideviewtopConstraint.constant = 0
            sidetitleLabel.text = ""
            sidetextfieldheightConstraint.constant = 0
            sidedownarrowheightConstraint.constant = 0
            forebackviewtopConstraint.constant = 0
            forehandtitleLabel.text = ""
            forehandtextfieldheightConstraint.constant = 0
            forehandarrowheightConstraint.constant = 0
            backhandtitleLabel.text = ""
            backhandhandtextfieldheightConstraint.constant = 0
            backhandarrowheightConstraint.constant = 0
            nameTextfield.text = self.opponentName
            dominatinghandTextfield.text = self.opponentdominatingHand
            
        }else if matchType == .UmpireAnglePlayer || matchType == .Practice{
            
            dominatinghandTextfield.text = self.playerdominatingHand
            sideTextfield.text = self.playerSide
            nameTextfield.text = self.playerName
            nameTextfield.isUserInteractionEnabled = false
            forebackviewtopConstraint.constant = 0
            forehandtitleLabel.text = ""
            forehandtextfieldheightConstraint.constant = 0
            forehandarrowheightConstraint.constant = 0
            backhandtitleLabel.text = ""
            backhandhandtextfieldheightConstraint.constant = 0
            backhandarrowheightConstraint.constant = 0
            
        }else if matchType == .CoachAngleOpponent{
            forehandTextfield.text = self.opponentforehand
            backhandTextfield.text = self.opponentbackhand
            popuptitleLabel.text = "Opponent Details"
            sideviewtopConstraint.constant = 0
            sidetitleLabel.text = ""
            sidetextfieldheightConstraint.constant = 0
            sidedownarrowheightConstraint.constant = 0
            nameTextfield.text = self.opponentName
            dominatinghandTextfield.text = self.opponentdominatingHand
            
        }else if matchType == .CoachAnglePlayer{
            forehandTextfield.text = self.playerforehand
            backhandTextfield.text = self.playerbackhand
            dominatinghandTextfield.text = self.playerdominatingHand
            sideTextfield.text = self.playerSide
            nameTextfield.text = self.playerName
            nameTextfield.isUserInteractionEnabled = false
            
        }
        setdominatingDropdown()
        setsideDropdown()
        setforehandDropdown()
        setbackhandDropdown()
        popupView.dropShadow(color: UIColor.darkGray.cgColor)
    }
    
    @objc func openDropdown(){
        dominatingDropdown.show()
    }
    @objc func openSideDropdown(){
        sideDropdown.show()
    }
    @objc func openforehandDropdown(){
        forehandDropdown.show()
    }
    @objc func openbackhandDropdown(){
        backhandDropdown.show()
    }
    
    func setdominatingDropdown(){
        self.dominatingDropdown.selectedTextColor = Colors.AppGreenColor
        self.dominatingDropdown.backgroundColor = UIColor.black
        self.dominatingDropdown.textColor = UIColor.white
        self.dominatingDropdown.textFont = UIFont(name: "montserrat-regular", size: 12)!
        self.dominatingDropdown.bottomOffset = CGPoint(x: 0, y: self.dominatinghandTextfield.bounds.height + 15)
        self.dominatingDropdown.anchorView = self.dominatingView.forLastBaselineLayout // UIView or UIBarButtonItem
        self.dominatingDropdown.direction = .bottom
        self.dominatingDropdown.dataSource = ["Left","Right"]
        self.dominatingDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print(item)
            self.dominatinghandTextfield.text = item
        }
    }
    func setsideDropdown(){
        self.sideDropdown.selectedTextColor = Colors.AppGreenColor
        self.sideDropdown.backgroundColor = UIColor.black
        self.sideDropdown.textColor = UIColor.white
        self.sideDropdown.textFont = UIFont(name: "montserrat-regular", size: 12)!
        self.sideDropdown.bottomOffset = CGPoint(x: 0, y: self.sideTextfield.bounds.height + 15)
        self.sideDropdown.anchorView = self.sideView.forLastBaselineLayout // UIView or UIBarButtonItem
        self.sideDropdown.direction = .bottom
        self.sideDropdown.dataSource = ["Left","Right"]
        self.sideDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print(item)
            self.sideTextfield.text = item
        }
    }
    func setforehandDropdown(){
        self.forehandDropdown.selectedTextColor = Colors.AppGreenColor
        self.forehandDropdown.backgroundColor = UIColor.black
        self.forehandDropdown.textColor = UIColor.white
        self.forehandDropdown.textFont = UIFont(name: "montserrat-regular", size: 12)!
        self.forehandDropdown.bottomOffset = CGPoint(x: 0, y: self.forehandTextfield.bounds.height + 15)
        self.forehandDropdown.anchorView = self.forehandView.forLastBaselineLayout // UIView or UIBarButtonItem
        self.forehandDropdown.direction = .bottom
        self.forehandDropdown.dataSource = ["Inverted Pips", "Long Pips Out", "Short Pips Out"]
        self.forehandDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print(item)
            self.forehandTextfield.text = item
        }
    }
    func setbackhandDropdown(){
        self.backhandDropdown.selectedTextColor = Colors.AppGreenColor
        self.backhandDropdown.backgroundColor = UIColor.black
        self.backhandDropdown.textColor = UIColor.white
        self.backhandDropdown.textFont = UIFont(name: "montserrat-regular", size: 12)!
        self.backhandDropdown.bottomOffset = CGPoint(x: 0, y: self.backhandTextfield.bounds.height + 15)
        self.backhandDropdown.anchorView = self.backhandView.forLastBaselineLayout // UIView or UIBarButtonItem
        self.backhandDropdown.direction = .bottom
        self.backhandDropdown.dataSource = ["Inverted Pips", "Long Pips Out", "Short Pips Out"]
        self.backhandDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print(item)
            self.backhandTextfield.text = item
        }
    }
    
}
