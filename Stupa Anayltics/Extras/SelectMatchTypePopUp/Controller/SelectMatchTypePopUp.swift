//
//  SelectMatchTypePopUp.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 21/04/21.
//

import UIKit

protocol MatchTypeSelected:class {
    func navigate(isRecord:Bool)
}

class SelectMatchTypePopUp: UIViewController {
    
    weak var naviagatedelegate:MatchTypeSelected?
    
    //MARK:- Outlets
    
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var recordView: UIView!
    @IBOutlet weak var uploadView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        popupView.dropShadow()
        addingGestures()
    }
    
    func addingGestures(){
        closeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissView)))
        recordView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(recordSelected)))
        uploadView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(uploadSelected)))
    }
    
    @objc func recordSelected(){
        self.dismiss(animated: false, completion: nil)
        self.naviagatedelegate?.navigate(isRecord: true)
    }
    
    @objc func uploadSelected(){
        self.dismiss(animated: false, completion: nil)
        self.naviagatedelegate?.navigate(isRecord: false)
    }
    
    @objc func dismissView(){
        self.dismiss(animated: false, completion: nil)
    }
}
