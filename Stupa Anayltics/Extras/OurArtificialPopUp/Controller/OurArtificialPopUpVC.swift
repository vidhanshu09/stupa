//
//  OurArtificialPopUpVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 19/04/21.
//

import UIKit

class OurArtificialPopUpVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var popupView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        popupView.dropShadow()
        popupView.layer.cornerRadius = 5
    }

    @IBAction func okaybtnAct(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

}
