//
//  ThingstoRememberVC.swift
//  Stupa
//
//  Created by Vidhanshu Bhardwaj on 03/04/21.
//

import UIKit

protocol ThingstoRememberDone:class {
    func update()
}

class ThingstoRememberVC: UIViewController {

    weak var delegate:ThingstoRememberDone?
    
    //MARK:- Outlets
    
    @IBOutlet weak var popupView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        popupView.dropShadow()
    }

    @IBAction func okaybtnAct(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        self.delegate?.update()
    }
}
