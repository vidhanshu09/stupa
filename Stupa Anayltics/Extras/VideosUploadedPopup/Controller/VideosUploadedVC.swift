//
//  VideosUploadedVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 21/04/21.
//

import UIKit

class VideosUploadedVC: UIViewController {

    var isCoachAngle:Bool? = false
    
    //MARK:- Outlets
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var descLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        
        if isCoachAngle ?? false{
            descLabel.text = "Your video(s) is uploaded successfully. It will reflect in my analysis within 2-3 days"
        }
        
        popupView.dropShadow()
        popupView.layer.cornerRadius = 5
    }

    @IBAction func okaybtnAct(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

}
