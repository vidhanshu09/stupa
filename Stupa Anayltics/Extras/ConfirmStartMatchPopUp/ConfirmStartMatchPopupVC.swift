//
//  ConfirmStartMatchPopupVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 21/04/21.
//

import UIKit

protocol ConfirmStartMatch:class {
    func start(start:Bool)
}

class ConfirmStartMatchPopupVC: UIViewController {

    weak var delegate:ConfirmStartMatch?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func yesbtnAct(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        delegate?.start(start: true)
    }
    @IBAction func nobtnAct(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        delegate?.start(start: false)
    }
}
