//
//  MatchDetailsVC.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 20/04/21.
//

import UIKit

class MatchDetailsVC: BaseVC {
    
    var savematchdetailsViewModel:SaveMatchDetailsViewModel!
    var opponentId:String?
    var isCoachAngle:Bool!
    var playerName:String?
    var playerdominatingHand:String?
    var playerSide:String?
    var playerforehand:String?
    var playerbackhand:String?
    var opponentforehand:String?
    var opponentbackhand:String?
    var opponentdominatingHand:String?
    var opponentName:String?
    var matchAngle:Utils.matchAngle?
    
    //MARK:- Outlets
    
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var segmentView: UIView!
    @IBOutlet weak var segmentviewtopConstraint: NSLayoutConstraint!
    @IBOutlet weak var segmentviewheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var participantLabel: UILabel!
    @IBOutlet weak var opponentarrowView: UIView!
    @IBOutlet weak var participantarrowView: UIView!
    @IBOutlet weak var oppnentviewtopConstraint: NSLayoutConstraint!
    @IBOutlet weak var opponentarrowImageView: UIImageView!
    @IBOutlet weak var opponentLabel: UILabel!
    @IBOutlet weak var opponentlabeltopConstraint: NSLayoutConstraint!
    @IBOutlet weak var opponentlabelbottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftyopponentbtnObj: UIButton!
    @IBOutlet weak var rightyopponentbtnObj: UIButton!
    @IBOutlet weak var customopponentbtnObj: UIButton!
    @IBOutlet weak var opponentarrowviewtopConstraint: NSLayoutConstraint!
    @IBOutlet weak var opponentarrowviewbottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        adjustingConstraints()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }
    
    func adjustingUI(){
        setNavigationwithTitle(navView: navigationView, navTitle: "Match Details")
        if let userName = SUserDefaults.value(forKey: SUDConstants.userName) as? String{
            playerName = userName
        }
        matchAngle = .CoachAngle
        if !isCoachAngle{
            matchAngle = .UmpireAngle
            if let leftplayerName = SUserDefaults.value(forKey: SUDConstants.LeftplayerName) as? String{
                self.opponentName = leftplayerName
            }
            if let leftplayerID = SUserDefaults.value(forKey: SUDConstants.LeftyplayerID) as? Int{
                self.opponentId = String(leftplayerID)
            }
            self.opponentdominatingHand = "Left"
        }
        leftyopponentbtnObj.backgroundColor = Colors.AppGreenColor
        leftyopponentbtnObj.setTitleColor(UIColor.white, for: .normal)
        participantLabel.text = playerName
        dateLabel.text = Utils().getCurrentDateString()
        addGestures()
        savematchdetailsViewModel = SaveMatchDetailsViewModel(root: self, matchAngle: matchAngle)
    }
    
    func addGestures(){
        participantarrowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openPopUpPlayer)))
        opponentarrowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openPopUpOpponent)))
    }
    
    func adjustingConstraints(){
        opponentarrowviewtopConstraint.constant = 0
        opponentarrowviewbottomConstraint.constant = 0
        opponentlabelbottomConstraint.constant = 0
        opponentlabeltopConstraint.constant = 0
        oppnentviewtopConstraint.constant = 0
        opponentLabel.text = ""
        opponentarrowImageView.isHidden = true
        if isCoachAngle{
            segmentView.isHidden = true
            segmentviewheightConstraint.constant = 0
            segmentviewtopConstraint.constant = -10
            self.opponentId = ""
            self.opponentdominatingHand = ""
            opponentlabelbottomConstraint.constant = 13.5
            opponentlabeltopConstraint.constant = 13.5
            oppnentviewtopConstraint.constant = 20
            opponentarrowviewtopConstraint.constant = 2
            opponentarrowviewbottomConstraint.constant = 2
            self.opponentLabel.text =  self.opponentName ?? "Opponent"
            opponentarrowImageView.isHidden = false
        }
    }
    
    @IBAction func leftybtnAct(_ sender: Any) {
        self.opponentdominatingHand = "Left"
        if let leftplayerName = SUserDefaults.value(forKey: SUDConstants.LeftplayerName) as? String{
            self.opponentName = leftplayerName
        }
        if let leftplayerID = SUserDefaults.value(forKey: SUDConstants.LeftyplayerID) as? Int{
            self.opponentId = String(leftplayerID)
        }
        leftyopponentbtnObj.backgroundColor = Colors.AppGreenColor
        leftyopponentbtnObj.setTitleColor(UIColor.white, for: .normal)
        rightyopponentbtnObj.backgroundColor = .white
        rightyopponentbtnObj.setTitleColor(UIColor.darkGray, for: .normal)
        customopponentbtnObj.backgroundColor = .white
        customopponentbtnObj.setTitleColor(UIColor.darkGray, for: .normal)
        adjustingConstraints()
    }
    @IBAction func rightybtnAct(_ sender: Any) {
        self.opponentdominatingHand = "Right"
        if let rightplayerName = SUserDefaults.value(forKey: SUDConstants.RightplayerName) as? String{
            self.opponentName = rightplayerName
        }
        if let rightplayerID = SUserDefaults.value(forKey: SUDConstants.RightyplayerID) as? Int{
            self.opponentId = String(rightplayerID)
        }
        rightyopponentbtnObj.backgroundColor = Colors.AppGreenColor
        rightyopponentbtnObj.setTitleColor(UIColor.white, for: .normal)
        leftyopponentbtnObj.backgroundColor = .white
        leftyopponentbtnObj.setTitleColor(UIColor.darkGray, for: .normal)
        customopponentbtnObj.backgroundColor = .white
        customopponentbtnObj.setTitleColor(UIColor.darkGray, for: .normal)
        adjustingConstraints()
    }
    
    @IBAction func custombtnAct(_ sender: Any) {
        self.opponentId = ""
        self.opponentdominatingHand = ""
        customopponentbtnObj.backgroundColor = Colors.AppGreenColor
        customopponentbtnObj.setTitleColor(UIColor.white, for: .normal)
        leftyopponentbtnObj.backgroundColor = .white
        leftyopponentbtnObj.setTitleColor(UIColor.darkGray, for: .normal)
        rightyopponentbtnObj.backgroundColor = .white
        rightyopponentbtnObj.setTitleColor(UIColor.darkGray, for: .normal)
        opponentarrowviewtopConstraint.constant = 2
        opponentarrowviewbottomConstraint.constant = 2
        opponentlabelbottomConstraint.constant = 13.5
        opponentlabeltopConstraint.constant = 13.5
        oppnentviewtopConstraint.constant = 20
        if self.opponentName == "Opponent Righty" || self.opponentName == "Opponent Lefty"{
            self.opponentLabel.text = "Opponent"
            self.opponentName = "Opponent"
        }else{
            self.opponentLabel.text =  self.opponentName ?? "Opponent"
        }
        opponentarrowImageView.isHidden = false
    }
    
    @objc func openPopUpPlayer(){
        let matchdetailsPopup = StoryBoards.ExtrasStoryboard.instantiateViewController(withIdentifier: "MatchDetailsPopUpVC") as! MatchDetailsPopUpVC
        if isCoachAngle{
            matchdetailsPopup.matchType = .CoachAnglePlayer
        }else{
            matchdetailsPopup.matchType = .UmpireAnglePlayer
        }
        matchdetailsPopup.playerName = self.playerName
        matchdetailsPopup.playerdominatingHand = self.playerdominatingHand
        matchdetailsPopup.playerSide = self.playerSide
        matchdetailsPopup.playerforehand = self.playerforehand
        matchdetailsPopup.playerbackhand = self.playerbackhand
        matchdetailsPopup.playerdelegate = self
        matchdetailsPopup.modalPresentationStyle = .overFullScreen
        self.present(matchdetailsPopup, animated: false, completion: nil)
    }
    @objc func openPopUpOpponent(){
        let matchdetailsPopup = StoryBoards.ExtrasStoryboard.instantiateViewController(withIdentifier: "MatchDetailsPopUpVC") as! MatchDetailsPopUpVC
        if isCoachAngle{
            matchdetailsPopup.matchType = .CoachAngleOpponent
        }else{
            matchdetailsPopup.matchType = .UmpireAngleOpponent
        }
        matchdetailsPopup.opponentName = self.opponentName ?? "Opponent"
        matchdetailsPopup.opponentforehand = self.opponentforehand
        matchdetailsPopup.opponentbackhand = self.opponentbackhand
        matchdetailsPopup.opponentdominatingHand = self.opponentdominatingHand
        matchdetailsPopup.opponentdelegate = self
        matchdetailsPopup.modalPresentationStyle = .overFullScreen
        self.present(matchdetailsPopup, animated: false, completion: nil)
    }
    @IBAction func savebtnAct(_ sender: Any) {
        if !checkValidation(){
            return
        }
        savematchdetailsViewModel.opponentId = self.opponentId
        savematchdetailsViewModel.playerdominatingHand = self.playerdominatingHand
        savematchdetailsViewModel.opponentdominatingHand = self.opponentdominatingHand
        savematchdetailsViewModel.playerSide = self.playerSide
        savematchdetailsViewModel.opponentName = self.opponentName
        if isCoachAngle{
            savematchdetailsViewModel.playerforehand = self.playerforehand
            savematchdetailsViewModel.playerbackhand = self.playerbackhand
            savematchdetailsViewModel.opponentforehand = self.opponentforehand
            savematchdetailsViewModel.opponentbackhand = self.opponentbackhand
            
            savematchdetailsViewModel.saveMatchDetailsCoachApi()
            return
        }
        savematchdetailsViewModel.saveMatchDetailsApi()
    }
    
    func checkValidation() -> Bool{
        
        var isValidate = true
        
        guard let playerdominatinghand = self.playerdominatingHand, !playerdominatinghand.isEmpty else {
            self.showAlert(with: AlertConstants.selectplayerDominating)
            isValidate = false
            return false
        }
        
        if isCoachAngle{
            guard let playerforeHand = self.playerforehand, !playerforeHand.isEmpty else {
                self.showAlert(with: AlertConstants.selectplayerforeHand)
                isValidate = false
                return false
            }
            guard let playerbackHand = self.playerbackhand, !playerbackHand.isEmpty else {
                self.showAlert(with: AlertConstants.selectplayerbackHand)
                isValidate = false
                return false
            }
        }
        
        guard let playerSide = self.playerSide, !playerSide.isEmpty else {
            self.showAlert(with: AlertConstants.selectplayerSide)
            isValidate = false
            return false
        }
        guard let opponentdominatinghand = self.opponentdominatingHand, !opponentdominatinghand.isEmpty else {
            self.showAlert(with: AlertConstants.selectOpponentDominating)
            isValidate = false
            return false
        }
        
        if isCoachAngle{
            guard let opponentforeHand = self.opponentforehand, !opponentforeHand.isEmpty else {
                self.showAlert(with: AlertConstants.selectopponentforeHand)
                isValidate = false
                return false
            }
            guard let opponentbackHand = self.opponentbackhand, !opponentbackHand.isEmpty else {
                self.showAlert(with: AlertConstants.selectopponentbackHand)
                isValidate = false
                return false
            }
        }
        
        return isValidate
    }
}

extension MatchDetailsVC:updatePlayerData{
    func update(dominatingHand: String?, side: String?, foreHand: String?, backHand: String?) {
        self.playerdominatingHand = dominatingHand
        self.playerSide = side
        self.playerforehand = foreHand
        self.playerbackhand = backHand
    }
}

extension MatchDetailsVC:updateOpponentData{
    func update(name: String?, dominatingHand: String?, foreHand: String?, backHand: String?) {
        if name?.trimmingCharacters(in: .whitespaces) == ""{
            self.opponentName = "Opponent"
            self.opponentLabel.text = "Opponent"
        }else{
            self.opponentName = name
            self.opponentLabel.text = name
        }
        self.opponentdominatingHand = dominatingHand
        self.opponentforehand = foreHand
        self.opponentbackhand = backHand
    }
}
