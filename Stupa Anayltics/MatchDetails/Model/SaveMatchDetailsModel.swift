//
//  SaveMatchDetailsModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 20/04/21.
//

import Foundation

// MARK: - SaveMatchDetailsRequestModel
struct SaveMatchDetailsRequestModel: Codable {
    let opponentID, chunkNumber, finalClip, videoID: String
    let userID, deviceID, deviceType, tokenValue: String
    let matchType, playerName, opponentName, playerSide: String
    let uploadType, fileName, fileURL, fromAngle: String
    let stroke, placement, ballType, matchDate: String
    let playerDominatinghand, opponentDominatinghand, playerForehand, playerBackhand: String
    let opponentForehand, opponentBackhand, deviceDetail, packageID: String
    let userPackageID, isDefault, minute, drillType: String

    enum CodingKeys: String, CodingKey {
        case opponentID = "OpponentId"
        case chunkNumber, finalClip
        case videoID = "VideoId"
        case userID = "UserId"
        case deviceID = "DeviceId"
        case deviceType = "DeviceType"
        case tokenValue = "TokenValue"
        case matchType = "MatchType"
        case playerName = "PlayerName"
        case opponentName = "OpponentName"
        case playerSide
        case uploadType = "UploadType"
        case fileName = "FileName"
        case fileURL = "FileUrl"
        case fromAngle = "FromAngle"
        case stroke = "Stroke"
        case placement = "Placement"
        case ballType = "BallType"
        case matchDate = "MatchDate"
        case playerDominatinghand = "PlayerDominatinghand"
        case opponentDominatinghand = "OpponentDominatinghand"
        case playerForehand = "PlayerForehand"
        case playerBackhand = "PlayerBackhand"
        case opponentForehand = "OpponentForehand"
        case opponentBackhand = "OpponentBackhand"
        case deviceDetail = "DeviceDetail"
        case packageID = "PackageID"
        case userPackageID = "UserPackageID"
        case isDefault = "IsDefault"
        case minute = "Minute"
        case drillType = "DrillType"
    }
}


// MARK: - SaveMatchDetailsResponseModel
struct SaveMatchDetailsResponseModel: Codable {
    let isSuccess: Bool?
    let message: String?
    let data: SaveMatchDetailsData?

    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case message = "Message"
        case data = "Data"
    }
}

// MARK: - DataClass
struct SaveMatchDetailsData: Codable {
    let videoID: Int?
    let fileName: String?

    enum CodingKeys: String, CodingKey {
        case videoID = "VideoId"
        case fileName = "FileName"
    }
}
