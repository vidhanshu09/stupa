//
//  SaveMatchDetailsViewModel.swift
//  Stupa Anayltics
//
//  Created by Vidhanshu Bhardwaj on 20/04/21.
//

import Foundation
import UIKit

class SaveMatchDetailsViewModel:NSObject{
    
    var playerforehand:String?
    var playerbackhand:String?
    var opponentforehand:String?
    var opponentbackhand:String?
    var videoId:Int?
    var opponentId:String?
    var playerdominatingHand:String?
    var opponentdominatingHand:String?
    var playerSide:String?
    var opponentName:String?
    var matchAngle:Utils.matchAngle?
    private var root:UIViewController!
    
    init(root:UIViewController,matchAngle:Utils.matchAngle?){
        self.root = root
        self.matchAngle = matchAngle
    }
    
    func saveMatchDetailsCoachApi(){
        
        guard let userID = SUserDefaults.value(forKey: SUDConstants.userID) as? Int else{
            return
        }
        guard let playerName = SUserDefaults.value(forKey: SUDConstants.userName) as? String else{
            return
        }

        let timeStamp = Utils().getCurrentTimeStamp()
        let filename = "\(userID)" + "_" + "\(timeStamp)" + ".mp4"
        
        let saveMatchParams = SaveMatchDetailsRequestModel(opponentID: opponentId ?? "", chunkNumber: "1", finalClip: "0", videoID: "0", userID: String(userID), deviceID: StupaStrings.DeviceUUID ?? "", deviceType: StupaStrings.DeviceType, tokenValue: Utils().getToken(), matchType: "Match", playerName: playerName, opponentName: opponentName ?? "", playerSide: playerSide ?? "", uploadType: "Live", fileName: filename, fileURL: "", fromAngle: matchAngle?.rawValue ?? "", stroke: "", placement: "", ballType: "Many Balls", matchDate: Utils().getCurrentDateString(), playerDominatinghand: playerdominatingHand ?? "", opponentDominatinghand: opponentdominatingHand ?? "", playerForehand: playerforehand ?? "", playerBackhand: playerbackhand ?? "", opponentForehand: opponentforehand ?? "", opponentBackhand: opponentbackhand ?? "", deviceDetail: "", packageID: "", userPackageID: "", isDefault: "false", minute: "0", drillType: "")
        
        print(saveMatchParams)
        
        let encoder = JSONEncoder()
        let jsonData = try! encoder.encode(saveMatchParams)
        
        Connector.sharedinstance.runDataApiwithModelandEncodedData(sender: root, data: jsonData, method: .post, parameters: [:], success: nil, url: ApiConstants.saveMatchDetails, completion: { (Data, Error) in
            if let error = Error{
                print(error.localizedDescription)
                return
            }
            guard let response = Data else{
                return
            }
            
            self.videoId = response.data?.videoID
            
            DispatchQueue.main.async {
                let navigate = StoryBoards.VideoRecordingStoryboard.instantiateViewController(withIdentifier: "ShowDemoVC") as! ShowDemoVC
                navigate.fixedTimeStamps = "\(timeStamp)"
                navigate.videoId = self.videoId
                navigate.playerSide = self.playerSide
                navigate.matchAngle = self.matchAngle
                self.root.navigationController?.pushViewController(navigate, animated: true)
            }
            
        }, type: SaveMatchDetailsResponseModel.self)
    }
 
    func saveMatchDetailsApi(){
        
        guard let userID = SUserDefaults.value(forKey: SUDConstants.userID) as? Int else{
            return
        }
        guard let playerName = SUserDefaults.value(forKey: SUDConstants.userName) as? String else{
            return
        }

        let timeStamp = Utils().getCurrentTimeStamp()
        let filename = "\(userID)" + "_" + "\(timeStamp)" + ".mp4"
        
        let saveMatchParams = SaveMatchDetailsRequestModel(opponentID: opponentId ?? "", chunkNumber: "1", finalClip: "0", videoID: "0", userID: String(userID), deviceID: StupaStrings.DeviceUUID ?? "", deviceType: StupaStrings.DeviceType, tokenValue: Utils().getToken(), matchType: "Match", playerName: playerName, opponentName: opponentName ?? "", playerSide: playerSide ?? "", uploadType: "Live", fileName: filename, fileURL: "", fromAngle: matchAngle?.rawValue ?? "", stroke: "", placement: "", ballType: "Many Balls", matchDate: Utils().getCurrentDateString(), playerDominatinghand: playerdominatingHand ?? "", opponentDominatinghand: opponentdominatingHand ?? "", playerForehand: "", playerBackhand: "", opponentForehand: "", opponentBackhand: "", deviceDetail: "", packageID: "", userPackageID: "", isDefault: "false", minute: "0", drillType: "")
        
        print(saveMatchParams)
        
        let encoder = JSONEncoder()
        let jsonData = try! encoder.encode(saveMatchParams)
        
        Connector.sharedinstance.runDataApiwithModelandEncodedData(sender: root, data: jsonData, method: .post, parameters: [:], success: nil, url: ApiConstants.saveMatchDetails, completion: { (Data, Error) in
            if let error = Error{
                print(error.localizedDescription)
                return
            }
            guard let response = Data else{
                return
            }
            
            self.videoId = response.data?.videoID
            
            DispatchQueue.main.async {
                let navigate = StoryBoards.VideoRecordingStoryboard.instantiateViewController(withIdentifier: "ShowDemoVC") as! ShowDemoVC
                navigate.fixedTimeStamps = "\(timeStamp)"
                navigate.videoId = self.videoId
                navigate.playerSide = self.playerSide
                navigate.matchAngle = self.matchAngle
                self.root.navigationController?.pushViewController(navigate, animated: true)
            }
            
        }, type: SaveMatchDetailsResponseModel.self)
        
    }
}
